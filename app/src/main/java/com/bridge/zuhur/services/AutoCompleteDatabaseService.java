package com.bridge.zuhur.services;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.DatabaseListener;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.models.AutoCompleteDatabaseModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by bridgetomarket on 6/12/16.
 */
public class AutoCompleteDatabaseService extends AbstractService implements DatabaseListener {
    /**
     * @param model
     */
    @Inject
    public AutoCompleteDatabaseService(AutoCompleteDatabaseModel model) {
        super(model);
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        ((AutoCompleteDatabaseModel) model).findItem(id, listener);
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        ((AutoCompleteDatabaseModel) model).findItems(filter, listener);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        ((AutoCompleteDatabaseModel) model).insertItem(value, listener);
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        ((AutoCompleteDatabaseModel) model).insertItems(value, listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {
        ((AutoCompleteDatabaseModel) model).updateItem(value, listener);
    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        ((AutoCompleteDatabaseModel) model).deleteItem(id, listener);
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        ((AutoCompleteDatabaseModel) model).deleteAllItems(listener);
    }


    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
