package com.bridge.zuhur.services;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.DatabaseListener;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.models.LogDataDatabaseModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by heshamkhaled on 6/13/16.
 */
public class LogDataDatabaseService extends AbstractService implements DatabaseListener {

    /**
     * @param model
     */
    @Inject
    public LogDataDatabaseService(LogDataDatabaseModel model) {
        super(model);
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        ((LogDataDatabaseModel) model).findItem(id, listener);
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        ((LogDataDatabaseModel) model).findItems(filter, listener);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        ((LogDataDatabaseModel) model).insertItem(value, listener);
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        ((LogDataDatabaseModel) model).insertItems(value, listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {
        ((LogDataDatabaseModel) model).updateItem(value, listener);
    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        ((LogDataDatabaseModel) model).deleteItem(id, listener);
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        ((LogDataDatabaseModel) model).deleteAllItems(listener);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
