package com.bridge.zuhur.services;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.web_service_mappers.InternationalShippingRequestMapper;
import com.bridge.zuhur.models.InternationalShippingRequestModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 17/04/16.
 */
public class InternationalShippingRequestService extends AbstractService {

    /**
     * @param model
     */
    @Inject
    public InternationalShippingRequestService(InternationalShippingRequestModel model) {
        super(model);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
       model.find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        model.find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        model.findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        model.save(entity, listener);
    }
}
