package com.bridge.zuhur.services;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.models.SignUpFacebookModel;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 6/15/16.
 */
public class SignUpFacebookService extends AbstractService {

    /**
     * @param model
     */
    public SignUpFacebookService(SignUpFacebookModel model) {
        super(model);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((SignUpFacebookModel) model).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((SignUpFacebookModel) model).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((SignUpFacebookModel) model).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((SignUpFacebookModel) model).save(entity, listener);
    }
}
