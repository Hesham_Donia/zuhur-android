package com.bridge.zuhur.services;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.models.BestSellerModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 07/04/16.
 */
public class BestSellerService extends AbstractService {

    /**
     * @param model
     */
    @Inject
    public BestSellerService(BestSellerModel model) {
        super(model);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((BestSellerModel) model).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((BestSellerModel) model).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((BestSellerModel) model).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((BestSellerModel) model).save(entity, listener);
    }
}
