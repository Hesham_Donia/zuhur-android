package com.bridge.zuhur.services;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.models.OrderStripeModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by bridgetomarket on 6/21/16.
 */
public class OrderStripeService extends AbstractService {

    /**
     * @param model
     */
    @Inject
    public OrderStripeService(OrderStripeModel model) {
        super(model);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        model.find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        model.find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        model.findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        model.save(entity, listener);
    }
}
