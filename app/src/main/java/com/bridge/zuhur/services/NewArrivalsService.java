package com.bridge.zuhur.services;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.models.NewArrivalsModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 07/04/16.
 */
public class NewArrivalsService extends AbstractService {

    /**
     * @param model
     */
    @Inject
    public NewArrivalsService(NewArrivalsModel model) {
        super(model);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((NewArrivalsModel) model).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((NewArrivalsModel) model).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((NewArrivalsModel) model).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((NewArrivalsModel) model).save(entity, listener);
    }
}
