package com.bridge.zuhur.services;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.models.RecentViewedModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 07/04/16.
 */
public class RecentViewedService extends AbstractService {
    /**
     * @param model
     */
    @Inject
    public RecentViewedService(RecentViewedModel model) {
        super(model);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((RecentViewedModel) model).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((RecentViewedModel) model).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((RecentViewedModel) model).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((RecentViewedModel) model).save(entity, listener);
    }
}
