package com.bridge.zuhur.services;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.models.CategoryProductsModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 06/04/16.
 */
public class CategoryProductsService extends AbstractService {

    /**
     * @param model
     */
    @Inject
    public CategoryProductsService(CategoryProductsModel model) {
        super(model);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((CategoryProductsModel) model).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((CategoryProductsModel) model).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((CategoryProductsModel) model).findAll(filters,listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((CategoryProductsModel) model).save(entity, listener);
    }
}
