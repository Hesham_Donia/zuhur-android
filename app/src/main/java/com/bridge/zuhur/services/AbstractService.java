package com.bridge.zuhur.services;

import com.bridge.zuhur.interfaces.ServiceListener;
import com.bridge.zuhur.models.AbstractModel;

/**
 * Created by hesham on 04/04/16.
 */
abstract public class AbstractService implements ServiceListener{

    protected AbstractModel model;

    /**
     *
     * @param model
     */
    public AbstractService(AbstractModel model){
        this.model = model;
    }
}
