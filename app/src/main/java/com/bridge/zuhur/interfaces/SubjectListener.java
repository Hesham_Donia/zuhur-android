package com.bridge.zuhur.interfaces;

/**
 * Created by hesham on 05/04/16.
 */
public interface SubjectListener {

    void register(ObserverListener observer);

    void unregister(ObserverListener observer);

    void notifyObservers(boolean internetConnectionExist);

}
