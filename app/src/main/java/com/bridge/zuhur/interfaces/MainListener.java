package com.bridge.zuhur.interfaces;

/**
 * Created by hesham on 04/04/16.
 */
public interface MainListener<T> {

    void onSuccess (T result);

    void onError (String error);
}
