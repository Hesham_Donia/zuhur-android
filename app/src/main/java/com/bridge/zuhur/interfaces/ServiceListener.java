package com.bridge.zuhur.interfaces;


import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public interface ServiceListener {
    void find(String id, MainListener<ArrayList<? extends Entity>> listener);

    void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener);

    void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener);

    void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener);
}
