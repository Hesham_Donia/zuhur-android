package com.bridge.zuhur.interfaces;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public interface DatabaseListener {

    void findItem(int id, MainListener<ArrayList<? extends Entity>> listener);

    void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener);

    void insertItem(Entity value, MainListener<Boolean> listener);

    void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener);

    void updateItem(Entity value, MainListener<Boolean> listener);

    void deleteItem(int id, MainListener<Boolean> listener);

    void deleteAllItems(MainListener<Boolean> listener);
    
}
