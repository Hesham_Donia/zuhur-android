package com.bridge.zuhur.interfaces;

import com.bridge.zuhur.entities.Entity;

import java.util.ArrayList;

/**
 * Created by hesham on 05/04/16.
 */
public interface ViewsListener {

    void onDataReceived(ArrayList<? extends Entity> result , int action);
}
