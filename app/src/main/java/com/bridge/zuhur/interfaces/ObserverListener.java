package com.bridge.zuhur.interfaces;

/**
 * Created by hesham on 04/04/16.
 */
public interface ObserverListener {
    void update(boolean internetConnectionExist);
}
