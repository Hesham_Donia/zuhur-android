package com.bridge.zuhur.interfaces;


import com.bridge.zuhur.entities.Entity;

/**
 * Created by heshamkhaled on 2/6/16.
 */
public interface Parser<T> {

    Entity parse(T object);

}
