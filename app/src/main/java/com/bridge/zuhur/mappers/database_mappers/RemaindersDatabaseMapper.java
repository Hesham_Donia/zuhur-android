package com.bridge.zuhur.mappers.database_mappers;

import android.content.Context;
import android.util.Log;

import com.bridge.zuhur.database.Remainder;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.utils.CommonConstants;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bridgetomarket on 6/9/16.
 */
public class RemaindersDatabaseMapper extends AbstractDatabaseMapper {

    public RemaindersDatabaseMapper(Context context) {
        super(context);
        mDao = mDbAdapter.getRemainderDao();
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        QueryBuilder queryBuilder = mDao.queryBuilder();
        try {
            queryBuilder.where().eq(Remainder.Remainder_ID, id);
            executeOperation(DatabaseOperation.SELECT, listener, mDao, queryBuilder, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        executeOperation(DatabaseOperation.SELECT, listener, mDao, null, null);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        Remainder remainder = (Remainder) value;
        try {
            if (mDao.queryBuilder().where().eq(Remainder.Remainder_ID, remainder.getRemainderId()).query().size() > 0) {
                listener.onSuccess(false);
            } else {
                executeOperation(DatabaseOperation.INSERT, listener, mDao, null, remainder);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        makeTransaction(value, 0, value.size(), listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {

        Remainder remainder = (Remainder) value;
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "id from mapper: " + remainder.getId());
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "remainder id from mapper: " + remainder.getRemainderId());
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "remainder id object from mapper: " + remainder.getRemainder().id);

        UpdateBuilder updateBuilder = mDao.updateBuilder();
        try {
            updateBuilder.updateColumnValue(Remainder.Remainder, remainder.getRemainder());
            updateBuilder.updateColumnValue(Remainder.Remainder_ID, remainder.getRemainder().id);
            updateBuilder.where().eq(Remainder.ID, remainder.getId());
            executeOperation(DatabaseOperation.UPDATE, listener, mDao, updateBuilder, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        DeleteBuilder deleteBuilder = mDao.deleteBuilder();
        try {
            deleteBuilder.where().eq(Remainder.Remainder_ID, id);
            executeOperation(DatabaseOperation.DELETE, listener, mDao, deleteBuilder, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        DeleteBuilder deleteBuilder = mDao.deleteBuilder();
        executeOperation(DatabaseOperation.DELETE, listener, mDao, deleteBuilder, null);
    }
}
