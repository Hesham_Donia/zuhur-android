package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 17/04/16.
 */
public class InternationalShippingRequestMapper extends AbstractWebServiceMapper {

    public InternationalShippingRequestMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "International_shipping";
    }

    @Override
    protected String initRequestTag() {
        return ExtrasController.MAKE_INTERNATIONAL_SHIPPING_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {
                SignUpResponse signUpResponse = new SignUpResponse();
                try {
                    if (response.has("value")) {
                        signUpResponse.value = response.getInt("value");
                    }

                    if (response.has("massage")) {
                        signUpResponse.message = response.getString("massage");
                    }

                    if (response.has("user_id")) {
                        signUpResponse.id = response.getInt("user_id");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return signUpResponse;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filters);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
