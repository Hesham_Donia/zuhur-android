package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Event;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;
import com.bridge.zuhur.utils.CommonConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 14/04/16.
 */
public class EventsMapper extends AbstractWebServiceMapper {


    int indicator = 0;
    String id = null;
    int countryId = 0;

    public EventsMapper(Context ctx) {
        super(ctx);
        SharedPreferences settings = ctx.getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
        countryId = settings.getInt("countryId", 0);
    }

    @Override
    protected String getUrl() {

        switch (indicator) {
            case 1:

                return "EventDetails/" + id;

            case 2:

                break;

            case 3:

                return "events/" + countryId;

            case 4:
                break;

            default:
                break;
        }

        return null;
    }

    @Override
    protected String initRequestTag() {
        switch (indicator) {
            case 1:

                return ExtrasController.EVENT_DETAILS_REQUEST_TAG;

            case 2:

                break;

            case 3:
                return ExtrasController.EVENTS_REQUEST_TAG;

            case 4:
                break;

            default:
                break;
        }

        return null;
    }

    @Override
    protected void initParser() {
        switch (indicator) {
            case 1:

                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        Event event = new Event();
                        try {
                            if (response.has("id")) {
                                event.id = response.getInt("id");
                            } else {
                                return null;
                            }

                            if (response.has("title_ar")) {
                                event.arabicName = response.getString("title_ar");
                            }

                            if (response.has("title_en")) {
                                event.englishName = response.getString("title_en");
                            }

                            if (response.has("details_ar")) {
                                event.arabicDetails = response.getString("details_ar");
                            }

                            if (response.has("details_en")) {
                                event.englishDetails = response.getString("details_en");
                            }

                            if (response.has("image_path")) {
                                event.imagePath = response.getString("image_path");
                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        return event;
                    }
                };
                break;

            case 2:

                break;

            case 3:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        Event event = new Event();
                        try {
                            if (response.has("id")) {
                                event.id = response.getInt("id");
                            } else {
                                return null;
                            }

                            if (response.has("title_ar")) {
                                event.arabicName = response.getString("title_ar");
                            }

                            if (response.has("title_en")) {
                                event.englishName = response.getString("title_en");
                            }

                            if (response.has("details_ar")) {
                                event.arabicDetails = response.getString("details_ar");
                            }

                            if (response.has("details_en")) {
                                event.englishDetails = response.getString("details_en");
                            }

                            if (response.has("image_path")) {
                                event.imagePath = response.getString("image_path");
                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        return event;
                    }
                };
                break;

            case 4:
                break;

            default:
                break;
        }
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 1;
        this.id = id;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "data", null);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 2;
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 3;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "data", null);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 4;
    }
}
