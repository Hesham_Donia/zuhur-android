package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.NotificationsController;
import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.database.WishList;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Event;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.NotificationEntity;
import com.bridge.zuhur.entities.Offer;
import com.bridge.zuhur.entities.Order;
import com.bridge.zuhur.entities.PaymentMethod;
import com.bridge.zuhur.entities.Place;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Remainder;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;
import com.bridge.zuhur.utils.CommonConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 17/04/16.
 */
public class NotificationsMapper extends AbstractWebServiceMapper {

    public NotificationsMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "listnotifications";
    }

    @Override
    protected String initRequestTag() {
        return NotificationsController.NOTIFICATIONS_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {
                NotificationEntity notificationEntity = new NotificationEntity();

                try {
                    if (response.has("type")) {
                        notificationEntity.type = response.getInt("type");
                    } else {
                        return null;
                    }

                    if (response.has("massage_ar")) {
                        notificationEntity.arabicMessage = response.getString("massage_ar");
                    }

                    if (response.has("massage_en")) {
                        notificationEntity.englishMessage = response.getString("massage_en");
                    }

                    if (response.has("img")) {
                        notificationEntity.imagePath = response.getString("img");
                    }

                    if (response.has("details")) {
                        JSONObject details = response.getJSONObject("details");
                        Log.d(CommonConstants.APPLICATION_LOG_TAG, "notification type in mapper: " + notificationEntity.type);
                        switch (notificationEntity.type) {
                            case 1:
                                Product favouriteProduct = new Product();

                                try {
                                    if (details.has("id")) {
                                        favouriteProduct.id = details.getInt("id");
                                    } else {
                                        return null;
                                    }

                                    if (details.has("cat_id")) {
                                        favouriteProduct.categoryId = details.getInt("cat_id");
                                    }

                                    if (details.has("name_ar")) {
                                        favouriteProduct.arabicName = details.getString("name_ar");
                                    }

                                    if (details.has("name_en")) {
                                        favouriteProduct.englishName = details.getString("name_en");
                                    }

                                    if (details.has("image_path")) {
                                        favouriteProduct.imagePath = details.getString("image_path");
                                    }

                                    if (details.has("description_ar")) {
                                        favouriteProduct.arabicDescription = details.getString("description_ar");
                                    }

                                    if (details.has("points")) {
                                        favouriteProduct.pointsPrice = details.getInt("points");
                                    }

                                    if (details.has("description_en")) {
                                        favouriteProduct.englishDescription = details.getString("description_en");
                                    }

                                    if (details.has("rate")) {
                                        favouriteProduct.rate = details.getDouble("rate");
                                    }

                                    if (details.has("price")) {
                                        favouriteProduct.price = details.getDouble("price");
                                    }

                                    if (details.has("is_exist")) {
                                        if (details.getString("is_exist").equals("1")) {
                                            favouriteProduct.isExist = true;
                                        } else {
                                            favouriteProduct.isExist = false;
                                        }
                                    }

                                    if (details.has("url")) {
                                        favouriteProduct.url = details.getString("url");
                                    }

                                    if (details.has("is_new_arrival")) {
                                        if (details.getString("is_new_arrival").equals("1")) {
                                            favouriteProduct.isNewArrival = true;
                                        } else {
                                            favouriteProduct.isNewArrival = false;
                                        }
                                    }

                                    if (details.has("create_date")) {
                                        favouriteProduct.creationDate = details.getString("create_date");
                                    }

                                    if (details.has("sell_counter")) {
                                        favouriteProduct.sellCounter = details.getInt("sell_counter");
                                    }

                                    if (details.has("seen_counter")) {
                                        favouriteProduct.seenCounter = details.getInt("seen_counter");
                                    }

                                    if (details.has("rate_counter")) {
                                        favouriteProduct.peopleRatedCounter = details.getInt("rate_counter");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                notificationEntity.entity = new Favourite(favouriteProduct);
                                break;

                            case 2:
                                Product wishListProduct = new Product();

                                try {
                                    if (details.has("id")) {
                                        wishListProduct.id = details.getInt("id");
                                    } else {
                                        return null;
                                    }

                                    if (details.has("cat_id")) {
                                        wishListProduct.categoryId = details.getInt("cat_id");
                                    }

                                    if (details.has("name_ar")) {
                                        wishListProduct.arabicName = details.getString("name_ar");
                                    }

                                    if (details.has("name_en")) {
                                        wishListProduct.englishName = details.getString("name_en");
                                    }

                                    if (details.has("image_path")) {
                                        wishListProduct.imagePath = details.getString("image_path");
                                    }

                                    if (details.has("description_ar")) {
                                        wishListProduct.arabicDescription = details.getString("description_ar");
                                    }

                                    if (details.has("points")) {
                                        wishListProduct.pointsPrice = details.getInt("points");
                                    }

                                    if (details.has("description_en")) {
                                        wishListProduct.englishDescription = details.getString("description_en");
                                    }

                                    if (details.has("rate")) {
                                        wishListProduct.rate = details.getDouble("rate");
                                    }

                                    if (details.has("price")) {
                                        wishListProduct.price = details.getDouble("price");
                                    }

                                    if (details.has("is_exist")) {
                                        if (details.getString("is_exist").equals("1")) {
                                            wishListProduct.isExist = true;
                                        } else {
                                            wishListProduct.isExist = false;
                                        }
                                    }

                                    if (details.has("url")) {
                                        wishListProduct.url = details.getString("url");
                                    }

                                    if (details.has("is_new_arrival")) {
                                        if (details.getString("is_new_arrival").equals("1")) {
                                            wishListProduct.isNewArrival = true;
                                        } else {
                                            wishListProduct.isNewArrival = false;
                                        }
                                    }

                                    if (details.has("create_date")) {
                                        wishListProduct.creationDate = details.getString("create_date");
                                    }

                                    if (details.has("sell_counter")) {
                                        wishListProduct.sellCounter = details.getInt("sell_counter");
                                    }

                                    if (details.has("seen_counter")) {
                                        wishListProduct.seenCounter = details.getInt("seen_counter");
                                    }

                                    if (details.has("rate_counter")) {
                                        wishListProduct.peopleRatedCounter = details.getInt("rate_counter");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                notificationEntity.entity = new WishList(wishListProduct);
                                break;

                            case 3:
                                Remainder remainder = new Remainder();

                                try {
                                    if (details.has("id")) {
                                        remainder.id = details.getInt("id");
                                    } else {
                                        return null;
                                    }

                                    if (details.has("order_id")) {
                                        remainder.orderId = details.getInt("order_id");
                                    }

                                    if (details.has("r_date")) {
                                        remainder.remainderDate = details.getString("r_date");
                                    }

                                    if (details.has("details")) {
                                        remainder.details = details.getString("details");
                                    }

                                    if (details.has("is_completed")) {
                                        if (details.getString("is_completed").equals("0")) {
                                            remainder.isCompleted = false;
                                        } else {
                                            remainder.isCompleted = true;
                                        }
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                notificationEntity.entity = remainder;
                                break;

                            case 4:
                                Order ConfirmedOrder = new Order();

                                try {
                                    if (details.has("id")) {
                                        ConfirmedOrder.id = details.getInt("id");
                                    } else {
                                        return null;
                                    }

                                    Place billPlace = new Place(), deliverPlace = new Place();


                                    if (details.has("bill_address")) {
                                        billPlace.address = details.getString("bill_address");
                                    }

                                    if (details.has("bill_address_X")) {
                                        billPlace.latitude = details.getString("bill_address_X");
                                    }

                                    if (details.has("bill_address_Y")) {
                                        billPlace.longitude = details.getString("bill_address_Y");
                                    }

                                    ConfirmedOrder.billingPlace = billPlace;

                                    if (response.has("deliver_address")) {
                                        deliverPlace.address = response.getString("deliver_address");
                                    }

                                    if (details.has("deliver_address_X")) {
                                        deliverPlace.latitude = details.getString("deliver_address_X");
                                    }

                                    if (details.has("deliver_address_Y")) {
                                        deliverPlace.longitude = details.getString("deliver_address_Y");
                                    }

                                    ConfirmedOrder.deliverPlace = deliverPlace;

                                    if (details.has("total_price")) {
                                        ConfirmedOrder.finalPrice = Double.valueOf(details.getString("total_price"));
                                    }

                                    if (details.has("create_date")) {
                                        ConfirmedOrder.creationDate = details.getString("create_date");
                                    }

                                    if (details.has("shipping_date")) {
                                        ConfirmedOrder.shippingDate = details.getString("shipping_date");
                                    }

                                    if (details.has("is_confirmed")) {
                                        if (details.getString("is_confirmed").equals("0")) {
                                            ConfirmedOrder.isConfirmed = false;
                                        } else {
                                            ConfirmedOrder.isConfirmed = true;
                                        }
                                    }

                                    if (details.has("is_completed")) {
                                        if (details.getString("is_completed").equals("0")) {
                                            ConfirmedOrder.isCompleted = false;
                                        } else {
                                            ConfirmedOrder.isCompleted = true;
                                        }
                                    }

                                    PaymentMethod paymentMethod = new PaymentMethod();

                                    if (details.has("payment_method_id")) {
                                        paymentMethod.id = details.getInt("payment_method_id");
                                    }

                                    ConfirmedOrder.mainPaymentMethod = paymentMethod;

                                    if (details.has("products")) {
                                        JSONArray productsJsonArray = details.getJSONArray("products");
                                        for (int i = 0; i < productsJsonArray.length(); i++) {
                                            Product product = new Product();
                                            JSONObject productJsonObject = productsJsonArray.getJSONObject(i);
                                            if (productJsonObject.has("id")) {
                                                product.id = productJsonObject.getInt("id");
                                            } else {
                                                return null;
                                            }

                                            if (productJsonObject.has("cat_id")) {
                                                product.categoryId = productJsonObject.getInt("cat_id");
                                            }

                                            if (productJsonObject.has("name_ar")) {
                                                product.arabicName = productJsonObject.getString("name_ar");
                                            }

                                            if (productJsonObject.has("name_en")) {
                                                product.englishName = productJsonObject.getString("name_en");
                                            }

                                            if (productJsonObject.has("image_path")) {
                                                product.imagePath = productJsonObject.getString("image_path");
                                            }

                                            if (productJsonObject.has("description_ar")) {
                                                product.arabicDescription = productJsonObject.getString("description_ar");
                                            }

                                            if (productJsonObject.has("points")) {
                                                product.pointsPrice = productJsonObject.getInt("points");
                                            }

                                            if (productJsonObject.has("description_en")) {
                                                product.englishDescription = productJsonObject.getString("description_en");
                                            }

                                            if (productJsonObject.has("rate")) {
                                                product.rate = productJsonObject.getDouble("rate");
                                            }

                                            if (productJsonObject.has("price")) {
                                                product.price = productJsonObject.getDouble("price");
                                            }

                                            if (productJsonObject.has("is_exist")) {
                                                if (productJsonObject.getString("is_exist").equals("1")) {
                                                    product.isExist = true;
                                                } else {
                                                    product.isExist = false;
                                                }
                                            }

                                            if (productJsonObject.has("url")) {
                                                product.url = productJsonObject.getString("url");
                                            }

                                            if (productJsonObject.has("is_new_arrival")) {
                                                if (productJsonObject.getString("is_new_arrival").equals("1")) {
                                                    product.isNewArrival = true;
                                                } else {
                                                    product.isNewArrival = false;
                                                }
                                            }

                                            if (productJsonObject.has("create_date")) {
                                                product.creationDate = productJsonObject.getString("create_date");
                                            }

                                            if (productJsonObject.has("sell_counter")) {
                                                product.sellCounter = productJsonObject.getInt("sell_counter");
                                            }

                                            if (productJsonObject.has("seen_counter")) {
                                                product.seenCounter = productJsonObject.getInt("seen_counter");
                                            }

                                            if (productJsonObject.has("rate_counter")) {
                                                product.peopleRatedCounter = productJsonObject.getInt("rate_counter");
                                            }

                                            ConfirmedOrder.products.add(product);

                                        }
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }

                                notificationEntity.entity = ConfirmedOrder;
                                break;

                            case 5:
                                Order CompletedOrder = new Order();

                                try {
                                    if (details.has("id")) {
                                        CompletedOrder.id = details.getInt("id");
                                    } else {
                                        return null;
                                    }

                                    Place billPlace = new Place(), deliverPlace = new Place();


                                    if (details.has("bill_address")) {
                                        billPlace.address = details.getString("bill_address");
                                    }

                                    if (details.has("bill_address_X")) {
                                        billPlace.latitude = details.getString("bill_address_X");
                                    }

                                    if (details.has("bill_address_Y")) {
                                        billPlace.longitude = details.getString("bill_address_Y");
                                    }

                                    CompletedOrder.billingPlace = billPlace;

                                    if (response.has("deliver_address")) {
                                        deliverPlace.address = response.getString("deliver_address");
                                    }

                                    if (details.has("deliver_address_X")) {
                                        deliverPlace.latitude = details.getString("deliver_address_X");
                                    }

                                    if (details.has("deliver_address_Y")) {
                                        deliverPlace.longitude = details.getString("deliver_address_Y");
                                    }

                                    CompletedOrder.deliverPlace = deliverPlace;

                                    if (details.has("total_price")) {
                                        CompletedOrder.finalPrice = Double.valueOf(details.getString("total_price"));
                                    }

                                    if (details.has("create_date")) {
                                        CompletedOrder.creationDate = details.getString("create_date");
                                    }

                                    if (details.has("shipping_date")) {
                                        CompletedOrder.shippingDate = details.getString("shipping_date");
                                    }

                                    if (details.has("is_confirmed")) {
                                        if (details.getString("is_confirmed").equals("0")) {
                                            CompletedOrder.isConfirmed = false;
                                        } else {
                                            CompletedOrder.isConfirmed = true;
                                        }
                                    }

                                    if (details.has("is_completed")) {
                                        if (details.getString("is_completed").equals("0")) {
                                            CompletedOrder.isCompleted = false;
                                        } else {
                                            CompletedOrder.isCompleted = true;
                                        }
                                    }

                                    PaymentMethod paymentMethod = new PaymentMethod();

                                    if (details.has("payment_method_id")) {
                                        paymentMethod.id = details.getInt("payment_method_id");
                                    }

                                    CompletedOrder.mainPaymentMethod = paymentMethod;

                                    if (details.has("products")) {
                                        JSONArray productsJsonArray = details.getJSONArray("products");
                                        for (int i = 0; i < productsJsonArray.length(); i++) {
                                            Product product = new Product();
                                            JSONObject productJsonObject = productsJsonArray.getJSONObject(i);
                                            if (productJsonObject.has("id")) {
                                                product.id = productJsonObject.getInt("id");
                                            } else {
                                                return null;
                                            }

                                            if (productJsonObject.has("cat_id")) {
                                                product.categoryId = productJsonObject.getInt("cat_id");
                                            }

                                            if (productJsonObject.has("name_ar")) {
                                                product.arabicName = productJsonObject.getString("name_ar");
                                            }

                                            if (productJsonObject.has("name_en")) {
                                                product.englishName = productJsonObject.getString("name_en");
                                            }

                                            if (productJsonObject.has("image_path")) {
                                                product.imagePath = productJsonObject.getString("image_path");
                                            }

                                            if (productJsonObject.has("description_ar")) {
                                                product.arabicDescription = productJsonObject.getString("description_ar");
                                            }

                                            if (productJsonObject.has("points")) {
                                                product.pointsPrice = productJsonObject.getInt("points");
                                            }

                                            if (productJsonObject.has("description_en")) {
                                                product.englishDescription = productJsonObject.getString("description_en");
                                            }

                                            if (productJsonObject.has("rate")) {
                                                product.rate = productJsonObject.getDouble("rate");
                                            }

                                            if (productJsonObject.has("price")) {
                                                product.price = productJsonObject.getDouble("price");
                                            }

                                            if (productJsonObject.has("is_exist")) {
                                                if (productJsonObject.getString("is_exist").equals("1")) {
                                                    product.isExist = true;
                                                } else {
                                                    product.isExist = false;
                                                }
                                            }

                                            if (productJsonObject.has("url")) {
                                                product.url = productJsonObject.getString("url");
                                            }

                                            if (productJsonObject.has("is_new_arrival")) {
                                                if (productJsonObject.getString("is_new_arrival").equals("1")) {
                                                    product.isNewArrival = true;
                                                } else {
                                                    product.isNewArrival = false;
                                                }
                                            }

                                            if (productJsonObject.has("create_date")) {
                                                product.creationDate = productJsonObject.getString("create_date");
                                            }

                                            if (productJsonObject.has("sell_counter")) {
                                                product.sellCounter = productJsonObject.getInt("sell_counter");
                                            }

                                            if (productJsonObject.has("seen_counter")) {
                                                product.seenCounter = productJsonObject.getInt("seen_counter");
                                            }

                                            if (productJsonObject.has("rate_counter")) {
                                                product.peopleRatedCounter = productJsonObject.getInt("rate_counter");
                                            }

                                            CompletedOrder.products.add(product);

                                        }
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }

                                notificationEntity.entity = CompletedOrder;
                                break;

                            case 6:

                                break;

                            case 7:
                                Event event = new Event();
                                try {
                                    if (details.has("id")) {
                                        event.id = details.getInt("id");
                                    } else {
                                        return null;
                                    }

                                    if (details.has("title_ar")) {
                                        event.arabicName = details.getString("title_ar");
                                    }

                                    if (details.has("title_en")) {
                                        event.englishName = details.getString("title_en");
                                    }

                                    if (details.has("details_ar")) {
                                        event.arabicDetails = details.getString("details_ar");
                                    }

                                    if (details.has("details_en")) {
                                        event.englishDetails = details.getString("details_en");
                                    }

                                    if (details.has("image_path")) {
                                        event.imagePath = details.getString("image_path");
                                    }

                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }

                                notificationEntity.entity = event;
                                break;

                            case 8:
                                final Offer offer = new Offer();

                                try {
                                    if (details.has("id")) {
                                        offer.id = details.getInt("id");
                                    } else {
                                        return null;
                                    }

                                    if (details.has("details_ar")) {
                                        offer.arabicDetails = details.getString("details_ar");
                                    }

                                    if (details.has("details_en")) {
                                        offer.englishDetails = details.getString("details_en");
                                    }

                                    if (details.has("image_path")) {
                                        offer.imagePath = details.getString("image_path");
                                    }

                                    if (details.has("total_price")) {
                                        offer.totalPrice = details.getDouble("total_price");
                                    }

                                    if (details.has("original_price")) {
                                        offer.originalPrice = details.getDouble("original_price");
                                    }

                                    if (details.has("create_date")) {
                                        offer.creationDate = details.getString("create_date");
                                    }

                                    if (details.has("end_date")) {
                                        offer.endDate = details.getString("end_date");
                                    }

                                    if (details.has("is_completed")) {
                                        if (details.getString("is_completed").equals("0")) {
                                            offer.isCompleted = false;
                                        } else {
                                            offer.isCompleted = true;
                                        }
                                    }

                                    if (details.has("products")) {
                                        JSONArray productsJsonArray = details.getJSONArray("products");
                                        for (int i = 0; i < productsJsonArray.length(); i++) {
                                            Product product = new Product();
                                            JSONObject productJsonObject = productsJsonArray.getJSONObject(i);
                                            if (productJsonObject.has("id")) {
                                                product.id = productJsonObject.getInt("id");
                                            } else {
                                                return null;
                                            }

                                            if (productJsonObject.has("cat_id")) {
                                                product.categoryId = productJsonObject.getInt("cat_id");
                                            }

                                            if (productJsonObject.has("name_ar")) {
                                                product.arabicName = productJsonObject.getString("name_ar");
                                            }

                                            if (productJsonObject.has("name_en")) {
                                                product.englishName = productJsonObject.getString("name_en");
                                            }

                                            if (productJsonObject.has("image_path")) {
                                                product.imagePath = productJsonObject.getString("image_path");
                                            }

                                            if (productJsonObject.has("description_ar")) {
                                                product.arabicDescription = productJsonObject.getString("description_ar");
                                            }

                                            if (productJsonObject.has("points")) {
                                                product.pointsPrice = productJsonObject.getInt("points");
                                            }

                                            if (productJsonObject.has("description_en")) {
                                                product.englishDescription = productJsonObject.getString("description_en");
                                            }

                                            if (productJsonObject.has("rate")) {
                                                product.rate = productJsonObject.getDouble("rate");
                                            }

                                            if (productJsonObject.has("price")) {
                                                product.price = productJsonObject.getDouble("price");
                                            }

                                            if (productJsonObject.has("is_exist")) {
                                                if (productJsonObject.getString("is_exist").equals("1")) {
                                                    product.isExist = true;
                                                } else {
                                                    product.isExist = false;
                                                }
                                            }

                                            if (productJsonObject.has("url")) {
                                                product.url = productJsonObject.getString("url");
                                            }

                                            if (productJsonObject.has("is_new_arrival")) {
                                                if (productJsonObject.getString("is_new_arrival").equals("1")) {
                                                    product.isNewArrival = true;
                                                } else {
                                                    product.isNewArrival = false;
                                                }
                                            }

                                            if (productJsonObject.has("create_date")) {
                                                product.creationDate = productJsonObject.getString("create_date");
                                            }

                                            if (productJsonObject.has("sell_counter")) {
                                                product.sellCounter = productJsonObject.getInt("sell_counter");
                                            }

                                            if (productJsonObject.has("seen_counter")) {
                                                product.seenCounter = productJsonObject.getInt("seen_counter");
                                            }

                                            if (productJsonObject.has("rate_counter")) {
                                                product.peopleRatedCounter = productJsonObject.getInt("rate_counter");
                                            }

                                            offer.products.add(product);

                                        }
                                    }

                                } catch (JSONException exception) {
                                    exception.printStackTrace();
                                }
                                notificationEntity.entity = offer;
                                break;

                            case 9:
                                Product newArrivalProduct = new Product();

                                try {
                                    if (details.has("id")) {
                                        newArrivalProduct.id = details.getInt("id");
                                    } else {
                                        return null;
                                    }

                                    if (details.has("cat_id")) {
                                        newArrivalProduct.categoryId = details.getInt("cat_id");
                                    }

                                    if (response.has("name_ar")) {
                                        newArrivalProduct.arabicName = response.getString("name_ar");
                                    }

                                    if (details.has("name_en")) {
                                        newArrivalProduct.englishName = details.getString("name_en");
                                    }

                                    if (details.has("image_path")) {
                                        newArrivalProduct.imagePath = details.getString("image_path");
                                    }

                                    if (details.has("rate")) {
                                        newArrivalProduct.rate = details.getDouble("rate");
                                    }

                                    if (details.has("price")) {
                                        newArrivalProduct.price = details.getDouble("price");
                                    }

                                    if (details.has("is_exist")) {
                                        if (details.getString("is_exist").equals("1")) {
                                            newArrivalProduct.isExist = true;
                                        } else {
                                            newArrivalProduct.isExist = false;
                                        }

                                    }

                                    if (details.has("is_new_arrival")) {
                                        if (details.getString("is_new_arrival").equals("1")) {
                                            newArrivalProduct.isNewArrival = true;
                                        } else {
                                            newArrivalProduct.isNewArrival = false;
                                        }
                                    }

                                    if (details.has("create_date")) {
                                        newArrivalProduct.creationDate = details.getString("create_date");
                                    }

                                    if (details.has("sell_counter")) {
                                        newArrivalProduct.sellCounter = details.getInt("sell_counter");
                                    }

                                    if (details.has("seen_counter")) {
                                        newArrivalProduct.seenCounter = details.getInt("seen_counter");
                                    }

                                    if (details.has("rate_counter")) {
                                        newArrivalProduct.peopleRatedCounter = details.getInt("rate_counter");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                notificationEntity.entity = newArrivalProduct;
                                break;

                            default:
                                break;
                        }
                    }

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

                return notificationEntity;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filters);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
