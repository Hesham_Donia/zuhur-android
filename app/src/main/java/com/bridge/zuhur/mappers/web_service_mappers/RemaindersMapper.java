package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Remainder;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 13/04/16.
 */
public class RemaindersMapper extends AbstractWebServiceMapper {

    int indicator = 0;
    Remainder mRemainder;

    public RemaindersMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        switch (indicator) {
            case 1:
                return null;

            case 2:
                return "delremainder";

            case 3:
                return "listremainder";

            case 4:
                if (mRemainder.isUpdate) {
                    return "editremainder";
                } else {
                    return "remainder";
                }

            default:
                return null;
        }
    }

    @Override
    protected String initRequestTag() {
        switch (indicator) {
            case 1:
                return null;

            case 2:
                return ExtrasController.DELETE_REMAINDERS_REQUEST_TAG;

            case 3:
                return ExtrasController.REMAINDERS_REQUEST_TAG;

            case 4:
                if (mRemainder.isUpdate) {
                    return ExtrasController.EDIT_REMAINDERS_REQUEST_TAG;

                } else {
                    return ExtrasController.ADD_REMAINDERS_REQUEST_TAG;

                }

            default:
                return null;
        }
    }

    @Override
    protected void initParser() {
        switch (indicator) {
            case 1:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        return null;
                    }
                };
                break;

            case 2:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {

                        SignUpResponse signUpResponse = new SignUpResponse();
                        try {
                            if (response.has("value")) {
                                signUpResponse.value = response.getInt("value");
                            }

                            if (response.has("massage")) {
                                signUpResponse.message = response.getString("massage");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return signUpResponse;

                    }
                };
                break;

            case 3:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        Remainder remainder = new Remainder();

                        try {
                            if (response.has("id")) {
                                remainder.id = response.getInt("id");
                            } else {
                                return null;
                            }

                            if (response.has("order_id")) {
                                remainder.orderId = response.getInt("order_id");
                            }

                            if (response.has("r_date")) {
                                remainder.remainderDate = response.getString("r_date");
                            }

                            if (response.has("details")) {
                                remainder.details = response.getString("details");
                            }

                            if (response.has("is_completed")) {
                                if (response.getString("is_completed").equals("0")) {
                                    remainder.isCompleted = false;
                                } else {
                                    remainder.isCompleted = true;
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        com.bridge.zuhur.database.Remainder remainder1 = new com.bridge.zuhur.database.Remainder(remainder);
                        return remainder1;
                    }
                };
                break;

            case 4:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {

                        SignUpResponse signUpResponse = new SignUpResponse();
                        try {
                            if (response.has("value")) {
                                signUpResponse.value = response.getInt("value");
                            }

                            if (response.has("massage")) {
                                signUpResponse.message = response.getString("massage");
                            }

                            if (response.has("id")) {
                                signUpResponse.id = response.getInt("id");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return signUpResponse;

                    }
                };
                break;

            default:
                break;
        }
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 1;
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 2;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filters);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 3;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filters);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 4;
        mRemainder = (Remainder) entity;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", entity);
    }
}
