package com.bridge.zuhur.mappers.database_mappers;

import android.content.Context;

import com.bridge.zuhur.database.LogData;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by heshamkhaled on 6/13/16.
 */
public class LogDataDatabaseMapper extends AbstractDatabaseMapper {

    public LogDataDatabaseMapper(Context context) {
        super(context);
        mDao = mDbAdapter.getLogDataDao();
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        executeOperation(DatabaseOperation.SELECT, listener, mDao, null, null);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        LogData logData = (LogData) value;
        try {
            if (mDao.queryBuilder().where().eq(LogData.LogData_ID, logData.getId()).query().size() > 0) {
                listener.onSuccess(false);
            } else {
                executeOperation(DatabaseOperation.INSERT, listener, mDao, null, logData);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        makeTransaction(value, 0, value.size(), listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {

    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {

    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        DeleteBuilder deleteBuilder = mDao.deleteBuilder();
        executeOperation(DatabaseOperation.DELETE, listener, mDao, deleteBuilder, null);
    }
}
