package com.bridge.zuhur.mappers.database_mappers;

import android.content.Context;

import com.bridge.zuhur.database.AutoComplete;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bridgetomarket on 6/12/16.
 */
public class AutoCompleteDatabaseMapper extends AbstractDatabaseMapper {

    public AutoCompleteDatabaseMapper(Context context) {
        super(context);
        mDao = mDbAdapter.getAutoCompleteDao();
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        if (filter != null) {
            QueryBuilder queryBuilder = mDao.queryBuilder();
            try {
                queryBuilder.limit(filter.limit).where().like(AutoComplete.AutoComplete_Name, "%" + filter.searchQuery + "%");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            executeOperation(DatabaseOperation.SELECT, listener, mDao, queryBuilder, null);
        } else {
            executeOperation(DatabaseOperation.SELECT, listener, mDao, null, null);
        }

    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {

    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        makeTransaction(value, 0, value.size(), listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {

    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {

    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        DeleteBuilder deleteBuilder = mDao.deleteBuilder();
        executeOperation(DatabaseOperation.DELETE, listener, mDao, deleteBuilder, null);
    }
}
