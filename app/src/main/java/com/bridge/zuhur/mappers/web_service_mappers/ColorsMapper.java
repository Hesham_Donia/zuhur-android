package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Color;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 6/19/16.
 */
public class ColorsMapper extends AbstractWebServiceMapper {


    public ColorsMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "colors";
    }

    @Override
    protected String initRequestTag() {
        return ExtrasController.COLORS_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {

                Color color = new Color();
                try {
                    if (response.has("id")) {
                        color.id = response.getInt("id");
                    } else {
                        return null;
                    }

                    if (response.has("name_ar")) {
                        color.arabicName = response.getString("name_ar");
                    }

                    if (response.has("name_en")) {
                        color.englishName = response.getString("name_en");
                    }

                    if (response.has("hex")) {
                        color.hexCode = response.getString("hex");
                    }

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
                return color;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "data", null);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
