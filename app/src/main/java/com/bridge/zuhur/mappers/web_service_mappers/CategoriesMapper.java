package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.CategoriesController;
import com.bridge.zuhur.entities.Category;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Tag;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 05/04/16.
 */
public class CategoriesMapper extends AbstractWebServiceMapper {


    public CategoriesMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "listcategories";
    }

    @Override
    protected String initRequestTag() {
        return CategoriesController.CATEGORIES_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {
                Category category = new Category();
                try {
                    if (response.has("id")) {
                        category.id = response.getInt("id");
                    } else {
                        return null;
                    }

                    if (response.has("name_ar")) {
                        category.arabicName = response.getString("name_ar");
                    }

                    if (response.has("name_en")) {
                        category.englishName = response.getString("name_en");
                    }

                    if (response.has("image_path")) {
                        category.imagePath = response.getString("image_path");
                    }

                    if (response.has("tags")) {
                        category.tags = new ArrayList<>();
                        JSONArray tagsJsonArray = response.getJSONArray("tags");
                        for (int i = 0; i < tagsJsonArray.length(); i++) {
                            JSONObject tagJsonObject = tagsJsonArray.getJSONObject(i);
                            Tag tag = new Tag();
                            if (tagJsonObject.has("id")) {
                                tag.id = response.getInt("id");
                            } else {
                                tag = null;
                            }

                            if (tagJsonObject.has("tags")) {
                                tag.name = response.getString("tags");
                            }

                            if (tag != null) {
                                category.tags.add(tag);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return category;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "Categories", null);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
