package com.bridge.zuhur.mappers.VolleyQueue;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class RequestsQueue {

    private static RequestQueue mRequestsQueue = null;
    public static Context context;

    /**
     *
     * @return the singleton object of the queue
     */
    public static RequestQueue getRequestQueue() {
        if (mRequestsQueue == null) {
            mRequestsQueue = Volley.newRequestQueue(context);
        }
        return mRequestsQueue;
    }

}
