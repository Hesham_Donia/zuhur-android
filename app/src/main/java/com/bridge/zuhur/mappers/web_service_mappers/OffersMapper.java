package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Offer;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;
import com.bridge.zuhur.utils.CommonConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 07/04/16.
 */
public class OffersMapper extends AbstractWebServiceMapper {

    int indicator = 0;
    String id = null;
    Filter mFilter;

    public OffersMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        switch (indicator) {
            case 1:
                return "OfferDetails/" + id;

            case 3:
                return "offers/" + mFilter.countryId;

            default:
                return null;
        }
    }

    @Override
    protected String initRequestTag() {
        switch (indicator) {
            case 1:
                return ExtrasController.OFFER_DETAILS_REQUEST_TAG;

            case 3:
                return ExtrasController.OFFERS_REQUEST_TAG;

            default:
                return null;
        }
    }

    @Override
    protected void initParser() {

        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {

                final Offer offer = new Offer();

                try {
                    if (response.has("id")) {
                        offer.id = response.getInt("id");
                    } else {
                        return null;
                    }

                    if (response.has("details_ar")) {
                        offer.arabicDetails = response.getString("details_ar");
                    }

                    if (response.has("details_en")) {
                        offer.englishDetails = response.getString("details_en");
                    }

                    if (response.has("image_path")) {
                        offer.imagePath = response.getString("image_path");
                    }

                    if (response.has("total_price")) {
                        offer.totalPrice = response.getDouble("total_price");
                    }

                    if (response.has("original_price")) {
                        offer.originalPrice = response.getDouble("original_price");
                    }

                    if (response.has("create_date")) {
                        offer.creationDate = response.getString("create_date");
                    }

                    if (response.has("end_date")) {
                        offer.endDate = response.getString("end_date");
                    }

                    if (response.has("is_completed")) {
                        if (response.getString("is_completed").equals("0")) {
                            offer.isCompleted = false;
                        } else {
                            offer.isCompleted = true;
                        }
                    }

                    if (response.has("products")) {
                        JSONArray productsJsonArray = response.getJSONArray("products");
                        for (int i = 0; i < productsJsonArray.length(); i++) {
                            Product product = new Product();
                            JSONObject productJsonObject = productsJsonArray.getJSONObject(i);
                            if (productJsonObject.has("id")) {
                                product.id = productJsonObject.getInt("id");
                            } else {
                                product = null;
                            }

                            if (productJsonObject.has("cat_id")) {
                                product.categoryId = productJsonObject.getInt("cat_id");
                            }

                            if (productJsonObject.has("name_ar")) {
                                product.arabicName = productJsonObject.getString("name_ar");
                            }

                            if (productJsonObject.has("description_ar")) {
                                product.arabicDescription = productJsonObject.getString("description_ar");
                            }

                            if (productJsonObject.has("description_en")) {
                                product.englishDescription = productJsonObject.getString("description_en");
                            }

                            if (productJsonObject.has("point_price")) {
                                product.pointsPrice = productJsonObject.getInt("point_price");
                            }

                            if (productJsonObject.has("name_en")) {
                                product.englishName = productJsonObject.getString("name_en");
                            }

                            if (productJsonObject.has("image_path")) {
                                product.imagePath = productJsonObject.getString("image_path");
                            }

                            if (productJsonObject.has("rate")) {
                                product.rate = productJsonObject.getDouble("rate");
                            }

                            if (productJsonObject.has("price")) {
                                product.price = productJsonObject.getDouble("price");
                            }

                            if (productJsonObject.has("url")) {
                                product.url = productJsonObject.getString("url");
                            }

                            if (productJsonObject.has("is_exist")) {
                                if (productJsonObject.getString("is_exist").equals("1")) {
                                    product.isExist = true;
                                } else {
                                    product.isExist = false;
                                }

                            }

                            if (productJsonObject.has("is_new_arrival")) {
                                if (productJsonObject.getString("is_new_arrival").equals("1")) {
                                    product.isNewArrival = true;
                                } else {
                                    product.isNewArrival = false;
                                }
                            }

                            if (productJsonObject.has("create_date")) {
                                product.creationDate = productJsonObject.getString("create_date");
                            }

                            if (productJsonObject.has("sell_counter")) {
                                product.sellCounter = productJsonObject.getInt("sell_counter");
                            }

                            if (productJsonObject.has("seen_counter")) {
                                product.seenCounter = productJsonObject.getInt("seen_counter");
                            }

                            if (productJsonObject.has("rate_counter")) {
                                product.peopleRatedCounter = productJsonObject.getInt("rate_counter");
                            }

                            if (product != null) {
                                offer.products.add(product);
                            }
                        }
                    }

                } catch (JSONException exception) {
                    exception.printStackTrace();
                }

                return offer;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 1;
        this.id = id;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "data", null);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 3;
        mFilter = filters;
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "getting offers");
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "data", null);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
