package com.bridge.zuhur.mappers.database_mappers;

import android.content.Context;

import com.bridge.zuhur.database.OrdersHistory;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by hesham on 13/04/16.
 */
public class OrdersDatabaseMapper extends AbstractDatabaseMapper {

    public OrdersDatabaseMapper(Context context) {
        super(context);
        mDao = mDbAdapter.getOrdersHistoryDao();
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        QueryBuilder queryBuilder = mDao.queryBuilder();
        try {
            queryBuilder.where().eq(OrdersHistory.OrdersHistory_Order_ID, id);
            executeOperation(DatabaseOperation.SELECT, listener, mDao, queryBuilder, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        executeOperation(DatabaseOperation.SELECT, listener, mDao, null, null);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        OrdersHistory ordersHistory = (OrdersHistory) value;
        try {
            if (mDao.queryBuilder().where().eq(OrdersHistory.OrdersHistory_Order_ID, ordersHistory.getOrderId()).query().size() > 0) {
                listener.onSuccess(false);
            } else {
                executeOperation(DatabaseOperation.INSERT, listener, mDao, null, ordersHistory);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        makeTransaction(value, 0, value.size(), listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {

    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        DeleteBuilder deleteBuilder = mDao.deleteBuilder();
        try {
            deleteBuilder.where().eq(OrdersHistory.OrdersHistory_Order_ID, id);
            executeOperation(DatabaseOperation.DELETE, listener, mDao, deleteBuilder, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        DeleteBuilder deleteBuilder = mDao.deleteBuilder();
        executeOperation(DatabaseOperation.DELETE, listener, mDao, deleteBuilder, null);
    }

}
