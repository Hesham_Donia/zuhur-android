package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Tag;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/9/16.
 */
public class TagsMapper extends AbstractWebServiceMapper {

    public TagsMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "tags";
    }

    @Override
    protected String initRequestTag() {
        return ExtrasController.TAGS_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {

                Tag tag = new Tag();

                try {
                    if (response.has("id")) {
                        tag.id = response.getInt("id");
                    } else {
                        return null;
                    }

                    if (response.has("tags")) {
                        tag.name = response.getString("tags");
                    }

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

                return tag;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "data", filters);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
