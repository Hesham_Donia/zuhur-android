package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 06/04/16.
 */
public class ProductDetailsMapper extends AbstractWebServiceMapper {

    String id;
    int indicator = 0;
    Filter mFilter;

    public ProductDetailsMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        switch (indicator) {
            case 1:
                return "product/" + id;

            case 2:
                return "product/" + mFilter.productId + "/" + mFilter.countryId;

            case 4:
                return "rate";

            default:
                return null;
        }
    }

    @Override
    protected String initRequestTag() {
        switch (indicator) {
            case 2:
                return ProductsController.GET_PRODUCT_REQUEST_TAG;

            case 4:
                return ProductsController.RATE_PRODUCT_REQUEST_TAG;

            default:
                return null;
        }
    }

    @Override
    protected void initParser() {

        switch (indicator) {
            case 2:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        Product product = new Product();

                        try {
                            if (response.has("id")) {
                                product.id = response.getInt("id");
                            } else {
                                return null;
                            }

                            if (response.has("cat_id")) {
                                product.categoryId = response.getInt("cat_id");
                            }

                            if (response.has("url")) {
                                product.url = response.getString("url");
                            }

                            if (response.has("name_ar")) {
                                product.arabicName = response.getString("name_ar");
                            }

                            if (response.has("name_en")) {
                                product.englishName = response.getString("name_en");
                            }

                            if (response.has("image_path")) {
                                product.imagePath = response.getString("image_path");
                            }

                            if (response.has("description_ar")) {
                                product.arabicDescription = response.getString("description_ar");
                            }

                            if (response.has("point_price")) {
                                product.pointsPrice = response.getInt("point_price");
                            }

                            if (response.has("description_en")) {
                                product.englishDescription = response.getString("description_en");
                            }

                            if (response.has("rate")) {
                                product.rate = response.getDouble("rate");
                            }

                            if (response.has("price")) {
                                product.price = response.getDouble("price");
                            }

                            if (response.has("is_exist")) {
                                if (response.getString("is_exist").equals("1")) {
                                    product.isExist = true;
                                } else {
                                    product.isExist = false;
                                }

                            }

                            if (response.has("is_new_arrival")) {
                                if (response.getString("is_new_arrival").equals("1")) {
                                    product.isNewArrival = true;
                                } else {
                                    product.isNewArrival = false;
                                }
                            }

                            if (response.has("create_date")) {
                                product.creationDate = response.getString("create_date");
                            }

                            if (response.has("sell_counter")) {
                                product.sellCounter = response.getInt("sell_counter");
                            }

                            if (response.has("seen_counter")) {
                                product.seenCounter = response.getInt("seen_counter");
                            }

                            if (response.has("rate_counter")) {
                                product.peopleRatedCounter = response.getInt("rate_counter");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return product;
                    }
                };
                break;

            case 4:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        SignUpResponse signUpResponse = new SignUpResponse();
                        try {
                            if (response.has("massage")) {
                                signUpResponse.message = response.getString("massage");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return signUpResponse;
                    }
                };
                break;
        }
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 1;
        this.id = id;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "product", null);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 2;
        mFilter = filters;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "product", null);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 4;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", entity);
    }
}
