package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.database.AutoComplete;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 6/12/16.
 */
public class AutoCompleteMapper extends AbstractWebServiceMapper {

    public AutoCompleteMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "listall";
    }

    @Override
    protected String initRequestTag() {
        return ProductsController.GET_AUTO_COMPLETE_SERVER_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {
                AutoComplete autoComplete = new AutoComplete();
                try {
                    if (response.has("name")) {
                        autoComplete.setName(response.getString("name"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return autoComplete;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "names", null);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
