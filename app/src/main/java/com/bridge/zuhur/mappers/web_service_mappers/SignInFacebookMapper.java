package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.AuthenticationController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Phone;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.entities.User;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 6/15/16.
 */
public class SignInFacebookMapper extends AbstractWebServiceMapper {

    private Filter mFilter;

    public SignInFacebookMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "loginfacebook";
    }

    @Override
    protected String initRequestTag() {
        return AuthenticationController.SIGN_IN_FACEBOOK_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {
                User user = new User();
                try {
                    if (response.has("id")) {
                        user.id = response.getInt("id");
                    } else {
                        return null;
                    }

                    if (response.has("name")) {
                        user.name = response.getString("name");
                    }

                    if (response.has("user_level")) {
                        user.level = response.getInt("user_level");
                    }

                    if (response.has("user_code")) {
                        user.code = response.getString("user_code");
                    }

                    if (response.has("email")) {
                        user.email = response.getString("email");
                    }

                    if (response.has("address")) {
                        user.address = response.getString("address");
                    }

                    if (response.has("username")) {
                        user.username = response.getString("username");
                    }

                    if (response.has("points")) {
                        user.points = response.getInt("points");
                    }

                    if (response.has("token")) {
                        Singleton.getInstance().token = response.getString("token");
                    }

                    if (response.has("phones")) {
                        JSONArray phonesJsonArray = response.getJSONArray("phones");
                        Phone phone = new Phone();
                        for (int i = 0; i < phonesJsonArray.length(); i++) {
                            JSONObject phoneJsonObject = phonesJsonArray.getJSONObject(i);
                            phone.number = phoneJsonObject.getString("num");
                            if (phoneJsonObject.getString("is_primary").equals("1")) {
                                phone.isPrimary = true;
                            } else {
                                phone.isPrimary = false;
                            }

                            user.phones.add(phone);
                        }
                    }

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

                return user;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        this.mFilter = filters;
        User user = new User();
        user.userDataAsJson = mFilter.searchQuery;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", user);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
