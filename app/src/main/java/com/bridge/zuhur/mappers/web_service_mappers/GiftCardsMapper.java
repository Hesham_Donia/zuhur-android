package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.GiftCard;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/8/16.
 */
public class GiftCardsMapper extends AbstractWebServiceMapper {

    public GiftCardsMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "giftcards/" + Singleton.getInstance().country.id;
    }

    @Override
    protected String initRequestTag() {
        return OrderController.GIFT_CARDS_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {

                GiftCard giftCard = new GiftCard();

                try {
                    if (response.has("id")) {
                        giftCard.id = response.getInt("id");
                    } else {
                        return null;
                    }

                    if (response.has("name_ar")) {
                        giftCard.nameAr = response.getString("name_ar");
                    }

                    if (response.has("name_en")) {
                        giftCard.nameEn = response.getString("name_en");
                    }

                    if (response.has("details_ar")) {
                        giftCard.detailsAr = response.getString("details_ar");
                    }

                    if (response.has("details_en")) {
                        giftCard.detailsEn = response.getString("details_en");
                    }

                    if (response.has("image_path")) {
                        giftCard.imagePath = response.getString("image_path");
                    }

                    if (response.has("point_price")) {
                        giftCard.pointsPrice = response.getInt("point_price");
                    }

                    if (response.has("price")) {
                        giftCard.price = response.getDouble("price");
                    }

                    if (response.has("code")) {
                        giftCard.code = response.getString("code");
                    }

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }


                return giftCard;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "data", filters);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
