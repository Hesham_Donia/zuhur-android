package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 07/04/16.
 */
public class AddRecentViewedMapper extends AbstractWebServiceMapper {

    public AddRecentViewedMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "addrecent";
    }

    @Override
    protected String initRequestTag() {
        return ExtrasController.ADD_RECENT_VIEWED_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {
                return null;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        Product product = (Product) entity;
        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;
        filter.productId = product.id;
        filter.countryId = Singleton.getInstance().country.id;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filter);
    }
}
