package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.AuthenticationController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 12/04/16.
 */
public class UpdatePasswordMapper extends AbstractWebServiceMapper {


    public UpdatePasswordMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "editpass";
    }

    @Override
    protected String initRequestTag() {
        return AuthenticationController.UPDATE_PASSWORD_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {
                SignUpResponse signUpResponse = new SignUpResponse();
                if (response.has("massage")) {
                    try {
                        signUpResponse.message = response.getString("massage");
                        signUpResponse.value = response.getInt("value");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return signUpResponse;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", entity);
    }
}
