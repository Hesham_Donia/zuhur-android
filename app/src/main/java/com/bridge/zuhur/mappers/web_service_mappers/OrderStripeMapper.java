package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 6/21/16.
 */
public class OrderStripeMapper extends AbstractWebServiceMapper {

    public OrderStripeMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "stripeorder";
    }

    @Override
    protected String initRequestTag() {
        return OrderController.ORDER_STRIPE_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {
                SignUpResponse signUpResponse = new SignUpResponse();
                try {
                    if (response.has("value")) {
                        signUpResponse.value = response.getInt("value");
                    }

                    if (response.has("massage")) {
                        signUpResponse.message = response.getString("massage");
                    }

                    if (response.has("order_id")) {
                        signUpResponse.id = response.getInt("order_id");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return signUpResponse;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", entity);
    }
}
