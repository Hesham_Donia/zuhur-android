package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.FavouritesController;
import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;
import com.bridge.zuhur.utils.CommonConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 10/04/16.
 */
public class FavouritesMapper extends AbstractWebServiceMapper {

    int indicator = 0;

    public FavouritesMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        switch (indicator) {
            case 1:

                return "delfav";

            case 2:

                return "";

            case 3:

                return "favlist";

            case 4:
                return "addfav";

            default:
                return "";
        }
    }

    @Override
    protected String initRequestTag() {
        switch (indicator) {
            case 1:

                return FavouritesController.DELETE_FAVOURITE_SERVER_REQUEST_TAG;

            case 2:

                return "";

            case 3:

                return FavouritesController.GET_ALL_FAVOURITES_SERVER_REQUEST_TAG;

            case 4:
                return FavouritesController.ADD_FAVOURITE_TO_SERVER_REQUEST_TAG;

            default:
                return "";
        }
    }

    @Override
    protected void initParser() {
        switch (indicator) {

            case 1:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        SignUpResponse signUpResponse = new SignUpResponse();
                        if (response.has("massage")) {
                            try {
                                signUpResponse.message = response.getString("massage");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        return signUpResponse;
                    }
                };
                break;

            case 3:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        Product product = new Product();


                        try {
                            if (response.has("id")) {
                                product.id = response.getInt("id");
                            } else {
                                return null;
                            }

                            if (response.has("cat_id")) {
                                product.categoryId = response.getInt("cat_id");
                            }

                            if (response.has("name_ar")) {
                                product.arabicName = response.getString("name_ar");
                            }

                            if (response.has("name_en")) {
                                product.englishName = response.getString("name_en");
                            }

                            if (response.has("image_path")) {
                                product.imagePath = response.getString("image_path");
                            }

                            if (response.has("description_ar")) {
                                product.arabicDescription = response.getString("description_ar");
                            }

                            if (response.has("points")) {
                                product.pointsPrice = response.getInt("points");
                            }

                            if (response.has("description_en")) {
                                product.englishDescription = response.getString("description_en");
                            }

                            if (response.has("rate")) {
                                product.rate = response.getDouble("rate");
                            }

                            if (response.has("price")) {
                                product.price = response.getDouble("price");
                            }

                            if (response.has("is_exist")) {
                                if (response.getString("is_exist").equals("1")) {
                                    product.isExist = true;
                                } else {
                                    product.isExist = false;
                                }
                            }

                            if (response.has("url")) {
                                product.url = response.getString("url");
                            }

                            if (response.has("is_new_arrival")) {
                                if (response.getString("is_new_arrival").equals("1")) {
                                    product.isNewArrival = true;
                                } else {
                                    product.isNewArrival = false;
                                }
                            }

                            if (response.has("create_date")) {
                                product.creationDate = response.getString("create_date");
                            }

                            if (response.has("sell_counter")) {
                                product.sellCounter = response.getInt("sell_counter");
                            }

                            if (response.has("seen_counter")) {
                                product.seenCounter = response.getInt("seen_counter");
                            }

                            if (response.has("rate_counter")) {
                                product.peopleRatedCounter = response.getInt("rate_counter");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return new Favourite(product);
                    }
                };
                break;

            case 4:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        SignUpResponse signUpResponse = new SignUpResponse();
                        if (response.has("massage")) {
                            try {
                                signUpResponse.message = response.getString("massage");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        return signUpResponse;
                    }
                };
                break;
        }
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 1;
        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "filter.productId: " + filter.productId);
        filter.productId = Integer.valueOf(id);
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filter);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "deleting favourite");
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 2;
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "getting favourite by filter");
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 3;
        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;
        filter.countryId = Singleton.getInstance().country.id;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filter);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 4;
        Favourite favourite = (Favourite) entity;
        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;
        filter.productId = favourite.getProduct().id;
        filter.countryId = Singleton.getInstance().country.id;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filter);
    }
}
