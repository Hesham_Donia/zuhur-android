package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Country;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/12/16.
 */
public class CountriesMapper extends AbstractWebServiceMapper {

    public CountriesMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "listcountries";
    }

    @Override
    protected String initRequestTag() {
        return ExtrasController.COUNTRIES_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {

                Country country = new Country();
                try {
                    if (response.has("id")) {
                        country.id = response.getInt("id");
                    } else {
                        return null;
                    }

                    if (response.has("name_ar")) {
                        country.arabicName = response.getString("name_ar");
                    }

                    if (response.has("name_en")) {
                        country.englishName = response.getString("name_en");
                    }

                    if (response.has("image_path")) {
                        country.imagePath = response.getString("image_path");
                    }

                    if (response.has("currency")) {
                        country.currency = response.getString("currency");
                    }

                    if (response.has("dollar")) {
                        country.dollarValue = response.getDouble("dollar");
                    }

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }


                return country;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "Categories", null);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
