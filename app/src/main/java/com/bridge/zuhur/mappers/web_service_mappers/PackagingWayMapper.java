package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.PackagingWay;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/9/16.
 */
public class PackagingWayMapper extends AbstractWebServiceMapper {

    public PackagingWayMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "packagings/" + Singleton.getInstance().country.id;
    }

    @Override
    protected String initRequestTag() {
        return OrderController.PACKAGING_WAYS_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {

                PackagingWay packagingWay = new PackagingWay();

                try {

                    if (response.has("id")) {
                        packagingWay.id = response.getInt("id");
                    } else {
                        return null;
                    }

                    if (response.has("name_ar")) {
                        packagingWay.nameAr = response.getString("name_ar");
                    }

                    if (response.has("name_en")) {
                        packagingWay.nameEn = response.getString("name_en");
                    }

                    if (response.has("details_ar")) {
                        packagingWay.detailsAr = response.getString("details_ar");
                    }

                    if (response.has("details_en")) {
                        packagingWay.detailsEn = response.getString("details_en");
                    }

                    if (response.has("image_path")) {
                        packagingWay.imagePath = response.getString("image_path");
                    }

                    if (response.has("point_price")) {
                        packagingWay.pointsPrice = response.getInt("point_price");
                    }

                    if (response.has("price")) {
                        packagingWay.price = response.getDouble("price");
                    }

                    if (response.has("code")) {
                        packagingWay.code = response.getString("code");
                    }

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

                return packagingWay;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "data", filters);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
