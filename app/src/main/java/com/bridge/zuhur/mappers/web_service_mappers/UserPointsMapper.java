package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;
import com.bridge.zuhur.utils.CommonConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 6/21/16.
 */
public class UserPointsMapper extends AbstractWebServiceMapper {

    public UserPointsMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "getpoints";
    }

    @Override
    protected String initRequestTag() {
        return OrderController.UPDATE_USER_POINTS_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {

                SignUpResponse signUpResponse = new SignUpResponse();
                if (response.has("points")) {
                    try {
                        Log.d(CommonConstants.APPLICATION_LOG_TAG, "points in mapper: " + response.getInt("points"));
                        signUpResponse.value = response.getInt("points");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return signUpResponse;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filters);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
