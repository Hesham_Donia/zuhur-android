package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 07/04/16.
 */
public class NewArrivalsMapper extends AbstractWebServiceMapper {

    Filter mFilter;

    public NewArrivalsMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "newarrival/" + mFilter.countryId;
    }

    @Override
    protected String initRequestTag() {
        return ExtrasController.NEW_ARRIVALS_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {
                Product product = new Product();

                try {
                    if (response.has("id")) {
                        product.id = response.getInt("id");
                    } else {
                        return null;
                    }

                    if (response.has("cat_id")) {
                        product.categoryId = response.getInt("cat_id");
                    }

                    if (response.has("name_ar")) {
                        product.arabicName = response.getString("name_ar");
                    }

                    if (response.has("name_en")) {
                        product.englishName = response.getString("name_en");
                    }

                    if (response.has("image_path")) {
                        product.imagePath = response.getString("image_path");
                    }

                    if (response.has("description_ar")) {
                        product.arabicDescription = response.getString("description_ar");
                    }

                    if (response.has("points")) {
                        product.pointsPrice = response.getInt("points");
                    }

                    if (response.has("description_en")) {
                        product.englishDescription = response.getString("description_en");
                    }

                    if (response.has("rate")) {
                        product.rate = response.getDouble("rate");
                    }

                    if (response.has("price")) {
                        product.price = response.getDouble("price");
                    }

                    if (response.has("is_exist")) {
                        if (response.getString("is_exist").equals("1")) {
                            product.isExist = true;
                        } else {
                            product.isExist = false;
                        }
                    }

                    if (response.has("url")) {
                        product.url = response.getString("url");
                    }

                    if (response.has("is_new_arrival")) {
                        if (response.getString("is_new_arrival").equals("1")) {
                            product.isNewArrival = true;
                        } else {
                            product.isNewArrival = false;
                        }
                    }

                    if (response.has("create_date")) {
                        product.creationDate = response.getString("create_date");
                    }

                    if (response.has("sell_counter")) {
                        product.sellCounter = response.getInt("sell_counter");
                    }

                    if (response.has("seen_counter")) {
                        product.seenCounter = response.getInt("seen_counter");
                    }

                    if (response.has("rate_counter")) {
                        product.peopleRatedCounter = response.getInt("rate_counter");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return product;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        mFilter = filters;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "data", null);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
