package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.ExperienceEvent;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 11/04/16.
 */
public class ExperienceEventsMapper extends AbstractWebServiceMapper {

    public ExperienceEventsMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "expevents/" + Singleton.getInstance().country.id;
    }

    @Override
    protected String initRequestTag() {
        return ExtrasController.EXPERIENCE_EVENTS_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {

                ExperienceEvent experience = new ExperienceEvent();
                try {
                    if (response.has("id")) {
                        experience.id = response.getInt("id");
                    } else {
                        return null;
                    }

                    if (response.has("name_ar")) {
                        experience.arabicName = response.getString("name_ar");
                    }

                    if (response.has("name_en")) {
                        experience.englishName = response.getString("name_en");
                    }

                    if (response.has("products")) {
                        JSONArray productsJsonArray = response.getJSONArray("products");
                        for (int i = 0; i < productsJsonArray.length(); i++) {
                            Product product = new Product();
                            JSONObject productJsonObject = productsJsonArray.getJSONObject(i);
                            if (productJsonObject.has("id")) {
                                product.id = productJsonObject.getInt("id");
                            } else {
                                product = null;
                            }

                            if (productJsonObject.has("cat_id")) {
                                product.categoryId = productJsonObject.getInt("cat_id");
                            }

                            if (productJsonObject.has("name_ar")) {
                                product.arabicName = productJsonObject.getString("name_ar");
                            }

                            if (productJsonObject.has("name_en")) {
                                product.englishName = productJsonObject.getString("name_en");
                            }

                            if (productJsonObject.has("image_path")) {
                                product.imagePath = productJsonObject.getString("image_path");
                            }

                            if (productJsonObject.has("point_price")) {
                                product.pointsPrice = productJsonObject.getInt("point_price");
                            }

                            if (productJsonObject.has("rate")) {
                                product.rate = productJsonObject.getDouble("rate");
                            }

                            if (productJsonObject.has("description_ar")) {
                                product.arabicDescription = productJsonObject.getString("description_ar");
                            }

                            if (productJsonObject.has("description_en")) {
                                product.englishDescription = productJsonObject.getString("description_en");
                            }

                            if (productJsonObject.has("price")) {
                                product.price = productJsonObject.getDouble("price");
                            }

                            if (productJsonObject.has("is_exist")) {
                                if (productJsonObject.getString("is_exist").equals("1")) {
                                    product.isExist = true;
                                } else {
                                    product.isExist = false;
                                }

                            }

                            if (productJsonObject.has("url")) {
                                product.url = productJsonObject.getString("url");
                            }

                            if (productJsonObject.has("is_new_arrival")) {
                                if (productJsonObject.getString("is_new_arrival").equals("1")) {
                                    product.isNewArrival = true;
                                } else {
                                    product.isNewArrival = false;
                                }
                            }

                            if (productJsonObject.has("create_date")) {
                                product.creationDate = productJsonObject.getString("create_date");
                            }

                            if (productJsonObject.has("sell_counter")) {
                                product.sellCounter = productJsonObject.getInt("sell_counter");
                            }

                            if (productJsonObject.has("seen_counter")) {
                                product.seenCounter = productJsonObject.getInt("seen_counter");
                            }

                            if (productJsonObject.has("rate_counter")) {
                                product.peopleRatedCounter = productJsonObject.getInt("rate_counter");
                            }

                            if (product != null) {
                                experience.products.add(product);
                            }
                        }
                    }

                } catch (JSONException exception) {
                    exception.printStackTrace();
                }


                return experience;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.GET, listener, "data", null);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
