package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Order;
import com.bridge.zuhur.entities.OrderStatus;
import com.bridge.zuhur.entities.PaymentMethod;
import com.bridge.zuhur.entities.Place;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;
import com.bridge.zuhur.utils.CommonConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hesham on 12/04/16.
 */
public class OrdersMapper extends AbstractWebServiceMapper {

    int indicator = 0;

    public OrdersMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        switch (indicator) {

            case 1:
                return "trackorder";

            case 2:
                return "listordernotcomplated";

            case 3:

                return "listorder";

            case 4:
                return "order";

            default:
                return "";
        }
    }

    @Override
    protected String initRequestTag() {
        switch (indicator) {

            case 1:
                return OrderController.TRACK_ORDER_REQUEST_TAG;

            case 2:
                return OrderController.GET_UN_COMPLETED_ORDERS_REQUEST_TAG;

            case 3:
                return OrderController.GET_ORDER_FROM_SERVER_REQUEST_TAG;

            case 4:
                return OrderController.ADD_ORDER_TO_SERVER_REQUEST_TAG;

            default:
                return "";
        }
    }

    @Override
    protected void initParser() {
        switch (indicator) {

            case 1:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {

                        OrderStatus orderStatus = new OrderStatus();

                        try {
                            if (response.has("id")) {
                                orderStatus.id = response.getInt("id");
                            }

                            if (response.has("order_id")) {
                                orderStatus.orderId = response.getInt("order_id");
                            } else {
                                return null;
                            }

                            if (response.has("create_date")) {
                                orderStatus.creationDate = response.getString("create_date");
                            }

                            if (response.has("end_date")) {
                                orderStatus.endDate = response.getString("end_date");
                            }

                            if (response.has("status_name_ar")) {
                                orderStatus.arabicName = response.getString("status_name_ar");
                            }

                            if (response.has("status_name_en")) {
                                orderStatus.englishName = response.getString("status_name_en");
                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }


                        return orderStatus;
                    }
                };
                break;

            case 2:
            case 3:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        Order order = new Order();

                        try {
                            if (response.has("id")) {
                                order.id = response.getInt("id");
                            } else {
                                return null;
                            }

                            Place billPlace = new Place(), deliverPlace = new Place();


                            if (response.has("bill_address")) {
                                billPlace.address = response.getString("bill_address");
                            }

                            if (response.has("bill_address_X")) {
                                billPlace.latitude = response.getString("bill_address_X");
                            }

                            if (response.has("bill_address_Y")) {
                                billPlace.longitude = response.getString("bill_address_Y");
                            }

                            order.billingPlace = billPlace;

                            if (response.has("deliver_address")) {
                                deliverPlace.address = response.getString("deliver_address");
                            }

                            if (response.has("deliver_address_X")) {
                                deliverPlace.latitude = response.getString("deliver_address_X");
                            }

                            if (response.has("deliver_address_Y")) {
                                deliverPlace.longitude = response.getString("deliver_address_Y");
                            }

                            order.deliverPlace = deliverPlace;

                            if (response.has("total_price")) {
                                order.finalPrice = Double.valueOf(response.getString("total_price"));
                            }

                            if (response.has("create_date")) {
                                order.creationDate = response.getString("create_date");
                            }

                            if (response.has("shipping_date")) {
                                order.shippingDate = response.getString("shipping_date");
                            }

                            if (response.has("is_confirmed")) {
                                if (response.getString("is_confirmed").equals("0")) {
                                    order.isConfirmed = false;
                                } else {
                                    order.isConfirmed = true;
                                }
                            }

                            if (response.has("is_completed")) {
                                if (response.getString("is_completed").equals("0")) {
                                    order.isCompleted = false;
                                } else {
                                    order.isCompleted = true;
                                }
                            }

                            PaymentMethod paymentMethod = new PaymentMethod();

                            if (response.has("payment_method_id")) {
                                paymentMethod.id = response.getInt("payment_method_id");
                            }

                            order.mainPaymentMethod = paymentMethod;

                            if (response.has("products")) {
                                JSONArray productsJsonArray = response.getJSONArray("products");
                                for (int i = 0; i < productsJsonArray.length(); i++) {
                                    Product product = new Product();
                                    JSONObject productJsonObject = productsJsonArray.getJSONObject(i);
                                    if (productJsonObject.has("id")) {
                                        product.id = productJsonObject.getInt("id");
                                    } else {
                                        product = null;
                                    }

                                    if (productJsonObject.has("cat_id")) {
                                        product.categoryId = productJsonObject.getInt("cat_id");
                                    }

                                    if (productJsonObject.has("name_ar")) {
                                        product.arabicName = productJsonObject.getString("name_ar");
                                    }

                                    if (productJsonObject.has("name_en")) {
                                        product.englishName = productJsonObject.getString("name_en");
                                    }

                                    if (productJsonObject.has("point_price")) {
                                        product.pointsPrice = productJsonObject.getInt("point_price");
                                    }

                                    if (productJsonObject.has("image_path")) {
                                        product.imagePath = productJsonObject.getString("image_path");
                                    }

                                    if (productJsonObject.has("rate")) {
                                        product.rate = productJsonObject.getDouble("rate");
                                    }

                                    if (productJsonObject.has("price")) {
                                        product.price = productJsonObject.getDouble("price");
                                    }

                                    if (productJsonObject.has("is_exist")) {
                                        if (productJsonObject.getString("is_exist").equals("1")) {
                                            product.isExist = true;
                                        } else {
                                            product.isExist = false;
                                        }

                                    }

                                    if (productJsonObject.has("is_new_arrival")) {
                                        if (productJsonObject.getString("is_new_arrival").equals("1")) {
                                            product.isNewArrival = true;
                                        } else {
                                            product.isNewArrival = false;
                                        }
                                    }

                                    if (productJsonObject.has("create_date")) {
                                        product.creationDate = productJsonObject.getString("create_date");
                                    }

                                    if (productJsonObject.has("sell_counter")) {
                                        product.sellCounter = productJsonObject.getInt("sell_counter");
                                    }

                                    if (productJsonObject.has("seen_counter")) {
                                        product.seenCounter = productJsonObject.getInt("seen_counter");
                                    }

                                    if (productJsonObject.has("rate_counter")) {
                                        product.peopleRatedCounter = productJsonObject.getInt("rate_counter");
                                    }

                                    if (product != null) {
                                        order.products.add(product);
                                    }
                                }
                            }


                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                        return order;
                    }
                };
                break;

            case 4:
                parser = new Parser<JSONObject>() {
                    @Override
                    public Entity parse(JSONObject response) {
                        SignUpResponse signUpResponse = new SignUpResponse();
                        try {
                            if (response.has("value")) {
                                signUpResponse.value = response.getInt("value");
                            }

                            if (response.has("massage")) {
                                signUpResponse.message = response.getString("massage");
                            }

                            if (response.has("order_id")) {
                                signUpResponse.id = response.getInt("order_id");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return signUpResponse;
                    }
                };
                break;
        }
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "tracking order");
        indicator = 1;
        Filter filter = new Filter();
        filter.orderId = id;
        filter.id = Singleton.getInstance().currentUser.id;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filter);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 2;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filters);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 3;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filters);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        indicator = 4;
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", entity);
    }
}
