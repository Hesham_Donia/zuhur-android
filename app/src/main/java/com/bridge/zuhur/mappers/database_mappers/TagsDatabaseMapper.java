package com.bridge.zuhur.mappers.database_mappers;

import android.content.Context;

import com.bridge.zuhur.database.Tag;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/9/16.
 */
public class TagsDatabaseMapper extends AbstractDatabaseMapper {

    public TagsDatabaseMapper(Context context) {
        super(context);
        mDao = mDbAdapter.getTagDao();
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        QueryBuilder queryBuilder = mDao.queryBuilder();
        try {
            queryBuilder.where().eq(Tag.Tag_TagObjectId, id);
            executeOperation(DatabaseOperation.SELECT, listener, mDao, queryBuilder, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        if (filter == null) {
            executeOperation(DatabaseOperation.SELECT, listener, mDao, null, null);
        } else {
            QueryBuilder queryBuilder = mDao.queryBuilder();
            try {
                queryBuilder.groupBy(Tag.Tag_TagObjectName).limit((long) 10).where().
                        like(Tag.Tag_TagObjectName, '%' + filter.searchQuery + '%').query();
                executeOperation(DatabaseOperation.SELECT, listener, mDao, queryBuilder, null);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        Tag tag = (Tag) value;
        try {
            if (mDao.queryBuilder().where().eq(Tag.Tag_TagObjectId, tag.getTagId()).query().size() > 0) {
                listener.onSuccess(false);
            } else {
                executeOperation(DatabaseOperation.INSERT, listener, mDao, null, tag);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        makeTransaction(value, 0, value.size(), listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {

    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        DeleteBuilder deleteBuilder = mDao.deleteBuilder();
        try {
            deleteBuilder.where().eq(Tag.Tag_TagObjectId, id);
            executeOperation(DatabaseOperation.DELETE, listener, mDao, deleteBuilder, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        DeleteBuilder deleteBuilder = mDao.deleteBuilder();
        executeOperation(DatabaseOperation.DELETE, listener, mDao, deleteBuilder, null);
    }
}
