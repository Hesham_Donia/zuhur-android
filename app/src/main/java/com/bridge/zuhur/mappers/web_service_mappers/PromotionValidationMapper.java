package com.bridge.zuhur.mappers.web_service_mappers;

import android.content.Context;

import com.android.volley.Request;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Promotion;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.interfaces.Parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/9/16.
 */
public class PromotionValidationMapper extends AbstractWebServiceMapper {

    public PromotionValidationMapper(Context ctx) {
        super(ctx);
    }

    @Override
    protected String getUrl() {
        return "promotion_codes";
    }

    @Override
    protected String initRequestTag() {
        return OrderController.PROMOTION_CODE_VALIDATION_REQUEST_TAG;
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject response) {

                Promotion promotion = new Promotion();
                try {
                    if (response.has("promotion")) {
                        promotion.promotionPercentage = response.getDouble("promotion");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return promotion;
            }
        };
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        executeRequest(JsonRequestType.JSON_OBJECT, Request.Method.POST, listener, "data", filters);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
