package com.bridge.zuhur.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by hesham on 05/04/16.
 */
public class Call {

    public static void makeCall(Context context, String phoneNumber) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));
        context.startActivity(callIntent);
    }
}
