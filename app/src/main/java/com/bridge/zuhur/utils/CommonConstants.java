package com.bridge.zuhur.utils;

/**
 * Created by hesham on 04/04/16.
 */
public class CommonConstants {

    public static final String APPLICATION_LOG_TAG = "zuhurApp";
    public static final String SHARED_PREFERENCES_NAME = "zuhur";
    public static final String BASIC_URL = "http://rosemart.online/demo/public/";

    public static final String SORT_PRICE_LOW_TO_HIGH = "low";
    public static final String SORT_PRICE_HIGH_TO_LOW = "high";
    public static final String SORT_BEST_SELLER = "sell";
    public static final String SORT_NEW_ARRIVALS = "new";
    public static final String SORT_MOST_VIEWED = "view";

    public static final int ADD_OPERATION = 1;
    public static final int EDIT_OPERATION = 2;
    public static final int DELETE_OPERATION = 3;

    public static final int WISH_LIST_TYPE = 4;
    public static final int FAVOURITE_TYPE = 5;
    public final static int REMAINDER_TYPE = 6;
    public static final String PAYPAL_CONFIG_CLIENT_ID = "ATL8rgvwdH9A3ZFtalirTjbIapWitxzK4qEr6noMBl_9_IxRSXEvheSKJcJbeq2jBZxdK_tXAoFh9_r4";
    public static final String TEST_SECRET_KEY = "sk_test_QtG2Rz08YImNlOW1JAGQtg0P";
    public static final String TEST_PUBLISHABLE_KEY = "pk_test_yNN1EqPYuL1WCxT29qEykFv2";
    public int lastAction = 78;
}
