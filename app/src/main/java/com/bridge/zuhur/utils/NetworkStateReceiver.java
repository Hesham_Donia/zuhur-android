package com.bridge.zuhur.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.NotificationsController;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.interfaces.ObserverListener;
import com.bridge.zuhur.interfaces.SubjectListener;

public class NetworkStateReceiver extends BroadcastReceiver implements SubjectListener {

    Singleton singleton;


    public NetworkStateReceiver() {
        singleton = Singleton.getInstance();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "connected");
            synchronizeOfflineData(context);
            synchronizeNotifications(context);
            notifyObservers(true);
        } else {
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "not connected");
            notifyObservers(false);
        }
    }

    private void synchronizeOfflineData(Context context) {
        Log.d("synch", "inside synchronizeOfflineData()");
        ExtrasController extrasController = new ExtrasController(context);
        Bundle bundle = new Bundle();
        bundle.putInt("action", ExtrasController.GET_ALL_LOG_DATA_FROM_DATABASE_ACTION);
        extrasController.request(ExtrasController.GET_ALL_LOG_DATA_FROM_DATABASE_ACTION, bundle);
    }

    private void synchronizeNotifications(Context context) {
        NotificationsController notificationsController = new NotificationsController(context);
        Bundle bundle = new Bundle();
        bundle.putInt("action", NotificationsController.NOTIFICATIONS_ACTION);
        notificationsController.request(NotificationsController.NOTIFICATIONS_ACTION, bundle);
    }

    @Override
    public void register(ObserverListener observer) {
        singleton.observers.add(observer);
    }

    @Override
    public void unregister(ObserverListener observer) {
        int index = singleton.observers.indexOf(observer);
        singleton.observers.remove(index);
    }

    @Override
    public void notifyObservers(boolean internetConnectionExist) {
        for (ObserverListener observer : singleton.observers) {
            observer.update(internetConnectionExist);
        }
    }
}
