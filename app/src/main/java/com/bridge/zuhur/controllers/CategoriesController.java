package com.bridge.zuhur.controllers;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.NetworkError;
import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.display.CategoriesDisplay;
import com.bridge.zuhur.entities.Category;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.services.CategoriesService;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.NetworkConnection;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public class CategoriesController extends AbstractController {


    public final static int GET_CATEGORIES_ACTION = 1;
    public final static String CATEGORIES_REQUEST_TAG = "CATEGORIES_REQUEST_TAG";
    private CategoriesDisplay mCategoriesDisplay;


    public CategoriesController(Context context) {
        super(context);
        mCategoriesDisplay = new CategoriesDisplay();
    }

    private void handleRequest() {
        if (NetworkConnection.networkConnectivity(mContext)) {
            switch (this.getAction()) {
                case GET_CATEGORIES_ACTION:
                    getCategories();
                    break;

                default:
                    break;
            }
        } else {
            onErrorCalled(new NetworkError());
        }
    }

    @Override
    void requestHandler(int action, Bundle args) {
        this.setAction(action);
        this.mRequest = args;
        handleRequest();
    }

    private void getCategories() {

        CategoriesService categoriesService = ZuhurApplication.objectGraph.get(CategoriesService.class);
        categoriesService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ArrayList<Category> categories = (ArrayList<Category>) result;
                mCategoriesDisplay.display(GET_CATEGORIES_ACTION, categories, mContext, mRequest);
            }

            @Override
            public void onError(String e) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "onError");
            }
        });
    }

}
