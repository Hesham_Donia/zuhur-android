package com.bridge.zuhur.controllers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import com.bridge.zuhur.R;
import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.database.WishList;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Event;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.NotificationEntity;
import com.bridge.zuhur.entities.Offer;
import com.bridge.zuhur.entities.Order;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Remainder;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.services.NotificationsService;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.utils.NetworkConnection;
import com.bridge.zuhur.views.activities.EventsActivity;
import com.bridge.zuhur.views.activities.HomeActivity;
import com.bridge.zuhur.views.activities.OfferDetailsActivity;
import com.bridge.zuhur.views.activities.ProductLandingPageActivity;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public class NotificationsController extends AbstractController {


    public final static int NOTIFICATIONS_ACTION = 46;
    public final static String NOTIFICATIONS_REQUEST_TAG = "NOTIFICATIONS_REQUEST_TAG";
    SharedPreferences settings;
    int notificationId = 0;

    public NotificationsController(Context context) {
        super(context);
        settings = context.getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }


    @Override
    void requestHandler(int action, Bundle args) {
        this.setAction(action);
        this.mRequest = args;
        handleRequest();
    }

    private void handleRequest() {
        if (NetworkConnection.networkConnectivity(mContext)) {
            switch (getAction()) {
                case NOTIFICATIONS_ACTION:
                    getNotifications();
                    break;
            }
        }
    }

    private void getNotifications() {
        NotificationsService notificationsService = ZuhurApplication.objectGraph.get(NotificationsService.class);
        Filter filter = new Filter();
        filter.id = settings.getInt("id", 0);
        filter.countryId = settings.getInt("countryId", 0);
        ;
        notificationsService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ArrayList<NotificationEntity> notificationEntities = (ArrayList<NotificationEntity>) result;
                String title, message;
                Intent intent;
                PendingIntent pendingIntent;
                for (NotificationEntity notificationEntity : notificationEntities) {
                    switch (notificationEntity.type) {

                        case 1://favourite
                            Favourite favourite = (Favourite) notificationEntity.entity;
                            if (Language.getCurrentLang(mContext).equals("en")) {
                                title = favourite.getProduct().englishName;
                                message = notificationEntity.englishMessage;
                            } else {
                                title = favourite.getProduct().arabicName;
                                message = notificationEntity.arabicMessage;
                            }
                            intent = new Intent(mContext, ProductLandingPageActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("product", favourite.getProduct());
                            intent.putExtras(bundle);
                            pendingIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
                            createNotification(title, message, pendingIntent);
                            break;

                        case 2://wish list
                            WishList wishList = (WishList) notificationEntity.entity;
                            if (Language.getCurrentLang(mContext).equals("en")) {
                                title = wishList.getProduct().englishName;
                                message = notificationEntity.englishMessage;
                            } else {
                                title = wishList.getProduct().englishName;
                                message = notificationEntity.arabicMessage;
                            }
                            intent = new Intent(mContext, ProductLandingPageActivity.class);
                            Bundle wishListBundle = new Bundle();
                            wishListBundle.putSerializable("product", wishList.getProduct());
                            intent.putExtras(wishListBundle);
                            pendingIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
                            createNotification(title, message, pendingIntent);
                            break;

                        case 3://remainder
                            Remainder remainder = (Remainder) notificationEntity.entity;
                            if (Language.getCurrentLang(mContext).equals("en")) {
                                title = remainder.details;
                                message = notificationEntity.englishMessage;
                            } else {
                                title = remainder.details;
                                message = notificationEntity.arabicMessage;
                            }
                            intent = new Intent(mContext, HomeActivity.class);
                            pendingIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
                            createNotification(title, message, pendingIntent);
                            break;

                        case 4:// order confirm
                            Order order = (Order) notificationEntity.entity;
                            if (Language.getCurrentLang(mContext).equals("en")) {
                                title = "Order #" + order.id;
                                message = notificationEntity.englishMessage;
                            } else {
                                title = "Order #" + order.id;
                                message = notificationEntity.arabicMessage;
                            }
                            intent = new Intent(mContext, HomeActivity.class);
                            pendingIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
                            createNotification(title, message, pendingIntent);
                            break;

                        case 5:// order completed
                            Order order1 = (Order) notificationEntity.entity;
                            if (Language.getCurrentLang(mContext).equals("en")) {
                                title = "Order #" + order1.id;
                                message = notificationEntity.englishMessage;
                            } else {
                                title = "Order #" + order1.id;
                                message = notificationEntity.arabicMessage;
                            }
                            intent = new Intent(mContext, HomeActivity.class);
                            pendingIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
                            createNotification(title, message, pendingIntent);
                            break;

                        case 7:// event
                            Event event = (Event) notificationEntity.entity;
                            if (Language.getCurrentLang(mContext).equals("en")) {
                                title = event.englishName;
                                message = notificationEntity.englishMessage;
                            } else {
                                title = event.arabicName;
                                message = notificationEntity.arabicMessage;
                            }
                            intent = new Intent(mContext, EventsActivity.class);
                            pendingIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
                            createNotification(title, message, pendingIntent);
                            break;

                        case 8: // offer
                            Offer offer = (Offer) notificationEntity.entity;
                            if (Language.getCurrentLang(mContext).equals("en")) {
                                title = "New offer";
                                message = notificationEntity.englishMessage;
                            } else {
                                title = "عروض جديدة";
                                message = notificationEntity.arabicMessage;
                            }
                            intent = new Intent(mContext, OfferDetailsActivity.class);
                            Bundle offerBundle = new Bundle();
                            offerBundle.putSerializable("offer", offer);
                            intent.putExtras(offerBundle);
                            pendingIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
                            createNotification(title, message, pendingIntent);
                            break;

                        case 9:// new arrival
                            Product product = (Product) notificationEntity.entity;
                            title = mContext.getResources().getString(R.string.newArrival);
                            if (Language.getCurrentLang(mContext).equals("en")) {
                                message = notificationEntity.englishMessage;
                            } else {
                                message = notificationEntity.arabicMessage;
                            }
                            intent = new Intent(mContext, ProductLandingPageActivity.class);
                            Bundle newArrivalBundle = new Bundle();
                            newArrivalBundle.putSerializable("product", product);
                            intent.putExtras(newArrivalBundle);
                            pendingIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
                            createNotification(title, message, pendingIntent);
                            break;

                        default:
                            break;
                    }
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }


    private void createNotification(String title, String message, PendingIntent pendingIntent) {
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            Notification mNotification = new Notification.Builder(mContext)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setSound(soundUri)
                    .addAction(R.drawable.messi, "View", pendingIntent)
                    .build();
            NotificationManager notificationManager =
                    (NotificationManager) mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
            notificationManager.notify(++notificationId, mNotification);
        }

    }
}
