package com.bridge.zuhur.controllers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.bridge.zuhur.R;
import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.database.LogData;
import com.bridge.zuhur.database.WishList;
import com.bridge.zuhur.display.ExtrasDisplay;
import com.bridge.zuhur.display.OrderDisplay;
import com.bridge.zuhur.display.RemaindersServiceDisplay;
import com.bridge.zuhur.entities.Color;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.ExperienceEvent;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Remainder;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.entities.Tag;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.services.AddRecentViewedService;
import com.bridge.zuhur.services.BestSellerService;
import com.bridge.zuhur.services.ColorsService;
import com.bridge.zuhur.services.CountriesService;
import com.bridge.zuhur.services.EventsService;
import com.bridge.zuhur.services.ExperienceEventsService;
import com.bridge.zuhur.services.InternationalShippingRequestService;
import com.bridge.zuhur.services.LogDataDatabaseService;
import com.bridge.zuhur.services.NewArrivalsService;
import com.bridge.zuhur.services.OffersService;
import com.bridge.zuhur.services.OurExperienceProductsService;
import com.bridge.zuhur.services.OurExperienceService;
import com.bridge.zuhur.services.PaymentMethodService;
import com.bridge.zuhur.services.RecentViewedService;
import com.bridge.zuhur.services.RemaindersDatabaseService;
import com.bridge.zuhur.services.RemaindersService;
import com.bridge.zuhur.services.TagsDatabaseService;
import com.bridge.zuhur.services.TagsService;
import com.bridge.zuhur.utils.Call;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.NetworkConnection;
import com.bridge.zuhur.views.activities.AbstractActivity;
import com.bridge.zuhur.views.activities.EditRemainderActivity;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public class ExtrasController extends AbstractController {

    //controller for getting best sellers and new arrivals and our experience and our experience products
    // and offers
    // and recent viewed and add to recent viewed

    public final static int BEST_SELLER_ACTION = 9;
    public final static String BEST_SELLER_REQUEST_TAG = "BEST_SELLER_REQUEST_TAG";
    public final static int NEW_ARRIVALS_ACTION = 10;
    public final static String NEW_ARRIVALS_REQUEST_TAG = "NEW_ARRIVALS_REQUEST_TAG";
    public final static int OUR_EXPERIENCE_ACTION = 11;
    public final static String OUR_EXPERIENCE_REQUEST_TAG = "OUR_EXPERIENCE_REQUEST_TAG";
    public final static int OUR_EXPERIENCE_PRODUCTS_ACTION = 12;
    public final static String OUR_EXPERIENCE_PRODUCTS_REQUEST_TAG = "OUR_EXPERIENCE_PRODUCTS_REQUEST_TAG";
    public final static int OFFERS_ACTION = 13;
    public final static String OFFERS_REQUEST_TAG = "OFFERS_REQUEST_TAG";
    public final static int OFFER_DETAILS_ACTION = 45;
    public final static String OFFER_DETAILS_REQUEST_TAG = "OFFER_DETAILS_REQUEST_TAG";
    public final static int RECENT_VIEWED_ACTION = 14;
    public final static String RECENT_VIEWED_REQUEST_TAG = "RECENT_VIEWED_REQUEST_TAG";
    public final static int ADD_RECENT_VIEWED_ACTION = 15;
    public final static String ADD_RECENT_VIEWED_REQUEST_TAG = "ADD_RECENT_VIEWED_REQUEST_TAG";
    public final static int EXPERIENCE_EVENTS_ACTION = 30;
    public final static String EXPERIENCE_EVENTS_REQUEST_TAG = "EXPERIENCE_EVENTS_REQUEST_TAG";
    public final static int PAYMENT_METHODS_ACTION = 32;
    public final static String PAYMENT_METHODS_REQUEST_TAG = "PAYMENT_METHODS_REQUEST_TAG";
    public final static int REMAINDERS_ACTION = 39;
    public final static String REMAINDERS_REQUEST_TAG = "REMAINDERS_REQUEST_TAG";
    public final static int ADD_REMAINDERS_ACTION = 40;
    public final static String ADD_REMAINDERS_REQUEST_TAG = "ADD_REMAINDERS_REQUEST_TAG";
    public final static int EDIT_REMAINDERS_ACTION = 41;
    public final static String EDIT_REMAINDERS_REQUEST_TAG = "EDIT_REMAINDERS_REQUEST_TAG";
    public final static int DELETE_REMAINDERS_ACTION = 42;
    public final static String DELETE_REMAINDERS_REQUEST_TAG = "DELETE_REMAINDERS_REQUEST_TAG";
    public final static int EVENT_DETAILS_ACTION = 43;
    public final static String EVENT_DETAILS_REQUEST_TAG = "EVENT_DETAILS_REQUEST_TAG";
    public final static int EVENTS_ACTION = 44;
    public final static String EVENTS_REQUEST_TAG = "EVENTS_REQUEST_TAG";
    public final static int MAKE_INTERNATIONAL_SHIPPING_ACTION = 47;
    public final static String MAKE_INTERNATIONAL_SHIPPING_REQUEST_TAG = "MAKE_INTERNATIONAL_SHIPPING_REQUEST_TAG";
    public final static int SEND_EMAIL_ACTION = 49;
    public final static int MAKE_CALL_ACTION = 50;
    public final static int OPEN_URL_ACTION = 51;
    public final static int TAGS_ACTION = 56;
    public final static String TAGS_REQUEST_TAG = "TAGS_REQUEST_TAG";
    public final static int GET_TAGS_FROM_DATABASE_ACTION = 57;
    public final static int COUNTRIES_ACTION = 60;
    public final static String COUNTRIES_REQUEST_TAG = "COUNTRIES_REQUEST_TAG";
    public final static int GET_ALL_REMAINDERS_DATABASE_ACTION = 61;
    public final static int EDIT_REMAINDER_DATABASE_ACTION = 62;
    public final static int DELETE_REMAINDER_DATABASE_ACTION = 63;
    public final static int DELETE_ALL_REMAINDERS_DATABASE_ACTION = 64;
    public final static int ADD_REMAINDER_DATABASE_ACTION = 65;
    public final static int ADD_LOG_DATA_TO_DATABASE_ACTION = 68;
    public final static int GET_ALL_LOG_DATA_FROM_DATABASE_ACTION = 69;
    public final static int DELETE_ALL_LOG_DATA_DATABASE_ACTION = 70;
    public final static int COLORS_ACTION = 74;
    public final static String COLORS_REQUEST_TAG = "COLORS_REQUEST_TAG";
    public final static int GET_ALL_TAGS_FROM_DATABASE = 75;


    SharedPreferences settings;
    ArrayList<com.bridge.zuhur.database.Remainder> remainders;

    public ExtrasController(Context context) {
        super(context);
        settings = mContext.getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }

    @Override
    void requestHandler(int action, Bundle args) {
        this.setAction(action);
        this.mRequest = args;
        handleRequest();
    }

    private void handleRequest() {
        switch (this.getAction()) {
            case BEST_SELLER_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getBestSeller();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case NEW_ARRIVALS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getNewArrivals();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case OUR_EXPERIENCE_ACTION:
                getOurExperience();
                break;

            case OUR_EXPERIENCE_PRODUCTS_ACTION:
                getOurExperienceProducts();
                break;

            case OFFERS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getOffers();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case OFFER_DETAILS_ACTION:
                getOfferDetailsFromServer();
                break;

            case RECENT_VIEWED_ACTION:
                getRecentViewed();
                break;

            case ADD_RECENT_VIEWED_ACTION:
                addToRecentViewed();
                break;

            case EXPERIENCE_EVENTS_ACTION:
                getExperienceEvents();
                break;

            case PAYMENT_METHODS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getPaymentMethods();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case REMAINDERS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getRemaindersFromServer();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case ADD_REMAINDERS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    addRemainderToServer();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case EDIT_REMAINDERS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    editRemainderAtServer();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case DELETE_REMAINDERS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    deleteRemainderFromServer();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case COLORS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getColors();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case EVENT_DETAILS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getEventDetails();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case EVENTS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getEventsFromServer();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case MAKE_INTERNATIONAL_SHIPPING_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    makeInternationalShippingRequest();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case SEND_EMAIL_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    senEmail();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case MAKE_CALL_ACTION:

                makeCall();
                break;

            case OPEN_URL_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    openUrl();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case TAGS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getTags();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case GET_TAGS_FROM_DATABASE_ACTION:
                getTagsFromDatabase();
                break;

            case GET_ALL_TAGS_FROM_DATABASE:
                getAllTagsFromDatabase();
                break;

            case COUNTRIES_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getCountries();
                } else {
                    onErrorCalled(new NetworkError());
                }

                break;

            case GET_ALL_REMAINDERS_DATABASE_ACTION:
                getAllRemaindersFromDatabase();
                break;

            case EDIT_REMAINDER_DATABASE_ACTION:
                editRemainderAtDatabase();
                break;

            case DELETE_REMAINDER_DATABASE_ACTION:
                deleteRemainderFromDatabase();
                break;

            case DELETE_ALL_REMAINDERS_DATABASE_ACTION:
                deleteAllRemaindersFromDatabase();
                break;

            case ADD_REMAINDER_DATABASE_ACTION:
                addRemainderDatabase();
                break;

            case ADD_LOG_DATA_TO_DATABASE_ACTION:
                addLogDataToDatabase();
                break;

            case GET_ALL_LOG_DATA_FROM_DATABASE_ACTION:
                getLogDataFromDatabase();
                break;

            case DELETE_ALL_LOG_DATA_DATABASE_ACTION:
                deleteAllLogDataFromDatabase();
                break;


            default:
                break;
        }
    }


    private void getColors() {
        ColorsService colorsService = ZuhurApplication.objectGraph.get(ColorsService.class);
        colorsService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                Singleton.getInstance().colors = (ArrayList<Color>) result;
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "error in getting colors: " + error);
            }
        });
    }

    private void addLogDataToDatabase() {
        Log.d("synch", "inside addLogDataToDatabase()");
        LogData logData = (LogData) mRequest.getSerializable("logData");
        LogDataDatabaseService logDataDatabaseService = ZuhurApplication.objectGraph.get(LogDataDatabaseService.class);
        logDataDatabaseService.insertItem(logData, new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "Log data added to database");
                }
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "error in add log data: " + error);
            }
        });
    }

    private void getLogDataFromDatabase() {
        Log.d("synch", "inside getLogDataFromDatabase()");
        LogDataDatabaseService logDataDatabaseService = ZuhurApplication.objectGraph.get(LogDataDatabaseService.class);
        logDataDatabaseService.findItems(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                Singleton.getInstance().logDataArrayList.addAll((ArrayList<LogData>) result);
                for (LogData logData : Singleton.getInstance().logDataArrayList) {
                    switch (logData.getItemType()) {
                        case CommonConstants.FAVOURITE_TYPE:
                            Favourite favourite = (Favourite) logData.getItem();

                            switch (logData.getOperation()) {
                                case CommonConstants.ADD_OPERATION:
                                    FavouritesController favouritesController1 = new FavouritesController(mContext);
                                    Bundle bundle2 = new Bundle();
                                    bundle2.putInt("action", FavouritesController.ADD_FAVOURITE_TO_SERVER_ACTION);
                                    bundle2.putSerializable("favourite", favourite);
                                    favouritesController1.request(FavouritesController.ADD_FAVOURITE_TO_SERVER_ACTION, bundle2);
                                    break;

                                case CommonConstants.DELETE_OPERATION:
                                    FavouritesController favouritesController = new FavouritesController(mContext);
                                    Bundle bundle1 = new Bundle();
                                    bundle1.putInt("action", FavouritesController.DELETE_FAVOURITE_SERVER_ACTION);
                                    bundle1.putSerializable("productId", favourite.getProductId());
                                    favouritesController.request(FavouritesController.DELETE_FAVOURITE_SERVER_ACTION, bundle1);
                                    break;

                                default:
                                    break;
                            }
                            break;


                        case CommonConstants.WISH_LIST_TYPE:
                            WishList wishList = (WishList) logData.getItem();
                            switch (logData.getOperation()) {
                                case CommonConstants.ADD_OPERATION:
                                    WishListController wishListController1 = new WishListController(mContext);
                                    Bundle bundle1 = new Bundle();
                                    bundle1.putInt("action", WishListController.ADD_WISH_LIST_TO_SERVER_ACTION);
                                    bundle1.putSerializable("wishList", wishList);
                                    wishListController1.request(WishListController.ADD_WISH_LIST_TO_SERVER_ACTION, bundle1);
                                    break;

                                case CommonConstants.DELETE_OPERATION:
                                    WishListController wishListController = new WishListController(mContext);
                                    Bundle bundle = new Bundle();
                                    bundle.putInt("action", WishListController.DELETE_WISH_LIST_FROM_SERVER_ACTION);
                                    bundle.putSerializable("productId", wishList.getProductId());
                                    wishListController.request(WishListController.DELETE_WISH_LIST_FROM_SERVER_ACTION, bundle);
                                    break;

                                default:
                                    break;
                            }
                            break;

                        case CommonConstants.REMAINDER_TYPE:
                            Remainder remainder = (Remainder) logData.getItem();
                            switch (logData.getOperation()) {
                                case CommonConstants.ADD_OPERATION:
                                    ExtrasController extrasController1 = new ExtrasController(mContext);
                                    Bundle bundle1 = new Bundle();
                                    bundle1.putInt("action", ExtrasController.ADD_REMAINDERS_ACTION);
                                    bundle1.putSerializable("remainder", remainder);
                                    extrasController1.request(ExtrasController.ADD_REMAINDERS_ACTION, bundle1);
                                    break;

                                case CommonConstants.EDIT_OPERATION:
                                    ExtrasController extrasController = new ExtrasController(mContext);
                                    Bundle bundle = new Bundle();
                                    bundle.putInt("action", ExtrasController.EDIT_REMAINDERS_ACTION);
                                    bundle.putSerializable("remainder", remainder);
                                    extrasController.request(ExtrasController.EDIT_REMAINDERS_ACTION, bundle);
                                    break;

                                case CommonConstants.DELETE_OPERATION:
                                    ExtrasController extrasController2 = new ExtrasController(mContext);
                                    Bundle bundle2 = new Bundle();
                                    bundle2.putInt("action", ExtrasController.DELETE_REMAINDERS_ACTION);
                                    bundle2.putSerializable("id", remainder.id);
                                    extrasController2.request(ExtrasController.DELETE_REMAINDERS_ACTION, bundle2);
                                    break;

                                default:
                                    break;
                            }
                            break;

                        default:
                            break;
                    }
                }

                deleteAllLogDataFromDatabase();
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteAllLogDataFromDatabase() {
        LogDataDatabaseService logDataDatabaseService = ZuhurApplication.objectGraph.get(LogDataDatabaseService.class);
        logDataDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                Singleton.getInstance().logDataArrayList.clear();
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void addRemainderDatabase() {
        com.bridge.zuhur.database.Remainder remainder = (com.bridge.zuhur.database.Remainder) mRequest.getSerializable("remainder");
        RemaindersDatabaseService remaindersDatabaseService = ZuhurApplication.objectGraph.get(RemaindersDatabaseService.class);
        remaindersDatabaseService.insertItem(remainder, new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.addRemSucc), Toast.LENGTH_LONG).show();
                    ((AbstractActivity) mContext).finish();
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteAllRemaindersFromDatabase() {
        RemaindersDatabaseService remaindersDatabaseService = ZuhurApplication.objectGraph.get(RemaindersDatabaseService.class);
        remaindersDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteRemainderFromDatabase() {
        int id = mRequest.getInt("id");
        RemaindersDatabaseService remaindersDatabaseService = ZuhurApplication.objectGraph.get(RemaindersDatabaseService.class);
        remaindersDatabaseService.deleteItem(id, new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void editRemainderAtDatabase() {

        com.bridge.zuhur.database.Remainder remainder = (com.bridge.zuhur.database.Remainder) mRequest.getSerializable("remainder");
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "remainder id: " + remainder.getRemainderId());
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "id: " + remainder.getId());
        RemaindersDatabaseService remaindersDatabaseService = ZuhurApplication.objectGraph.get(RemaindersDatabaseService.class);
        remaindersDatabaseService.updateItem(remainder, new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    if (mContext instanceof EditRemainderActivity) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.editRemSucc), Toast.LENGTH_LONG).show();
                        ((EditRemainderActivity) mContext).finish();
                    } else {
                        Log.d(CommonConstants.APPLICATION_LOG_TAG, "updated successfully");
                    }
                }

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getAllRemaindersFromDatabase() {
        RemaindersDatabaseService remaindersDatabaseService = ZuhurApplication.objectGraph.get(RemaindersDatabaseService.class);
        remaindersDatabaseService.findItems(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                if (mRequest.containsKey("fromService")) {
                    RemaindersServiceDisplay remaindersServiceDisplay = new RemaindersServiceDisplay();
                    remaindersServiceDisplay.display(mAction, result, mContext, null);
                } else {
                    ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                    extrasDisplay.display(mAction, result, mContext, null);
                }
            }

            @Override
            public void onError(String error) {
                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                extrasDisplay.display(mAction, null, mContext, null);
            }
        });
    }

    private void addRemaindersToDatabase(final ArrayList<? extends Entity> values) {
        final RemaindersDatabaseService remaindersDatabaseService = ZuhurApplication.objectGraph.get(RemaindersDatabaseService.class);
        remaindersDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    remaindersDatabaseService.insertItems(values, new MainListener<Boolean>() {
                        @Override
                        public void onSuccess(Boolean result) {

                        }

                        @Override
                        public void onError(String error) {

                        }
                    });
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void openUrl() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
        mContext.startActivity(browserIntent);
    }

    private void makeCall() {
        Call.makeCall(mContext, "01119993362");
    }

    private void senEmail() {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        emailIntent.setType("plain/text");

        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                new String[]{"abc@gmail.com"});

        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                "Email Subject");

        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                "Email Body");

        mContext.startActivity(Intent.createChooser(
                emailIntent, "Send mail..."));
    }

    private void makeInternationalShippingRequest() {
        InternationalShippingRequestService service = ZuhurApplication.objectGraph.get(InternationalShippingRequestService.class);
        Filter filter = new Filter();
        filter.orderId = mRequest.getString("id");
        filter.countryName = mRequest.getString("country");
        filter.id = Singleton.getInstance().currentUser.id;

        service.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getOfferDetailsFromServer() {
        OffersService offersService = ZuhurApplication.objectGraph.get(OffersService.class);
        offersService.find(String.valueOf(mRequest.getInt("id")), new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getEventDetails() {
        EventsService eventsService = ZuhurApplication.objectGraph.get(EventsService.class);
        eventsService.find(String.valueOf(mRequest.getInt("id")), new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getEventsFromServer() {
        EventsService eventsService = ZuhurApplication.objectGraph.get(EventsService.class);
        eventsService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                extrasDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                extrasDisplay.display(mAction, null, mContext, null);
            }
        });
    }

    private void getRemaindersFromServer() {
        RemaindersService remaindersService = ZuhurApplication.objectGraph.get(RemaindersService.class);
        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;
        remaindersService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                addRemaindersToDatabase(result);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void addRemainderToServer() {
        RemaindersService remaindersService = ZuhurApplication.objectGraph.get(RemaindersService.class);
        final Remainder remainder = (Remainder) mRequest.getSerializable("remainder");
        remainder.isUpdate = false;
        remaindersService.save(remainder, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                SignUpResponse signUpResponse = (SignUpResponse) result.get(0);
                remainder.id = signUpResponse.id;
                getAllRemaindersFromDatabase2(remainder);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getAllRemaindersFromDatabase2(final Remainder remainder) {
        RemaindersDatabaseService remaindersDatabaseService = ZuhurApplication.objectGraph.get(RemaindersDatabaseService.class);
        remaindersDatabaseService.findItems(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                remainders = (ArrayList<com.bridge.zuhur.database.Remainder>) result;
                remainders.get(remainders.size() - 1).setRemainder(remainder);
                remainders.get(remainders.size() - 1).setRemainderId(remainder.id);
                mRequest.remove("remainder");
                mRequest.putSerializable("remainder", remainders.get(remainders.size() - 1));
                editRemainderAtDatabase();
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void editRemainderAtServer() {
        RemaindersService remaindersService = ZuhurApplication.objectGraph.get(RemaindersService.class);
        Remainder remainder = (Remainder) mRequest.getSerializable("remainder");
        remainder.isUpdate = true;
        remaindersService.save(remainder, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteRemainderFromServer() {
        RemaindersService remaindersService = ZuhurApplication.objectGraph.get(RemaindersService.class);
        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;
        filter.remainderId = mRequest.getInt("id");
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "deleted id: " + filter.remainderId + "");
        remaindersService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getPaymentMethods() {
        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;
        filter.totalPrice = mRequest.getDouble("total");

        PaymentMethodService paymentMethodService = ZuhurApplication.objectGraph.get(PaymentMethodService.class);
        paymentMethodService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                OrderDisplay orderDisplay = new OrderDisplay();
                orderDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getExperienceEvents() {
        ExperienceEventsService experienceEventsService = ZuhurApplication.objectGraph.get(ExperienceEventsService.class);
        experienceEventsService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                Singleton.getInstance().experienceEvents = (ArrayList<ExperienceEvent>) result;
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "error in getting experience events: " + error);
            }
        });
    }

    private void addToRecentViewed() {

        if (settings.getBoolean("isLogged", false) || settings.getBoolean("isFacebookLogged", false)) {
            AddRecentViewedService addRecentViewedService = ZuhurApplication.objectGraph.get(AddRecentViewedService.class);
            addRecentViewedService.save((Product) mRequest.getSerializable("product"), new MainListener<ArrayList<? extends Entity>>() {
                @Override
                public void onSuccess(ArrayList<? extends Entity> result) {

                }

                @Override
                public void onError(String error) {

                }
            });
        }
    }

    private void getRecentViewed() {

        if (settings.getBoolean("isLogged", false) || settings.getBoolean("isFacebookLogged", false)) {
            RecentViewedService recentViewedService =
                    ZuhurApplication.objectGraph.get(RecentViewedService.class);

            Filter filter = new Filter();
            filter.id = Singleton.getInstance().currentUser.id;
            filter.countryId = mRequest.getInt("countryId");
            recentViewedService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
                @Override
                public void onSuccess(ArrayList<? extends Entity> result) {
                    ArrayList<Product> recentViewedProducts = (ArrayList<Product>) result;
                    for (int i = 0; i < recentViewedProducts.size(); i++) {
                        Log.d(CommonConstants.APPLICATION_LOG_TAG, recentViewedProducts.get(i).arabicName);
                    }
                }

                @Override
                public void onError(String error) {

                }
            });
        }
    }

    private void getOffers() {
        Filter filter = new Filter();
        filter.countryId = mRequest.getInt("countryId");

        OffersService offersService = ZuhurApplication.objectGraph.get(OffersService.class);
        offersService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                extrasDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                if (error.equals("No offers Available")) {
                    ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                    ArrayList<? extends Entity> result = new ArrayList<Entity>();
                    extrasDisplay.display(mAction, result, mContext, null);
                }
            }
        });
    }

    private void getOurExperienceProducts() {
        OurExperienceProductsService ourExperienceProductsService =
                ZuhurApplication.objectGraph.get(OurExperienceProductsService.class);

        Filter filter = new Filter();
        filter.productId = mRequest.getInt("id");
        ourExperienceProductsService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getOurExperience() {
        OurExperienceService ourExperienceService = ZuhurApplication.objectGraph.get(OurExperienceService.class);
        ourExperienceService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getNewArrivals() {

        Filter filter = new Filter();
        filter.countryId = mRequest.getInt("countryId");

        NewArrivalsService newArrivalsService = ZuhurApplication.objectGraph.get(NewArrivalsService.class);
        newArrivalsService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                extrasDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "new arrivals error: " + error);
                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                ArrayList<? extends Entity> result = new ArrayList<Entity>();
                extrasDisplay.display(mAction, result, mContext, null);
            }
        });
    }

    private void getBestSeller() {
        Filter filter = new Filter();
        filter.countryId = Singleton.getInstance().country.id;

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "Country ID: " + filter.countryId);

        BestSellerService bestSellerService = ZuhurApplication.objectGraph.get(BestSellerService.class);
        bestSellerService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                extrasDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "Best sellers error: " + error);
                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                ArrayList<? extends Entity> result = new ArrayList<Entity>();
                extrasDisplay.display(mAction, result, mContext, null);
            }
        });
    }

    private void getTags() {
        TagsService tagsService = ZuhurApplication.objectGraph.get(TagsService.class);
        tagsService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ArrayList<Tag> tags = (ArrayList<Tag>) result;
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "From Server");
                for (Tag tag : tags) {
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, tag.name);
                }
                addTagsToDatabase(tags);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void addTagsToDatabase(final ArrayList<Tag> tags) {

        final TagsDatabaseService tagsDatabaseService = ZuhurApplication.objectGraph.get(TagsDatabaseService.class);
        tagsDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    tagsDatabaseService.insertItems(tags, new MainListener<Boolean>() {
                        @Override
                        public void onSuccess(Boolean result) {
                            if (result) {
                                Log.d(CommonConstants.APPLICATION_LOG_TAG, "tags added to database successfully");
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    });
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getTagsFromDatabase() {
        Filter filter = new Filter();
        filter.searchQuery = mRequest.getString("query");
        TagsDatabaseService tagsDatabaseService = ZuhurApplication.objectGraph.get(TagsDatabaseService.class);
        tagsDatabaseService.findItems(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                extrasDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getAllTagsFromDatabase() {
        TagsDatabaseService tagsDatabaseService = ZuhurApplication.objectGraph.get(TagsDatabaseService.class);
        tagsDatabaseService.findItems(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                extrasDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getCountries() {
        CountriesService countriesService = ZuhurApplication.objectGraph.get(CountriesService.class);
        countriesService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ExtrasDisplay extrasDisplay = new ExtrasDisplay();
                extrasDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }


}
