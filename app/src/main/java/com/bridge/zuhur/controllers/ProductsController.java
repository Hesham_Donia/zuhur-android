package com.bridge.zuhur.controllers;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.NetworkError;
import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.database.AutoComplete;
import com.bridge.zuhur.display.ProductsDisplay;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.services.AllProductsService;
import com.bridge.zuhur.services.AutoCompleteDatabaseService;
import com.bridge.zuhur.services.AutoCompleteService;
import com.bridge.zuhur.services.CategoryProductsService;
import com.bridge.zuhur.services.ProductDetailsService;
import com.bridge.zuhur.services.RelatedProductsService;
import com.bridge.zuhur.services.SearchByNameService;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.NetworkConnection;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public class ProductsController extends AbstractController {

    //controller for getting all products and search by categoryId and search by name

    public static final String GET_CATEGORY_PRODUCTS_REQUEST_TAG = "CATEGORY_PRODUCTS_REQUEST_TAG";
    public static final int GET_CATEGORY_PRODUCTS_ACTION = 2;
    public static final String GET_ALL_PRODUCTS_REQUEST_TAG = "ALL_PRODUCTS_REQUEST_TAG";
    public static final int GET_ALL_PRODUCTS_ACTION = 3;
    public static final String GET_PRODUCT_REQUEST_TAG = "PRODUCT_REQUEST_TAG";
    public static final int GET_PRODUCT_ACTION = 4;
    public static final String SEARCH_BY_NAME_REQUEST_TAG = "SEARCH_BY_NAME_REQUEST_TAG";
    public static final int SEARCH_BY_NAME_ACTION = 28;
    public static final String RATE_PRODUCT_REQUEST_TAG = "RATE_PRODUCT_REQUEST_TAG";
    public static final int RATE_PRODUCT_ACTION = 48;
    public static final String RELATED_PRODUCTS_REQUEST_TAG = "RELATED_PRODUCTS_REQUEST_TAG";
    public static final int RELATED_PRODUCTS_ACTION = 53;
    public static final int GET_AUTO_COMPLETE_SERVER = 66;
    public static final String GET_AUTO_COMPLETE_SERVER_TAG = "GET_AUTO_COMPLETE_SERVER_TAG";
    public static final int GET_AUTO_COMPLETE_DATABASE = 67;
    public static final String SIMILAR_PRODUCTS_REQUEST_TAG = "SIMILAR_PRODUCTS_REQUEST_TAG";
    public static final int SIMILAR_PRODUCTS_ACTION = 71;


    private ProductsDisplay mProductsDisplay;

    public ProductsController(Context context) {
        super(context);
        mProductsDisplay = new ProductsDisplay();
    }

    private void handleRequest() {
        if (NetworkConnection.networkConnectivity(mContext)) {
            switch (this.getAction()) {
                case GET_CATEGORY_PRODUCTS_ACTION:
                    getCategoryProducts();
                    break;

                case GET_ALL_PRODUCTS_ACTION:
                    getAllProducts();
                    break;

                case GET_PRODUCT_ACTION:
                    getProduct();
                    break;

                case SEARCH_BY_NAME_ACTION:
                    searchByName();
                    break;

                case RATE_PRODUCT_ACTION:
                    rateProduct();
                    break;

                case RELATED_PRODUCTS_ACTION:
                    getRelatedProducts();
                    break;

                case GET_AUTO_COMPLETE_SERVER:
                    getAutoCompleteFromServer();
                    break;

                case GET_AUTO_COMPLETE_DATABASE:
                    getAutoCompleteFromDatabase();
                    break;

                case SIMILAR_PRODUCTS_ACTION:
                    getSimilarProducts();
                    break;

                default:
                    break;
            }
        } else {
            onErrorCalled(new NetworkError());
        }
    }

    @Override
    void requestHandler(int action, Bundle args) {
        this.setAction(action);
        this.mRequest = args;
        handleRequest();
    }

    private void getCategoryProducts() {
        CategoryProductsService categoryProductsService = ZuhurApplication.objectGraph.get(CategoryProductsService.class);
        Filter filter = new Filter();
        filter.categoryId = mRequest.getInt("categoryId");
        filter.countryId = mRequest.getInt("countryId");

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "countryId: " + filter.countryId);

        if (mRequest.containsKey("sort")) {
            filter.sortType = mRequest.getString("sort");
        }

        if (mRequest.containsKey("tags")) {
            filter.tags = mRequest.getString("tags");
        }

        if (mRequest.containsKey("priceFrom")) {
            filter.priceFrom = mRequest.getInt("priceFrom");
        }

        if (mRequest.containsKey("priceTo")) {
            filter.priceTo = mRequest.getInt("priceTo");
        }


        categoryProductsService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                mProductsDisplay.display(GET_CATEGORY_PRODUCTS_ACTION, result, mContext, null);
            }

            @Override
            public void onError(String e) {
                ArrayList<Product> emptyProducts = new ArrayList<>();
                mProductsDisplay.display(GET_CATEGORY_PRODUCTS_ACTION, emptyProducts, mContext, null);
            }
        });
    }

    private void searchByName() {
        SearchByNameService searchByNameService = ZuhurApplication.objectGraph.get(SearchByNameService.class);
        Filter filter = new Filter();
        filter.searchQuery = mRequest.getString("keyWord");
        if (mRequest.containsKey("sort")) {
            filter.sortType = mRequest.getString("sort");
        } else {
            filter.sortType = CommonConstants.SORT_PRICE_HIGH_TO_LOW;
        }
        filter.countryId = mRequest.getInt("countryId");
        searchByNameService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                mProductsDisplay.display(SEARCH_BY_NAME_ACTION, result, mContext, null);
            }

            @Override
            public void onError(String e) {
                ArrayList<Product> emptyProducts = new ArrayList<>();
                mProductsDisplay.display(SEARCH_BY_NAME_ACTION, emptyProducts, mContext, null);
            }
        });
    }

    private void getAllProducts() {
        AllProductsService allProductsService = ZuhurApplication.objectGraph.get(AllProductsService.class);
        Filter filter = new Filter();
        if (mRequest.containsKey("sort")) {
            filter.sortType = mRequest.getString("sort");
        } else {
            filter.sortType = CommonConstants.SORT_PRICE_HIGH_TO_LOW;
        }

        if (mRequest.containsKey("tags")) {
            filter.tags = mRequest.getString("tags");
        }

        if (mRequest.containsKey("priceFrom")) {
            filter.priceFrom = mRequest.getInt("priceFrom");
        }

        if (mRequest.containsKey("priceTo")) {
            filter.priceTo = mRequest.getInt("priceTo");
        }

        filter.countryId = mRequest.getInt("countryId");
        allProductsService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                mProductsDisplay.display(GET_ALL_PRODUCTS_ACTION, result, mContext, null);
            }

            @Override
            public void onError(String e) {
                ArrayList<Product> emptyProducts = new ArrayList<>();
                mProductsDisplay.display(GET_ALL_PRODUCTS_ACTION, emptyProducts, mContext, null);
            }
        });
    }

    private void getProduct() {
        ProductDetailsService productDetailsService = ZuhurApplication.objectGraph.get(ProductDetailsService.class);
        Filter filter = new Filter();
        filter.productId = mRequest.getInt("productId");
        filter.countryId = mRequest.getInt("countryId");
        productDetailsService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                mProductsDisplay.display(GET_PRODUCT_ACTION, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "onError");
            }
        });
    }

    private void rateProduct() {
        ProductDetailsService productDetailsService = ZuhurApplication.objectGraph.get(ProductDetailsService.class);
        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;
        filter.productId = mRequest.getInt("id");
        filter.rate = mRequest.getDouble("rate");
        productDetailsService.save(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getRelatedProducts() {
        RelatedProductsService relatedProductsService = ZuhurApplication.objectGraph.get(RelatedProductsService.class);
        Filter filter = new Filter();
        filter.productsIds = mRequest.getString("id");
        filter.countryId = mRequest.getInt("countryId");
        relatedProductsService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ProductsDisplay productsDisplay = new ProductsDisplay();
                productsDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getSimilarProducts() {
        RelatedProductsService relatedProductsService = ZuhurApplication.objectGraph.get(RelatedProductsService.class);
        Filter filter = new Filter();
        filter.productId = mRequest.getInt("id");
        filter.countryId = mRequest.getInt("countryId");
        relatedProductsService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ProductsDisplay productsDisplay = new ProductsDisplay();
                productsDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getAutoCompleteFromServer() {
        AutoCompleteService autoCompleteService = ZuhurApplication.objectGraph.get(AutoCompleteService.class);
        autoCompleteService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                addAutoCompleteToDatabase((ArrayList<AutoComplete>) result);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void addAutoCompleteToDatabase(final ArrayList<AutoComplete> result) {
        final AutoCompleteDatabaseService autoCompleteDatabaseService = ZuhurApplication.objectGraph.get(AutoCompleteDatabaseService.class);
        autoCompleteDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean isDeleted) {
                if (isDeleted) {
                    autoCompleteDatabaseService.insertItems(result, new MainListener<Boolean>() {
                        @Override
                        public void onSuccess(Boolean result) {
                            if (result) {
                                Log.d(CommonConstants.APPLICATION_LOG_TAG, "Auto complete added to database");
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    });
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getAutoCompleteFromDatabase() {

        Filter filter = new Filter();
        filter.searchQuery = mRequest.getString("keyword");

        final AutoCompleteDatabaseService autoCompleteDatabaseService = ZuhurApplication.objectGraph.get(AutoCompleteDatabaseService.class);
        autoCompleteDatabaseService.findItems(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                ProductsDisplay productsDisplay = new ProductsDisplay();
                productsDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
