package com.bridge.zuhur.controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.bridge.zuhur.R;
import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.database.WishList;
import com.bridge.zuhur.display.WishListDisplay;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.services.WishListDatabaseService;
import com.bridge.zuhur.services.WishListService;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.NetworkConnection;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public class WishListController extends AbstractController {

    public final static int ADD_WISH_LIST_TO_SERVER_ACTION = 22;
    public final static String ADD_WISH_LIST_TO_SERVER_REQUEST_TAG = "ADD_WISH_LIST_TO_SERVER_REQUEST_TAG";
    public final static int GET_WISH_LIST_FROM_SERVER_ACTION = 23;
    public final static String GET_WISH_LIST_FROM_SERVER_REQUEST_TAG = "GET_WISH_LIST_FROM_SERVER_REQUEST_TAG";
    public final static int GET_ALL_WISH_LIST_DATABASE_ACTION = 24;
    public final static int ADD_WISH_LIST_DATABASE_ACTION = 25;
    public final static int DELETE_WISH_LIST_DATABASE_ACTION = 26;
    public final static int DELETE_ALL_WISH_LIST_DATABASE_ACTION = 27;
    public final static int DELETE_WISH_LIST_FROM_SERVER_ACTION = 30;
    public final static String DELETE_WISH_LIST_FROM_SERVER_REQUEST_TAG = "DELETE_WISH_LIST_FROM_SERVER_REQUEST_TAG";

    public WishListController(Context context) {
        super(context);
    }

    @Override
    void requestHandler(int action, Bundle args) {
        this.setAction(action);
        this.mRequest = args;
        handleRequest();
    }

    private void handleRequest() {
        switch (getAction()) {
            case ADD_WISH_LIST_TO_SERVER_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    addWishListToServer();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;

            case GET_WISH_LIST_FROM_SERVER_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getWishListFromServer();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;

            case GET_ALL_WISH_LIST_DATABASE_ACTION:
                getAllWishListFromDatabase();
                break;

            case ADD_WISH_LIST_DATABASE_ACTION:
                addWishListToDatabase();
                break;

            case DELETE_WISH_LIST_DATABASE_ACTION:
                deleteWishListToDatabase();
                break;

            case DELETE_ALL_WISH_LIST_DATABASE_ACTION:
                deleteAllWishListFromDatabase();
                break;

            case DELETE_WISH_LIST_FROM_SERVER_ACTION:
                deleteWishListFromServer();
                break;

            default:
                break;
        }
    }

    private void getWishListFromServer() {
        WishListService wishListService = ZuhurApplication.objectGraph.get(WishListService.class);
        wishListService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                addWishListItemsToDatabase(result);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void addWishListToServer() {
        WishListService wishListService = ZuhurApplication.objectGraph.get(WishListService.class);
        wishListService.save((WishList) mRequest.getSerializable("wishList"), new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteWishListFromServer() {
        WishListService wishListService = ZuhurApplication.objectGraph.get(WishListService.class);
        wishListService.find(String.valueOf(mRequest.getInt("productId")), new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getAllWishListFromDatabase() {
        WishListDatabaseService wishListDatabaseService = ZuhurApplication.objectGraph.get(WishListDatabaseService.class);
        wishListDatabaseService.findItems(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "wish list count: " + result.size());
                WishListDisplay wishListDisplay = new WishListDisplay();
                wishListDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void addWishListItemsToDatabase(final ArrayList<? extends Entity> result) {

        final WishListDatabaseService wishListDatabaseService = ZuhurApplication.objectGraph.get(WishListDatabaseService.class);
        wishListDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result1) {
                if (result1) {
                    wishListDatabaseService.insertItems(result, new MainListener<Boolean>() {
                        @Override
                        public void onSuccess(Boolean result2) {
                            if (result2) {
                                SharedPreferences settings = mContext.getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                                        Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putBoolean("isGetWishList", true);
                                editor.commit();
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    });
                }
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "error in delete all wish list: " + error);
            }
        });


    }

    private void addWishListToDatabase() {
        WishListDatabaseService wishListDatabaseService = ZuhurApplication.objectGraph.get(WishListDatabaseService.class);
        wishListDatabaseService.insertItem((WishList) mRequest.getSerializable("wishList"), new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.addWishSuccess), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.addWishError), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "save wish list in database error: " + error);
            }
        });
    }

    private void deleteWishListToDatabase() {
        WishListDatabaseService wishListDatabaseService = ZuhurApplication.objectGraph.get(WishListDatabaseService.class);
        wishListDatabaseService.deleteItem(mRequest.getInt("id"), new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteAllWishListFromDatabase() {
        WishListDatabaseService wishListDatabaseService = ZuhurApplication.objectGraph.get(WishListDatabaseService.class);
        wishListDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "deleting all wish list result is : " + result);
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "deleting all wish list error is : " + error);
            }
        });
    }

}
