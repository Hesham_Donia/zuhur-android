package com.bridge.zuhur.controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.bridge.zuhur.R;
import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.display.AuthenticationDisplay;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.entities.User;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.services.ForgetMyPasswordService;
import com.bridge.zuhur.services.SignInFacebookService;
import com.bridge.zuhur.services.SignInService;
import com.bridge.zuhur.services.SignUpFacebookService;
import com.bridge.zuhur.services.SignUpService;
import com.bridge.zuhur.services.UpdatePasswordService;
import com.bridge.zuhur.services.UpdateProfileService;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.NetworkConnection;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public class AuthenticationController extends AbstractController {

    public final static int SIGN_UP_ACTION = 5;
    public final static String SIGN_UP_REQUEST_TAG = "SIGN_UP_REQUEST_TAG";
    public final static int SIGN_IN_ACTION = 6;
    public final static String SIGN_IN_REQUEST_TAG = "SIGN_IN_REQUEST_TAG";
    public final static int UPDATE_PROFILE_ACTION = 7;
    public final static String UPDATE_PROFILE_REQUEST_TAG = "UPDATE_PROFILE_REQUEST_TAG";
    public final static int FORGET_MY_PASSWORD_ACTION = 8;
    public final static String FORGET_MY_PASSWORD_REQUEST_TAG = "FORGET_MY_PASSWORD_REQUEST_TAG";
    public final static String UPDATE_PASSWORD_REQUEST_TAG = "UPDATE_PROFILE_REQUEST_TAG";
    public final static int UPDATE_PASSWORD_ACTION = 31;
    public final static int SIGN_UP_FACEBOOK_ACTION = 72;
    public final static String SIGN_UP_FACEBOOK_REQUEST_TAG = "SIGN_UP_FACEBOOK_REQUEST_TAG";
    public final static int SIGN_IN_FACEBOOK_ACTION = 73;
    public final static String SIGN_IN_FACEBOOK_REQUEST_TAG = "SIGN_IN_FACEBOOK_REQUEST_TAG";


    public AuthenticationController(Context context) {
        super(context);
    }

    private void handleRequest() {
        if (NetworkConnection.networkConnectivity(mContext)) {
            switch (this.getAction()) {
                case SIGN_UP_ACTION:
                    signUp();
                    break;

                case SIGN_UP_FACEBOOK_ACTION:
                    signUpWithFacebookData();
                    break;

                case SIGN_IN_ACTION:
                    signIn();
                    break;

                case SIGN_IN_FACEBOOK_ACTION:
                    signInWithFacebook();
                    break;

                case UPDATE_PROFILE_ACTION:
                    updateProfile();
                    break;

                case FORGET_MY_PASSWORD_ACTION:
                    forgetMyPassword();
                    break;

                case UPDATE_PASSWORD_ACTION:
                    updatePassword();
                    break;

                default:
                    break;
            }
        } else {
            onErrorCalled(new NetworkError());
        }
    }

    @Override
    void requestHandler(int action, Bundle args) {
        this.setAction(action);
        this.mRequest = args;
        handleRequest();
    }

    private void signUp() {
        SignUpService signUpService = ZuhurApplication.objectGraph.get(SignUpService.class);
        User user = (User) mRequest.getSerializable("user");
        signUpService.save(user, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, result, mContext, null);
                Toast.makeText(mContext, mContext.getResources().getString(R.string.signUpSuccess), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(String error) {
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, null, mContext, null);
            }
        });
    }

    private void signUpWithFacebookData() {
        SignUpFacebookService signUpFacebookService = ZuhurApplication.objectGraph.get(SignUpFacebookService.class);
        final User user = (User) mRequest.getSerializable("user");
        signUpFacebookService.save(user, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                //when sign up success
                // update isFacebookFirstTime to yes
                final SharedPreferences settings = mContext.getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("isFacebookFirstTime", "yes");
                editor.commit();

                user.id = ((SignUpResponse) result.get(0)).id;
                // call login facebook service to get id and token of the user
                Bundle bundle = new Bundle();
                bundle.putInt("action", SIGN_IN_FACEBOOK_ACTION);
                bundle.putSerializable("user", user);
                request(SIGN_IN_FACEBOOK_ACTION, bundle);


            }

            @Override
            public void onError(String error) {
                Bundle bundle = new Bundle();
                bundle.putInt("action", SIGN_IN_FACEBOOK_ACTION);
                bundle.putSerializable("user", user);
                request(SIGN_IN_FACEBOOK_ACTION, bundle);
            }
        });
    }

    private void signInWithFacebook() {
        SignInFacebookService signInFacebookService = ZuhurApplication.objectGraph.get(SignInFacebookService.class);
        Filter filter = new Filter();
        User user = (User) mRequest.getSerializable("user");
        filter.searchQuery = user.userDataAsJson;
        signInFacebookService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                ArrayList<? extends Entity> result = new ArrayList<>();
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, result, mContext, null);
                Toast.makeText(mContext, mContext.getResources().getString(R.string.loginError), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void signIn() {
        SignInService signInService = ZuhurApplication.objectGraph.get(SignInService.class);
        Filter filter = new Filter();
        User user = (User) mRequest.getSerializable("user");
        filter.searchQuery = user.userDataAsJson;
        signInService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                ArrayList<? extends Entity> result = new ArrayList<>();
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, result, mContext, null);
                Toast.makeText(mContext, mContext.getResources().getString(R.string.loginError), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateProfile() {
        UpdateProfileService signUpService = ZuhurApplication.objectGraph.get(UpdateProfileService.class);
        final User user = (User) mRequest.getSerializable("user");
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "controller user.userDataAsJson: " + user.userDataAsJson);
        signUpService.save(user, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, null, mContext, null);
            }
        });
    }

    private void forgetMyPassword() {
        ForgetMyPasswordService forgetMyPasswordService =
                ZuhurApplication.objectGraph.get(ForgetMyPasswordService.class);
        final Filter filter = new Filter();
        filter.email = mRequest.getString("email");
        forgetMyPasswordService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                if (error.equals(" Email Not Registered ")) {
                    ArrayList<SignUpResponse> signUpResponses = new ArrayList<>();
                    SignUpResponse signUpResponse = new SignUpResponse();
                    signUpResponse.message = error;
                    signUpResponses.add(signUpResponse);
                    AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                    authenticationDisplay.display(mAction, signUpResponses, mContext, null);
                } else {
                    AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                    authenticationDisplay.display(mAction, null, mContext, null);
                }
            }
        });
    }

    private void updatePassword() {
        UpdatePasswordService updatePasswordService = ZuhurApplication.objectGraph.get(UpdatePasswordService.class);
        Filter filter = new Filter();
        filter.password = mRequest.getString("password");
        filter.newPassword = mRequest.getString("newPassword");
        filter.id = Singleton.getInstance().currentUser.id;
        updatePasswordService.save(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                AuthenticationDisplay authenticationDisplay = new AuthenticationDisplay();
                authenticationDisplay.display(mAction, null, mContext, null);
            }
        });
    }
}
