package com.bridge.zuhur.controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.bridge.zuhur.R;
import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.display.FavouritesDisplay;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.services.FavouritesDatabaseService;
import com.bridge.zuhur.services.FavouritesService;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.NetworkConnection;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public class FavouritesController extends AbstractController {


    public final static int ADD_FAVOURITE_TO_SERVER_ACTION = 16;
    public final static String ADD_FAVOURITE_TO_SERVER_REQUEST_TAG = "ADD_FAVOURITE_TO_SERVER_REQUEST_TAG";
    public final static int GET_ALL_FAVOURITES_SERVER_ACTION = 17;
    public final static String GET_ALL_FAVOURITES_SERVER_REQUEST_TAG = "GET_ALL_FAVOURITES_SERVER_REQUEST_TAG";
    public final static int GET_ALL_FAVOURITES_DATABASE_ACTION = 18;
    public final static int ADD_FAVOURITE_DATABASE_ACTION = 19;
    public final static int DELETE_FAVOURITE_DATABASE_ACTION = 20;
    public final static int DELETE_ALL_FAVOURITES_DATABASE_ACTION = 21;
    public final static int DELETE_FAVOURITE_SERVER_ACTION = 29;
    public final static String DELETE_FAVOURITE_SERVER_REQUEST_TAG = "GET_ALL_FAVOURITES_SERVER_REQUEST_TAG";

    public FavouritesController(Context context) {
        super(context);
    }

    @Override
    void requestHandler(int action, Bundle args) {
        this.setAction(action);
        this.mRequest = args;
        handleRequest();
    }

    private void handleRequest() {
        switch (getAction()) {
            case ADD_FAVOURITE_TO_SERVER_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    addFavouriteToServer();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;

            case GET_ALL_FAVOURITES_SERVER_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getAllFavouritesFromServer();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;

            case GET_ALL_FAVOURITES_DATABASE_ACTION:
                getAllFavouritesFromDatabase();
                break;

            case ADD_FAVOURITE_DATABASE_ACTION:
                addFavouriteToDatabase();
                break;

            case DELETE_FAVOURITE_DATABASE_ACTION:
                deleteFavouriteToDatabase();
                break;

            case DELETE_ALL_FAVOURITES_DATABASE_ACTION:
                deleteAllFavouriteFromDatabase();
                break;

            case DELETE_FAVOURITE_SERVER_ACTION:
                deleteFavouriteFromServer();
                break;

            default:
                break;
        }
    }

    private void addFavouriteToServer() {
        FavouritesService favouritesService = ZuhurApplication.objectGraph.get(FavouritesService.class);
        Favourite favourite = (Favourite) mRequest.getSerializable("favourite");
        favouritesService.save(favourite, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getAllFavouritesFromServer() {
        FavouritesService favouritesService = ZuhurApplication.objectGraph.get(FavouritesService.class);
        favouritesService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                addFavouritesToDatabase(result);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteFavouriteFromServer() {
        FavouritesService favouritesService = ZuhurApplication.objectGraph.get(FavouritesService.class);
        favouritesService.find(String.valueOf(mRequest.getInt("productId")), new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getAllFavouritesFromDatabase() {
        FavouritesDatabaseService favouritesDatabaseService = ZuhurApplication.objectGraph.get(FavouritesDatabaseService.class);
        favouritesDatabaseService.findItems(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "favourites count: " + result.size());
                FavouritesDisplay favouritesDisplay = new FavouritesDisplay();
                favouritesDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "favourites error: " + error);
            }
        });
    }

    private void addFavouriteToDatabase() {

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "favourite product id from controller: " + ((Favourite) mRequest.getSerializable("favourite")).getProductId());

        FavouritesDatabaseService favouritesDatabaseService = ZuhurApplication.objectGraph.get(FavouritesDatabaseService.class);
        favouritesDatabaseService.insertItem((Favourite) mRequest.getSerializable("favourite"), new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.addFavSuccess), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.addFavError), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "save favourite in database error: " + error);
            }
        });
    }

    private void addFavouritesToDatabase(final ArrayList<? extends Entity> result) {

        final FavouritesDatabaseService favouritesDatabaseService = ZuhurApplication.objectGraph.get(FavouritesDatabaseService.class);
        favouritesDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result1) {
                if (result1) {
                    favouritesDatabaseService.insertItems(result, new MainListener<Boolean>() {
                        @Override
                        public void onSuccess(Boolean result2) {
                            if (result2) {
                                SharedPreferences settings = mContext.getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                                        Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putBoolean("isGetFavourites", true);
                                editor.commit();
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    });
                }
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "error in delete all favs: " + error);
            }
        });
    }

    private void deleteFavouriteToDatabase() {
        FavouritesDatabaseService favouritesDatabaseService = ZuhurApplication.objectGraph.get(FavouritesDatabaseService.class);
        favouritesDatabaseService.deleteItem(mRequest.getInt("id"), new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteAllFavouriteFromDatabase() {
        final FavouritesDatabaseService favouritesDatabaseService = ZuhurApplication.objectGraph.get(FavouritesDatabaseService.class);
        favouritesDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "deleting all favs result is : " + result);
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "deleting all favs error is : " + error);
            }
        });
    }
}
