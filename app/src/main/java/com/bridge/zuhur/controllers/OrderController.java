package com.bridge.zuhur.controllers;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.bridge.zuhur.R;
import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.display.OrderDisplay;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Order;
import com.bridge.zuhur.entities.Promotion;
import com.bridge.zuhur.entities.SignUpResponse;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.services.GiftCardsService;
import com.bridge.zuhur.services.OrderStripeService;
import com.bridge.zuhur.services.OrdersDatabaseService;
import com.bridge.zuhur.services.OrdersService;
import com.bridge.zuhur.services.PackagingWayService;
import com.bridge.zuhur.services.PromotionValidationService;
import com.bridge.zuhur.services.UserPointsService;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.NetworkConnection;
import com.bridge.zuhur.views.activities.AbstractActivity;
import com.bridge.zuhur.views.activities.HomeActivity;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public class OrderController extends AbstractController {


    public final static int ADD_ORDER_TO_SERVER_ACTION = 33;
    public final static String ADD_ORDER_TO_SERVER_REQUEST_TAG = "ADD_ORDER_TO_SERVER_REQUEST_TAG";
    public final static int GET_ORDERS_FROM_SERVER_ACTION = 34;
    public final static String TRACK_ORDER_REQUEST_TAG = "TRACK_ORDER_REQUEST_TAG";
    public final static int TRACK_ORDER__ACTION = 52;
    public final static String GET_ORDER_FROM_SERVER_REQUEST_TAG = "GET_ORDER_FROM_SERVER_REQUEST_TAG";
    public final static int GET_ALL_ORDER_DATABASE_ACTION = 35;
    public final static int ADD_ORDER_DATABASE_ACTION = 36;
    public final static int DELETE_ORDER_DATABASE_ACTION = 37;
    public final static int DELETE_ALL_ORDER_DATABASE_ACTION = 38;
    public final static int GIFT_CARDS_ACTION = 54;
    public final static String GIFT_CARDS_REQUEST_TAG = "GIFT_CARDS_REQUEST_TAG";
    public final static int PACKAGING_WAYS_ACTION = 55;
    public final static String PACKAGING_WAYS_REQUEST_TAG = "PACKAGING_WAYS_REQUEST_TAG";
    public final static int PROMOTION_CODE_VALIDATION_ACTION = 58;
    public final static String PROMOTION_CODE_VALIDATION_REQUEST_TAG = "PROMOTION_CODE_VALIDATION_REQUEST_TAG";
    public final static int GET_UN_COMPLETED_ORDERS_ACTION = 76;
    public final static String GET_UN_COMPLETED_ORDERS_REQUEST_TAG = "GET_UN_COMPLETED_ORDERS_REQUEST_TAG";
    public final static int UPDATE_USER_POINTS_ACTION = 77;
    public final static String UPDATE_USER_POINTS_REQUEST_TAG = "UPDATE_USER_POINTS_REQUEST_TAG";
    public final static int ORDER_STRIPE_ACTION = 78;
    public final static String ORDER_STRIPE_REQUEST_TAG = "ORDER_STRIPE_REQUEST_TAG";

    public OrderController(Context context) {
        super(context);

    }

    @Override
    void requestHandler(int action, Bundle args) {
        this.setAction(action);
        this.mRequest = args;
        handleRequest();
    }

    private void handleRequest() {
        switch (getAction()) {

            case ADD_ORDER_TO_SERVER_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    makeOrder();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;

            case ORDER_STRIPE_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    makeStripeOrder();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;

            case GET_ORDERS_FROM_SERVER_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getOrdersFromServer();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;

            case GET_UN_COMPLETED_ORDERS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getUnCompletedOrdersFromServer();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;

            case GET_ALL_ORDER_DATABASE_ACTION:
                getAllOrdersFromDatabase();
                break;

            case ADD_ORDER_DATABASE_ACTION:
                addOrderToDatabase();
                break;

            case DELETE_ORDER_DATABASE_ACTION:
                deleteOrderFromDatabase();
                break;

            case DELETE_ALL_ORDER_DATABASE_ACTION:
                deleteAllOrdersFromDatabase();
                break;

            case TRACK_ORDER__ACTION:
                trackOrder();
                break;

            case PROMOTION_CODE_VALIDATION_ACTION:
                validatePromotionCode();
                break;

            case GIFT_CARDS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getGiftCards();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;

            case PACKAGING_WAYS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    getPackagingWays();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;

            case UPDATE_USER_POINTS_ACTION:
                if (NetworkConnection.networkConnectivity(mContext)) {
                    updateUserPoints();
                } else {
                    onErrorCalled(new NetworkError());
                }
                break;


            default:
                break;
        }
    }

    private void makeStripeOrder() {
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "user points before order: " + Singleton.getInstance().currentUser.points);
        final OrderStripeService ordersService = ZuhurApplication.objectGraph.get(OrderStripeService.class);
        final Order order = (Order) mRequest.getSerializable("order");
        order.countryId = Singleton.getInstance().country.id;
        ordersService.save(order, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
//                Toast.makeText(mContext, mContext.getResources().getString(R.string.orderSuccess), Toast.LENGTH_LONG).show();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        mContext);

                // set title
                alertDialogBuilder.setTitle("Order success");

                // set dialog message
                alertDialogBuilder
                        .setMessage(mContext.getResources().getString(R.string.orderSuccess))
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current activity
                                Singleton.getInstance().cart.clear();
                                updateUserPoints();
                                Intent intent = new Intent(mContext, HomeActivity.class);
                                mContext.startActivity(intent);
                                ((AbstractActivity) mContext).finish();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(mContext, "error in making order: " + error, Toast.LENGTH_LONG).show();
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "error in making order: " + error);
            }
        });
    }

    private void getUnCompletedOrdersFromServer() {
        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;
        filter.countryId = Singleton.getInstance().country.id;
        OrdersService ordersService = ZuhurApplication.objectGraph.get(OrdersService.class);
        ordersService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                OrderDisplay orderDisplay = new OrderDisplay();
                orderDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void trackOrder() {
        OrdersService ordersService = ZuhurApplication.objectGraph.get(OrdersService.class);
        ordersService.find(String.valueOf(mRequest.getString("id")), new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                OrderDisplay orderDisplay = new OrderDisplay();
                orderDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void makeOrder() {

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "user points before order: " + Singleton.getInstance().currentUser.points);
        final OrdersService ordersService = ZuhurApplication.objectGraph.get(OrdersService.class);
        final Order order = (Order) mRequest.getSerializable("order");
        order.countryId = Singleton.getInstance().country.id;
        ordersService.save(order, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
//                Toast.makeText(mContext, mContext.getResources().getString(R.string.orderSuccess), Toast.LENGTH_LONG).show();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        mContext);

                // set title
                alertDialogBuilder.setTitle("Order success");

                // set dialog message
                alertDialogBuilder
                        .setMessage(mContext.getResources().getString(R.string.orderSuccess))
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current activity
                                Singleton.getInstance().cart.clear();
                                updateUserPoints();
                                Intent intent = new Intent(mContext, HomeActivity.class);
                                mContext.startActivity(intent);
                                ((AbstractActivity) mContext).finish();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(mContext, "error in making order: " + error, Toast.LENGTH_LONG).show();
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "error in making order: " + error);
            }
        });
    }

    private void updateUserPoints() {

        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;

        UserPointsService userPointsService = ZuhurApplication.objectGraph.get(UserPointsService.class);
        userPointsService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                SignUpResponse signUpResponse = new SignUpResponse();
                Singleton.getInstance().currentUser.points = Singleton.getInstance().currentUser.points + signUpResponse.value;
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "user points after order: " + Singleton.getInstance().currentUser.points);
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "error in user points: " + error);
            }
        });
    }

    private void getOrdersFromServer() {
        OrdersService ordersService = ZuhurApplication.objectGraph.get(OrdersService.class);
        Filter filter = new Filter();
        filter.id = Singleton.getInstance().currentUser.id;
        filter.countryId = Singleton.getInstance().country.id;
        ordersService.findAll(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                addOrdersToDatabase(result);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getAllOrdersFromDatabase() {
        OrdersDatabaseService ordersDatabaseService = ZuhurApplication.objectGraph.get(OrdersDatabaseService.class);
        ordersDatabaseService.findItems(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                OrderDisplay orderDisplay = new OrderDisplay();
                orderDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void addOrderToDatabase() {
        OrdersDatabaseService ordersDatabaseService = ZuhurApplication.objectGraph.get(OrdersDatabaseService.class);
        ordersDatabaseService.insertItem((Order) mRequest.getSerializable("order"), new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void addOrdersToDatabase(final ArrayList<? extends Entity> result) {

        final OrdersDatabaseService ordersDatabaseService = ZuhurApplication.objectGraph.get(OrdersDatabaseService.class);
        ordersDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result1) {
                ordersDatabaseService.insertItems(result, new MainListener<Boolean>() {
                    @Override
                    public void onSuccess(Boolean result) {

                    }

                    @Override
                    public void onError(String error) {

                    }
                });
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteOrderFromDatabase() {
        OrdersDatabaseService ordersDatabaseService = ZuhurApplication.objectGraph.get(OrdersDatabaseService.class);
        ordersDatabaseService.deleteItem(mRequest.getInt("id"), new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void deleteAllOrdersFromDatabase() {
        OrdersDatabaseService ordersDatabaseService = ZuhurApplication.objectGraph.get(OrdersDatabaseService.class);
        ordersDatabaseService.deleteAllItems(new MainListener<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "deleting all orders result is : " + result);
            }

            @Override
            public void onError(String error) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "deleting all orders error is : " + error);
            }
        });
    }

    private void validatePromotionCode() {
        Filter filter = new Filter();
        filter.promotionCode = mRequest.getString("code");
        filter.id = Singleton.getInstance().currentUser.id;

        PromotionValidationService promotionValidationService = ZuhurApplication.objectGraph.get(PromotionValidationService.class);
        promotionValidationService.find(filter, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                OrderDisplay orderDisplay = new OrderDisplay();
                ArrayList<Promotion> promotions = (ArrayList<Promotion>) result;
                promotions.get(0).promotionCode = mRequest.getString("code");
                orderDisplay.display(mAction, result, mContext, null);
            }

            @Override
            public void onError(String error) {
                OrderDisplay orderDisplay = new OrderDisplay();
                orderDisplay.display(mAction, null, mContext, null);
            }
        });

    }

    private void getGiftCards() {
        GiftCardsService giftCardsService = ZuhurApplication.objectGraph.get(GiftCardsService.class);
        giftCardsService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                OrderDisplay orderDisplay = new OrderDisplay();
                orderDisplay.display(mAction, result, mContext, mRequest);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getPackagingWays() {
        PackagingWayService packagingWayService = ZuhurApplication.objectGraph.get(PackagingWayService.class);
        packagingWayService.findAll(null, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {
                OrderDisplay orderDisplay = new OrderDisplay();
                orderDisplay.display(mAction, result, mContext, mRequest);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
