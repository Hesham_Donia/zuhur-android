package com.bridge.zuhur.controllers;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.bridge.zuhur.utils.Sensor;

/**
 * Created by hesham on 04/04/16.
 */
abstract public class AbstractController {

    protected int mAction;
    protected Context mContext;
    protected Bundle mRequest;

    public AbstractController(Context context) {
        this.mContext = context;

    }

    /**
     * @param action specify the needed data
     * @param args   contains the data about the request
     */
    abstract void requestHandler(int action, Bundle args);


    /**
     * @param action specify the type of needed data
     * @param args   the properties of the request
     */
    public void request(int action, Bundle args) {
        if (mContext instanceof Activity) {
            Sensor.lockSensor(mContext);
        }

        requestHandler(action, args);
    }

    public int getAction() {
        return mAction;
    }

    public void setAction(int mAction) {
        this.mAction = mAction;
    }

    public static void onErrorCalled(Exception error) {
//        if (error instanceof TimeoutError || error instanceof ServerError) {
//            YellowApplication.objectGraph.get(DisplayErrorFragment.class).displayServiceDownFragment();
//        } else if (error instanceof NoConnectionError || error instanceof NetworkError) {
//            YellowApplication.objectGraph.get(DisplayErrorFragment.class).displayConnectionErrorFragment();
//        }
    }

}
