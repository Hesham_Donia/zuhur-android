package com.bridge.zuhur.models;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.web_service_mappers.SignUpFacebookMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by bridgetomarket on 6/15/16.
 */
public class SignUpFacebookModel extends AbstractModel {

    /**
     * @param mapper
     */
    @Inject
    public SignUpFacebookModel(SignUpFacebookMapper mapper) {
        super(mapper);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((SignUpFacebookMapper) mapper).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((SignUpFacebookMapper) mapper).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((SignUpFacebookMapper) mapper).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((SignUpFacebookMapper) mapper).save(entity, listener);
    }
}
