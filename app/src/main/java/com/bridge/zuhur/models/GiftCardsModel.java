package com.bridge.zuhur.models;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.web_service_mappers.GiftCardsMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by bridgetomarket on 5/8/16.
 */
public class GiftCardsModel extends AbstractModel {

    /**
     * @param mapper
     */
    @Inject
    public GiftCardsModel(GiftCardsMapper mapper) {
        super(mapper);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((GiftCardsMapper) mapper).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((GiftCardsMapper) mapper).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((GiftCardsMapper) mapper).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((GiftCardsMapper) mapper).save(entity, listener);
    }
}
