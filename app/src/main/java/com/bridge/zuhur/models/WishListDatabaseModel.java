package com.bridge.zuhur.models;

import com.bridge.zuhur.database.WishList;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.DatabaseListener;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.database_mappers.WishListDatabaseMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 10/04/16.
 */
public class WishListDatabaseModel extends AbstractModel implements DatabaseListener {

    /**
     * @param mapper
     */
    @Inject
    public WishListDatabaseModel(WishListDatabaseMapper mapper) {
        super(mapper);
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        ((WishListDatabaseMapper) mapper).findItem(id, listener);
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        ((WishListDatabaseMapper) mapper).findItems(filter,listener);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        WishList wishList = (WishList) value;
        ((WishListDatabaseMapper) mapper).insertItem(wishList, listener);
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        ArrayList<WishList> wishLists = (ArrayList<WishList>) value;
        ((WishListDatabaseMapper) mapper).insertItems(wishLists, listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {
        ((WishListDatabaseMapper) mapper).updateItem(value, listener);
    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        ((WishListDatabaseMapper) mapper).deleteItem(id, listener);
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        ((WishListDatabaseMapper) mapper).deleteAllItems(listener);
    }


    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
