package com.bridge.zuhur.models;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.web_service_mappers.InternationalShippingRequestMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 17/04/16.
 */
public class InternationalShippingRequestModel extends AbstractModel {

    /**
     * @param mapper
     */
    @Inject
    public InternationalShippingRequestModel(InternationalShippingRequestMapper mapper) {
        super(mapper);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((InternationalShippingRequestMapper) mapper).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((InternationalShippingRequestMapper) mapper).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((InternationalShippingRequestMapper) mapper).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((InternationalShippingRequestMapper) mapper).save(entity, listener);
    }
}
