package com.bridge.zuhur.models;

import com.bridge.zuhur.database.Remainder;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.DatabaseListener;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.database_mappers.RemaindersDatabaseMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by bridgetomarket on 6/9/16.
 */
public class RemaindersDatabaseModel extends AbstractModel implements DatabaseListener {

    /**
     * @param mapper
     */
    @Inject
    public RemaindersDatabaseModel(RemaindersDatabaseMapper mapper) {
        super(mapper);
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        ((RemaindersDatabaseMapper) mapper).findItem(id, listener);
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        ((RemaindersDatabaseMapper) mapper).findItems(filter, listener);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        Remainder remainder = (Remainder) value;
        ((RemaindersDatabaseMapper) mapper).insertItem(remainder, listener);
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        ArrayList<Remainder> remainders = (ArrayList<Remainder>) value;
        ((RemaindersDatabaseMapper) mapper).insertItems(remainders, listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {
        ((RemaindersDatabaseMapper) mapper).updateItem(value, listener);
    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        ((RemaindersDatabaseMapper) mapper).deleteItem(id, listener);
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        ((RemaindersDatabaseMapper) mapper).deleteAllItems(listener);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
