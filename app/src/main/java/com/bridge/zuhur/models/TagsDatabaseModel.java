package com.bridge.zuhur.models;

import com.bridge.zuhur.database.Tag;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.DatabaseListener;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.database_mappers.TagsDatabaseMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by bridgetomarket on 5/9/16.
 */
public class TagsDatabaseModel extends AbstractModel implements DatabaseListener {

    /**
     * @param mapper
     */
    @Inject
    public TagsDatabaseModel(TagsDatabaseMapper mapper) {
        super(mapper);
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        ((TagsDatabaseMapper) mapper).findItem(id, listener);
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        ((TagsDatabaseMapper) mapper).findItems(filter, listener);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        com.bridge.zuhur.entities.Tag tag = (com.bridge.zuhur.entities.Tag) value;
        Tag favourite = new Tag(tag);
        ((TagsDatabaseMapper) mapper).insertItem(favourite, listener);
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        ArrayList<com.bridge.zuhur.entities.Tag> tags = (ArrayList<com.bridge.zuhur.entities.Tag>) value;
        ArrayList<Tag> tagArrayList = new ArrayList<>();

        for (com.bridge.zuhur.entities.Tag tag : tags) {
            tagArrayList.add(new Tag(tag));
        }
        ((TagsDatabaseMapper) mapper).insertItems(tagArrayList, listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {
        ((TagsDatabaseMapper) mapper).updateItem(value, listener);
    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        ((TagsDatabaseMapper) mapper).deleteItem(id, listener);
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        ((TagsDatabaseMapper) mapper).deleteAllItems(listener);
    }


    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
