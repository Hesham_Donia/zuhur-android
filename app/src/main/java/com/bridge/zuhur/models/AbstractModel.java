package com.bridge.zuhur.models;

import com.bridge.zuhur.interfaces.ServiceListener;
import com.bridge.zuhur.mappers.AbstractMapper;

/**
 * Created by hesham on 04/04/16.
 */
abstract public class AbstractModel implements ServiceListener{

    protected AbstractMapper mapper;

    /**
     * @param mapper
     */
    public AbstractModel (AbstractMapper mapper){
        this.mapper = mapper;
    }
}
