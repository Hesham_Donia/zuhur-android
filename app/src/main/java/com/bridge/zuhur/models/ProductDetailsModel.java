package com.bridge.zuhur.models;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.web_service_mappers.ProductDetailsMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 06/04/16.
 */
public class ProductDetailsModel extends AbstractModel{
    /**
     * @param mapper
     */
    @Inject
    public ProductDetailsModel(ProductDetailsMapper mapper) {
        super(mapper);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((ProductDetailsMapper) mapper).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((ProductDetailsMapper) mapper).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((ProductDetailsMapper) mapper).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((ProductDetailsMapper) mapper).save(entity, listener);
    }
}
