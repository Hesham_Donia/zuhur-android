package com.bridge.zuhur.models;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.PaymentMethod;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.web_service_mappers.PaymentMethodMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 12/04/16.
 */
public class PaymentMethodModel extends AbstractModel {

    /**
     * @param mapper
     */
    @Inject
    public PaymentMethodModel(PaymentMethodMapper mapper) {
        super(mapper);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((PaymentMethodMapper) mapper).find(id, listener);

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((PaymentMethodMapper) mapper).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((PaymentMethodMapper) mapper).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((PaymentMethodMapper) mapper).save(entity, listener);
    }
}
