package com.bridge.zuhur.models;

import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.DatabaseListener;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.database_mappers.AutoCompleteDatabaseMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by bridgetomarket on 6/12/16.
 */
public class AutoCompleteDatabaseModel extends AbstractModel implements DatabaseListener {

    /**
     * @param mapper
     */
    @Inject
    public AutoCompleteDatabaseModel(AutoCompleteDatabaseMapper mapper) {
        super(mapper);
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        ((AutoCompleteDatabaseMapper) mapper).findItem(id, listener);
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        ((AutoCompleteDatabaseMapper) mapper).findItems(filter, listener);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        Favourite favourite = (Favourite) value;
        ((AutoCompleteDatabaseMapper) mapper).insertItem(favourite, listener);
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        ArrayList<Favourite> AutoComplete = (ArrayList<Favourite>) value;
        ((AutoCompleteDatabaseMapper) mapper).insertItems(AutoComplete, listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {
        ((AutoCompleteDatabaseMapper) mapper).updateItem(value, listener);
    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        ((AutoCompleteDatabaseMapper) mapper).deleteItem(id, listener);
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        ((AutoCompleteDatabaseMapper) mapper).deleteAllItems(listener);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
