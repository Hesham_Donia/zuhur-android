package com.bridge.zuhur.models;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.web_service_mappers.NotificationsMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 17/04/16.
 */
public class NotificationsModel extends AbstractModel {

    /**
     * @param mapper
     */
    @Inject
    public NotificationsModel(NotificationsMapper mapper) {
        super(mapper);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((NotificationsMapper) mapper).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((NotificationsMapper) mapper).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((NotificationsMapper) mapper).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((NotificationsMapper) mapper).save(entity, listener);
    }
}
