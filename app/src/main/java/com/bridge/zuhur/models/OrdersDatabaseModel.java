package com.bridge.zuhur.models;

import com.bridge.zuhur.database.OrdersHistory;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.entities.Order;
import com.bridge.zuhur.interfaces.DatabaseListener;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.database_mappers.OrdersDatabaseMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 13/04/16.
 */
public class OrdersDatabaseModel extends AbstractModel implements DatabaseListener {

    /**
     * @param mapper
     */
    @Inject
    public OrdersDatabaseModel(OrdersDatabaseMapper mapper) {
        super(mapper);
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        ((OrdersDatabaseMapper) mapper).findItem(id, listener);
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        ((OrdersDatabaseMapper) mapper).findItems(filter, listener);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        ((OrdersDatabaseMapper) mapper).insertItem(value, listener);
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {

        ArrayList<Order> orders = (ArrayList<Order>) value;
        ArrayList<OrdersHistory> ordersHistory = new ArrayList<>();

        for (Order order : orders) {
            ordersHistory.add(new OrdersHistory(order));
        }

        ((OrdersDatabaseMapper) mapper).insertItems(ordersHistory, listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {
        ((OrdersDatabaseMapper) mapper).updateItem(value, listener);
    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        ((OrdersDatabaseMapper) mapper).deleteItem(id, listener);
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        ((OrdersDatabaseMapper) mapper).deleteAllItems(listener);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
