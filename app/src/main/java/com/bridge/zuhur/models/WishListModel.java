package com.bridge.zuhur.models;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.web_service_mappers.WishListMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 10/04/16.
 */
public class WishListModel extends AbstractModel {

    /**
     * @param mapper
     */
    @Inject
    public WishListModel(WishListMapper mapper) {
        super(mapper);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((WishListMapper) mapper).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((WishListMapper) mapper).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((WishListMapper) mapper).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((WishListMapper) mapper).save(entity, listener);
    }
}
