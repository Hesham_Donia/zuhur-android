package com.bridge.zuhur.models;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.DatabaseListener;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.database_mappers.LogDataDatabaseMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by heshamkhaled on 6/13/16.
 */
public class LogDataDatabaseModel extends AbstractModel implements DatabaseListener {

    /**
     * @param mapper
     */
    @Inject
    public LogDataDatabaseModel(LogDataDatabaseMapper mapper) {
        super(mapper);
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        ((LogDataDatabaseMapper) mapper).findItem(id, listener);
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        ((LogDataDatabaseMapper) mapper).findItems(filter, listener);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        ((LogDataDatabaseMapper) mapper).insertItem(value, listener);
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        ((LogDataDatabaseMapper) mapper).insertItems(value, listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {
        ((LogDataDatabaseMapper) mapper).updateItem(value, listener);
    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        ((LogDataDatabaseMapper) mapper).deleteItem(id, listener);
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        ((LogDataDatabaseMapper) mapper).deleteAllItems(listener);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
