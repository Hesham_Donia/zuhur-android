package com.bridge.zuhur.models;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.web_service_mappers.CategoriesMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 05/04/16.
 */
public class CategoriesModel extends AbstractModel {
    /**
     * @param mapper
     */

    @Inject
    public CategoriesModel(CategoriesMapper mapper) {
        super(mapper);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {
        ((CategoriesMapper)mapper).find(id, listener);
    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((CategoriesMapper)mapper).find(filters, listener);
    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {
        ((CategoriesMapper)mapper).findAll(filters, listener);
    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {
        ((CategoriesMapper)mapper).save(entity, listener);
    }
}
