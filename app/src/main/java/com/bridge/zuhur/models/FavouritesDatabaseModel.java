package com.bridge.zuhur.models;

import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Filter;
import com.bridge.zuhur.interfaces.DatabaseListener;
import com.bridge.zuhur.interfaces.MainListener;
import com.bridge.zuhur.mappers.database_mappers.FavouritesDatabaseMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by hesham on 10/04/16.
 */
public class FavouritesDatabaseModel extends AbstractModel implements DatabaseListener {
    /**
     * @param mapper
     */
    @Inject
    public FavouritesDatabaseModel(FavouritesDatabaseMapper mapper) {
        super(mapper);
    }

    @Override
    public void findItem(int id, MainListener<ArrayList<? extends Entity>> listener) {
        ((FavouritesDatabaseMapper) mapper).findItem(id, listener);
    }

    @Override
    public void findItems(Filter filter, MainListener<ArrayList<? extends Entity>> listener) {
        ((FavouritesDatabaseMapper) mapper).findItems(filter, listener);
    }

    @Override
    public void insertItem(Entity value, MainListener<Boolean> listener) {
        Favourite favourite = (Favourite) value;
        ((FavouritesDatabaseMapper) mapper).insertItem(favourite, listener);
    }

    @Override
    public void insertItems(ArrayList<? extends Entity> value, MainListener<Boolean> listener) {
        ArrayList<Favourite> favourites = (ArrayList<Favourite>) value;
        ((FavouritesDatabaseMapper) mapper).insertItems(favourites, listener);
    }

    @Override
    public void updateItem(Entity value, MainListener<Boolean> listener) {
        ((FavouritesDatabaseMapper) mapper).updateItem(value, listener);
    }

    @Override
    public void deleteItem(int id, MainListener<Boolean> listener) {
        ((FavouritesDatabaseMapper) mapper).deleteItem(id, listener);
    }

    @Override
    public void deleteAllItems(MainListener<Boolean> listener) {
        ((FavouritesDatabaseMapper) mapper).deleteAllItems(listener);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
