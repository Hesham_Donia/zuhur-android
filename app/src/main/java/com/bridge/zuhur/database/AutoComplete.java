package com.bridge.zuhur.database;

import com.bridge.zuhur.entities.Entity;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by bridgetomarket on 6/12/16.
 */

@DatabaseTable(tableName = "AutoComplete")
public class AutoComplete extends Entity {

    public static final String AutoComplete_ID = "ID";
    public static final String AutoComplete_Name = "Name";
    @DatabaseField(generatedId = true, columnName = AutoComplete_ID)
    private int mId;
    @DatabaseField(columnName = AutoComplete_Name)
    private String mName;

    public AutoComplete() {
    }

    public AutoComplete(String name) {
        setName(name);
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }
}
