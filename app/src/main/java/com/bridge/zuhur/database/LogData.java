package com.bridge.zuhur.database;

import com.bridge.zuhur.entities.Entity;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by bridgetomarket on 5/28/16.
 */
@DatabaseTable(tableName = "LogData")
public class LogData extends Entity {

    public static final String LogData_ID = "ID";
    public static final String LogData_Item_ID = "Item_ID";
    public static final String LogData_Operation = "Operation";
    public static final String LogData_Item_Type = "Item_Type";

    @DatabaseField(generatedId = true, columnName = LogData_ID)
    private int mId;

    @DatabaseField(columnName = LogData_Operation)
    private int mOperation;

    @DatabaseField(columnName = LogData_Item_Type)
    private int mItemType;

    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = LogData_Item_ID)
    private Entity mItem;

    public LogData() {

    }

    public LogData(Entity item, int operation, int type) {
        setItem(item);
        setItemType(type);
        setOperation(operation);
    }

    public int getId() {
        return mId;
    }

    public int getOperation() {
        return mOperation;
    }

    public void setOperation(int mOperation) {
        this.mOperation = mOperation;
    }

    public int getItemType() {
        return mItemType;
    }

    public void setItemType(int mItemType) {
        this.mItemType = mItemType;
    }

    public Entity getItem() {
        return mItem;
    }

    public void setItem(Entity mItem) {
        this.mItem = mItem;
    }
}
