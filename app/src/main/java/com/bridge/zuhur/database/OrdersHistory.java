package com.bridge.zuhur.database;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Order;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by hesham on 05/04/16.
 */

@DatabaseTable(tableName = "OrdersHistory")
public class OrdersHistory extends Entity{


    public static final String OrdersHistory_ID = "ID";
    public static final String OrdersHistory_Order = "Order";
    public static final String OrdersHistory_Order_ID = "Order_ID";

    public OrdersHistory() {

    }

    public OrdersHistory(Order Order) {
        setOrderId(Order.id);
        setOrder(Order);
    }

    @DatabaseField(generatedId = true, columnName = OrdersHistory_ID)
    private int mId;

    @DatabaseField(columnName = OrdersHistory_Order_ID)
    private int mOrderId;

    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = OrdersHistory_Order)
    private Order mOrder;

    public int getId() {
        return mId;
    }

    public int getOrderId() {
        return mOrderId;
    }

    public void setOrderId(int mOrderId) {
        this.mOrderId = mOrderId;
    }

    public Order getOrder() {
        return mOrder;
    }

    public void setOrder(Order mOrder) {
        this.mOrder = mOrder;
    }
}
