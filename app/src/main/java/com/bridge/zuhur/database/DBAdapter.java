package com.bridge.zuhur.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by heshamkhaled on 2/3/16.
 */
public class DBAdapter extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "rosemart";
    private static final int DATABASE_VERSION = 1;
    private Dao<Favourite, Integer> mFavouriteDao = null;
    private Dao<WishList, Integer> mWishListDao = null;
    private Dao<OrdersHistory, Integer> mOrdersHistoryDao = null;
    private Dao<Tag, Integer> mTagDao = null;
    private Dao<LogData, Integer> mLogDataDao = null;
    private Dao<Remainder, Integer> mRemainderDao = null;
    private Dao<AutoComplete, Integer> mAutoCompleteDao = null;

    public DBAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Favourite.class);
            TableUtils.createTable(connectionSource, WishList.class);
            TableUtils.createTable(connectionSource, OrdersHistory.class);
            TableUtils.createTable(connectionSource, Tag.class);
            TableUtils.createTable(connectionSource, LogData.class);
            TableUtils.createTable(connectionSource, Remainder.class);
            TableUtils.createTable(connectionSource, AutoComplete.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void close() {
        super.close();
        setFavouriteDao(null);
        setWishListDao(null);
        setOrdersHistoryDao(null);
        setTagDao(null);
        setLogDataDao(null);
        setRemainderDao(null);
        setAutoCompleteDao(null);
    }

    public Dao<Favourite, Integer> getFavouriteDao() {
        if (mFavouriteDao == null) {
            try {
                mFavouriteDao = getDao(Favourite.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mFavouriteDao;
    }

    public void setFavouriteDao(Dao<Favourite, Integer> favouriteDao) {
        this.mFavouriteDao = favouriteDao;
    }

    public Dao<WishList, Integer> getWishListDao() {
        if (mWishListDao == null) {
            try {
                mWishListDao = getDao(WishList.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mWishListDao;
    }

    public void setWishListDao(Dao<WishList, Integer> mWishListDao) {
        this.mWishListDao = mWishListDao;
    }

    public Dao<OrdersHistory, Integer> getOrdersHistoryDao() {
        if (mOrdersHistoryDao == null) {
            try {
                mOrdersHistoryDao = getDao(OrdersHistory.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mOrdersHistoryDao;
    }

    public void setOrdersHistoryDao(Dao<OrdersHistory, Integer> mOrdersHistoryDao) {
        this.mOrdersHistoryDao = mOrdersHistoryDao;
    }

    public Dao<Tag, Integer> getTagDao() {
        if (mTagDao == null) {
            try {
                mTagDao = getDao(Tag.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mTagDao;
    }

    public void setTagDao(Dao<Tag, Integer> mTagDao) {
        this.mTagDao = mTagDao;
    }

    public Dao<LogData, Integer> getLogDataDao() {
        if (mLogDataDao == null) {
            try {
                mLogDataDao = getDao(LogData.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mLogDataDao;
    }

    public void setLogDataDao(Dao<LogData, Integer> mLogDataDao) {
        this.mLogDataDao = mLogDataDao;
    }

    public Dao<Remainder, Integer> getRemainderDao() {
        if (mRemainderDao == null) {
            try {
                mRemainderDao = getDao(Remainder.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mRemainderDao;
    }

    public void setRemainderDao(Dao<Remainder, Integer> mRemainderDao) {
        this.mRemainderDao = mRemainderDao;
    }

    public Dao<AutoComplete, Integer> getAutoCompleteDao() {
        if (mAutoCompleteDao == null) {
            try {
                mAutoCompleteDao = getDao(AutoComplete.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mAutoCompleteDao;
    }

    public void setAutoCompleteDao(Dao<AutoComplete, Integer> mAutoCompleteDao) {
        this.mAutoCompleteDao = mAutoCompleteDao;
    }
}
