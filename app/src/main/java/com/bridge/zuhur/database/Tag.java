package com.bridge.zuhur.database;

import com.bridge.zuhur.entities.Entity;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by bridgetomarket on 5/9/16.
 */
@DatabaseTable(tableName = "Tag")
public class Tag extends Entity {

    public static final String Tag_ID = "ID";
    public static final String Tag_TagObjectId = "TagObjectId";
    public static final String Tag_TagObjectName = "TagObjectName";
    public static final String Tag_TagObject = "TagObject";

    @DatabaseField(generatedId = true, columnName = Tag_ID)
    private int mId;

    @DatabaseField(columnName = Tag_TagObjectId)
    private int mTagId;

    @DatabaseField(columnName = Tag_TagObjectName)
    private String mTagName;

    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = Tag_TagObject)
    private com.bridge.zuhur.entities.Tag mTag;

    private boolean checked = false;
    private boolean colored = false;

    public Tag() {

    }

    public Tag(com.bridge.zuhur.entities.Tag tag) {
        setTag(tag);
        setTagId(tag.id);
        setTagName(tag.name);
    }

    public int getId() {
        return mId;
    }

    public com.bridge.zuhur.entities.Tag getTag() {
        return mTag;
    }

    public void setTag(com.bridge.zuhur.entities.Tag mTag) {
        this.mTag = mTag;
    }

    public int getTagId() {
        return mTagId;
    }

    public void setTagId(int mTagId) {
        this.mTagId = mTagId;
    }

    public String getTagName() {
        return mTagName;
    }

    public void setTagName(String mTagName) {
        this.mTagName = mTagName;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isColored() {
        return colored;
    }

    public void setColored(boolean colored) {
        this.colored = colored;
    }
}
