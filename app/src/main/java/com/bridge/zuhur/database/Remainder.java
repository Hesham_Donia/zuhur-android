package com.bridge.zuhur.database;

import com.bridge.zuhur.entities.Entity;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by bridgetomarket on 6/9/16.
 */
@DatabaseTable(tableName = "Remainder")
public class Remainder extends Entity {

    public static final String ID = "ID";
    public static final String Remainder = "Remainder";
    public static final String Remainder_ID = "Remainder_ID";
    @DatabaseField(generatedId = true, columnName = ID)
    private int mId;
    @DatabaseField(columnName = Remainder_ID)
    private int mRemainderId;
    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = Remainder)
    private com.bridge.zuhur.entities.Remainder mRemainder;

    public Remainder() {

    }

    public Remainder(com.bridge.zuhur.entities.Remainder remainder) {
        setRemainder(remainder);
        setRemainderId(remainder.id);
    }

    public int getId() {
        return mId;
    }

    public int getRemainderId() {
        return mRemainderId;
    }

    public void setRemainderId(int mRemainderId) {
        this.mRemainderId = mRemainderId;
    }

    public com.bridge.zuhur.entities.Remainder getRemainder() {
        return mRemainder;
    }

    public void setRemainder(com.bridge.zuhur.entities.Remainder mRemainder) {
        this.mRemainder = mRemainder;
    }
}
