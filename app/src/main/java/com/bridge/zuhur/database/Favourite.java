package com.bridge.zuhur.database;

import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Product;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by hesham on 05/04/16.
 */

@DatabaseTable(tableName = "Favourite")
public class Favourite extends Entity{

    public static final String Favourite_ID = "ID";
    public static final String Favourite_Product = "Product";
    public static final String Favourite_Product_ID = "Product_ID";

    public Favourite() {

    }

    public Favourite(Product product) {
        setProductId(product.id);
        setProduct(product);
    }

    @DatabaseField(generatedId = true, columnName = Favourite_ID)
    private int mId;

    @DatabaseField(columnName = Favourite_Product_ID)
    private int mProductId;

    @DatabaseField(dataType = DataType.SERIALIZABLE, columnName = Favourite_Product)
    private Product mProduct;

    public int getId() {
        return mId;
    }

    public int getProductId() {
        return mProductId;
    }

    public void setProductId(int mProductId) {
        this.mProductId = mProductId;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product mProduct) {
        this.mProduct = mProduct;
    }
}

