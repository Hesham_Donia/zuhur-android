package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.back_services.NotificationsService;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.FavouritesController;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.controllers.WishListController;
import com.bridge.zuhur.entities.NavigationViewItem;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.views.activities.AbstractActivity;
import com.bridge.zuhur.views.activities.AllProductsActivity;
import com.bridge.zuhur.views.activities.ContactUsActivity;
import com.bridge.zuhur.views.activities.EventsActivity;
import com.bridge.zuhur.views.activities.HomeActivity;
import com.bridge.zuhur.views.activities.LoginActivity;
import com.bridge.zuhur.views.activities.MoreActivity;
import com.bridge.zuhur.views.activities.NewArrivalsActivity;
import com.facebook.AccessToken;
import com.facebook.Profile;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 5/30/16.
 */
public class NavigationViewRecyclerViewAdapter extends RecyclerView.Adapter<NavigationViewRecyclerViewAdapter.ViewHolder> {


    ViewHolder viewHolder;
    Typeface typeface;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<NavigationViewItem> mItems;

    public NavigationViewRecyclerViewAdapter(Context context, ArrayList<NavigationViewItem> items) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.navigation_view_list_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();
        Picasso.with(mContext).load(mItems.get(position).icon).into(ordinaryHolder.listItemImageView);
        ordinaryHolder.listItemTextView.setTypeface(typeface);
        if (position == 7) {
            if (Singleton.getInstance().currentUser == null) {
                ordinaryHolder.listItemTextView.setText(mContext.getResources().getString(R.string.login));
            } else {
                ordinaryHolder.listItemTextView.setText(mContext.getResources().getString(R.string.logout));
            }
        } else {
            ordinaryHolder.listItemTextView.setText(mItems.get(position).name);
        }

    }

    @Override
    public int getItemCount() {
        return mItems.size() - 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.listItemImageView)
        ImageView listItemImageView;

        @InjectView(R.id.listItemTextView)
        TextView listItemTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (getAdapterPosition()) {
                        case 0: // Home
                            if (mContext instanceof HomeActivity) {
                                ((HomeActivity) mContext).closeDrawer();
                            } else {
                                Intent intent = new Intent(mContext, HomeActivity.class);
                                mContext.startActivity(intent);
                                ((AbstractActivity) mContext).closeDrawer();
                            }
                            break;

                        case 1: // All Products
                            if (mContext instanceof AllProductsActivity) {
                                ((AllProductsActivity) mContext).closeDrawer();
                            } else {
                                Intent intent = new Intent(mContext, AllProductsActivity.class);
                                mContext.startActivity(intent);
                                ((AbstractActivity) mContext).closeDrawer();
                            }
                            break;

                        case 2: //New Arrivals
                            if (mContext instanceof NewArrivalsActivity) {
                                ((NewArrivalsActivity) mContext).closeDrawer();
                            } else {
                                Intent intent = new Intent(mContext, NewArrivalsActivity.class);
                                mContext.startActivity(intent);
                                ((AbstractActivity) mContext).closeDrawer();
                            }
                            break;

                        case 3: //Events
                            if (mContext instanceof EventsActivity) {
                                ((EventsActivity) mContext).closeDrawer();
                            } else {
                                Intent intent = new Intent(mContext, EventsActivity.class);
                                mContext.startActivity(intent);
                                ((AbstractActivity) mContext).closeDrawer();
                            }
                            break;

                        case 4: //Language
                            Language.changeLanguage(mContext);
                            ((AbstractActivity) mContext).closeDrawer();
                            break;

                        case 5: //Contact us
                            if (mContext instanceof ContactUsActivity) {
                                ((ContactUsActivity) mContext).closeDrawer();
                            } else {
                                Intent intent = new Intent(mContext, ContactUsActivity.class);
                                mContext.startActivity(intent);
                                ((AbstractActivity) mContext).closeDrawer();
                            }
                            break;

                        case 6: // More
                            if (mContext instanceof MoreActivity) {
                                ((MoreActivity) mContext).closeDrawer();
                            } else {
                                Intent intent = new Intent(mContext, MoreActivity.class);
                                mContext.startActivity(intent);
                                ((AbstractActivity) mContext).closeDrawer();
                            }
                            ((AbstractActivity) mContext).closeDrawer();

                            break;

                        case 7: // Login or logout
                            if (listItemTextView.getText().toString().equals(mContext.getResources().getString(R.string.login))) {
                                Intent intent = new Intent(mContext, LoginActivity.class);
                                mContext.startActivity(intent);
                            } else {
                                logout();
                            }
                            break;

                        default:
                            break;
                    }
                }
            });
        }

        public void emptyViewHolder() {
            listItemImageView.setImageBitmap(null);
            listItemTextView.setText("");
        }

        private void logout() {

            Singleton.getInstance().currentUser = null;
            Singleton.getInstance().token = null;


            SharedPreferences settings = mContext.getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("isLogged", false);

            if (settings.getBoolean("isFacebookLogged", false)) {
                AccessToken.setCurrentAccessToken(null);
                Profile.setCurrentProfile(null);
            }

            editor.putBoolean("isFacebookLogged", false);
            editor.putBoolean("isGetFavourites", false);
            editor.putBoolean("isGetWishList", false);
            editor.putBoolean("isGetOrders", false);
            editor.putBoolean("isGetRemainders", false);
            editor.remove("name");
            editor.remove("address");
            editor.remove("username");
            editor.remove("email");
            editor.remove("id");
            int phoneNumber = settings.getInt("phoneNumber", 0);
            for (int i = 0; i < phoneNumber; i++) {
                editor.remove("number" + i);
                editor.remove("number" + i + "isPrimary");
            }
            editor.remove("phoneNumber");
            editor.apply();


            FavouritesController favouritesController = new FavouritesController(mContext);
            Bundle bundle1 = new Bundle();
            bundle1.putInt("action", FavouritesController.DELETE_ALL_FAVOURITES_DATABASE_ACTION);
            favouritesController.request(FavouritesController.DELETE_ALL_FAVOURITES_DATABASE_ACTION, bundle1);

            WishListController wishListController = new WishListController(mContext);
            Bundle bundle = new Bundle();
            bundle.putInt("action", WishListController.DELETE_ALL_WISH_LIST_DATABASE_ACTION);
            wishListController.request(WishListController.DELETE_ALL_WISH_LIST_DATABASE_ACTION, bundle);

            OrderController orderController = new OrderController(mContext);
            Bundle bundle2 = new Bundle();
            bundle2.putInt("action", OrderController.DELETE_ALL_ORDER_DATABASE_ACTION);
            orderController.request(OrderController.DELETE_ALL_ORDER_DATABASE_ACTION, bundle2);

            ExtrasController extrasController = new ExtrasController(mContext);
            Bundle bundle3 = new Bundle();
            bundle3.putInt("action", ExtrasController.DELETE_ALL_REMAINDERS_DATABASE_ACTION);
            extrasController.request(ExtrasController.DELETE_ALL_REMAINDERS_DATABASE_ACTION, bundle3);

            Intent intent = new Intent(mContext, NotificationsService.class);
            NotificationsService.isFromMyApp = true;
            mContext.stopService(intent);

            int counter = Singleton.activityStack.size();
            for (int i = counter - 1; i >= 0; i--) {
                AbstractActivity abstractActivity = Singleton.activityStack.get(i);
                abstractActivity.finish();
                if (i == 0) {
                    Intent startAgain = new Intent(mContext, HomeActivity.class);
                    mContext.startActivity(startAgain);
                }
            }
        }
    }
}
