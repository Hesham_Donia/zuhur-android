package com.bridge.zuhur.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.NavigationViewItem;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.interfaces.ObserverListener;
import com.bridge.zuhur.interfaces.ViewsListener;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.utils.NetworkConnection;
import com.bridge.zuhur.utils.NetworkStateReceiver;
import com.bridge.zuhur.views.adapters.NavigationViewRecyclerViewAdapter;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
abstract public class AbstractActivity extends AppCompatActivity implements ViewsListener, ObserverListener {

    final static int COUNTRIES_REQUEST_CODE = 1;
    public boolean isInForegroundMode;
    protected Toolbar mToolbar;
    protected DrawerLayout mDrawer;
    protected ActionBarDrawerToggle mToggle;
    protected NavigationView navView;
    protected RelativeLayout mLoadingContainer;
    protected Button mCartItemsCount;
    protected RecyclerView mNavigationViewRecyclerView;
    protected Button mCountriesButton;
    private NetworkStateReceiver stateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.loadLanguage(this);
        Singleton.getInstance().currentActivity = this;
        Singleton.activityStack.push(this);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "stack size is: " + Singleton.activityStack.size());
        stateReceiver = new NetworkStateReceiver();
    }

    @Override
    public void update(boolean internetConnectionExist) {
        if (internetConnectionExist) {
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "Connected from activity");
//            hideNoConnectionContainer();
        } else {
//            showNoConnectionContainer();
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "not Connected from activity");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isInForegroundMode = true;
        stateReceiver.register(this);
        if (Singleton.getInstance().country != null) {
            if (Language.getCurrentLang(this).equals("en")) {
                mCountriesButton.setText(Singleton.getInstance().country.englishName);
            } else {
                mCountriesButton.setText(Singleton.getInstance().country.arabicName);
            }
        }

        if (NetworkConnection.networkConnectivity(this)) {
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "Connected from activity");
//            hideNoConnectionContainer();
        } else {
//            showNoConnectionContainer();
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "not Connected from activity");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInForegroundMode = false;

    }

    @Override
    protected void onStart() {
        super.onStart();
        updateCartItemsCount();
        if (Singleton.getInstance().country != null) {
            if (Language.getCurrentLang(this).equals("en")) {
                mCountriesButton.setText(Singleton.getInstance().country.englishName);
            } else {
                mCountriesButton.setText(Singleton.getInstance().country.arabicName);
            }
        }
        mCountriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AbstractActivity.this, CountriesActivity.class);
                startActivityForResult(intent, COUNTRIES_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        stateReceiver.unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Singleton.activityStack.pop();
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "stack size is: " + Singleton.activityStack.size());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Singleton.getInstance().currentActivity = this;
        mNavigationViewRecyclerView.getAdapter().notifyDataSetChanged();
        if (Singleton.getInstance().country != null) {
            if (Language.getCurrentLang(this).equals("en")) {
                mCountriesButton.setText(Singleton.getInstance().country.englishName);
            } else {
                mCountriesButton.setText(Singleton.getInstance().country.arabicName);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onBackPressed() {

        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void closeDrawer() {
        mDrawer.closeDrawer(GravityCompat.START);
    }

    public void updateCartItemsCount() {
        if (mCartItemsCount != null)
            mCartItemsCount.setText("" + Singleton.getInstance().cart.size());
    }

    protected void setupNavigationView() {
        mNavigationViewRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<NavigationViewItem> navigationViewItems = new ArrayList<>();
        navigationViewItems.add(new NavigationViewItem(R.drawable.home, getResources().getString(R.string.home)));
        navigationViewItems.add(new NavigationViewItem(R.drawable.all_products, getResources().getString(R.string.allProducts)));
        navigationViewItems.add(new NavigationViewItem(R.drawable.new_arrivals, getResources().getString(R.string.newArrival)));
        navigationViewItems.add(new NavigationViewItem(R.drawable.events, getResources().getString(R.string.events)));
        navigationViewItems.add(new NavigationViewItem(R.drawable.language, getResources().getString(R.string.language)));
        navigationViewItems.add(new NavigationViewItem(R.drawable.contact_us, getResources().getString(R.string.contactUs)));
        navigationViewItems.add(new NavigationViewItem(R.drawable.more, getResources().getString(R.string.more)));
        navigationViewItems.add(new NavigationViewItem(R.drawable.auth, getResources().getString(R.string.login)));
        navigationViewItems.add(new NavigationViewItem(R.drawable.auth, getResources().getString(R.string.logout)));

        NavigationViewRecyclerViewAdapter navigationViewRecyclerViewAdapter = new NavigationViewRecyclerViewAdapter(this, navigationViewItems);
        mNavigationViewRecyclerView.setAdapter(navigationViewRecyclerViewAdapter);
    }
//
//    public void hideLoadingView() {
//        mLoadingView.setVisibility(View.GONE);
//    }
//
//    public void showNoConnectionContainer() {
//        mNoConnectionBar.setVisibility(View.VISIBLE);
//        mNoConnectionBar.bringToFront();
//    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, 0);
    }
//
//    public void hideNoConnectionContainer() {
//        mNoConnectionBar.setVisibility(View.GONE);
//    }
//
//    public void showMainLayoutContainer() {
//        mMainLayoutContainer.setVisibility(View.VISIBLE);
//    }
//
//    public void hideMainLayoutContainer() {
//        mMainLayoutContainer.setVisibility(View.GONE);
//    }
//
//    public boolean isMainLayoutContainerVisible() {
//        return mMainLayoutContainer.getVisibility() == View.VISIBLE;
//    }


}
