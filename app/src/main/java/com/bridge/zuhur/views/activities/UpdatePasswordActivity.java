package com.bridge.zuhur.views.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.AuthenticationController;
import com.bridge.zuhur.entities.Entity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class UpdatePasswordActivity extends AbstractActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.loadingContainer)
    RelativeLayout relativeLayout;

    @InjectView(R.id.cartIconImageView)
    ImageView cartIconImageView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.oldPasswordEditText)
    EditText oldPasswordEditText;

    @InjectView(R.id.newPasswordEditText)
    EditText newPasswordEditText;

    @InjectView(R.id.updatePasswordCardView)
    CardView updatePasswordCardView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.countryButton)
    Button countryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);

        ButterKnife.inject(this);

        setToolbar();
        setDrawerLayout();
        setNavigationView();
        setLoadingContainer();
        setCartItemsCount();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setCountriesButton();
        countryButton.setVisibility(View.GONE);

        setSupportActionBar(mToolbar);

        titleTextView.setText(getResources().getString(R.string.updatePassword));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setVisibility(View.INVISIBLE);
        cartIconImageView.setVisibility(View.INVISIBLE);
        cartItemsCount.setVisibility(View.INVISIBLE);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/arcena.ttf");
        oldPasswordEditText.setTypeface(typeface);
        newPasswordEditText.setTypeface(typeface);

        updatePasswordCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (oldPasswordEditText.getText().toString().equals("") || newPasswordEditText.getText().toString().equals("")) {
                    Toast.makeText(UpdatePasswordActivity.this, getResources().getString(R.string.emptyFieldsError), Toast.LENGTH_SHORT).show();
                } else {

                    relativeLayout.setVisibility(View.VISIBLE);
                    AuthenticationController authenticationController = new AuthenticationController(UpdatePasswordActivity.this);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("action", AuthenticationController.UPDATE_PASSWORD_ACTION);
                    bundle1.putString("password", oldPasswordEditText.getText().toString());
                    bundle1.putString("newPassword", newPasswordEditText.getText().toString());
                    authenticationController.request(AuthenticationController.UPDATE_PASSWORD_ACTION, bundle1);
                }
            }
        });
    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    private void setLoadingContainer() {
        mLoadingContainer = relativeLayout;
    }

    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {
        if (result == null) {
            Toast.makeText(this, getResources().getString(R.string.updatePasswordError), Toast.LENGTH_LONG).show();
            oldPasswordEditText.setText("");
            newPasswordEditText.setText("");
        } else {
            Toast.makeText(this, getResources().getString(R.string.updatePasswordSuccess), Toast.LENGTH_LONG).show();
            finish();
        }
    }
}
