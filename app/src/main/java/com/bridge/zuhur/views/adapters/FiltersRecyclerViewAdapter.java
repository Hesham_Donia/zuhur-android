package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.database.Tag;
import com.bridge.zuhur.views.activities.FilterOptionsActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 6/28/16.
 */
public class FiltersRecyclerViewAdapter extends RecyclerView.Adapter<FiltersRecyclerViewAdapter.ViewHolder> {

    Typeface typeface;
    ViewHolder viewHolder;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<Tag> mTags;

    public FiltersRecyclerViewAdapter(Context context, ArrayList<Tag> tags) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mTags = tags;
    }

    @Override
    public FiltersRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.filter_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FiltersRecyclerViewAdapter.ViewHolder holder, final int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        ordinaryHolder.filterNameTextView.setText(mTags.get(position).getTag().name);
        ordinaryHolder.filterNameTextView.setTypeface(typeface);
        ordinaryHolder.deleteFilterImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FilterOptionsActivity) mContext).updateOccasionsRecyclerView(mTags.get(position));
                ((FilterOptionsActivity) mContext).removeFromTagsArrayList(mTags.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTags.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.filterNameTextView)
        TextView filterNameTextView;

        @InjectView(R.id.deleteFilterImageView)
        ImageView deleteFilterImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }

        public void emptyViewHolder() {
            filterNameTextView.setText("");
        }
    }
}
