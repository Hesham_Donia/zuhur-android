package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.database.Tag;
import com.bridge.zuhur.views.activities.FilterOptionsActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 6/19/16.
 */
public class OccasionsRecyclerViewAdapter extends RecyclerView.Adapter<OccasionsRecyclerViewAdapter.ViewHolder> {


    ViewHolder viewHolder;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<Tag> mTags;


    public OccasionsRecyclerViewAdapter(Context context, ArrayList<Tag> tags) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mTags = tags;
    }


    @Override
    public OccasionsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.tags_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(OccasionsRecyclerViewAdapter.ViewHolder holder, final int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        ordinaryHolder.tagNameTextView.setText(mTags.get(position).getTag().name);
        ordinaryHolder.tagItemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ((FilterOptionsActivity) mContext).addToTagsArrayList(mTags.get(position));
                } else {
                    ((FilterOptionsActivity) mContext).removeFromTagsArrayList(mTags.get(position));
                }
                mTags.get(position).setChecked(isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTags.size();
    }

    public void updateRecyclerView(Tag tag) {
        for (int i = 0; i < mTags.size(); i++) {
            if (tag == mTags.get(i)) {
                mTags.get(i).setChecked(false);
                notifyItemChanged(i);
            }
        }
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.tagItemCheckBox)
        CheckBox tagItemCheckBox;

        @InjectView(R.id.tagNameTextView)
        TextView tagNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tagItemCheckBox.isChecked()) {
                        tagItemCheckBox.setChecked(false);
                    } else {
                        tagItemCheckBox.setChecked(true);
                    }
                }
            });
        }

        public void emptyViewHolder() {
            tagItemCheckBox.setOnCheckedChangeListener(null);
            if (mTags.get(getAdapterPosition()).isChecked()) {
                tagItemCheckBox.setChecked(true);
            } else {
                tagItemCheckBox.setChecked(false);
            }
            tagNameTextView.setText("");
        }
    }
}
