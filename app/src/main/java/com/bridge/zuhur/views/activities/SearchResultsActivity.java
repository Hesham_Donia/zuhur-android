package com.bridge.zuhur.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.entities.Country;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.utils.Sensor;
import com.bridge.zuhur.views.adapters.SearchResultsRecyclerViewAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SearchResultsActivity extends AbstractActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.loadingContainer)
    RelativeLayout relativeLayout;

    @InjectView(R.id.cartIconImageView)
    ImageView cartIconImageView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    @InjectView(R.id.productsRecyclerView)
    RecyclerView productsRecyclerView;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.countryButton)
    Button countryButton;

    String keyword = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        ButterKnife.inject(this);

        keyword = getIntent().getExtras().getString("keyword");

        setToolbar();
        setDrawerLayout();
        setNavigationView();
        setLoadingContainer();
        setCartItemsCount();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setCountriesButton();
        setSupportActionBar(mToolbar);

        titleTextView.setText(getResources().getString(R.string.searchRes));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        relativeLayout.setVisibility(View.VISIBLE);
        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setVisibility(View.VISIBLE);
                titleTextView.setVisibility(View.GONE);

                searchEditText.requestFocus();
                searchEditText.setThreshold(2);
                searchEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        ProductsController productsController = new ProductsController(SearchResultsActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", ProductsController.GET_AUTO_COMPLETE_DATABASE);
                        bundle1.putString("keyword", s.toString());
                        productsController.request(ProductsController.GET_AUTO_COMPLETE_DATABASE, bundle1);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            relativeLayout.setVisibility(View.VISIBLE);

                            productsRecyclerView.setAdapter(new SearchResultsRecyclerViewAdapter(SearchResultsActivity.this, new ArrayList<Product>()));

                            ProductsController productsController = new ProductsController(SearchResultsActivity.this);
                            Bundle bundle1 = new Bundle();
                            bundle1.putInt("action", ProductsController.SEARCH_BY_NAME_ACTION);
                            bundle1.putString("sort", CommonConstants.SORT_PRICE_LOW_TO_HIGH);
                            bundle1.putString("keyWord", searchEditText.getText().toString());
                            bundle1.putInt("countryId", Singleton.getInstance().country.id);
                            productsController.request(ProductsController.SEARCH_BY_NAME_ACTION, bundle1);
                            hideKeyboard(searchEditText);
                            return true;
                        }
                        return false;
                    }
                });
            }
        });

        cartIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Singleton.getInstance().cart.size() > 0) {
                    Intent intent = new Intent(SearchResultsActivity.this, ResetActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(SearchResultsActivity.this, getResources().getString(R.string.cartEmpty), Toast.LENGTH_LONG).show();
                }
            }
        });

        ProductsController productsController = new ProductsController(SearchResultsActivity.this);
        Bundle bundle1 = new Bundle();
        bundle1.putInt("action", ProductsController.SEARCH_BY_NAME_ACTION);
        bundle1.putString("sort", CommonConstants.SORT_PRICE_LOW_TO_HIGH);
        bundle1.putString("keyWord", keyword);
        bundle1.putInt("countryId", Singleton.getInstance().country.id);
        productsController.request(ProductsController.SEARCH_BY_NAME_ACTION, bundle1);

    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    private void setLoadingContainer() {
        mLoadingContainer = relativeLayout;
    }

    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {
        ArrayList<Product> products = (ArrayList<Product>) result;
        SearchResultsRecyclerViewAdapter searchResultsRecyclerViewAdapter = new SearchResultsRecyclerViewAdapter(this, products);
        productsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        productsRecyclerView.setAdapter(searchResultsRecyclerViewAdapter);
        relativeLayout.setVisibility(View.GONE);
        Sensor.unlockSensor(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == COUNTRIES_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {
                Bundle bundle = data.getExtras();
                if (bundle.containsKey("country")) {
                    SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                            Context.MODE_PRIVATE);
                    Singleton.getInstance().country = (Country) bundle.getSerializable("country");
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt("countryId", Singleton.getInstance().country.id);
                    editor.putString("countryEnglishName", Singleton.getInstance().country.englishName);
                    editor.putString("countryArabicName", Singleton.getInstance().country.arabicName);
                    editor.putString("countryCurrency", Singleton.getInstance().country.currency);
                    editor.putFloat("countryDollarValue", (float) Singleton.getInstance().country.dollarValue);
                    editor.commit();

                    if (Language.getCurrentLang(this).equals("en")) {
                        mCountriesButton.setText(Singleton.getInstance().country.englishName);
                    } else {
                        mCountriesButton.setText(Singleton.getInstance().country.arabicName);
                    }

                    relativeLayout.setVisibility(View.VISIBLE);

                    ProductsController productsController = new ProductsController(SearchResultsActivity.this);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("action", ProductsController.SEARCH_BY_NAME_ACTION);
                    bundle1.putString("sort", CommonConstants.SORT_PRICE_LOW_TO_HIGH);
                    bundle1.putString("keyWord", keyword);
                    bundle1.putInt("countryId", Singleton.getInstance().country.id);
                    productsController.request(ProductsController.SEARCH_BY_NAME_ACTION, bundle1);

                }
            }
        }
    }
}
