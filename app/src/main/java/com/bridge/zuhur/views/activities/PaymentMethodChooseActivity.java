package com.bridge.zuhur.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bridge.zuhur.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class PaymentMethodChooseActivity extends Activity {

    @InjectView(R.id.paymentMethodsRadioGroup)
    RadioGroup paymentMethodRadioGroup;

    @InjectView(R.id.confirmPaymentMethodChooseButton)
    Button confirmPaymentMethodChooseButton;

    int selectedPaymentMethod = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method_choose);

        ButterKnife.inject(this);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);


        getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        paymentMethodRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.pay1RadioButton: //cash
                        selectedPaymentMethod = 1;
                        break;

                    case R.id.pay2RadioButton: //paybal
                        selectedPaymentMethod = 2;
                        break;

                    case R.id.pay3RadioButton: //credit
                        selectedPaymentMethod = 3;
                        break;

                    default:
                        break;
                }
            }
        });

        confirmPaymentMethodChooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPaymentMethod != 0) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putInt("method", selectedPaymentMethod);
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(PaymentMethodChooseActivity.this, getResources().getString(R.string.payentMethodError), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
