package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.FavouritesController;
import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.database.LogData;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.utils.NetworkConnection;
import com.bridge.zuhur.views.activities.ProductLandingPageActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class FavouritesRecyclerViewAdapter extends RecyclerView.Adapter<FavouritesRecyclerViewAdapter.ViewHolder> {


    Typeface typeface;
    ViewHolder viewHolder;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<Favourite> mFavourites;

    public FavouritesRecyclerViewAdapter(Context context, ArrayList<Favourite> favourites) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mFavourites = favourites;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.favourite_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        Picasso.with(mContext).load(mFavourites.get(position).getProduct().imagePath).into(ordinaryHolder.favouriteImageView);
        if (Language.getCurrentLang(mContext).equals("en")) {
            ordinaryHolder.favouriteProductNameTextView.setText(mFavourites.get(position).getProduct().englishName);
            ordinaryHolder.favouriteProductDescriptionTextView.setText(mFavourites.get(position).getProduct().englishDescription);
        } else {
            ordinaryHolder.favouriteProductNameTextView.setText(mFavourites.get(position).getProduct().arabicName);
            ordinaryHolder.favouriteProductDescriptionTextView.setText(mFavourites.get(position).getProduct().arabicDescription);
        }

        ordinaryHolder.favouriteProductNameTextView.setTypeface(typeface);
        ordinaryHolder.favouriteProductDescriptionTextView.setTypeface(typeface);

        ordinaryHolder.deleteFavouriteProductImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FavouritesController favouritesController1 = new FavouritesController(mContext);
                Bundle bundle = new Bundle();
                bundle.putInt("action", FavouritesController.DELETE_FAVOURITE_DATABASE_ACTION);
                bundle.putSerializable("id", mFavourites.get(position).getProductId());
                favouritesController1.request(FavouritesController.DELETE_FAVOURITE_DATABASE_ACTION, bundle);

                if (NetworkConnection.networkConnectivity(mContext)) {
                    FavouritesController favouritesController = new FavouritesController(mContext);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("action", FavouritesController.DELETE_FAVOURITE_SERVER_ACTION);
                    bundle1.putSerializable("productId", mFavourites.get(position).getProductId());
                    favouritesController.request(FavouritesController.DELETE_FAVOURITE_SERVER_ACTION, bundle1);
                } else {
                    LogData logData = new LogData(mFavourites.get(position), CommonConstants.DELETE_OPERATION, CommonConstants.FAVOURITE_TYPE);
                    ExtrasController extrasController = new ExtrasController(mContext);
                    Bundle logDataBundle = new Bundle();
                    logDataBundle.putSerializable("logData", logData);
                    logDataBundle.putInt("action", ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION);
                    extrasController.request(ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION, logDataBundle);
                }

                mFavourites.remove(position);

                notifyItemRemoved(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFavourites.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.favouriteProductImageView)
        ImageView favouriteImageView;

        @InjectView(R.id.favouriteProductNameTextView)
        TextView favouriteProductNameTextView;

        @InjectView(R.id.favouriteProductDescriptionTextView)
        TextView favouriteProductDescriptionTextView;

        @InjectView(R.id.deleteFavouriteProductImageView)
        ImageView deleteFavouriteProductImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ProductLandingPageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("product", mFavourites.get(getAdapterPosition()).getProduct());
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }

        public void emptyViewHolder() {
            favouriteImageView.setImageBitmap(null);
            favouriteProductNameTextView.setText("");
            favouriteProductDescriptionTextView.setText("");
        }
    }
}
