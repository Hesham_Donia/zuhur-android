package com.bridge.zuhur.views.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.interfaces.ViewsListener;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.views.adapters.RelatedProductsRecyclerViewAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class RelatedProductsActivity extends Activity implements ViewsListener {


    @InjectView(R.id.relatedProductsRecyclerView)
    RecyclerView relatedProductsRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_related_products);
        ButterKnife.inject(this);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);


        getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        int count = getIntent().getExtras().getInt("count");
        ArrayList<Product> products = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            products.add((Product) getIntent().getExtras().getSerializable("product-" + i));
            Log.d(CommonConstants.APPLICATION_LOG_TAG, products.get(i).arabicName);
        }
        RelatedProductsRecyclerViewAdapter relatedProductsRecyclerViewAdapter = new RelatedProductsRecyclerViewAdapter(this, products);
        relatedProductsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        relatedProductsRecyclerView.setAdapter(relatedProductsRecyclerViewAdapter);
        relatedProductsRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {

    }
}
