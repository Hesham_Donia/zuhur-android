package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.FavouritesController;
import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.database.LogData;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.NetworkConnection;
import com.bridge.zuhur.views.activities.AllProductsActivity;
import com.bridge.zuhur.views.activities.ProductLandingPageActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 5/28/16.
 */
public class AllProductsRecyclerViewAdapter extends RecyclerView.Adapter<AllProductsRecyclerViewAdapter.ViewHolder> {

    Typeface typeface;
    ViewHolder viewHolder;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<Product> mProducts, mLeftProducts = new ArrayList<>(), mRightProducts = new ArrayList<>();


    public AllProductsRecyclerViewAdapter(Context context, ArrayList<Product> products) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mProducts = products;
        for (int i = 0; i < mProducts.size(); i++) {
            if (i % 2 == 0) {
                mLeftProducts.add(mProducts.get(i));
            } else {
                mRightProducts.add(mProducts.get(i));
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.category_product_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        if (mRightProducts.size() != 0 && position < mRightProducts.size()) {
            Picasso.with(mContext).load(mRightProducts.get(position).imagePath).into(ordinaryHolder.rightProductImageView);
            if (Singleton.getInstance().currentLanguage.equals("en")) {
                ordinaryHolder.rightProductTextView.setText(mRightProducts.get(position).englishName + "\n" + mRightProducts.get(position).price + " " + Singleton.getInstance().country.currency);
            } else {
                ordinaryHolder.rightProductTextView.setText(mRightProducts.get(position).arabicName + "\n" + mRightProducts.get(position).price + " " + Singleton.getInstance().country.currency);
            }

            ordinaryHolder.rightProductTextView.setTypeface(typeface);

            ordinaryHolder.rightCartImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isExist = false;

                    for (Product product : Singleton.getInstance().cart) {
                        if (product.id == mRightProducts.get(position).id) {
                            isExist = true;
                        }
                    }

                    if (isExist) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.addCartError), Toast.LENGTH_LONG).show();
                    } else {
                        Singleton.getInstance().cart.add(mRightProducts.get(position));
                        ((AllProductsActivity) mContext).updateCartItemsCount();
                    }

                }
            });

            ordinaryHolder.rightFavouriteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (Singleton.getInstance().currentUser == null) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                    } else {
                        //add to local favourites
                        Favourite favourite = new Favourite(mRightProducts.get(position));

                        FavouritesController favouritesController = new FavouritesController(mContext);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", FavouritesController.ADD_FAVOURITE_DATABASE_ACTION);
                        bundle1.putSerializable("favourite", favourite);
                        favouritesController.request(FavouritesController.ADD_FAVOURITE_DATABASE_ACTION, bundle1);

                        // add to online
                        if (NetworkConnection.networkConnectivity(mContext)) {
                            FavouritesController favouritesController1 = new FavouritesController(mContext);
                            Bundle bundle2 = new Bundle();
                            bundle2.putInt("action", FavouritesController.ADD_FAVOURITE_TO_SERVER_ACTION);
                            bundle2.putSerializable("favourite", favourite);
                            favouritesController1.request(FavouritesController.ADD_FAVOURITE_TO_SERVER_ACTION, bundle1);
                        } else {
                            LogData logData = new LogData(favourite, CommonConstants.ADD_OPERATION, CommonConstants.FAVOURITE_TYPE);
                            ExtrasController extrasController = new ExtrasController(mContext);
                            Bundle logDataBundle = new Bundle();
                            logDataBundle.putSerializable("logData", logData);
                            logDataBundle.putInt("action", ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION);
                            extrasController.request(ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION, logDataBundle);
                        }
                    }
                }
            });

            ordinaryHolder.rightProductCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ProductLandingPageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("product", mRightProducts.get(position));
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        } else {
            ordinaryHolder.rightProductCardView.setVisibility(View.INVISIBLE);
        }

        if (mLeftProducts.size() != 0 && position < mLeftProducts.size()) {
            Picasso.with(mContext).load(mLeftProducts.get(position).imagePath).into(ordinaryHolder.leftProductImageView);
            if (Singleton.getInstance().currentLanguage.equals("en")) {
                ordinaryHolder.leftProductTextView.setText(mLeftProducts.get(position).englishName + "\n" + mLeftProducts.get(position).price + " " + Singleton.getInstance().country.currency);
            } else {
                ordinaryHolder.leftProductTextView.setText(mLeftProducts.get(position).arabicName + "\n" + mLeftProducts.get(position).price + " " + Singleton.getInstance().country.currency);
            }

            ordinaryHolder.leftProductTextView.setTypeface(typeface);

            ordinaryHolder.leftCartImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isExist = false;

                    for (Product product : Singleton.getInstance().cart) {
                        if (product.id == mLeftProducts.get(position).id) {
                            isExist = true;
                        }
                    }

                    if (isExist) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.addCartError), Toast.LENGTH_LONG).show();
                    } else {
                        Singleton.getInstance().cart.add(mLeftProducts.get(position));
                        ((AllProductsActivity) mContext).updateCartItemsCount();
                    }

                }
            });

            ordinaryHolder.leftFavouriteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (Singleton.getInstance().currentUser == null) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                    } else {
                        Favourite favourite = new Favourite(mLeftProducts.get(position));

                        FavouritesController favouritesController = new FavouritesController(mContext);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", FavouritesController.ADD_FAVOURITE_DATABASE_ACTION);
                        bundle1.putSerializable("favourite", favourite);
                        favouritesController.request(FavouritesController.ADD_FAVOURITE_DATABASE_ACTION, bundle1);

                        // add to online
                        if (NetworkConnection.networkConnectivity(mContext)) {
                            FavouritesController favouritesController1 = new FavouritesController(mContext);
                            Bundle bundle2 = new Bundle();
                            bundle2.putInt("action", FavouritesController.ADD_FAVOURITE_TO_SERVER_ACTION);
                            bundle2.putSerializable("favourite", favourite);
                            favouritesController1.request(FavouritesController.ADD_FAVOURITE_TO_SERVER_ACTION, bundle1);
                        } else {
                            Log.d("synch", "inside calling log data controller");
                            LogData logData = new LogData(favourite, CommonConstants.ADD_OPERATION, CommonConstants.FAVOURITE_TYPE);
                            ExtrasController extrasController = new ExtrasController(mContext);
                            Bundle logDataBundle = new Bundle();
                            logDataBundle.putSerializable("logData", logData);
                            logDataBundle.putInt("action", ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION);
                            extrasController.request(ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION, logDataBundle);
                        }
                    }

                }
            });

            ordinaryHolder.leftProductCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ProductLandingPageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("product", mLeftProducts.get(position));
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        } else {
            ordinaryHolder.leftProductCardView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if (mProducts.size() % 2 == 0) {
            return mProducts.size() / 2;
        } else {
            return (mProducts.size() + 1) / 2;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.leftProductCardView)
        CardView leftProductCardView;

        @InjectView(R.id.leftProductImageView)
        ImageView leftProductImageView;

        @InjectView(R.id.leftFavouriteImageView)
        ImageView leftFavouriteImageView;

        @InjectView(R.id.leftCartImageView)
        ImageView leftCartImageView;

        @InjectView(R.id.leftProductTextView)
        TextView leftProductTextView;

        @InjectView(R.id.rightProductCardView)
        CardView rightProductCardView;

        @InjectView(R.id.rightProductImageView)
        ImageView rightProductImageView;

        @InjectView(R.id.rightFavouriteImageView)
        ImageView rightFavouriteImageView;

        @InjectView(R.id.rightCartImageView)
        ImageView rightCartImageView;

        @InjectView(R.id.rightProductTextView)
        TextView rightProductTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);

        }

        public void emptyViewHolder() {
            leftProductCardView.setVisibility(View.VISIBLE);
            rightProductCardView.setVisibility(View.VISIBLE);
            leftProductImageView.setImageBitmap(null);
            rightProductImageView.setImageBitmap(null);
            leftProductTextView.setText("");
            rightProductTextView.setText("");
        }
    }
}
