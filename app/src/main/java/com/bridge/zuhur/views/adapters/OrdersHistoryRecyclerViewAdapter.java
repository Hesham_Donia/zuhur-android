package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.database.OrdersHistory;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.views.activities.OrderDetailsActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 6/8/16.
 */
public class OrdersHistoryRecyclerViewAdapter extends RecyclerView.Adapter<OrdersHistoryRecyclerViewAdapter.ViewHolder> {


    Typeface typeface;
    ViewHolder viewHolder;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<OrdersHistory> mOrdersHistory;

    public OrdersHistoryRecyclerViewAdapter(Context context, ArrayList<OrdersHistory> ordersHistory) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mOrdersHistory = ordersHistory;
    }

    @Override
    public OrdersHistoryRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.order_history_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(OrdersHistoryRecyclerViewAdapter.ViewHolder holder, int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        ordinaryHolder.orderCreationDateTextView.setText(mOrdersHistory.get(position).getOrder().creationDate);
        ordinaryHolder.orderNumberTextView.setText("# " + mOrdersHistory.get(position).getOrder().id);
        ordinaryHolder.orderTotalPriceTextView.setText(mOrdersHistory.get(position).getOrder().finalPrice + " " + Singleton.getInstance().country.currency);
        ordinaryHolder.orderCreationDateTextView.setTypeface(typeface);
        ordinaryHolder.orderNumberTextView.setTypeface(typeface);
        ordinaryHolder.orderTotalPriceTextView.setTypeface(typeface);
    }

    @Override
    public int getItemCount() {
        return mOrdersHistory.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.orderCreationDateTextView)
        TextView orderCreationDateTextView;

        @InjectView(R.id.orderNumberTextView)
        TextView orderNumberTextView;

        @InjectView(R.id.orderTotalPriceTextView)
        TextView orderTotalPriceTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("order", mOrdersHistory.get(getAdapterPosition()).getOrder());
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }

        public void emptyViewHolder() {
            orderCreationDateTextView.setText("");
            orderNumberTextView.setText("");
            orderTotalPriceTextView.setText("");
        }
    }
}
