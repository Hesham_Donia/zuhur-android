package com.bridge.zuhur.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Country;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.Call;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ContactUsActivity extends AbstractActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.cartIconImageView)
    ImageView cartIconImageView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.emailCardView)
    CardView emailCardView;

    @InjectView(R.id.emailTextView)
    TextView emailTextView;

    @InjectView(R.id.callCardView)
    CardView callCardView;

    @InjectView(R.id.callTextView)
    TextView callTextView;

    @InjectView(R.id.websiteCardView)
    CardView websiteCardView;

    @InjectView(R.id.websiteTextView)
    TextView websiteTextView;

    @InjectView(R.id.countryButton)
    Button countryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        ButterKnife.inject(this);

        setToolbar();
        setDrawerLayout();
        setNavigationView();
        setCartItemsCount();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setCountriesButton();

        setSupportActionBar(mToolbar);

        titleTextView.setText(getResources().getString(R.string.contactUs));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setVisibility(View.INVISIBLE);
        cartIconImageView.setVisibility(View.INVISIBLE);
        cartItemsCount.setVisibility(View.INVISIBLE);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/arcena.ttf");
        emailTextView.setTypeface(typeface);
        callTextView.setTypeface(typeface);
        websiteTextView.setTypeface(typeface);

        emailCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL, emailTextView.getText().toString());
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                intent.setType("text/plain");
                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });

        callCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call.makeCall(ContactUsActivity.this, callTextView.getText().toString());
            }
        });

        websiteCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://" + websiteTextView.getText().toString()));
                startActivity(i);
            }
        });

    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }


    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == COUNTRIES_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {
                Bundle bundle = data.getExtras();
                if (bundle.containsKey("country")) {
                    SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                            Context.MODE_PRIVATE);
                    Singleton.getInstance().country = (Country) bundle.getSerializable("country");
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt("countryId", Singleton.getInstance().country.id);
                    editor.putString("countryEnglishName", Singleton.getInstance().country.englishName);
                    editor.putString("countryArabicName", Singleton.getInstance().country.arabicName);
                    editor.putString("countryCurrency", Singleton.getInstance().country.currency);
                    editor.putFloat("countryDollarValue", (float) Singleton.getInstance().country.dollarValue);
                    editor.commit();

                    if (Language.getCurrentLang(this).equals("en")) {
                        mCountriesButton.setText(Singleton.getInstance().country.englishName);
                    } else {
                        mCountriesButton.setText(Singleton.getInstance().country.arabicName);
                    }

                }
            }
        }
    }
}
