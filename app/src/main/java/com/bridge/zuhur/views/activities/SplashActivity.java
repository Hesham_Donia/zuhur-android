package com.bridge.zuhur.views.activities;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.back_services.NotificationsService;
import com.bridge.zuhur.back_services.RemaindersService;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.entities.Country;
import com.bridge.zuhur.entities.Phone;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.entities.User;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    Handler handler;
    boolean isInBackground = false;

    @InjectView(R.id.splashImageView)
    ImageView splashImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_splash);
        } catch (Exception e) {
        }
        ButterKnife.inject(this);

        Picasso.with(this).load(R.drawable.splash1).into(splashImageView);

        SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);

        Log.d(CommonConstants.APPLICATION_LOG_TAG, settings.getBoolean("isLogged", false) + "");

        if (settings.getString("isFacebookFirstTime", null) == null) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("isFacebookFirstTime", "no");
            editor.commit();
        }
        if (settings.getBoolean("isLogged", false) || settings.getBoolean("isFacebookLogged", false)) {
            User user = new User();
            user.name = settings.getString("name", null);
            user.username = settings.getString("username", null);
            user.email = settings.getString("email", null);
            user.address = settings.getString("address", null);
            user.points = settings.getInt("points", 0);
            user.phones = new ArrayList<>(settings.getInt("phonesNumber", 0));
            user.id = settings.getInt("id", 0);
            for (int i = 0; i < settings.getInt("phonesNumber", 0); i++) {
                user.phones.add(new Phone(settings.getString("number" + i, null),
                        settings.getBoolean("number" + i + "isPrimary", false)));
            }
            Singleton.getInstance().token = settings.getString("token", null);
            Singleton.getInstance().currentUser = user;
        } else {
            Singleton.getInstance().currentUser = null;
        }

        if (settings.getInt("countryId", 0) != 0) {
            Singleton.getInstance().country = new Country();
            Singleton.getInstance().country.id = settings.getInt("countryId", 0);
            Singleton.getInstance().country.arabicName = settings.getString("countryArabicName", "");
            Singleton.getInstance().country.englishName = settings.getString("countryEnglishName", "");
            Singleton.getInstance().country.currency = settings.getString("countryCurrency", "");
            Singleton.getInstance().country.dollarValue = (double) settings.getFloat("countryDollarValue", (float) 0.0);
        }


        Language.loadLanguage(this);

        ProductsController productsController = new ProductsController(this);
        Bundle bundle = new Bundle();
        bundle.putInt("action", ProductsController.GET_AUTO_COMPLETE_SERVER);
        productsController.request(ProductsController.GET_AUTO_COMPLETE_SERVER, bundle);


        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isInBackground) {
                    SplashActivity.this.finish();
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    SplashActivity.this.startActivity(intent);
                }
            }
        }, SPLASH_TIME_OUT);

        // TODO Call Notifications service here
        if (Singleton.getInstance().currentUser != null) {
            Intent intent = new Intent(this, NotificationsService.class);
            startService(intent);
        }

        // TODO call remainders service
        Intent intent1 = new Intent(this, RemaindersService.class);
        startService(intent1);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isInBackground = false;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isInBackground) {
                    SplashActivity.this.finish();
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    SplashActivity.this.startActivity(intent);
                }
            }
        }, SPLASH_TIME_OUT - 1500);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isInBackground = true;
    }
}
