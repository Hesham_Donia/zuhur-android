package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.WishListController;
import com.bridge.zuhur.database.LogData;
import com.bridge.zuhur.database.WishList;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.utils.NetworkConnection;
import com.bridge.zuhur.views.activities.ProductLandingPageActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 6/1/16.
 */
public class WishListRecyclerViewAdapter extends RecyclerView.Adapter<WishListRecyclerViewAdapter.ViewHolder> {

    Typeface typeface;
    ViewHolder viewHolder;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<WishList> mWishList;

    public WishListRecyclerViewAdapter(Context context, ArrayList<WishList> wishList) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mWishList = wishList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.wish_list_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        Picasso.with(mContext).load(mWishList.get(position).getProduct().imagePath).into(ordinaryHolder.wishListProductImageView);
        if (Language.getCurrentLang(mContext).equals("en")) {
            ordinaryHolder.wishListProductNameTextView.setText(mWishList.get(position).getProduct().englishName);
            ordinaryHolder.wishListProductDescriptionTextView.setText(mWishList.get(position).getProduct().englishDescription);
        } else {
            ordinaryHolder.wishListProductNameTextView.setText(mWishList.get(position).getProduct().arabicName);
            ordinaryHolder.wishListProductDescriptionTextView.setText(mWishList.get(position).getProduct().arabicDescription);
        }

        ordinaryHolder.wishListProductNameTextView.setTypeface(typeface);
        ordinaryHolder.wishListProductDescriptionTextView.setTypeface(typeface);

        ordinaryHolder.deleteWishListProductImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                WishListController wishListController = new WishListController(mContext);
                Bundle bundle = new Bundle();
                bundle.putInt("action", WishListController.DELETE_WISH_LIST_DATABASE_ACTION);
                bundle.putSerializable("id", mWishList.get(position).getProductId());
                wishListController.request(WishListController.DELETE_WISH_LIST_DATABASE_ACTION, bundle);

                if (NetworkConnection.networkConnectivity(mContext)) {
                    WishListController wishListController1 = new WishListController(mContext);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("action", WishListController.DELETE_WISH_LIST_FROM_SERVER_ACTION);
                    bundle1.putInt("productId", mWishList.get(position).getProductId());
                    wishListController1.request(WishListController.DELETE_WISH_LIST_FROM_SERVER_ACTION, bundle1);
                } else {

                    LogData logData = new LogData(mWishList.get(position), CommonConstants.DELETE_OPERATION, CommonConstants.WISH_LIST_TYPE);
                    ExtrasController extrasController = new ExtrasController(mContext);
                    Bundle logDataBundle = new Bundle();
                    logDataBundle.putSerializable("logData", logData);
                    logDataBundle.putInt("action", ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION);
                    extrasController.request(ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION, logDataBundle);
                }

                mWishList.remove(position);

                notifyItemRemoved(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mWishList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.wishListProductImageView)
        ImageView wishListProductImageView;

        @InjectView(R.id.wishListProductNameTextView)
        TextView wishListProductNameTextView;

        @InjectView(R.id.wishListProductDescriptionTextView)
        TextView wishListProductDescriptionTextView;

        @InjectView(R.id.deletewishListProductImageView)
        ImageView deleteWishListProductImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ProductLandingPageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("product", mWishList.get(getAdapterPosition()).getProduct());
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }

        public void emptyViewHolder() {
            wishListProductImageView.setImageBitmap(null);
            wishListProductNameTextView.setText("");
            wishListProductDescriptionTextView.setText("");
        }
    }
}
