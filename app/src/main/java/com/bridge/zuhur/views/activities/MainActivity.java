package com.bridge.zuhur.views.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.utils.CommonConstants;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends AbstractActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.login_button)
    LoginButton loginButton;

    @InjectView(R.id.googleButton)
    Button googleButton;

    CallbackManager callbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        setToolbar();
        setDrawerLayout();
        setNavigationView();

        setSupportActionBar(mToolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);
        navView.setNavigationItemSelectedListener(this);
        final SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settings.getString("isFacebookFirstTime", "no").equals("no")) {
                    loginButton.setReadPermissions("public_profile", "email");
                    loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            Log.d(CommonConstants.APPLICATION_LOG_TAG, "onSuccess");
                            AccessToken accessToken = loginResult.getAccessToken();
                            GraphRequest request = GraphRequest.newMeRequest(
                                    accessToken,
                                    new GraphRequest.GraphJSONObjectCallback() {
                                        @Override
                                        public void onCompleted(
                                                JSONObject object,
                                                GraphResponse response) {
                                            // Application code
                                            Log.d(CommonConstants.APPLICATION_LOG_TAG, object.toString());
                                            if (object.has("email")) {
                                                //sign up using these data
                                                //when sign up success call login facebook service to get id and token of the user
                                                // update isFacebookFirstTime to yes
                                                //when facebook login returns data, update shared preferences and update singleton object

                                            } else {
                                                // error message
                                            }

                                        }
                                    });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id,name,link,email,birthday");
                            request.setParameters(parameters);
                            request.executeAsync();
                        }

                        @Override
                        public void onCancel() {
                            Log.d(CommonConstants.APPLICATION_LOG_TAG, "onCancel");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Log.d(CommonConstants.APPLICATION_LOG_TAG, "onError: " + error.toString());
                        }
                    });
                } else {
                    //call login facebook service to get id and token of the user
                    // update isFacebookFirstTime to yes
                    //when facebook login returns data, update shared preferences and update singleton object
                    finish();
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "Nooooooooooo");
                }
            }
        });


        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String possibleEmail = "************* Get Registered Gmail Account *************\n\n";
                    Account[] accounts =
                            AccountManager.get(MainActivity.this).getAccountsByType("com.google");

                    if (accounts.length == 0) {
                        Log.d(CommonConstants.APPLICATION_LOG_TAG, "No google accounts registered");
                    } else {
                        for (Account account : accounts) {

                            possibleEmail += " --> " + account.name + " : " + account.type + " , \n";
                            possibleEmail += " \n\n";

                        }
                        Log.d(CommonConstants.APPLICATION_LOG_TAG, possibleEmail);
                    }

                } catch (Exception e) {
                    Log.i("Exception", "Exception:" + e);
                }


            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            // Handle the camera action
        } else if (id == R.id.allProducts) {

        } else if (id == R.id.ourExperience) {

        } else if (id == R.id.events) {

        } else if (id == R.id.language) {

        } else if (id == R.id.contactUs) {

        } else if (id == R.id.more) {

        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {

    }
}
