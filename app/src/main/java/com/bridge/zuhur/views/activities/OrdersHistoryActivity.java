package com.bridge.zuhur.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.database.AutoComplete;
import com.bridge.zuhur.database.OrdersHistory;
import com.bridge.zuhur.entities.Country;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.utils.Sensor;
import com.bridge.zuhur.views.adapters.OrdersHistoryRecyclerViewAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class OrdersHistoryActivity extends AbstractActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.cartIconImageView)
    ImageView cartIconImageView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    @InjectView(R.id.ordersHistoryRecyclerView)
    RecyclerView ordersHistoryRecyclerView;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.countryButton)
    Button countryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_history);

        ButterKnife.inject(this);

        setToolbar();
        setDrawerLayout();
        setNavigationView();
        setCartItemsCount();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setCountriesButton();
        setSupportActionBar(mToolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        titleTextView.setText(getResources().getString(R.string.ordersHistory));

        navView = (NavigationView) findViewById(R.id.nav_view);

        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setVisibility(View.VISIBLE);
                titleTextView.setVisibility(View.GONE);
                searchEditText.requestFocus();
                searchEditText.setThreshold(2);
                searchEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        ProductsController productsController = new ProductsController(OrdersHistoryActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", ProductsController.GET_AUTO_COMPLETE_DATABASE);
                        bundle1.putString("keyword", s.toString());
                        productsController.request(ProductsController.GET_AUTO_COMPLETE_DATABASE, bundle1);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            Intent intent = new Intent(OrdersHistoryActivity.this, SearchResultsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("keyword", searchEditText.getText().toString());
                            intent.putExtras(bundle);
                            startActivity(intent);
                            hideKeyboard(searchEditText);
                            return true;
                        }
                        return false;
                    }
                });
            }
        });

        cartIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Singleton.getInstance().cart.size() > 0) {
                    Intent intent = new Intent(OrdersHistoryActivity.this, ResetActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(OrdersHistoryActivity.this, getResources().getString(R.string.cartEmpty), Toast.LENGTH_LONG).show();
                }
            }
        });

        OrderController orderController = new OrderController(OrdersHistoryActivity.this);
        Bundle bundle1 = new Bundle();
        bundle1.putInt("action", OrderController.GET_ALL_ORDER_DATABASE_ACTION);
        orderController.request(OrderController.GET_ALL_ORDER_DATABASE_ACTION, bundle1);
    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {

        switch (action) {

            case OrderController.GET_ALL_ORDER_DATABASE_ACTION:
                ArrayList<OrdersHistory> ordersHistory = (ArrayList<OrdersHistory>) result;
                OrdersHistoryRecyclerViewAdapter ordersHistoryRecyclerViewAdapter = new OrdersHistoryRecyclerViewAdapter(this, ordersHistory);
                ordersHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                ordersHistoryRecyclerView.setAdapter(ordersHistoryRecyclerViewAdapter);
                break;

            case ProductsController.GET_AUTO_COMPLETE_DATABASE:
                try {
                    ArrayAdapter adapter;
                    ArrayList<AutoComplete> autoCompleteCaches = (ArrayList) result;
                    ArrayList<String> autoCompleteNames = new ArrayList<>();

                    for (AutoComplete autoComplete : autoCompleteCaches) {
                        autoCompleteNames.add(autoComplete.getName());
                    }
                    adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, autoCompleteNames);
                    searchEditText.setThreshold(2);
                    searchEditText.setAdapter(adapter);
                } catch (Exception e) {

                }
                break;


            default:
                break;
        }
        Sensor.unlockSensor(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == COUNTRIES_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {
                Bundle bundle = data.getExtras();
                if (bundle.containsKey("country")) {
                    SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                            Context.MODE_PRIVATE);
                    Singleton.getInstance().country = (Country) bundle.getSerializable("country");
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt("countryId", Singleton.getInstance().country.id);
                    editor.putString("countryEnglishName", Singleton.getInstance().country.englishName);
                    editor.putString("countryArabicName", Singleton.getInstance().country.arabicName);
                    editor.putString("countryCurrency", Singleton.getInstance().country.currency);
                    editor.putFloat("countryDollarValue", (float) Singleton.getInstance().country.dollarValue);
                    editor.commit();

                    if (Language.getCurrentLang(this).equals("en")) {
                        mCountriesButton.setText(Singleton.getInstance().country.englishName);
                    } else {
                        mCountriesButton.setText(Singleton.getInstance().country.arabicName);
                    }

                }
            }
        }
    }
}
