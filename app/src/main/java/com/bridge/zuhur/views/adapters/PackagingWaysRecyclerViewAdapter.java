package com.bridge.zuhur.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.PackagingWay;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.views.activities.PackagingWaysActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 6/22/16.
 */
public class PackagingWaysRecyclerViewAdapter extends RecyclerView.Adapter<PackagingWaysRecyclerViewAdapter.ViewHolder> {


    ViewHolder viewHolder;
    Typeface typeface;
    private Context mContext;
    private ArrayList<PackagingWay> mPackageWays;
    private LayoutInflater mInflater = null;
    private int mProductId;

    public PackagingWaysRecyclerViewAdapter(Context context, ArrayList<PackagingWay> packageWays, int productId) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mPackageWays = packageWays;
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mProductId = productId;
    }

    @Override
    public PackagingWaysRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.package_way_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PackagingWaysRecyclerViewAdapter.ViewHolder holder, final int position) {
        ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();
        Picasso.with(mContext).load(mPackageWays.get(position).imagePath).into(ordinaryHolder.packageWayImageView);

        if (Singleton.getInstance().currentLanguage.equals("en")) {
            ordinaryHolder.packageWayTextView.setText(mPackageWays.get(position).nameEn);
        } else {
            ordinaryHolder.packageWayTextView.setText(mPackageWays.get(position).nameAr);
        }

        ordinaryHolder.packageWayTextView.setTypeface(typeface);

        ordinaryHolder.addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < Singleton.getInstance().cart.size(); i++) {
                    if (Singleton.getInstance().cart.get(i).id == mProductId) {
                        Singleton.getInstance().cart.get(i).packagingWay = mPackageWays.get(position);
                        ((PackagingWaysActivity) mContext).setResult(Activity.RESULT_OK);
                        ((PackagingWaysActivity) mContext).finish();
                        break;
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPackageWays.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.packageWayImageView)
        ImageView packageWayImageView;

        @InjectView(R.id.packageWayTextView)
        TextView packageWayTextView;

        @InjectView(R.id.addToCartButton)
        Button addToCartButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }

        public void emptyViewHolder() {
            packageWayImageView.setImageBitmap(null);
            packageWayTextView.setText("");
        }
    }
}
