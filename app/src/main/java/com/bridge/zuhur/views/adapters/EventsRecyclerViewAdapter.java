package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Event;
import com.bridge.zuhur.utils.Language;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 6/9/16.
 */
public class EventsRecyclerViewAdapter extends RecyclerView.Adapter<EventsRecyclerViewAdapter.ViewHolder> {


    Typeface typeface;
    ViewHolder viewHolder;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<Event> mEvents;

    public EventsRecyclerViewAdapter(Context context, ArrayList<Event> events) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mEvents = events;
    }

    @Override
    public EventsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.event_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EventsRecyclerViewAdapter.ViewHolder holder, int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        Picasso.with(mContext).load(mEvents.get(position).imagePath).into(ordinaryHolder.eventImageView);

        if (Language.getCurrentLang(mContext).equals("en")) {
            ordinaryHolder.eventNameTextView.setText(mEvents.get(position).englishName);
            ordinaryHolder.eventDetailsTextView.setText(mEvents.get(position).englishDetails);
        } else {
            ordinaryHolder.eventNameTextView.setText(mEvents.get(position).arabicName);
            ordinaryHolder.eventDetailsTextView.setText(mEvents.get(position).arabicDetails);
        }
    }

    @Override
    public int getItemCount() {
        return mEvents.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.eventImageView)
        ImageView eventImageView;

        @InjectView(R.id.eventNameTextView)
        TextView eventNameTextView;

        @InjectView(R.id.eventDetailsTextView)
        TextView eventDetailsTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }

        public void emptyViewHolder() {
            eventImageView.setImageBitmap(null);
            eventNameTextView.setText("");
            eventDetailsTextView.setText("");
        }
    }

}
