package com.bridge.zuhur.views.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.PackagingWay;
import com.bridge.zuhur.interfaces.ViewsListener;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.views.adapters.PackagingWaysRecyclerViewAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class PackagingWaysActivity extends Activity implements ViewsListener {

    @InjectView(R.id.packagingWaysRecyclerView)
    RecyclerView packagingWaysRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packaging_ways);

        ButterKnife.inject(this);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);

        getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        int productId = getIntent().getExtras().getInt("productId");
        int count = getIntent().getExtras().getInt("count");
        ArrayList<PackagingWay> packagingWays = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            packagingWays.add((PackagingWay) getIntent().getExtras().getSerializable("package-" + i));
            Log.d(CommonConstants.APPLICATION_LOG_TAG, packagingWays.get(i).nameEn);
        }

        PackagingWaysRecyclerViewAdapter packagingWaysRecyclerViewAdapter = new PackagingWaysRecyclerViewAdapter(this, packagingWays, productId);
        packagingWaysRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        packagingWaysRecyclerView.setAdapter(packagingWaysRecyclerViewAdapter);
        packagingWaysRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {

    }
}
