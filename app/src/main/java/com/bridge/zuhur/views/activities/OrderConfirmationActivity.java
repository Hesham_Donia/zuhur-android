package com.bridge.zuhur.views.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.database.AutoComplete;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Order;
import com.bridge.zuhur.entities.PaymentMethod;
import com.bridge.zuhur.entities.Place;
import com.bridge.zuhur.entities.Promotion;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class OrderConfirmationActivity extends AbstractActivity implements OnMapReadyCallback {

    public static final int PAYMENT_METHOD_REQUEST_CODE = 1;
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static final int REQUEST_CODE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CommonConstants.PAYPAL_CONFIG_CLIENT_ID);
    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.cartIconImageView)
    ImageView cartIconImageView;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    SupportMapFragment billingAddressMapFragment;
    SupportMapFragment deliveryAddressMapFragment;
    GoogleMap billingAddressMap, deliveryAddressMap;

    @InjectView(R.id.billingAddressTextView)
    TextView billingAddressTextView;

    @InjectView(R.id.deliveryAddressTextView)
    TextView deliveryAddressTextView;

    @InjectView(R.id.billingAddressEditText)
    EditText billingAddressEditText;

    @InjectView(R.id.deliveryAddressEditText)
    EditText deliveryAddressEditText;

    @InjectView(R.id.deliveryAddressPhoneEditText)
    EditText deliveryAddressPhoneEditText;

    @InjectView(R.id.preferredShippingDateTextView)
    TextView preferredShippingDateTextView;

    @InjectView(R.id.dateChooserTextView)
    TextView dateChooserTextView;

    @InjectView(R.id.paymentMethodTextView)
    TextView paymentMethodTextView;

    @InjectView(R.id.paymentMethodRadioGroup)
    RadioGroup paymentMethodRadioGroup;

    @InjectView(R.id.promotionCodeEditText)
    EditText promotionCodeEditText;

    @InjectView(R.id.applyPromotionCodeButton)
    CardView applyPromotionCodeButton;

    @InjectView(R.id.confirmResetTextView)
    TextView confirmResetTextView;

    @InjectView(R.id.totalPriceTextView)
    TextView totalPriceTextView;

    @InjectView(R.id.finalPriceTextView)
    TextView finalPriceTextView;

    @InjectView(R.id.makeOrderButton)
    CardView makeOrderButton;

    @InjectView(R.id.makeOrderTextView)
    TextView makeOrderTextView;

    @InjectView(R.id.orTextView)
    TextView orTextView;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.billingAddressCardView)
    CardView billingAddressCardView;

    @InjectView(R.id.countryButton)
    Button countryButton;

    @InjectView(R.id.creditCardDataCardView)
    CardView creditCardDataCardView;

    @InjectView(R.id.creditCardNumberEditText)
    EditText creditCardNumberEditText;

    @InjectView(R.id.creditCardExpMonthEditText)
    EditText creditCardExpMonthEditText;

    @InjectView(R.id.creditCardExpYearEditText)
    EditText creditCardExpYearEditText;

    @InjectView(R.id.creditCardVCSEditText)
    EditText creditCardVCSEditText;

    @InjectView(R.id.creditCardDataTextView)
    TextView creditCardDataTextView;


    Marker billingMarker = null, deliveryMarker = null;
    double finalPriceMoney = 0.0, originalPriceMoney = 0.0;
    int finalPricePoints = 0;
    Order order = new Order();
    PayPalPayment thingToBuy;

    PaymentMethod selectedPaymentMethod = null;
    ArrayList<PaymentMethod> paymentMethods = new ArrayList<>();
    Typeface typeface;
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            dateChooserTextView.setText("" + year + "-" + (month + 1) + "-" + day);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmation);

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        ButterKnife.inject(this);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/arcena.ttf");


        finalPricePoints = getIntent().getExtras().getInt("points");
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "points from confirm order : " + finalPricePoints);
        originalPriceMoney = finalPriceMoney = getIntent().getExtras().getDouble("money");

        setupToolbar();
        getPaymentMethods();
        setupMaps();
        applyFonts();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setCountriesButton();

        countryButton.setVisibility(View.GONE);

        dateChooserTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);

                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(OrderConfirmationActivity.this, myDateListener, year, month, day).show();
            }
        });


        applyPromotionCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Singleton.getInstance().currentUser == null) {
                    Toast.makeText(OrderConfirmationActivity.this, getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                } else {
                    if (promotionCodeEditText.getText().toString().equals("")) {
                        Toast.makeText(OrderConfirmationActivity.this, getResources().getString(R.string.emptyPromotion), Toast.LENGTH_SHORT).show();
                    } else {
                        OrderController orderController = new OrderController(OrderConfirmationActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putString("code", promotionCodeEditText.getText().toString());
                        bundle1.putInt("action", OrderController.PROMOTION_CODE_VALIDATION_ACTION);
                        orderController.request(OrderController.PROMOTION_CODE_VALIDATION_ACTION, bundle1);
                    }
                }
            }
        });
        /*
        {"data":[{"user_id":8,"total_price":"129.99","original_total_price":"129.99","bill_address_X":"25.288035551425203","bill_address_Y":"51.45735885947943","bill_address":"Al Manthour St,Doha,Qatar",
                "deliver_address_X":"25.28938060813327","deliver_address_Y":"51.443969272077084","deliver_address":"Muraykh,Doha,Qatar",
                "shipping_date":"2016-5-31","payment_method_id":{"sub":1,"main":5},"products":[{"product_id":3}]}]}
         */

        makeOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order.products = Singleton.getInstance().cart;

                if (Singleton.getInstance().currentUser == null) {
                    Toast.makeText(OrderConfirmationActivity.this, getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                } else {
                    if (deliveryMarker == null && deliveryAddressEditText.getText().toString().equals("")) {
                        Toast.makeText(OrderConfirmationActivity.this, getResources().getString(R.string.deliveryAddressEmptyError), Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        Place billingPlace = new Place(), deliverPlace = new Place();

                        if (billingMarker != null && billingAddressEditText.getText().toString().equals("")) {
                            billingPlace.address = billingMarker.getTitle();
                            billingPlace.latitude = String.valueOf(billingMarker.getPosition().latitude);
                            billingPlace.longitude = String.valueOf(billingMarker.getPosition().longitude);
                        } else {
                            if (billingAddressCardView.getVisibility() == View.VISIBLE) {
                                billingPlace.address = billingAddressEditText.getText().toString();
                                billingPlace.latitude = "0";
                                billingPlace.longitude = "0";
                            }
                        }

                        if (deliveryAddressEditText.getText().toString().equals("")) {
                            deliverPlace.address = deliveryMarker.getTitle();
                            deliverPlace.latitude = String.valueOf(deliveryMarker.getPosition().latitude);
                            deliverPlace.longitude = String.valueOf(deliveryMarker.getPosition().longitude);
                        } else {
                            deliverPlace.address = deliveryAddressEditText.getText().toString();
                            deliverPlace.latitude = "0";
                            deliverPlace.longitude = "0";
                            if (deliveryAddressPhoneEditText.getText().toString().equals("")) {
                                Toast.makeText(OrderConfirmationActivity.this, getResources().getString(R.string.deliveryAddressPhoneEmptyError), Toast.LENGTH_SHORT).show();
                                return;
                            } else {
                                deliverPlace.phone = deliveryAddressPhoneEditText.getText().toString();
                            }
                        }

                        order.billingPlace = billingPlace;
                        order.deliverPlace = deliverPlace;
                    }


                    if (!dateChooserTextView.getText().toString().equals(getResources().getString(R.string.chooseDate))) {
                        order.shippingDate = dateChooserTextView.getText().toString();
                    }

                    order.isCompleted = false;
                    order.isConfirmed = false;

                    if (selectedPaymentMethod == null) {
                        Toast.makeText(OrderConfirmationActivity.this, getResources().getString(R.string.payentMethodError), Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        Log.d(CommonConstants.APPLICATION_LOG_TAG, "finalPriceMoney: " + finalPriceMoney);
                        switch (selectedPaymentMethod.id) { // cash on delivery
                            case 1:
                                order.finalPrice = finalPriceMoney;
                                order.originalPrice = originalPriceMoney;
                                order.mainPaymentMethod = selectedPaymentMethod;
                                OrderController orderController = new OrderController(OrderConfirmationActivity.this);
                                Bundle bundle1 = new Bundle();
                                bundle1.putInt("action", OrderController.ADD_ORDER_TO_SERVER_ACTION);
                                bundle1.putSerializable("order", order);
                                if (order.promotion != null) {
                                    bundle1.putString("code", order.promotion.promotionCode);
                                }
                                orderController.request(OrderController.ADD_ORDER_TO_SERVER_ACTION, bundle1);
                                break;

                            case 2: // pay pal
                                order.finalPrice = finalPriceMoney;
                                order.originalPrice = originalPriceMoney;
                                double finalPrice = order.finalPrice / Singleton.getInstance().country.dollarValue;
                                thingToBuy = new PayPalPayment(new BigDecimal(String.valueOf(finalPrice)), "USD",
                                        "Flowers", PayPalPayment.PAYMENT_INTENT_SALE);
                                Intent intent = new Intent(OrderConfirmationActivity.this,
                                        PaymentActivity.class);
                                intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
                                startActivityForResult(intent, REQUEST_CODE_PAYMENT);
                                break;

                            case 3: // credit card
                                order.finalPrice = finalPriceMoney;
                                order.originalPrice = originalPriceMoney;
                                if (creditCardExpMonthEditText.getText().toString().equals("") ||
                                        creditCardExpYearEditText.getText().toString().equals("") ||
                                        creditCardNumberEditText.getText().toString().equals("") ||
                                        creditCardVCSEditText.getText().toString().equals("")) {
                                    Toast.makeText(OrderConfirmationActivity.this, getResources().getString(R.string.emptyFieldsError), Toast.LENGTH_LONG).show();
                                } else {

                                    //card number: 4242 4242 4242 4242
                                    //exp month: 12
                                    //exp year: 2016
                                    //vcs: 123
                                    Card card = new Card(creditCardNumberEditText.getText().toString(),
                                            Integer.valueOf(creditCardExpMonthEditText.getText().toString()),
                                            Integer.valueOf(creditCardExpYearEditText.getText().toString()),
                                            creditCardVCSEditText.getText().toString());
                                    boolean valid = card.validateCard();
                                    if (valid) {
                                        try {
                                            Stripe stripe = new Stripe(CommonConstants.TEST_PUBLISHABLE_KEY);
                                            com.stripe.Stripe.apiKey = CommonConstants.TEST_SECRET_KEY;

                                            stripe.createToken(card, new TokenCallback() {
                                                @Override
                                                public void onError(Exception error) {
                                                    Log.d("hesham", "error in create token: " + error.toString());
                                                }

                                                @Override
                                                public void onSuccess(Token token) {

                                                    OrderController orderController1 = new OrderController(OrderConfirmationActivity.this);
                                                    Bundle bundle2 = new Bundle();
                                                    bundle2.putInt("action", OrderController.ORDER_STRIPE_ACTION);
                                                    order.token = token;
                                                    bundle2.putSerializable("order", order);
                                                    if (order.promotion != null) {
                                                        bundle2.putString("code", order.promotion.promotionCode);
                                                    }
                                                    orderController1.request(OrderController.ORDER_STRIPE_ACTION, bundle2);
                                                }
                                            });
                                        } catch (AuthenticationException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                break;

                            case 5: //points
                                order.finalPrice = 0.0;
                                order.originalPrice = 0.0;
                                order.pointsPrice = finalPricePoints;
                                order.mainPaymentMethod = selectedPaymentMethod;
                                OrderController orderController1 = new OrderController(OrderConfirmationActivity.this);
                                Bundle bundle2 = new Bundle();
                                bundle2.putInt("action", OrderController.ADD_ORDER_TO_SERVER_ACTION);
                                bundle2.putSerializable("order", order);
                                if (order.promotion != null) {
                                    bundle2.putString("code", order.promotion.promotionCode);
                                }
                                orderController1.request(OrderController.ADD_ORDER_TO_SERVER_ACTION, bundle2);
                                break;

                            case 4: //points + money
                                if (Singleton.getInstance().currentUser.points >= finalPricePoints) {
                                    order.finalPrice = 0.0;
                                    order.originalPrice = 0.0;
                                    order.pointsPrice = finalPricePoints;
                                    order.mainPaymentMethod = selectedPaymentMethod;
                                    OrderController orderController2 = new OrderController(OrderConfirmationActivity.this);
                                    Bundle bundle3 = new Bundle();
                                    bundle3.putInt("action", OrderController.ADD_ORDER_TO_SERVER_ACTION);
                                    bundle3.putSerializable("order", order);
                                    if (order.promotion != null) {
                                        bundle3.putString("code", order.promotion.promotionCode);
                                    }
                                    orderController2.request(OrderController.ADD_ORDER_TO_SERVER_ACTION, bundle3);
                                } else {
                                    double convertedMoney = finalPriceMoney - ((double) Singleton.getInstance().currentUser.points) / 100;
                                    order.finalPrice = convertedMoney;
                                    order.originalPrice = convertedMoney;
                                    order.pointsPrice = Singleton.getInstance().currentUser.points;
                                    order.mainPaymentMethod = selectedPaymentMethod;
                                    switch (order.subPaymentMethod.id) {
                                        case 1:
                                            OrderController orderController2 = new OrderController(OrderConfirmationActivity.this);
                                            Bundle bundle3 = new Bundle();
                                            bundle3.putInt("action", OrderController.ADD_ORDER_TO_SERVER_ACTION);
                                            bundle3.putSerializable("order", order);
                                            if (order.promotion != null) {
                                                bundle3.putString("code", order.promotion.promotionCode);
                                            }
                                            orderController2.request(OrderController.ADD_ORDER_TO_SERVER_ACTION, bundle3);
                                            break;

                                        case 2:
                                            double finalPrice1 = order.finalPrice / Singleton.getInstance().country.dollarValue;
                                            thingToBuy = new PayPalPayment(new BigDecimal(String.valueOf(finalPrice1)), "USD",
                                                    "Flowers", PayPalPayment.PAYMENT_INTENT_SALE);
                                            Intent intent1 = new Intent(OrderConfirmationActivity.this,
                                                    PaymentActivity.class);
                                            intent1.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
                                            startActivityForResult(intent1, REQUEST_CODE_PAYMENT);
                                            break;

                                        case 3:
                                            if (creditCardExpMonthEditText.getText().toString().equals("") ||
                                                    creditCardExpYearEditText.getText().toString().equals("") ||
                                                    creditCardNumberEditText.getText().toString().equals("") ||
                                                    creditCardVCSEditText.getText().toString().equals("")) {
                                                Toast.makeText(OrderConfirmationActivity.this, getResources().getString(R.string.emptyFieldsError), Toast.LENGTH_LONG).show();
                                            } else {

                                                //card number: 4242 4242 4242 4242
                                                //exp month: 12
                                                //exp year: 2016
                                                //vcs: 123
                                                Card card = new Card(creditCardNumberEditText.getText().toString(),
                                                        Integer.valueOf(creditCardExpMonthEditText.getText().toString()),
                                                        Integer.valueOf(creditCardExpYearEditText.getText().toString()),
                                                        creditCardVCSEditText.getText().toString());
                                                boolean valid1 = card.validateCard();
                                                if (valid1) {
                                                    try {
                                                        Stripe stripe = new Stripe(CommonConstants.TEST_PUBLISHABLE_KEY);
                                                        com.stripe.Stripe.apiKey = CommonConstants.TEST_SECRET_KEY;


                                                        stripe.createToken(card, new TokenCallback() {
                                                            @Override
                                                            public void onError(Exception error) {
                                                                Log.d("hesham", "error in create token: " + error.toString());
                                                            }

                                                            @Override
                                                            public void onSuccess(Token token) {

                                                                OrderController orderController1 = new OrderController(OrderConfirmationActivity.this);
                                                                Bundle bundle2 = new Bundle();
                                                                bundle2.putInt("action", OrderController.ORDER_STRIPE_ACTION);
                                                                order.token = token;
                                                                bundle2.putSerializable("order", order);
                                                                if (order.promotion != null) {
                                                                    bundle2.putString("code", order.promotion.promotionCode);
                                                                }
                                                                orderController1.request(OrderController.ORDER_STRIPE_ACTION, bundle2);

                                                            }
                                                        });
                                                    } catch (AuthenticationException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                            break;

                                        default:
                                            break;
                                    }
                                }
                                break;

                            default:
                                break;
                        }

                        order.mainPaymentMethod = selectedPaymentMethod;
                    }


                }


            }
        });
    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYMENT_METHOD_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {
                switch (data.getExtras().getInt("method")) {
                    case 1:// cash on delivery
                        order.mainPaymentMethod = selectedPaymentMethod;
                        order.subPaymentMethod = paymentMethods.get(0);
                        billingAddressCardView.setVisibility(View.VISIBLE);
                        creditCardDataCardView.setVisibility(View.GONE);
                        break;

                    case 2:// paybal
                        order.mainPaymentMethod = selectedPaymentMethod;
                        order.subPaymentMethod = paymentMethods.get(1);
                        billingAddressCardView.setVisibility(View.GONE);
                        creditCardDataCardView.setVisibility(View.GONE);
                        break;

                    case 3:// credit card
                        order.mainPaymentMethod = selectedPaymentMethod;
                        order.subPaymentMethod = paymentMethods.get(2);
                        billingAddressCardView.setVisibility(View.GONE);
                        creditCardDataCardView.setVisibility(View.VISIBLE);
                        break;

                    default:
                        break;

                }

                Log.d(CommonConstants.APPLICATION_LOG_TAG, "user points: " + Singleton.getInstance().currentUser.points);
                double convertedMoney = finalPriceMoney - ((double) Singleton.getInstance().currentUser.points) / 100;

                DecimalFormat twoDForm2 = new DecimalFormat("#.##");
                convertedMoney = Double.valueOf(twoDForm2.format(convertedMoney));

                totalPriceTextView.setText(getResources().getString(R.string.originalPrice) + " " + convertedMoney + " " + Singleton.getInstance().country.currency);
                finalPriceTextView.setText(getResources().getString(R.string.points) + " " + Singleton.getInstance().currentUser.points);
            }
        }

        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    order.mainPaymentMethod = selectedPaymentMethod;
                    OrderController orderController1 = new OrderController(OrderConfirmationActivity.this);
                    Bundle bundle2 = new Bundle();
                    bundle2.putInt("action", OrderController.ADD_ORDER_TO_SERVER_ACTION);
                    bundle2.putSerializable("order", order);
                    if (order.promotion != null) {
                        bundle2.putString("code", order.promotion.promotionCode);
                    }
                    orderController1.request(OrderController.ADD_ORDER_TO_SERVER_ACTION, bundle2);
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                System.out.println("The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                System.out.println("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {
        switch (action) {
            case OrderController.PROMOTION_CODE_VALIDATION_ACTION:
                if (result != null) {
                    ArrayList<Promotion> promotions = (ArrayList<Promotion>) result;
                    order.promotion = promotions.get(0);
                    finalPriceMoney = finalPriceMoney - ((finalPriceMoney * order.promotion.promotionPercentage) / 100);
                    finalPriceTextView.setText(getResources().getString(R.string.finalPrice) + " " + finalPriceMoney + " " + Singleton.getInstance().country.currency);
                }
                break;

            case ExtrasController.PAYMENT_METHODS_ACTION:
                paymentMethods = (ArrayList<PaymentMethod>) result;
                for (PaymentMethod paymentMethod : paymentMethods) {
                    RadioButton radioButton = new RadioButton(this);
                    radioButton.setId(paymentMethod.id);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    radioButton.setLayoutParams(layoutParams);

                    if (Language.getCurrentLang(this).equals("en")) {
                        radioButton.setText(paymentMethod.englishName);
                    } else {
                        radioButton.setText(paymentMethod.arabicName);
                    }

                    radioButton.setTypeface(typeface);
                    paymentMethodRadioGroup.addView(radioButton);
                }

                paymentMethodRadioGroup.invalidate();
                paymentMethodRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        for (PaymentMethod paymentMethod : paymentMethods) {
                            if (checkedId == paymentMethod.id) {
                                selectedPaymentMethod = paymentMethod;
                                Log.d(CommonConstants.APPLICATION_LOG_TAG, "when check finalPriceMoney: " + finalPriceMoney);
                                switch (checkedId) {
                                    case 1:// cash on delivery
                                        finalPriceTextView.setVisibility(View.VISIBLE);
                                        orTextView.setVisibility(View.VISIBLE);

                                        DecimalFormat twoDForm = new DecimalFormat("#.##");
                                        originalPriceMoney = Double.valueOf(twoDForm.format(originalPriceMoney));
                                        finalPriceMoney = Double.valueOf(twoDForm.format(finalPriceMoney));

                                        totalPriceTextView.setText(getResources().getString(R.string.originalPrice) + " " + originalPriceMoney + " " + Singleton.getInstance().country.currency);
                                        finalPriceTextView.setText(getResources().getString(R.string.finalPrice) + " " + finalPriceMoney + " " + Singleton.getInstance().country.currency);
                                        billingAddressCardView.setVisibility(View.VISIBLE);
                                        creditCardDataCardView.setVisibility(View.GONE);
                                        break;

                                    case 2:// paypal
                                        finalPriceTextView.setVisibility(View.VISIBLE);
                                        orTextView.setVisibility(View.VISIBLE);

                                        DecimalFormat twoDForm1 = new DecimalFormat("#.##");
                                        originalPriceMoney = Double.valueOf(twoDForm1.format(originalPriceMoney));
                                        finalPriceMoney = Double.valueOf(twoDForm1.format(finalPriceMoney));

                                        totalPriceTextView.setText(getResources().getString(R.string.originalPrice) + " " + originalPriceMoney + " " + Singleton.getInstance().country.currency);
                                        finalPriceTextView.setText(getResources().getString(R.string.finalPrice) + " " + finalPriceMoney + " " + Singleton.getInstance().country.currency);
                                        billingAddressCardView.setVisibility(View.GONE);
                                        creditCardDataCardView.setVisibility(View.GONE);
                                        break;

                                    case 3:// credit card
                                        finalPriceTextView.setVisibility(View.VISIBLE);
                                        orTextView.setVisibility(View.VISIBLE);

                                        DecimalFormat twoDForm2 = new DecimalFormat("#.##");
                                        originalPriceMoney = Double.valueOf(twoDForm2.format(originalPriceMoney));
                                        finalPriceMoney = Double.valueOf(twoDForm2.format(finalPriceMoney));

                                        totalPriceTextView.setText(getResources().getString(R.string.originalPrice) + " " + originalPriceMoney + " " + Singleton.getInstance().country.currency);
                                        finalPriceTextView.setText(getResources().getString(R.string.finalPrice) + " " + finalPriceMoney + " " + Singleton.getInstance().country.currency);
                                        billingAddressCardView.setVisibility(View.GONE);
                                        creditCardDataCardView.setVisibility(View.VISIBLE);
                                        break;

                                    case 4:// points
                                        if (Singleton.getInstance().currentUser.points >= finalPricePoints) {
                                            totalPriceTextView.setText(getResources().getString(R.string.finalPrice) + " " + finalPricePoints + " " + Singleton.getInstance().country.currency);
                                            finalPriceTextView.setVisibility(View.INVISIBLE);
                                            orTextView.setVisibility(View.INVISIBLE);
                                        } else {
                                            Intent intent = new Intent(OrderConfirmationActivity.this, PaymentMethodChooseActivity.class);
                                            startActivityForResult(intent, PAYMENT_METHOD_REQUEST_CODE);
                                        }
                                        billingAddressCardView.setVisibility(View.GONE);
                                        creditCardDataCardView.setVisibility(View.GONE);
                                        break;

                                    case 5:// points + money TODO: this case not needed
                                        if (Singleton.getInstance().currentUser.points >= finalPricePoints) {
                                            Toast.makeText(OrderConfirmationActivity.this, getResources().getString(R.string.pointsEnough), Toast.LENGTH_LONG).show();
                                        } else {
                                            Intent intent = new Intent(OrderConfirmationActivity.this, PaymentMethodChooseActivity.class);
                                            startActivityForResult(intent, PAYMENT_METHOD_REQUEST_CODE);
                                        }
                                        billingAddressCardView.setVisibility(View.GONE);
                                        creditCardDataCardView.setVisibility(View.GONE);
                                        break;

                                }
                            }
                        }
                    }
                });
                break;

            case ProductsController.GET_AUTO_COMPLETE_DATABASE:
                try {
                    ArrayAdapter adapter;
                    ArrayList<AutoComplete> autoCompleteCaches = (ArrayList) result;
                    ArrayList<String> autoCompleteNames = new ArrayList<>();

                    for (AutoComplete autoComplete : autoCompleteCaches) {
                        autoCompleteNames.add(autoComplete.getName());
                    }
                    adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, autoCompleteNames);
                    searchEditText.setThreshold(2);
                    searchEditText.setAdapter(adapter);
                } catch (Exception e) {

                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng placePosition = new LatLng(Double.valueOf("25.289174"),
                Double.valueOf("51.4594188"));

        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(placePosition, 13));
    }

    private void getPaymentMethods() {
        ExtrasController extrasController = new ExtrasController(this);
        Bundle bundle1 = new Bundle();
        bundle1.putInt("action", ExtrasController.PAYMENT_METHODS_ACTION);
        bundle1.putDouble("total", finalPriceMoney);
        extrasController.request(ExtrasController.PAYMENT_METHODS_ACTION, bundle1);
    }

    private void setupMaps() {
        try {
            billingAddressMapFragment = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.billingAddressMap));
            billingAddressMapFragment.getMapAsync(this);
            billingAddressMap = billingAddressMapFragment.getMap();

            billingAddressMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {

                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(OrderConfirmationActivity.this, Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String country = addresses.get(0).getCountryName();

                        if (billingMarker != null) {
                            billingMarker.remove();
                        }

                        billingMarker = billingAddressMap.addMarker(new MarkerOptions()
                                .title(address + "," + city + "," + country)
                                .position(latLng));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            });

            deliveryAddressMapFragment = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.deliveryAddressMap));
            deliveryAddressMapFragment.getMapAsync(this);
            deliveryAddressMap = deliveryAddressMapFragment.getMap();
            deliveryAddressMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(OrderConfirmationActivity.this, Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String country = addresses.get(0).getCountryName();

                        if (deliveryMarker != null) {
                            deliveryMarker.remove();
                        }

                        deliveryMarker = deliveryAddressMap.addMarker(new MarkerOptions()
                                .title(address + "," + city + "," + country)
                                .position(latLng));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void applyFonts() {
        billingAddressTextView.setTypeface(typeface);
        deliveryAddressTextView.setTypeface(typeface);
        paymentMethodTextView.setTypeface(typeface);
        orTextView.setTypeface(typeface);
        totalPriceTextView.setTypeface(typeface);
        finalPriceTextView.setTypeface(typeface);
        preferredShippingDateTextView.setTypeface(typeface);
        creditCardDataTextView.setTypeface(typeface);
        creditCardNumberEditText.setTypeface(typeface);
        creditCardVCSEditText.setTypeface(typeface);
        creditCardExpYearEditText.setTypeface(typeface);
        creditCardExpMonthEditText.setTypeface(typeface);
    }

    private void setupToolbar() {
        setToolbar();
        setDrawerLayout();
        setNavigationView();
        setCartItemsCount();

        setSupportActionBar(mToolbar);

        titleTextView.setText(getResources().getString(R.string.orderConfirmation));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setVisibility(View.VISIBLE);
                titleTextView.setVisibility(View.GONE);
                searchEditText.requestFocus();
                searchEditText.setThreshold(2);
                searchEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        ProductsController productsController = new ProductsController(OrderConfirmationActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", ProductsController.GET_AUTO_COMPLETE_DATABASE);
                        bundle1.putString("keyword", s.toString());
                        productsController.request(ProductsController.GET_AUTO_COMPLETE_DATABASE, bundle1);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            Intent intent = new Intent(OrderConfirmationActivity.this, SearchResultsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("keyword", searchEditText.getText().toString());
                            intent.putExtras(bundle);
                            startActivity(intent);
                            hideKeyboard(searchEditText);
                            return true;
                        }
                        return false;
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, PayPalService.class));
    }
}
