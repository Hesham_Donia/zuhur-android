package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Offer;
import com.bridge.zuhur.views.activities.OfferDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/27/16.
 */
public class OffersPagerViewAdapter extends PagerAdapter {


    private Context mContext;
    private ArrayList<Offer> mOffers;

    public OffersPagerViewAdapter(Context context, ArrayList<Offer> offers) {
        mContext = context;
        mOffers = offers;
    }

    @Override
    public int getCount() {
        if (mOffers.size() == 0) {
            return 0;
        }
        return mOffers.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.offers_pager_view_item, container, false);
        ImageView imageView = (ImageView) layout.findViewById(R.id.offerImageView);
        Picasso.with(mContext).load(mOffers.get(position).imagePath).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(mContext, "Go to Offer details. Modify the OffersPagerViewAdapter. " + mOffers.size(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(mContext, OfferDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("offer", mOffers.get(position));
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
