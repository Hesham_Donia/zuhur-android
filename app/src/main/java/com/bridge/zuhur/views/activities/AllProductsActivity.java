package com.bridge.zuhur.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.database.AutoComplete;
import com.bridge.zuhur.entities.Country;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.utils.Sensor;
import com.bridge.zuhur.views.adapters.AllProductsRecyclerViewAdapter;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AllProductsActivity extends AbstractActivity {

    final static int COUNTRIES_REQUEST_CODE = 1;
    final int SORT_TYPE_REQUEST_CODE = 3;
    final int FILTER_TYPE_REQUEST_CODE = 2;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.loadingContainer)
    RelativeLayout relativeLayout;

    @InjectView(R.id.cartIconImageView)
    ImageView cartIconImageView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    @InjectView(R.id.productsRecyclerView)
    RecyclerView productsRecyclerView;

    @InjectView(R.id.filterAction)
    FloatingActionButton filterActionButton;

    @InjectView(R.id.sortAction)
    FloatingActionButton sortActionButton;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.countryButton)
    Button countryButton;

    String sortType = "", tags = "";
    int priceFrom = 0, priceTo = 0;
    Country country = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_products);

        ButterKnife.inject(this);

        setToolbar();
        setDrawerLayout();
        setNavigationView();
        setLoadingContainer();
        setCartItemsCount();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setCountriesButton();

        setSupportActionBar(mToolbar);

        titleTextView.setText(getResources().getString(R.string.allProducts));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        relativeLayout.setVisibility(View.VISIBLE);
        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setVisibility(View.VISIBLE);
                titleTextView.setVisibility(View.GONE);
                searchEditText.requestFocus();
                searchEditText.setThreshold(2);
                searchEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        ProductsController productsController = new ProductsController(AllProductsActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", ProductsController.GET_AUTO_COMPLETE_DATABASE);
                        bundle1.putString("keyword", s.toString());
                        productsController.request(ProductsController.GET_AUTO_COMPLETE_DATABASE, bundle1);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            Intent intent = new Intent(AllProductsActivity.this, SearchResultsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("keyword", searchEditText.getText().toString());
                            intent.putExtras(bundle);
                            startActivity(intent);
                            hideKeyboard(searchEditText);
                            return true;
                        }
                        return false;
                    }
                });
            }
        });

        ProductsController controller = new ProductsController(this);
        Bundle allProductsBundle = new Bundle();
        allProductsBundle.putInt("action", ProductsController.GET_ALL_PRODUCTS_ACTION);
        allProductsBundle.putInt("countryId", Singleton.getInstance().country.id);
        controller.request(ProductsController.GET_ALL_PRODUCTS_ACTION, allProductsBundle);

        filterActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllProductsActivity.this, FilterOptionsActivity.class);
                Bundle bundle = new Bundle();
                if (country != null) {
                    bundle.putSerializable("country", country);
                }

                if (!tags.equals("")) {
                    bundle.putString("tags", tags);
                }

                if (priceFrom != getResources().getInteger(R.integer.startPriceRange) || priceTo != getResources().getInteger(R.integer.endPriceRange)) {
                    bundle.putInt("priceFrom", priceFrom);
                    bundle.putInt("priceTo", priceTo);
                }
                intent.putExtras(bundle);
                startActivityForResult(intent, FILTER_TYPE_REQUEST_CODE);
            }
        });

        sortActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllProductsActivity.this, SortOptionsActivity.class);
                startActivityForResult(intent, SORT_TYPE_REQUEST_CODE);
            }
        });

        cartIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Singleton.getInstance().cart.size() > 0) {
                    Intent intent = new Intent(AllProductsActivity.this, ResetActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(AllProductsActivity.this, getResources().getString(R.string.cartEmpty), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    private void setLoadingContainer() {
        mLoadingContainer = relativeLayout;
    }

    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        ProductsController controller = new ProductsController(this);
        Bundle allProductsBundle = new Bundle();
        switch (requestCode) {
            case SORT_TYPE_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    int selectedSortType = data.getExtras().getInt("type");
                    if (!tags.equals("")) {
                        allProductsBundle.putString("tags", tags);
                    }

                    if (country == null) {
                        allProductsBundle.putInt("countryId", Singleton.getInstance().country.id);
                    } else {
                        allProductsBundle.putInt("countryId", country.id);
                    }

                    if (priceFrom != getResources().getInteger(R.integer.startPriceRange) || priceTo != getResources().getInteger(R.integer.endPriceRange)) {
                        allProductsBundle.putInt("priceFrom", priceFrom);
                        allProductsBundle.putInt("priceTo", priceTo);
                    }
                    switch (selectedSortType) {
                        case 1:
                            allProductsBundle.putInt("action", ProductsController.GET_ALL_PRODUCTS_ACTION);
                            allProductsBundle.putString("sort", CommonConstants.SORT_PRICE_HIGH_TO_LOW);
                            controller.request(ProductsController.GET_ALL_PRODUCTS_ACTION, allProductsBundle);
                            break;

                        case 2:
                            allProductsBundle.putInt("action", ProductsController.GET_ALL_PRODUCTS_ACTION);
                            allProductsBundle.putString("sort", CommonConstants.SORT_PRICE_LOW_TO_HIGH);
                            sortType = CommonConstants.SORT_PRICE_LOW_TO_HIGH;
                            controller.request(ProductsController.GET_ALL_PRODUCTS_ACTION, allProductsBundle);
                            break;

                        case 3:
                            allProductsBundle.putInt("action", ProductsController.GET_ALL_PRODUCTS_ACTION);
                            allProductsBundle.putString("sort", CommonConstants.SORT_BEST_SELLER);
                            sortType = CommonConstants.SORT_BEST_SELLER;
                            controller.request(ProductsController.GET_ALL_PRODUCTS_ACTION, allProductsBundle);
                            break;

                        case 4:
                            allProductsBundle.putInt("action", ProductsController.GET_ALL_PRODUCTS_ACTION);
                            allProductsBundle.putString("sort", CommonConstants.SORT_NEW_ARRIVALS);
                            sortType = CommonConstants.SORT_NEW_ARRIVALS;
                            controller.request(ProductsController.GET_ALL_PRODUCTS_ACTION, allProductsBundle);
                            break;

                        case 5:
                            allProductsBundle.putInt("action", ProductsController.GET_ALL_PRODUCTS_ACTION);
                            allProductsBundle.putString("sort", CommonConstants.SORT_MOST_VIEWED);
                            sortType = CommonConstants.SORT_MOST_VIEWED;
                            controller.request(ProductsController.GET_ALL_PRODUCTS_ACTION, allProductsBundle);
                            break;

                        default:
                            break;
                    }
                }
                break;

            case FILTER_TYPE_REQUEST_CODE:

                if (resultCode == RESULT_OK && data != null) {
                    if (!sortType.equals("")) {
                        allProductsBundle.putString("sort", sortType);
                    }

                    allProductsBundle.putInt("action", ProductsController.GET_ALL_PRODUCTS_ACTION);

                    if (data.getExtras().containsKey("country")) {
                        country = (Country) data.getExtras().getSerializable("country");
                    }

                    if (country == null) {
                        allProductsBundle.putInt("countryId", Singleton.getInstance().country.id);
                    } else {
                        allProductsBundle.putInt("countryId", country.id);
                    }


                    if (data.getExtras().containsKey("tags")) {
                        tags = "";
                        tags = data.getExtras().getString("tags");
                        allProductsBundle.putString("tags", tags);
                    }

                    if (data.getExtras().containsKey("priceFrom")) {
                        priceFrom = data.getExtras().getInt("priceFrom");
                        allProductsBundle.putInt("priceFrom", priceFrom);
                    }

                    if (data.getExtras().containsKey("priceTo")) {
                        priceTo = data.getExtras().getInt("priceTo");
                        allProductsBundle.putInt("priceTo", priceTo);
                    }
                    controller.request(ProductsController.GET_ALL_PRODUCTS_ACTION, allProductsBundle);
                }
                break;

            case COUNTRIES_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle.containsKey("country")) {
                        SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                                Context.MODE_PRIVATE);
                        Singleton.getInstance().country = (Country) bundle.getSerializable("country");
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putInt("countryId", Singleton.getInstance().country.id);
                        editor.putString("countryEnglishName", Singleton.getInstance().country.englishName);
                        editor.putString("countryArabicName", Singleton.getInstance().country.arabicName);
                        editor.putString("countryCurrency", Singleton.getInstance().country.currency);
                        editor.putFloat("countryDollarValue", (float) Singleton.getInstance().country.dollarValue);
                        editor.commit();

                        if (Language.getCurrentLang(this).equals("en")) {
                            mCountriesButton.setText(Singleton.getInstance().country.englishName);
                        } else {
                            mCountriesButton.setText(Singleton.getInstance().country.arabicName);
                        }

                        relativeLayout.setVisibility(View.VISIBLE);
                        allProductsBundle.putInt("action", ProductsController.GET_ALL_PRODUCTS_ACTION);
                        allProductsBundle.putInt("countryId", Singleton.getInstance().country.id);
                        controller.request(ProductsController.GET_ALL_PRODUCTS_ACTION, allProductsBundle);
                    }
                }
                break;


            default:
                break;
        }
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {

        switch (action) {

            case ProductsController.GET_AUTO_COMPLETE_DATABASE:
                try {
                    ArrayAdapter adapter;
                    ArrayList<AutoComplete> autoCompleteCaches = (ArrayList) result;
                    ArrayList<String> autoCompleteNames = new ArrayList<>();

                    for (AutoComplete autoComplete : autoCompleteCaches) {
                        autoCompleteNames.add(autoComplete.getName());
                    }
                    adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, autoCompleteNames);
                    searchEditText.setThreshold(2);
                    searchEditText.setAdapter(adapter);
                } catch (Exception e) {

                }

            case ProductsController.GET_ALL_PRODUCTS_ACTION:
                ArrayList<Product> products = (ArrayList<Product>) result;
                AllProductsRecyclerViewAdapter allProductsRecyclerViewAdapter = new AllProductsRecyclerViewAdapter(this, products);
                productsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                productsRecyclerView.setAdapter(allProductsRecyclerViewAdapter);
                relativeLayout.setVisibility(View.GONE);
                break;

            default:
                break;
        }
        Sensor.unlockSensor(this);

    }

}
