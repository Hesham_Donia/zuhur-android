package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Order;
import com.bridge.zuhur.utils.Language;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by heshamkhaled on 6/20/16.
 */
public class UnCompletedRecyclerViewAdapter extends RecyclerView.Adapter<UnCompletedRecyclerViewAdapter.ViewHolder> {

    Typeface typeface;
    ViewHolder viewHolder;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<Order> mOrders;

    public UnCompletedRecyclerViewAdapter(Context context, ArrayList<Order> orders) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mOrders = orders;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.un_completed_order_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        ordinaryHolder.orderCreationDateTextView.setText(mOrders.get(position).creationDate);
        ordinaryHolder.orderNumberTextView.setText("# " + mOrders.get(position).id);
        if (Language.getCurrentLang(mContext).equals("en")) {
            ordinaryHolder.orderTotalPriceTextView.setText(mOrders.get(position).orderStatus.englishName);
        } else {
            ordinaryHolder.orderTotalPriceTextView.setText(mOrders.get(position).orderStatus.arabicName);
        }
        ordinaryHolder.orderCreationDateTextView.setTypeface(typeface);
        ordinaryHolder.orderNumberTextView.setTypeface(typeface);
        ordinaryHolder.orderTotalPriceTextView.setTypeface(typeface);
    }

    @Override
    public int getItemCount() {
        return mOrders.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.orderCreationDateTextView)
        TextView orderCreationDateTextView;

        @InjectView(R.id.orderNumberTextView)
        TextView orderNumberTextView;

        @InjectView(R.id.orderTotalPriceTextView)
        TextView orderTotalPriceTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(mContext, OrderDetailsActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("order", mOrders.get(getAdapterPosition()));
//                    intent.putExtras(bundle);
//                    mContext.startActivity(intent);
//                }
//            });
        }

        public void emptyViewHolder() {
            orderCreationDateTextView.setText("");
            orderNumberTextView.setText("");
            orderTotalPriceTextView.setText("");
        }
    }
}
