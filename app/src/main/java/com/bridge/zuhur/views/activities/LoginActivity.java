package com.bridge.zuhur.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.back_services.NotificationsService;
import com.bridge.zuhur.controllers.AuthenticationController;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.FavouritesController;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.controllers.WishListController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.entities.User;
import com.bridge.zuhur.utils.CommonConstants;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class LoginActivity extends AbstractActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.loadingContainer)
    RelativeLayout relativeLayout;

    @InjectView(R.id.cartIconImageView)
    ImageView cartIconImageView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.usernameEditText)
    EditText usernameEditText;

    @InjectView(R.id.passwordEditText)
    EditText passwordEditText;

    @InjectView(R.id.loginCardView)
    CardView loginCardView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.loginFacebookButton)
    LoginButton loginFacebookButton;

    @InjectView(R.id.signUpTextView)
    TextView signUpTextView;

    @InjectView(R.id.forgetMyPasswordTextView)
    TextView forgetMyPasswordTextView;

    @InjectView(R.id.countryButton)
    Button countryButton;


    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);

        ButterKnife.inject(this);


        setToolbar();
        setDrawerLayout();
        setNavigationView();
        setLoadingContainer();
        setCartItemsCount();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setCountriesButton();

        countryButton.setVisibility(View.GONE);
        setSupportActionBar(mToolbar);

        titleTextView.setText(getResources().getString(R.string.login));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setVisibility(View.INVISIBLE);
        cartIconImageView.setVisibility(View.INVISIBLE);
        cartItemsCount.setVisibility(View.INVISIBLE);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/arcena.ttf");
        usernameEditText.setTypeface(typeface);
        passwordEditText.setTypeface(typeface);

        loginCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usernameEditText.getText().toString().equals("") || passwordEditText.getText().toString().equals("")) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.emptyFieldsError), Toast.LENGTH_SHORT).show();
                } else {

                    relativeLayout.setVisibility(View.VISIBLE);
                    AuthenticationController authenticationController = new AuthenticationController(LoginActivity.this);
                    Bundle bundle1 = new Bundle();
                    JSONObject mainJsonObject = new JSONObject(), userDataJsonObject = new JSONObject();
                    JSONArray dataJsonArray = new JSONArray();
                    try {
                        userDataJsonObject.put("username", usernameEditText.getText().toString());
                        userDataJsonObject.put("password", passwordEditText.getText().toString());
                        dataJsonArray.put(userDataJsonObject);
                        mainJsonObject.put("data", dataJsonArray);
                        User user = new User();
                        user.userDataAsJson = mainJsonObject.toString();
                        bundle1.putInt("action", AuthenticationController.SIGN_IN_ACTION);
                        bundle1.putSerializable("user", user);
                        authenticationController.request(AuthenticationController.SIGN_IN_ACTION, bundle1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        final SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);

        loginFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settings.getString("isFacebookFirstTime", "no").equals("no")) {
                    loginFacebookButton.setReadPermissions("public_profile", "email");
                    loginFacebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            Log.d(CommonConstants.APPLICATION_LOG_TAG, "onSuccess");
                            AccessToken accessToken = loginResult.getAccessToken();
                            GraphRequest request = GraphRequest.newMeRequest(
                                    accessToken,
                                    new GraphRequest.GraphJSONObjectCallback() {
                                        @Override
                                        public void onCompleted(
                                                JSONObject object,
                                                GraphResponse response) {
                                            // Application code
                                            Log.d(CommonConstants.APPLICATION_LOG_TAG, object.toString());
                                            if (object.has("email")) {
                                                //sign up using these data
                                                AuthenticationController authenticationController = new AuthenticationController(LoginActivity.this);
                                                Bundle bundle1 = new Bundle();
                                                JSONObject mainJsonObject = new JSONObject();
                                                JSONArray dataJsonArray = new JSONArray();
                                                try {
                                                    JSONObject userJsonObject = new JSONObject();
                                                    userJsonObject.put("name", object.getString("name"));
                                                    userJsonObject.put("email", object.getString("email"));
                                                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "name: " + object.getString("name"));
//                    userJsonObject.put("code", "akijhsgdjk");
                                                    dataJsonArray.put(userJsonObject);

                                                    mainJsonObject.put("data", dataJsonArray);


                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                User user = new User();
                                                user.userDataAsJson = mainJsonObject.toString();
                                                Log.d(CommonConstants.APPLICATION_LOG_TAG, mainJsonObject.toString());
                                                bundle1.putInt("action", AuthenticationController.SIGN_UP_FACEBOOK_ACTION);
                                                bundle1.putSerializable("user", user);
                                                authenticationController.request(AuthenticationController.SIGN_UP_FACEBOOK_ACTION, bundle1);

                                            } else {
                                                // error message
                                                Toast.makeText(LoginActivity.this, getResources().getString(R.string.emailFacebookError), Toast.LENGTH_LONG).show();
                                            }

                                        }
                                    });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id,name,link,email,birthday");
                            request.setParameters(parameters);
                            request.executeAsync();
                        }

                        @Override
                        public void onCancel() {
                            Log.d(CommonConstants.APPLICATION_LOG_TAG, "onCancel");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Log.d(CommonConstants.APPLICATION_LOG_TAG, "onError: " + error.toString());
                        }
                    });
                } else {

                    loginFacebookButton.setReadPermissions("public_profile", "email");
                    loginFacebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            Log.d(CommonConstants.APPLICATION_LOG_TAG, "onSuccess");
                            AccessToken accessToken = loginResult.getAccessToken();
                            GraphRequest request = GraphRequest.newMeRequest(
                                    accessToken,
                                    new GraphRequest.GraphJSONObjectCallback() {
                                        @Override
                                        public void onCompleted(
                                                JSONObject object,
                                                GraphResponse response) {
                                            // Application code
                                            Log.d(CommonConstants.APPLICATION_LOG_TAG, object.toString());
                                            if (object.has("email")) {
                                                //sign up using these data
                                                JSONObject mainJsonObject = new JSONObject(), userDataJsonObject = new JSONObject();
                                                JSONArray dataJsonArray = new JSONArray();
                                                try {
                                                    userDataJsonObject.put("email", object.getString("email"));
                                                    dataJsonArray.put(userDataJsonObject);
                                                    mainJsonObject.put("data", dataJsonArray);
                                                    User user = new User();
                                                    user.userDataAsJson = mainJsonObject.toString();

                                                    AuthenticationController authenticationController = new AuthenticationController(LoginActivity.this);
                                                    Bundle bundle = new Bundle();
                                                    bundle.putInt("action", AuthenticationController.SIGN_IN_FACEBOOK_ACTION);
                                                    bundle.putSerializable("user", user);
                                                    authenticationController.request(AuthenticationController.SIGN_IN_FACEBOOK_ACTION, bundle);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            } else {
                                                // error message
                                                Toast.makeText(LoginActivity.this, getResources().getString(R.string.emailFacebookError), Toast.LENGTH_LONG).show();
                                            }

                                        }
                                    });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id,name,link,email,birthday");
                            request.setParameters(parameters);
                            request.executeAsync();
                        }

                        @Override
                        public void onCancel() {
                            Log.d(CommonConstants.APPLICATION_LOG_TAG, "onCancel");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Log.d(CommonConstants.APPLICATION_LOG_TAG, "onError: " + error.toString());
                        }
                    });
                }
            }
        });

        signUpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        forgetMyPasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetMyPasswordActivity.class);
                startActivity(intent);
            }
        });


    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    private void setLoadingContainer() {
        mLoadingContainer = relativeLayout;
    }

    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {


        switch (action) {

            case AuthenticationController.SIGN_IN_ACTION:
                if (result.size() == 0) {
                    relativeLayout.setVisibility(View.GONE);
                } else {
                    User user1 = (User) result.get(0);
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "user id in login: " + user1.id);
                    Singleton.getInstance().currentUser = user1;

                    SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                            Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putBoolean("isLogged", true);
                    editor.putString("address", user1.address);
                    editor.putString("username", user1.username);
                    editor.putString("email", user1.email);
                    editor.putInt("id", user1.id);
                    editor.putString("name", user1.name);
                    editor.putInt("phonesNumber", user1.phones.size());
                    editor.putInt("points", user1.points);
                    for (int i = 0; i < user1.phones.size(); i++) {
                        editor.putString("number" + i, user1.phones.get(i).number);
                        editor.putBoolean("number" + i + "isPrimary", user1.phones.get(i).isPrimary);
                    }
                    editor.putString("token", Singleton.getInstance().token);
                    editor.putInt("level", user1.level);
                    editor.putString("code", user1.code);


                    relativeLayout.setVisibility(View.GONE);

                    if (!settings.getBoolean("isGetFavourites", false)) {
                        FavouritesController favouritesController = new FavouritesController(LoginActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", FavouritesController.GET_ALL_FAVOURITES_SERVER_ACTION);
                        favouritesController.request(FavouritesController.GET_ALL_FAVOURITES_SERVER_ACTION, bundle1);
                        editor.putBoolean("isGetFavourites", true);
                    }

                    if (!settings.getBoolean("isGetWishList", false)) {
                        WishListController wishListController = new WishListController(LoginActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", WishListController.GET_WISH_LIST_FROM_SERVER_ACTION);
                        wishListController.request(WishListController.GET_WISH_LIST_FROM_SERVER_ACTION, bundle1);
                        editor.putBoolean("isGetWishList", true);
                    }

                    if (!settings.getBoolean("isGetOrders", false)) {
                        Log.d(CommonConstants.APPLICATION_LOG_TAG, "I'm getting orders");
                        OrderController orderController = new OrderController(LoginActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", OrderController.GET_ORDERS_FROM_SERVER_ACTION);
                        orderController.request(OrderController.GET_ORDERS_FROM_SERVER_ACTION, bundle1);
                        editor.putBoolean("isGetOrders", true);
                    }

                    if (!settings.getBoolean("isGetRemainders", false)) {
                        ExtrasController extrasController = new ExtrasController(LoginActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", ExtrasController.REMAINDERS_ACTION);
                        extrasController.request(ExtrasController.REMAINDERS_ACTION, bundle1);
                        editor.putBoolean("isGetRemainders", true);
                    }

                    Intent intent = new Intent(this, NotificationsService.class);
                    startService(intent);


                    editor.commit();

                    Toast.makeText(this, getResources().getString(R.string.loginSuccess), Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;

            case AuthenticationController.SIGN_IN_FACEBOOK_ACTION:
                User user1 = (User) result.get(0);
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "user id in login: " + user1.id);
                Singleton.getInstance().currentUser = user1;

                SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("isLogged", true);
                editor.putBoolean("isFacebookLogged", true);
                if (user1.address != null) {
                    editor.putString("address", user1.address);
                }

                if (user1.username != null) {
                    editor.putString("username", user1.username);
                }

                if (user1.id != 0) {
                    editor.putInt("id", user1.id);
                }

                if (user1.name != null) {
                    editor.putString("name", user1.name);
                }

                if (user1.phones.size() != 0) {
                    editor.putInt("phonesNumber", user1.phones.size());
                    for (int i = 0; i < user1.phones.size(); i++) {
                        editor.putString("number" + i, user1.phones.get(i).number);
                        editor.putBoolean("number" + i + "isPrimary", user1.phones.get(i).isPrimary);
                    }
                } else {
                    editor.putInt("phonesNumber", 0);
                }

                editor.putInt("points", user1.points);

                editor.putString("token", Singleton.getInstance().token);
                editor.putInt("level", user1.level);
                editor.putString("code", user1.code);
                relativeLayout.setVisibility(View.GONE);

                if (!settings.getBoolean("isGetFavourites", false)) {
                    FavouritesController favouritesController = new FavouritesController(LoginActivity.this);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("action", FavouritesController.GET_ALL_FAVOURITES_SERVER_ACTION);
                    favouritesController.request(FavouritesController.GET_ALL_FAVOURITES_SERVER_ACTION, bundle1);
                    editor.putBoolean("isGetFavourites", true);
                }

                if (!settings.getBoolean("isGetWishList", false)) {
                    WishListController wishListController = new WishListController(LoginActivity.this);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("action", WishListController.GET_WISH_LIST_FROM_SERVER_ACTION);
                    wishListController.request(WishListController.GET_WISH_LIST_FROM_SERVER_ACTION, bundle1);
                    editor.putBoolean("isGetWishList", true);
                }

                if (!settings.getBoolean("isGetOrders", false)) {
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "I'm getting orders");
                    OrderController orderController = new OrderController(LoginActivity.this);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("action", OrderController.GET_ORDERS_FROM_SERVER_ACTION);
                    orderController.request(OrderController.GET_ORDERS_FROM_SERVER_ACTION, bundle1);
                    editor.putBoolean("isGetOrders", true);
                }

                if (!settings.getBoolean("isGetRemainders", false)) {
                    ExtrasController extrasController = new ExtrasController(LoginActivity.this);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("action", ExtrasController.REMAINDERS_ACTION);
                    extrasController.request(ExtrasController.REMAINDERS_ACTION, bundle1);
                    editor.putBoolean("isGetRemainders", true);
                }

                Intent intent = new Intent(this, NotificationsService.class);
                startService(intent);


                editor.commit();

                Toast.makeText(this, getResources().getString(R.string.loginSuccess), Toast.LENGTH_SHORT).show();
                finish();

                break;

            default:
                break;
        }


    }
}
