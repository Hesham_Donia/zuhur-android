package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.database.Tag;
import com.bridge.zuhur.entities.Color;
import com.bridge.zuhur.views.activities.FilterOptionsActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 6/19/16.
 */
public class ColorsRecyclerViewAdapter extends RecyclerView.Adapter<ColorsRecyclerViewAdapter.ViewHolder> {


    ViewHolder viewHolder;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<Color> mColors;

    public ColorsRecyclerViewAdapter(Context context, ArrayList<Color> colors) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mColors = colors;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.color_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

//        ordinaryHolder.drawable.setColorFilter(android.graphics.Color.parseColor(mColors.get(position).hexCode), PorterDuff.Mode.SRC_ATOP);
        ordinaryHolder.colorItemImageView.setBackgroundDrawable(ordinaryHolder.drawable);
        GradientDrawable shapeDrawable = (GradientDrawable) ordinaryHolder.colorItemImageView.getBackground();
        shapeDrawable.setColor(android.graphics.Color.parseColor(mColors.get(position).hexCode));
    }

    @Override
    public int getItemCount() {
        return mColors.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.colorItemImageView)
        ImageView colorItemImageView;

        Drawable drawable = mContext.getResources().getDrawable(R.drawable.yellow_circle);

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    com.bridge.zuhur.entities.Tag tag = new com.bridge.zuhur.entities.Tag();
                    tag.name = mColors.get(getAdapterPosition()).englishName;

                    Tag databaseTag = new Tag(tag);
                    databaseTag.setColored(true);
                    ((FilterOptionsActivity) mContext).addToTagsArrayList(databaseTag);
                }
            });
        }

        public void emptyViewHolder() {
            colorItemImageView.setImageBitmap(null);
        }
    }

}
