package com.bridge.zuhur.views.custom_views;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.database.Tag;
import com.tokenautocomplete.TokenCompleteTextView;

/**
 * Created by bridgetomarket on 6/2/16.
 */
public class ContactsCompletionView extends TokenCompleteTextView<Tag> {
    public ContactsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View getViewForObject(Tag tag) {

        LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) l.inflate(R.layout.auto_complete_token, (ViewGroup) ContactsCompletionView.this.getParent(), false);
        ((TextView) view.findViewById(R.id.name)).setText(tag.getTagName());

        return view;
    }

    @Override
    protected Tag defaultObject(String completionText) {

        com.bridge.zuhur.entities.Tag tag = new com.bridge.zuhur.entities.Tag();
        tag.name = completionText;

        return new Tag(tag);
        //Stupid simple example of guessing if we have an email or not
//        int index = completionText.indexOf('@');
//        if (index == -1) {
//            return new Person(completionText, completionText.replace(" ", "") + "@example.com");
//        } else {
//            return new Person(completionText.substring(0, index), completionText);
//        }
    }
}
