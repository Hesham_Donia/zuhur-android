package com.bridge.zuhur.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.AuthenticationController;
import com.bridge.zuhur.entities.Country;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.entities.User;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class UpdateProfileActivity extends AbstractActivity {

    final static int COUNTRIES_REQUEST_CODE = 1;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.loadingContainer)
    RelativeLayout relativeLayout;

    @InjectView(R.id.cartIconImageView)
    ImageView cartIconImageView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.usernameEditText)
    EditText usernameEditText;

    @InjectView(R.id.nameEditText)
    EditText nameEditText;

    @InjectView(R.id.emailEditText)
    EditText emailEditText;

    @InjectView(R.id.phoneEditText)
    EditText phoneEditText;

    @InjectView(R.id.countryTextView)
    TextView countryTextView;

    @InjectView(R.id.updateProfileCardView)
    CardView updateProfileCardView;

    @InjectView(R.id.updatePasswordCardView)
    CardView updatePasswordCardView;

    @InjectView(R.id.countryButton)
    Button countryButton;

    Country country = null;
    User updatedUser = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        ButterKnife.inject(this);

        setToolbar();
        setDrawerLayout();
        setNavigationView();
        setLoadingContainer();
        setCartItemsCount();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setCountriesButton();
        countryButton.setVisibility(View.GONE);

        setSupportActionBar(mToolbar);

        titleTextView.setText(getResources().getString(R.string.updateProfile));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setVisibility(View.INVISIBLE);
        cartIconImageView.setVisibility(View.INVISIBLE);
        cartItemsCount.setVisibility(View.INVISIBLE);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/arcena.ttf");
        usernameEditText.setTypeface(typeface);
        nameEditText.setTypeface(typeface);
        emailEditText.setTypeface(typeface);
        phoneEditText.setTypeface(typeface);
        countryTextView.setTypeface(typeface);

        final User user = Singleton.getInstance().currentUser;
        usernameEditText.setText(user.username);
        nameEditText.setText(user.name);
        emailEditText.setText(user.email);
        phoneEditText.setText(user.phones.get(0).number);

        countryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UpdateProfileActivity.this, CountriesActivity.class);
                startActivityForResult(intent, COUNTRIES_REQUEST_CODE);
            }
        });

        updateProfileCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (nameEditText.getText().toString().equals("") || usernameEditText.getText().toString().equals("")
                        || emailEditText.getText().toString().equals("") || phoneEditText.getText().toString().equals("") || country == null) {
                    Toast.makeText(UpdateProfileActivity.this, getResources().getString(R.string.emptyFieldsError), Toast.LENGTH_LONG).show();
                } else {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches()) {
                        Toast.makeText(UpdateProfileActivity.this, getResources().getString(R.string.emailError), Toast.LENGTH_LONG).show();
                        emailEditText.setText("");
                    } else {

                        relativeLayout.setVisibility(View.VISIBLE);
                        AuthenticationController authenticationController = new AuthenticationController(UpdateProfileActivity.this);
                        Bundle bundle1 = new Bundle();
                        JSONObject mainJsonObject = new JSONObject();
                        JSONArray dataJsonArray = new JSONArray();
                        try {
                            JSONObject userJsonObject = new JSONObject();
                            userJsonObject.put("name", nameEditText.getText().toString());
                            userJsonObject.put("username", usernameEditText.getText().toString());
                            userJsonObject.put("email", emailEditText.getText().toString());
                            userJsonObject.put("address", country.englishName);

                            JSONArray phonesJsonArray = new JSONArray();
                            JSONObject phone1JsonObject = new JSONObject();
                            phone1JsonObject.put("phone", phoneEditText.getText().toString());
                            phone1JsonObject.put("is_primary", "1");

                            phonesJsonArray.put(phone1JsonObject);

                            userJsonObject.put("phones", phonesJsonArray);

                            dataJsonArray.put(userJsonObject);

                            mainJsonObject.put("data", dataJsonArray);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        updatedUser.name = nameEditText.getText().toString();
                        updatedUser.username = usernameEditText.getText().toString();
                        updatedUser.email = emailEditText.getText().toString();
                        updatedUser.address = country.englishName;
                        updatedUser.userDataAsJson = mainJsonObject.toString();
                        Log.d(CommonConstants.APPLICATION_LOG_TAG, "updatedUser.userDataAsJson: " + updatedUser.userDataAsJson);
                        bundle1.putInt("action", AuthenticationController.UPDATE_PROFILE_ACTION);
                        bundle1.putSerializable("user", updatedUser);
                        authenticationController.request(AuthenticationController.UPDATE_PROFILE_ACTION, bundle1);
                    }
                }
            }
        });

        updatePasswordCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UpdateProfileActivity.this, UpdatePasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == COUNTRIES_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {
                Bundle bundle = data.getExtras();
                if (bundle.containsKey("country")) {
                    country = (Country) bundle.getSerializable("country");
                    if (Language.getCurrentLang(this).equals("en")) {
                        countryTextView.setText(country.englishName);
                    } else {
                        countryTextView.setText(country.arabicName);
                    }
                }
            }
        }
    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    private void setLoadingContainer() {
        mLoadingContainer = relativeLayout;
    }

    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }


    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {
        relativeLayout.setVisibility(View.GONE);
        if (result == null) {
            Toast.makeText(this, getResources().getString(R.string.updateProfileError), Toast.LENGTH_LONG).show();
            nameEditText.setText("");
            usernameEditText.setText("");
            country = null;
            countryTextView.setText(getResources().getString(R.string.country));
            emailEditText.setText("");
            phoneEditText.setText("");
        } else {
            Toast.makeText(this, getResources().getString(R.string.updateProfileSuccess), Toast.LENGTH_LONG).show();

            Singleton.getInstance().currentUser.name = updatedUser.name;
            Singleton.getInstance().currentUser.username = updatedUser.username;
            Singleton.getInstance().currentUser.email = updatedUser.email;
            Singleton.getInstance().currentUser.address = updatedUser.address;

            SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();

            editor.putString("name", updatedUser.name);
            editor.putString("address", updatedUser.address);
            editor.putString("username", updatedUser.username);
            editor.putString("email", updatedUser.email);

            editor.commit();

            finish();
        }
    }
}
