package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.views.activities.ResetActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 5/29/16.
 */
public class ResetRecyclerViewAdapter extends RecyclerView.Adapter<ResetRecyclerViewAdapter.ViewHolder> {

    Typeface typeface;
    ViewHolder viewHolder;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<Product> mProducts;

    public ResetRecyclerViewAdapter(Context context, ArrayList<Product> products) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mProducts = products;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.reset_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        Picasso.with(mContext).load(mProducts.get(position).imagePath).into(ordinaryHolder.resetProductImageView);

        if (Language.getCurrentLang(mContext).equals("en")) {
            ordinaryHolder.resetProductNameTextView.setText(mProducts.get(position).englishName);
        } else {
            ordinaryHolder.resetProductNameTextView.setText(mProducts.get(position).arabicName);
        }

        ordinaryHolder.priceTextView.setText(String.valueOf(mProducts.get(position).price + " " + Singleton.getInstance().country.currency));

        ordinaryHolder.finalPriceTextView.setText(String.valueOf(Integer.valueOf(ordinaryHolder.quantityTextView.getText().toString()) *
                mProducts.get(position).price) + " " + Singleton.getInstance().country.currency);

        ordinaryHolder.increaseQuantityImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ++Singleton.getInstance().cart.get(position).quantity;
                ordinaryHolder.quantityTextView.setText(String.valueOf(Integer.valueOf(ordinaryHolder.quantityTextView.getText().toString()) + 1));
                ordinaryHolder.finalPriceTextView.setText(String.valueOf(Integer.valueOf(ordinaryHolder.quantityTextView.getText().toString()) *
                        mProducts.get(position).price) + " " + Singleton.getInstance().country.currency);

                ((ResetActivity) mContext).updateTotalPriceView();
            }
        });

        ordinaryHolder.decreaseQuantityImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                --Singleton.getInstance().cart.get(position).quantity;
                if (Integer.valueOf(ordinaryHolder.quantityTextView.getText().toString()) > 0) {
                    ordinaryHolder.quantityTextView.setText(String.valueOf(Integer.valueOf(ordinaryHolder.quantityTextView.getText().toString()) - 1));
                    ordinaryHolder.finalPriceTextView.setText(String.valueOf(Integer.valueOf(ordinaryHolder.quantityTextView.getText().toString()) *
                            mProducts.get(position).price) + " " + Singleton.getInstance().country.currency);
                    ((ResetActivity) mContext).updateTotalPriceView();
                }
            }
        });

        ordinaryHolder.deleteResetProductImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Singleton.getInstance().cart.remove(position);
                notifyItemRemoved(position);
                notifyDataSetChanged();
                ((ResetActivity) mContext).updateCartItemsCount();
                ((ResetActivity) mContext).updateTotalPriceView();
            }
        });

        if (mProducts.get(position).giftCard != null) {
            ordinaryHolder.giftCardCardView.setVisibility(View.VISIBLE);
            ordinaryHolder.giftCardCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            if (Language.getCurrentLang(mContext).equals("en")) {
                ordinaryHolder.giftCardNameReset.setText(mProducts.get(position).giftCard.nameEn);
            } else {
                ordinaryHolder.giftCardNameReset.setText(mProducts.get(position).giftCard.nameAr);
            }

            ordinaryHolder.giftCardPriceReset.setText(String.valueOf(mProducts.get(position).giftCard.price * mProducts.get(position).quantity) + " " + Singleton.getInstance().country.currency);

            ordinaryHolder.deleteGiftCardImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProducts.get(position).giftCard = null;
                    ordinaryHolder.giftCardCardView.setVisibility(View.GONE);
                    ((ResetActivity) mContext).updateTotalPriceView();
                }
            });
        }

        if (mProducts.get(position).packagingWay != null) {
            ordinaryHolder.packageWayCardView.setVisibility(View.VISIBLE);
            ordinaryHolder.packageWayCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            if (Language.getCurrentLang(mContext).equals("en")) {
                ordinaryHolder.packageWayNameReset.setText(mProducts.get(position).packagingWay.nameEn);
            } else {
                ordinaryHolder.packageWayNameReset.setText(mProducts.get(position).packagingWay.nameAr);
            }

            ordinaryHolder.packageWayPriceReset.setText(String.valueOf(mProducts.get(position).packagingWay.price * mProducts.get(position).quantity) + " " + Singleton.getInstance().country.currency);

            ordinaryHolder.deletePackageWayImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProducts.get(position).packagingWay = null;
                    ordinaryHolder.packageWayCardView.setVisibility(View.GONE);
                    ((ResetActivity) mContext).updateTotalPriceView();
                }
            });
        }

        ordinaryHolder.addGiftCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderController extrasController = new OrderController(mContext);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", OrderController.GIFT_CARDS_ACTION);
                bundle1.putInt("productId", mProducts.get(position).id);
                extrasController.request(OrderController.GIFT_CARDS_ACTION, bundle1);
            }
        });

        ordinaryHolder.addPackageWayCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderController extrasController = new OrderController(mContext);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", OrderController.PACKAGING_WAYS_ACTION);
                bundle1.putInt("productId", mProducts.get(position).id);
                extrasController.request(OrderController.PACKAGING_WAYS_ACTION, bundle1);
            }
        });


        ordinaryHolder.resetProductNameTextView.setTypeface(typeface);
        ordinaryHolder.priceTextView.setTypeface(typeface);
        ordinaryHolder.xTextView.setTypeface(typeface);
        ordinaryHolder.quantityTextView.setTypeface(typeface);
        ordinaryHolder.equalTextView.setTypeface(typeface);
        ordinaryHolder.finalPriceTextView.setTypeface(typeface);
        ordinaryHolder.addGiftCardTextView.setTypeface(typeface);
        ordinaryHolder.addPackageWayTextView.setTypeface(typeface);
        ordinaryHolder.giftCardNameReset.setTypeface(typeface);
        ordinaryHolder.giftCardPriceReset.setTypeface(typeface);
        ordinaryHolder.packageWayNameReset.setTypeface(typeface);
        ordinaryHolder.packageWayPriceReset.setTypeface(typeface);
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.resetProductImageView)
        ImageView resetProductImageView;

        @InjectView(R.id.resetProductNameTextView)
        TextView resetProductNameTextView;

        @InjectView(R.id.priceTextView)
        TextView priceTextView;

        @InjectView(R.id.xTextView)
        TextView xTextView;

        @InjectView(R.id.increaseQuantityImageView)
        ImageView increaseQuantityImageView;

        @InjectView(R.id.quantityTextView)
        TextView quantityTextView;

        @InjectView(R.id.decreaseQuantityImageView)
        ImageView decreaseQuantityImageView;

        @InjectView(R.id.equalTextView)
        TextView equalTextView;

        @InjectView(R.id.finalPriceTextView)
        TextView finalPriceTextView;

        @InjectView(R.id.deleteResetProductImageView)
        ImageView deleteResetProductImageView;

        @InjectView(R.id.giftCardCardView)
        CardView giftCardCardView;

        @InjectView(R.id.giftCardNameReset)
        TextView giftCardNameReset;

        @InjectView(R.id.giftCardPriceReset)
        TextView giftCardPriceReset;

        @InjectView(R.id.deleteGiftCardImageView)
        ImageView deleteGiftCardImageView;

        @InjectView(R.id.packageWayCardView)
        CardView packageWayCardView;

        @InjectView(R.id.packageWayNameReset)
        TextView packageWayNameReset;

        @InjectView(R.id.packageWayPriceReset)
        TextView packageWayPriceReset;

        @InjectView(R.id.deletePackageWayImageView)
        ImageView deletePackageWayImageView;

        @InjectView(R.id.addPackageWayCardView)
        CardView addPackageWayCardView;

        @InjectView(R.id.addGiftCardView)
        CardView addGiftCardView;

        @InjectView(R.id.addPackageWayTextView)
        TextView addPackageWayTextView;

        @InjectView(R.id.addGiftCardTextView)
        TextView addGiftCardTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }

        public void emptyViewHolder() {
            resetProductImageView.setImageBitmap(null);
            resetProductNameTextView.setText("");
            priceTextView.setText("");
            quantityTextView.setText("" + mProducts.get(getAdapterPosition()).quantity);
            finalPriceTextView.setText("" + (mProducts.get(getAdapterPosition()).price * mProducts.get(getAdapterPosition()).quantity));
        }
    }
}
