package com.bridge.zuhur.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Country;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class CountriesActivity extends Activity {

    @InjectView(R.id.countriesRadioGroup)
    RadioGroup countriesRadioGroup;

    @InjectView(R.id.confirmCountryChooseButton)
    Button confirmCountryChooseButton;

    Country selectedCountry = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries);

        ButterKnife.inject(this);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 1);

        getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        Log.d(CommonConstants.APPLICATION_LOG_TAG, "countries length: " + Singleton.getInstance().countries.size());

        for (int i = 0; i < Singleton.getInstance().countries.size(); i++) {
            Country country = Singleton.getInstance().countries.get(i);
            RadioButton radioButton = new RadioButton(this);
            radioButton.setId(i);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            radioButton.setLayoutParams(layoutParams);

            if (Language.getCurrentLang(this).equals("en")) {
                radioButton.setText(country.englishName);
            } else {
                radioButton.setText(country.arabicName);
            }

            countriesRadioGroup.addView(radioButton);
        }

        countriesRadioGroup.invalidate();
        countriesRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selectedCountry = Singleton.getInstance().countries.get(checkedId);
            }
        });

        confirmCountryChooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();

                if (selectedCountry != null) {
                    bundle.putSerializable("country", selectedCountry);
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "selectedCountryId: " + selectedCountry.id);
                }
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();

            }
        });
    }
}
