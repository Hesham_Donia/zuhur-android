package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.MoreItem;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.views.activities.FavouritesActivity;
import com.bridge.zuhur.views.activities.OrdersHistoryActivity;
import com.bridge.zuhur.views.activities.RemaindersActivity;
import com.bridge.zuhur.views.activities.UnCompletedOrdersActivity;
import com.bridge.zuhur.views.activities.UpdateProfileActivity;
import com.bridge.zuhur.views.activities.WishListActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 5/30/16.
 */
public class MoreRecyclerViewAdapter extends RecyclerView.Adapter<MoreRecyclerViewAdapter.ViewHolder> {

    ViewHolder viewHolder;
    Typeface typeface;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<MoreItem> mItems;

    public MoreRecyclerViewAdapter(Context context, ArrayList<MoreItem> items) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.more_list_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();
        Picasso.with(mContext).load(mItems.get(position).icon).into(ordinaryHolder.listItemImageView);
        ordinaryHolder.listItemTextView.setTypeface(typeface);
        ordinaryHolder.listItemTextView.setText(mItems.get(position).name);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.listItemImageView)
        ImageView listItemImageView;

        @InjectView(R.id.listItemTextView)
        TextView listItemTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences settings = mContext.getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                            Context.MODE_PRIVATE);

                    switch (getAdapterPosition()) {
                        case 0:
                            if (settings.getBoolean("isLogged", false) || settings.getBoolean("isFacebookLogged", false)) {
                                Intent intent = new Intent(mContext, FavouritesActivity.class);
                                mContext.startActivity(intent);
                            } else {
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                            }
                            break;

                        case 1:
                            if (settings.getBoolean("isLogged", false) || settings.getBoolean("isFacebookLogged", false)) {
                                Intent intent = new Intent(mContext, WishListActivity.class);
                                mContext.startActivity(intent);
                            } else {
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                            }
                            break;

                        case 2:
                            if (settings.getBoolean("isLogged", false) || settings.getBoolean("isFacebookLogged", false)) {
                                Intent intent = new Intent(mContext, OrdersHistoryActivity.class);
                                mContext.startActivity(intent);
                            } else {
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                            }
                            break;

                        case 3:
                            if (settings.getBoolean("isLogged", false) || settings.getBoolean("isFacebookLogged", false)) {
                                Intent intent = new Intent(mContext, RemaindersActivity.class);
                                mContext.startActivity(intent);
                            } else {
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                            }
                            break;

                        case 4:
                            if (settings.getBoolean("isLogged", false) || settings.getBoolean("isFacebookLogged", false)) {
                                Intent intent = new Intent(mContext, UpdateProfileActivity.class);
                                mContext.startActivity(intent);
                            } else {
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                            }
                            break;

                        case 5: // track orders
                            if (settings.getBoolean("isLogged", false) || settings.getBoolean("isFacebookLogged", false)) {
                                Intent intent = new Intent(mContext, UnCompletedOrdersActivity.class);
                                mContext.startActivity(intent);
                            } else {
                                Toast.makeText(mContext, mContext.getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                            }
                            break;

                        default:
                            break;
                    }
                }
            });
        }

        public void emptyViewHolder() {
            listItemImageView.setImageBitmap(null);
            listItemTextView.setText("");
        }
    }
}
