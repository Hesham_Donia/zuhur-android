package com.bridge.zuhur.views.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.database.AutoComplete;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.GiftCard;
import com.bridge.zuhur.entities.PackagingWay;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.views.adapters.ResetRecyclerViewAdapter;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ResetActivity extends AbstractActivity {

    final static int PACKAGING_WAYS_REQUEST_CODE = 1;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    @InjectView(R.id.resetProductsRecyclerView)
    RecyclerView resetProductsRecyclerView;

    @InjectView(R.id.confirmResetButton)
    CardView confirmResetButton;

    @InjectView(R.id.confirmResetTextView)
    TextView confirmResetTextView;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.totalPriceTextView)
    TextView totalPriceTextView;

    @InjectView(R.id.countryButton)
    Button countryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);

        ButterKnife.inject(this);

        setToolbar();
        setDrawerLayout();
        setNavigationView();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setCartItemsCount();
        setCountriesButton();
        setSupportActionBar(mToolbar);

        countryButton.setVisibility(View.GONE);

        ProductsController productsController = new ProductsController(this);
        Bundle bundle1 = new Bundle();
        String productsIds = "";
        for (int i = 0; i < Singleton.getInstance().cart.size(); i++) {
            if (i == (Singleton.getInstance().cart.size() - 1)) {
                productsIds = productsIds + Singleton.getInstance().cart.get(i).id;
            } else {
                productsIds = productsIds + Singleton.getInstance().cart.get(i).id + ",";
            }

        }
        bundle1.putString("id", productsIds);
        bundle1.putInt("countryId", Singleton.getInstance().country.id);
        bundle1.putInt("action", ProductsController.RELATED_PRODUCTS_ACTION);
        productsController.request(ProductsController.RELATED_PRODUCTS_ACTION, bundle1);

        titleTextView.setText(getResources().getString(R.string.reset));


        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setVisibility(View.VISIBLE);
                titleTextView.setVisibility(View.GONE);
                searchEditText.requestFocus();
                searchEditText.setThreshold(2);
                searchEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        ProductsController productsController = new ProductsController(ResetActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", ProductsController.GET_AUTO_COMPLETE_DATABASE);
                        bundle1.putString("keyword", s.toString());
                        productsController.request(ProductsController.GET_AUTO_COMPLETE_DATABASE, bundle1);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            Intent intent = new Intent(ResetActivity.this, SearchResultsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("keyword", searchEditText.getText().toString());
                            intent.putExtras(bundle);
                            startActivity(intent);
                            hideKeyboard(searchEditText);
                            return true;
                        }
                        return false;
                    }
                });
            }
        });

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/arcena.ttf");
        confirmResetTextView.setTypeface(typeface);

        confirmResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Singleton.getInstance().currentUser == null) {
                    Toast.makeText(ResetActivity.this, getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                } else {
                    if (Singleton.getInstance().cart.size() > 0) {
                        Intent intent = new Intent(ResetActivity.this, OrderConfirmationActivity.class);
                        Bundle bundle = new Bundle();
                        int pointsPrice = 0;
                        double finalPrice = 0.0;

                        for (Product product : Singleton.getInstance().cart) {
                            finalPrice = finalPrice + (product.price * product.quantity);
                            pointsPrice = pointsPrice + (product.pointsPrice * product.quantity);

                            if (product.giftCard != null) {
                                finalPrice = finalPrice + product.giftCard.price;
                                pointsPrice = pointsPrice + product.giftCard.pointsPrice;
                            }

                            if (product.packagingWay != null) {
                                finalPrice = finalPrice + product.packagingWay.price;
                                pointsPrice = pointsPrice + product.packagingWay.pointsPrice;
                            }
                        }

                        bundle.putInt("points", pointsPrice);
                        Log.d(CommonConstants.APPLICATION_LOG_TAG, "points from reset : " + pointsPrice);
                        bundle.putDouble("money", finalPrice);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } else {
                        Toast.makeText(ResetActivity.this, getResources().getString(R.string.cartEmpty), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        updateTotalPriceView();


    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateTotalPriceView();
        updateCartItemsCount();

    }

    public void updateTotalPriceView() {
        ResetRecyclerViewAdapter resetRecyclerViewAdapter = new ResetRecyclerViewAdapter(this, Singleton.getInstance().cart);
        resetProductsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        resetProductsRecyclerView.setAdapter(resetRecyclerViewAdapter);

        double totalPrice = 0.0;
        for (Product product : Singleton.getInstance().cart) {
            totalPrice = totalPrice + product.price * product.quantity;
            if (product.giftCard != null) {
                totalPrice = totalPrice + product.giftCard.price * product.quantity;
            }

            if (product.packagingWay != null) {
                totalPrice = totalPrice + product.packagingWay.price * product.quantity;
            }
        }

        DecimalFormat twoDForm = new DecimalFormat("#.##");
        totalPrice = Double.valueOf(twoDForm.format(totalPrice));

        totalPriceTextView.setText("" + getResources().getString(R.string.total) + " " + totalPrice + " " + Singleton.getInstance().country.currency);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/arcena.ttf");
        totalPriceTextView.setTypeface(typeface);
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {
        switch (action) {
            case ProductsController.GET_AUTO_COMPLETE_DATABASE:
                try {
                    ArrayAdapter adapter;
                    ArrayList<AutoComplete> autoCompleteCaches = (ArrayList) result;
                    ArrayList<String> autoCompleteNames = new ArrayList<>();

                    for (AutoComplete autoComplete : autoCompleteCaches) {
                        autoCompleteNames.add(autoComplete.getName());
                    }
                    adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, autoCompleteNames);
                    searchEditText.setThreshold(2);
                    searchEditText.setAdapter(adapter);
                } catch (Exception e) {

                }
                break;

            case ProductsController.RELATED_PRODUCTS_ACTION:
                if (result.size() > 0) {
                    ArrayList<Product> products = (ArrayList<Product>) result;
                    Intent intent = new Intent(ResetActivity.this, RelatedProductsActivity.class);
                    Bundle bundle = new Bundle();

                    for (int i = 0; i < products.size(); i++) {
                        bundle.putSerializable("product-" + i, products.get(i));
                    }
                    bundle.putInt("count", result.size());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;

            default:
                break;
        }
    }

    public void onDataReceived(ArrayList<? extends Entity> result, int action, Bundle bundleData) {
        switch (action) {
            case OrderController.PACKAGING_WAYS_ACTION:
                if (result.size() > 0) {
                    ArrayList<PackagingWay> packagingWays = (ArrayList<PackagingWay>) result;
                    Intent intent = new Intent(ResetActivity.this, PackagingWaysActivity.class);
                    Bundle bundle = new Bundle();

                    for (int i = 0; i < packagingWays.size(); i++) {
                        bundle.putSerializable("package-" + i, packagingWays.get(i));
                    }
                    bundle.putInt("count", result.size());
                    bundle.putInt("productId", bundleData.getInt("productId"));
                    intent.putExtras(bundle);
                    startActivityForResult(intent, PACKAGING_WAYS_REQUEST_CODE);
                }
                break;

            case OrderController.GIFT_CARDS_ACTION:
                if (result.size() > 0) {
                    ArrayList<GiftCard> giftCards = (ArrayList<GiftCard>) result;
                    Intent intent = new Intent(ResetActivity.this, GiftCardsActivity.class);
                    Bundle bundle = new Bundle();

                    for (int i = 0; i < giftCards.size(); i++) {
                        bundle.putSerializable("gift-" + i, giftCards.get(i));
                    }
                    bundle.putInt("count", result.size());
                    bundle.putInt("productId", bundleData.getInt("productId"));
                    intent.putExtras(bundle);
                    startActivityForResult(intent, PACKAGING_WAYS_REQUEST_CODE);
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PACKAGING_WAYS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                updateTotalPriceView();
            }
        }
    }
}
