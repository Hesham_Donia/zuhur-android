package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.views.activities.ProductLandingPageActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 5/27/16.
 */
public class BestSellersRecyclerViewAdapter extends RecyclerView.Adapter<BestSellersRecyclerViewAdapter.ViewHolder> {

    ViewHolder viewHolder;
    Typeface typeface;
    private Context mContext;
    private ArrayList<Product> mProducts;
    private LayoutInflater mInflater = null;
    public BestSellersRecyclerViewAdapter(Context context, ArrayList<Product> products) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mProducts = products;
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
    }

    @Override
    public BestSellersRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.best_seller_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BestSellersRecyclerViewAdapter.ViewHolder holder, int position) {
        ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();
        Picasso.with(mContext).load(mProducts.get(position).imagePath).into(ordinaryHolder.bestSellerProductImageView);
        if (Singleton.getInstance().currentLanguage.equals("en")) {
            ordinaryHolder.bestSellerProductTextView.setText(mProducts.get(position).englishName);
        } else {
            ordinaryHolder.bestSellerProductTextView.setText(mProducts.get(position).arabicName);
        }


        ordinaryHolder.bestSellerProductTextView.setTypeface(typeface);
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.bestSellerProductImageView)
        ImageView bestSellerProductImageView;

        @InjectView(R.id.bestSellerProductTextView)
        TextView bestSellerProductTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ProductLandingPageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("product", mProducts.get(getAdapterPosition()));
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }

        public void emptyViewHolder() {
            bestSellerProductImageView.setImageBitmap(null);
            bestSellerProductTextView.setText("");
        }
    }
}
