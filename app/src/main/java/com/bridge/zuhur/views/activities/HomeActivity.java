package com.bridge.zuhur.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.CategoriesController;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.database.AutoComplete;
import com.bridge.zuhur.entities.Category;
import com.bridge.zuhur.entities.Country;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Offer;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.utils.Sensor;
import com.bridge.zuhur.views.adapters.BestSellersRecyclerViewAdapter;
import com.bridge.zuhur.views.adapters.CategoriesRecyclerViewAdapter;
import com.bridge.zuhur.views.adapters.OffersPagerViewAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class HomeActivity extends AbstractActivity {

    final static int COUNTRIES_REQUEST_CODE = 1;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.loadingContainer)
    RelativeLayout relativeLayout;

    @InjectView(R.id.offersPagerView)
    AutoScrollViewPager offersPagerView;

    @InjectView(R.id.categoriesRecyclerView)
    RecyclerView categoriesRecyclerView;

    @InjectView(R.id.cartIconImageView)
    ImageView cartIconImageView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    @InjectView(R.id.bestSellersRecyclerView)
    RecyclerView bestSellersRecyclerView;

    @InjectView(R.id.bestSellersTextView)
    TextView bestSellersTextView;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.countryButton)
    Button countryButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.inject(this);
        setToolbar();
        setDrawerLayout();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setNavigationView();
        setLoadingContainer();
        setCartItemsCount();
        setCountriesButton();


        setSupportActionBar(mToolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        titleTextView.setText(getResources().getString(R.string.home));

        relativeLayout.setVisibility(View.VISIBLE);
        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setVisibility(View.VISIBLE);
                titleTextView.setVisibility(View.GONE);
                searchEditText.requestFocus();
                searchEditText.setThreshold(2);
                searchEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        ProductsController productsController = new ProductsController(HomeActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", ProductsController.GET_AUTO_COMPLETE_DATABASE);
                        bundle1.putString("keyword", s.toString());
                        productsController.request(ProductsController.GET_AUTO_COMPLETE_DATABASE, bundle1);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            Intent intent = new Intent(HomeActivity.this, SearchResultsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("keyword", searchEditText.getText().toString());
                            intent.putExtras(bundle);
                            startActivity(intent);
                            hideKeyboard(searchEditText);
                            return true;
                        }
                        return false;
                    }
                });
            }
        });

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/arcena.ttf");
        bestSellersTextView.setTypeface(typeface);

        cartIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Singleton.getInstance().cart.size() > 0) {
                    Intent intent = new Intent(HomeActivity.this, ResetActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(HomeActivity.this, getResources().getString(R.string.cartEmpty), Toast.LENGTH_LONG).show();
                }
            }
        });

        if (Singleton.getInstance().countries.size() == 0) {
            ExtrasController extrasController = new ExtrasController(HomeActivity.this);
            Bundle bundle1 = new Bundle();
            bundle1.putInt("action", ExtrasController.COUNTRIES_ACTION);
            extrasController.request(ExtrasController.COUNTRIES_ACTION, bundle1);
        } else {
            ExtrasController extrasController = new ExtrasController(HomeActivity.this);
            Bundle bundle1 = new Bundle();
            bundle1.putInt("countryId", Singleton.getInstance().country.id);
            bundle1.putInt("action", ExtrasController.OFFERS_ACTION);
            extrasController.request(ExtrasController.OFFERS_ACTION, bundle1);

            ExtrasController extrasController1 = new ExtrasController(HomeActivity.this);
            Bundle bundle2 = new Bundle();
            bundle1.putInt("action", ExtrasController.BEST_SELLER_ACTION);
            bundle1.putInt("countryId", Singleton.getInstance().country.id);
            extrasController1.request(ExtrasController.BEST_SELLER_ACTION, bundle2);
        }

        CategoriesController controller = new CategoriesController(this);
        final Bundle bundle = new Bundle();
        bundle.putInt("action", CategoriesController.GET_CATEGORIES_ACTION);
        controller.request(CategoriesController.GET_CATEGORIES_ACTION, bundle);

        ExtrasController extrasController = new ExtrasController(HomeActivity.this);
        Bundle bundle1 = new Bundle();
        bundle1.putInt("action", ExtrasController.TAGS_ACTION);
        extrasController.request(ExtrasController.TAGS_ACTION, bundle1);

        ExtrasController extrasController1 = new ExtrasController(HomeActivity.this);
        Bundle bundle2 = new Bundle();
        bundle2.putInt("action", ExtrasController.COLORS_ACTION);
        extrasController1.request(ExtrasController.COLORS_ACTION, bundle2);
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setToolbar();
        setDrawerLayout();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setNavigationView();
        setLoadingContainer();
        setCartItemsCount();
        setCountriesButton();
    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    private void setLoadingContainer() {
        mLoadingContainer = relativeLayout;
    }

    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {
        switch (action) {
            case ExtrasController.COUNTRIES_ACTION:
                ArrayList<Country> countries = (ArrayList<Country>) result;
                Singleton.getInstance().countries = countries;
                if (Singleton.getInstance().country == null) {
                    Intent intent = new Intent(this, CountriesActivity.class);
                    startActivityForResult(intent, COUNTRIES_REQUEST_CODE);
                } else {
                    ExtrasController extrasController = new ExtrasController(this);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("countryId", Singleton.getInstance().country.id);
                    bundle1.putInt("action", ExtrasController.OFFERS_ACTION);
                    extrasController.request(ExtrasController.OFFERS_ACTION, bundle1);

                    ExtrasController extrasController1 = new ExtrasController(this);
                    Bundle bundle2 = new Bundle();
                    bundle1.putInt("action", ExtrasController.BEST_SELLER_ACTION);
                    bundle1.putInt("countryId", Singleton.getInstance().country.id);
                    extrasController1.request(ExtrasController.BEST_SELLER_ACTION, bundle2);
                }
                break;

            case ExtrasController.OFFERS_ACTION:
                ArrayList<Offer> mOffers = (ArrayList<Offer>) result;
                OffersPagerViewAdapter mOffersPagerViewAdapter = new OffersPagerViewAdapter(this, mOffers);
                offersPagerView.setAdapter(mOffersPagerViewAdapter);
                offersPagerView.startAutoScroll(5000);
                offersPagerView.setCycle(true);
                offersPagerView.setInterval(5000);
                relativeLayout.setVisibility(View.GONE);
                if (mOffers.size() == 0) {
                    Toast.makeText(this, getResources().getString(R.string.noOffersError), Toast.LENGTH_LONG).show();
                    relativeLayout.setVisibility(View.GONE);
                }
                break;

            case CategoriesController.GET_CATEGORIES_ACTION:
                ArrayList<Category> categories = (ArrayList<Category>) result;
                CategoriesRecyclerViewAdapter categoriesRecyclerViewAdapter = new CategoriesRecyclerViewAdapter(this, categories);
                categoriesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                categoriesRecyclerView.setAdapter(categoriesRecyclerViewAdapter);
                relativeLayout.setVisibility(View.GONE);
                break;

            case ExtrasController.BEST_SELLER_ACTION:
                ArrayList<Product> bestSellers = (ArrayList<Product>) result;
                BestSellersRecyclerViewAdapter bestSellersRecyclerViewAdapter = new BestSellersRecyclerViewAdapter(this, bestSellers);
                bestSellersRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                bestSellersRecyclerView.setAdapter(bestSellersRecyclerViewAdapter);
                relativeLayout.setVisibility(View.GONE);
                if (bestSellers.size() == 0) {
                    Toast.makeText(this, getResources().getString(R.string.noProductsError), Toast.LENGTH_LONG).show();
                    relativeLayout.setVisibility(View.GONE);
                }
                break;

            case ProductsController.GET_AUTO_COMPLETE_DATABASE:
                try {
                    ArrayAdapter adapter;
                    ArrayList<AutoComplete> autoCompleteCaches = (ArrayList) result;
                    ArrayList<String> autoCompleteNames = new ArrayList<>();

                    for (AutoComplete autoComplete : autoCompleteCaches) {
                        autoCompleteNames.add(autoComplete.getName());
                    }
                    adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, autoCompleteNames);
                    searchEditText.setThreshold(2);
                    searchEditText.setAdapter(adapter);
                } catch (Exception e) {

                }
                break;


            default:
                break;
        }

        Sensor.unlockSensor(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == COUNTRIES_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {
                Bundle bundle = data.getExtras();
                if (bundle.containsKey("country")) {
                    SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                            Context.MODE_PRIVATE);
                    Singleton.getInstance().country = (Country) bundle.getSerializable("country");
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt("countryId", Singleton.getInstance().country.id);
                    editor.putString("countryEnglishName", Singleton.getInstance().country.englishName);
                    editor.putString("countryArabicName", Singleton.getInstance().country.arabicName);
                    editor.putString("countryCurrency", Singleton.getInstance().country.currency);
                    editor.putFloat("countryDollarValue", (float) Singleton.getInstance().country.dollarValue);
                    editor.commit();

                    if (Language.getCurrentLang(this).equals("en")) {
                        mCountriesButton.setText(Singleton.getInstance().country.englishName);
                    } else {
                        mCountriesButton.setText(Singleton.getInstance().country.arabicName);
                    }

                    relativeLayout.setVisibility(View.VISIBLE);

                    ExtrasController extrasController = new ExtrasController(this);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("countryId", Singleton.getInstance().country.id);
                    bundle1.putInt("action", ExtrasController.OFFERS_ACTION);
                    extrasController.request(ExtrasController.OFFERS_ACTION, bundle1);

                    ExtrasController extrasController1 = new ExtrasController(this);
                    Bundle bundle2 = new Bundle();
                    bundle1.putInt("action", ExtrasController.BEST_SELLER_ACTION);
                    bundle1.putInt("countryId", Singleton.getInstance().country.id);
                    extrasController1.request(ExtrasController.BEST_SELLER_ACTION, bundle2);

                }
            } else {
                Singleton.getInstance().country = Singleton.getInstance().countries.get(0);
                SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("countryId", Singleton.getInstance().country.id);
                editor.putString("countryEnglishName", Singleton.getInstance().country.englishName);
                editor.putString("countryArabicName", Singleton.getInstance().country.arabicName);
                editor.putString("countryCurrency", Singleton.getInstance().country.currency);
                editor.putFloat("countryDollarValue", (float) Singleton.getInstance().country.dollarValue);
                editor.commit();
                if (Language.getCurrentLang(this).equals("en")) {
                    mCountriesButton.setText(Singleton.getInstance().countries.get(0).englishName);
                } else {
                    mCountriesButton.setText(Singleton.getInstance().countries.get(0).arabicName);
                }

                relativeLayout.setVisibility(View.VISIBLE);

                ExtrasController extrasController = new ExtrasController(this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("countryId", Singleton.getInstance().country.id);
                bundle1.putInt("action", ExtrasController.OFFERS_ACTION);
                extrasController.request(ExtrasController.OFFERS_ACTION, bundle1);

                ExtrasController extrasController1 = new ExtrasController(this);
                Bundle bundle2 = new Bundle();
                bundle1.putInt("action", ExtrasController.BEST_SELLER_ACTION);
                bundle1.putInt("countryId", Singleton.getInstance().country.id);
                extrasController1.request(ExtrasController.BEST_SELLER_ACTION, bundle2);
            }
        }
    }


}
