package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 6/14/16.
 */
public class RelatedProductsRecyclerViewAdapter extends RecyclerView.Adapter<RelatedProductsRecyclerViewAdapter.ViewHolder> {

    ViewHolder viewHolder;
    Typeface typeface;
    private Context mContext;
    private ArrayList<Product> mProducts;
    private LayoutInflater mInflater = null;

    public RelatedProductsRecyclerViewAdapter(Context context, ArrayList<Product> products) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mProducts = products;
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
    }

    @Override
    public RelatedProductsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.related_product_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();
        Picasso.with(mContext).load(mProducts.get(position).imagePath).into(ordinaryHolder.relatedProductImageView);

        if (Singleton.getInstance().currentLanguage.equals("en")) {
            ordinaryHolder.relatedProductTextView.setText(mProducts.get(position).englishName);
        } else {
            ordinaryHolder.relatedProductTextView.setText(mProducts.get(position).arabicName);
        }

        ordinaryHolder.relatedProductTextView.setTypeface(typeface);

        ordinaryHolder.addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Singleton.getInstance().cart.add(mProducts.get(position));

                mProducts.remove(position);

                notifyItemRemoved(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.relatedProductImageView)
        ImageView relatedProductImageView;

        @InjectView(R.id.relatedProductTextView)
        TextView relatedProductTextView;

        @InjectView(R.id.addToCartButton)
        Button addToCartButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }

        public void emptyViewHolder() {
            relatedProductImageView.setImageBitmap(null);
            relatedProductTextView.setText("");
        }
    }
}
