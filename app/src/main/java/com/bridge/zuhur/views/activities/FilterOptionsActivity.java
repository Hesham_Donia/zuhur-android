package com.bridge.zuhur.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.database.Tag;
import com.bridge.zuhur.entities.Country;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.views.adapters.ColorsRecyclerViewAdapter;
import com.bridge.zuhur.views.adapters.FiltersRecyclerViewAdapter;
import com.bridge.zuhur.views.adapters.OccasionsRecyclerViewAdapter;
import com.tokenautocomplete.TokenCompleteTextView;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class FilterOptionsActivity extends Activity implements TokenCompleteTextView.TokenListener {

    final static int COUNTRIES_REQUEST_CODE = 1;

    @InjectView(R.id.applyCardView)
    CardView applyCardView;

    @InjectView(R.id.occasionsRecyclerView)
    RecyclerView occasionsRecyclerView;

    @InjectView(R.id.colorsRecyclerView)
    RecyclerView colorsRecyclerView;

    @InjectView(R.id.filtersRecyclerView)
    RecyclerView filtersRecyclerView;

    @InjectView(R.id.rangeSeekBar)
    RangeSeekBar rangeSeekBar;

    @InjectView(R.id.filtersCardView)
    CardView filtersCardView;

    ArrayList<Tag> tagsArrayList = new ArrayList<>();

    String tags = "";
    int priceFrom = 0, priceTo = 0;
    Country country = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_options);

        ButterKnife.inject(this);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 1);


        ColorsRecyclerViewAdapter colorsRecyclerViewAdapter = new ColorsRecyclerViewAdapter(this, Singleton.getInstance().colors);
        colorsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        colorsRecyclerView.setAdapter(colorsRecyclerViewAdapter);

        ExtrasController extrasController = new ExtrasController(this);
        Bundle bundle1 = new Bundle();
        bundle1.putInt("action", ExtrasController.GET_ALL_TAGS_FROM_DATABASE);
        extrasController.request(ExtrasController.GET_ALL_TAGS_FROM_DATABASE, bundle1);


        getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        Bundle bundle = getIntent().getExtras();


//        if (bundle.containsKey("country")) {
//            country = (Country) bundle.getSerializable("country");
//            if (Language.getCurrentLang(this).equals("en")) {
//                countryChooserTextView.setText(country.englishName);
//            } else {
//                countryChooserTextView.setText(country.arabicName);
//            }
//        }
////
//        if (bundle.containsKey("tags")) {
//
//
//        }

        if (bundle.containsKey("priceFrom")) {
            priceFrom = bundle.getInt("priceFrom");
            rangeSeekBar.setSelectedMinValue(priceFrom);
        }

        if (bundle.containsKey("priceTo")) {
            priceTo = bundle.getInt("priceTo");
            rangeSeekBar.setSelectedMaxValue(priceTo);
        }

//        tagsAutoCompleteTextView.setThreshold(1);
//        tagsAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                ExtrasController extrasController = new ExtrasController(FilterOptionsActivity.this);
//                Bundle bundle = new Bundle();
//                bundle.putString("query", s.toString());
//                bundle.putInt("action", ExtrasController.GET_TAGS_FROM_DATABASE_ACTION);
//                extrasController.request(ExtrasController.GET_TAGS_FROM_DATABASE_ACTION, bundle);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//        tagsAutoCompleteTextView.setTokenListener(this);
//        tagsAutoCompleteTextView.allowDuplicates(false);
//
//        purpleCircleImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                com.bridge.zuhur.entities.Tag myTag = new com.bridge.zuhur.entities.Tag();
//                myTag.name = getResources().getString(R.string.purple);
//
//                Tag tag = new Tag(myTag);
//                tagsAutoCompleteTextView.addObject(tag);
//            }
//        });
//
//        redCircleImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                com.bridge.zuhur.entities.Tag myTag = new com.bridge.zuhur.entities.Tag();
//                myTag.name = getResources().getString(R.string.red);
//
//                Tag tag = new Tag(myTag);
//                tagsAutoCompleteTextView.addObject(tag);
//            }
//        });
//
//        blueCircleImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                com.bridge.zuhur.entities.Tag myTag = new com.bridge.zuhur.entities.Tag();
//                myTag.name = getResources().getString(R.string.blue);
//
//                Tag tag = new Tag(myTag);
//                tagsAutoCompleteTextView.addObject(tag);
//            }
//        });
//
//        whiteCircleImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                com.bridge.zuhur.entities.Tag myTag = new com.bridge.zuhur.entities.Tag();
//                myTag.name = getResources().getString(R.string.white);
//
//                Tag tag = new Tag(myTag);
//                tagsAutoCompleteTextView.addObject(tag);
//            }
//        });
//
//        yellowCircleImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                com.bridge.zuhur.entities.Tag myTag = new com.bridge.zuhur.entities.Tag();
//                myTag.name = getResources().getString(R.string.yellow);
//
//                Tag tag = new Tag(myTag);
//                tagsAutoCompleteTextView.addObject(tag);
//            }
//        });
//
//        peachCircleImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                com.bridge.zuhur.entities.Tag myTag = new com.bridge.zuhur.entities.Tag();
//                myTag.name = getResources().getString(R.string.peach);
//
//                Tag tag = new Tag(myTag);
//                tagsAutoCompleteTextView.addObject(tag);
//            }
//        });
//
//        pinkCircleImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                com.bridge.zuhur.entities.Tag myTag = new com.bridge.zuhur.entities.Tag();
//                myTag.name = getResources().getString(R.string.pink);
//
//                Tag tag = new Tag(myTag);
//                tagsAutoCompleteTextView.addObject(tag);
//            }
//        });
//
//        orangeCircleImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                com.bridge.zuhur.entities.Tag myTag = new com.bridge.zuhur.entities.Tag();
//                myTag.name = getResources().getString(R.string.orange);
//
//                Tag tag = new Tag(myTag);
//                tagsAutoCompleteTextView.addObject(tag);
//            }
//        });
//
//        countryChooserTextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(FilterOptionsActivity.this, CountriesActivity.class);
//                startActivityForResult(intent, COUNTRIES_REQUEST_CODE);
//            }
//        });

        applyCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                priceFrom = (int) rangeSeekBar.getSelectedMinValue();
                priceTo = (int) rangeSeekBar.getSelectedMaxValue();


                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "tags when finish: " + tags);
                for (Tag tag : tagsArrayList) {
                    tags = tags + "," + tag.getTag().name;
                }
                if (!tags.equals("") || !tags.equals(" ")) {
                    bundle.putString("tags", tags);
                }

                if (priceFrom != getResources().getInteger(R.integer.startPriceRange) ||
                        priceTo != getResources().getInteger(R.integer.endPriceRange)) {
                    bundle.putInt("priceFrom", priceFrom);
                    bundle.putInt("priceTo", priceTo);
                }

                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
                tags = "";

            }
        });

    }

    public void addToTagsArrayList(Tag tag) {
        tagsArrayList.add(tag);
        filtersCardView.setVisibility(View.VISIBLE);
        updateFiltersRecyclerView();
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "tagsArrayList.size(): " + tagsArrayList.size());
    }

    public void removeFromTagsArrayList(Tag tag) {
        tagsArrayList.remove(tag);
        if (tagsArrayList.size() == 0) {
            filtersCardView.setVisibility(View.GONE);
        } else {
            updateFiltersRecyclerView();
        }
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "tagsArrayList.size(): " + tagsArrayList.size());
    }

    private void updateFiltersRecyclerView() {
        filtersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        FiltersRecyclerViewAdapter filtersRecyclerViewAdapter = new FiltersRecyclerViewAdapter(this, tagsArrayList);
        filtersRecyclerView.setAdapter(filtersRecyclerViewAdapter);
        filtersRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == COUNTRIES_REQUEST_CODE) {
//            if (resultCode == RESULT_OK && data != null) {
//                Bundle bundle = data.getExtras();
//                if (bundle.containsKey("country")) {
//                    country = (Country) bundle.getSerializable("country");
//                    if (Language.getCurrentLang(this).equals("en")) {
//                        countryChooserTextView.setText(country.englishName);
//                    } else {
//                        countryChooserTextView.setText(country.arabicName);
//                    }
//                }
//            }
//        }
    }

    public void onDataReceived(ArrayList<? extends Entity> result, int action) {

        switch (action) {
            case ExtrasController.GET_TAGS_FROM_DATABASE_ACTION:
                if (result == null) {
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "Not matched any thing");
                } else {
                    ArrayList<com.bridge.zuhur.database.Tag> tags = (ArrayList<com.bridge.zuhur.database.Tag>) result;
                    ArrayAdapter<com.bridge.zuhur.database.Tag> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, tags);
                }
                break;

            case ExtrasController.GET_ALL_TAGS_FROM_DATABASE:
                ArrayList<Tag> databaseTags = (ArrayList<Tag>) result;
                OccasionsRecyclerViewAdapter occasionsRecyclerViewAdapter = new OccasionsRecyclerViewAdapter(this, databaseTags);
                occasionsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                occasionsRecyclerView.setAdapter(occasionsRecyclerViewAdapter);
                occasionsRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        int action = e.getAction();
                        switch (action) {
                            case MotionEvent.ACTION_MOVE:
                                rv.getParent().requestDisallowInterceptTouchEvent(true);
                                break;
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });
                break;

            default:
                break;
        }

    }

    public void updateOccasionsRecyclerView(Tag tag) {
        ((OccasionsRecyclerViewAdapter) occasionsRecyclerView.getAdapter()).updateRecyclerView(tag);
    }

    @Override
    public void onTokenAdded(Object token) {
        com.bridge.zuhur.database.Tag tag = (com.bridge.zuhur.database.Tag) token;
        tags = tags + tag.getTagName() + ",";
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "tags: " + tags);
    }

    @Override
    public void onTokenRemoved(Object token) {
        com.bridge.zuhur.database.Tag tag = (com.bridge.zuhur.database.Tag) token;
        tags = tags.replace(tag.getTagName() + ",", "");
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "tags: " + tags);
    }
}
