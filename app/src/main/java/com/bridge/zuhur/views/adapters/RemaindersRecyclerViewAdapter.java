package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.database.LogData;
import com.bridge.zuhur.database.Remainder;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.NetworkConnection;
import com.bridge.zuhur.views.activities.EditRemainderActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 6/9/16.
 */
public class RemaindersRecyclerViewAdapter extends RecyclerView.Adapter<RemaindersRecyclerViewAdapter.ViewHolder> {

    ViewHolder viewHolder;
    Typeface typeface;
    private LayoutInflater mInflater = null;
    private Context mContext;
    private ArrayList<Remainder> mRemainders;

    public RemaindersRecyclerViewAdapter(Context context, ArrayList<Remainder> remainders) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mRemainders = remainders;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.remainder_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();
        ordinaryHolder.remainderDateTextView.setTypeface(typeface);
        ordinaryHolder.remainderDateTextView.setText(mRemainders.get(position).getRemainder().remainderDate);
        ordinaryHolder.deleteRemainderImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(mContext);
                Bundle bundle = new Bundle();
                bundle.putInt("action", ExtrasController.DELETE_REMAINDER_DATABASE_ACTION);
                bundle.putSerializable("id", mRemainders.get(position).getRemainderId());
                extrasController.request(ExtrasController.DELETE_REMAINDER_DATABASE_ACTION, bundle);

                if (NetworkConnection.networkConnectivity(mContext)) {
                    ExtrasController extrasController1 = new ExtrasController(mContext);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("action", ExtrasController.DELETE_REMAINDERS_ACTION);
                    bundle1.putSerializable("id", mRemainders.get(position).getRemainderId());
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "id from adapter: " + mRemainders.get(position).getRemainderId());
                    extrasController1.request(ExtrasController.DELETE_REMAINDERS_ACTION, bundle1);
                } else {
                    LogData logData = new LogData(mRemainders.get(position).getRemainder(), CommonConstants.DELETE_OPERATION, CommonConstants.REMAINDER_TYPE);
                    ExtrasController extrasController2 = new ExtrasController(mContext);
                    Bundle logDataBundle = new Bundle();
                    logDataBundle.putSerializable("logData", logData);
                    logDataBundle.putInt("action", ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION);
                    extrasController2.request(ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION, logDataBundle);
                    //TODO: make the log data controller, service, model, mapper and here save the data to the table
                    //TODO: for actions and type see CommonConstants class
                }


                mRemainders.remove(position);

                notifyItemRemoved(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mRemainders.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.deleteRemainderImageView)
        ImageView deleteRemainderImageView;

        @InjectView(R.id.remainderDateTextView)
        TextView remainderDateTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, EditRemainderActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("remainder", mRemainders.get(getAdapterPosition()));
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }

        public void emptyViewHolder() {
            remainderDateTextView.setText("");
        }
    }
}
