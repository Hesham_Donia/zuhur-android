package com.bridge.zuhur.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.GiftCard;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.views.activities.GiftCardsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 6/22/16.
 */
public class GiftCardsRecyclerViewAdapter extends RecyclerView.Adapter<GiftCardsRecyclerViewAdapter.ViewHolder> {

    ViewHolder viewHolder;
    Typeface typeface;
    private Context mContext;
    private ArrayList<GiftCard> mGiftCards;
    private LayoutInflater mInflater = null;
    private int mProductId;

    public GiftCardsRecyclerViewAdapter(Context context, ArrayList<GiftCard> giftCards, int productId) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mGiftCards = giftCards;
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        mProductId = productId;
    }

    @Override
    public GiftCardsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.gift_card_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GiftCardsRecyclerViewAdapter.ViewHolder holder, final int position) {
        final ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();
        Picasso.with(mContext).load(mGiftCards.get(position).imagePath).into(ordinaryHolder.giftCardImageView);

        if (Singleton.getInstance().currentLanguage.equals("en")) {
            ordinaryHolder.giftCardTextView.setText(mGiftCards.get(position).nameEn);
        } else {
            ordinaryHolder.giftCardTextView.setText(mGiftCards.get(position).nameAr);
        }

        ordinaryHolder.giftCardTextView.setTypeface(typeface);

        ordinaryHolder.addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ordinaryHolder.giftCardContentEditText.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.giftCardContentError), Toast.LENGTH_SHORT).show();
                } else {
                    mGiftCards.get(position).content = ordinaryHolder.giftCardContentEditText.getText().toString();
                    for (int i = 0; i < Singleton.getInstance().cart.size(); i++) {
                        if (Singleton.getInstance().cart.get(i).id == mProductId) {
                            Singleton.getInstance().cart.get(i).giftCard = mGiftCards.get(position);
                            ((GiftCardsActivity) mContext).setResult(Activity.RESULT_OK);
                            ((GiftCardsActivity) mContext).finish();
                            break;
                        }
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mGiftCards.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.giftCardImageView)
        ImageView giftCardImageView;

        @InjectView(R.id.giftCardTextView)
        TextView giftCardTextView;

        @InjectView(R.id.giftCardContentEditText)
        EditText giftCardContentEditText;

        @InjectView(R.id.addToCartButton)
        Button addToCartButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }

        public void emptyViewHolder() {
            giftCardImageView.setImageBitmap(null);
            giftCardTextView.setText("");
            giftCardContentEditText.setText("");
        }
    }


}
