package com.bridge.zuhur.views.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.FavouritesController;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.controllers.WishListController;
import com.bridge.zuhur.database.AutoComplete;
import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.database.LogData;
import com.bridge.zuhur.database.WishList;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.utils.NetworkConnection;
import com.bridge.zuhur.utils.Sensor;
import com.bridge.zuhur.views.adapters.BestSellersRecyclerViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ProductLandingPageActivity extends AbstractActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.cartIconImageView)
    ImageView cartIconImageView;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.searchIconImageView)
    ImageView searchIconImageView;

    @InjectView(R.id.searchEditText)
    AutoCompleteTextView searchEditText;

    @InjectView(R.id.notif_count)
    Button cartItemsCount;

    @InjectView(R.id.productImageView)
    ImageView productImageView;

    @InjectView(R.id.productNameTextView)
    TextView productNameTextView;

    @InjectView(R.id.productPriceTextView)
    TextView productPriceTextView;

    @InjectView(R.id.descriptionTextView)
    TextView descriptionTextView;

    @InjectView(R.id.productDescriptionTextView)
    TextView productDescriptionTextView;

    @InjectView(R.id.availabilityTextView)
    TextView availabilityTextView;

    @InjectView(R.id.productAvailabilityTextView)
    TextView productAvailabilityTextView;

    @InjectView(R.id.shareImageView)
    ImageView shareImageView;

    @InjectView(R.id.cartImageView)
    ImageView cartImageView;

    @InjectView(R.id.wishListImageView)
    ImageView wishListImageView;

    @InjectView(R.id.favouriteImageView)
    ImageView favouriteImageView;

    @InjectView(R.id.relatedProductsContainer)
    LinearLayout relatedProductsContainer;

    @InjectView(R.id.relatedProductsTextView)
    TextView relatedProductsTextView;

    @InjectView(R.id.relatedProductsRecyclerView)
    RecyclerView relatedProductsRecyclerView;

    @InjectView(R.id.navigationViewRecyclerView)
    RecyclerView navigationViewRecyclerView;

    @InjectView(R.id.titleTextView)
    TextView titleTextView;

    @InjectView(R.id.countryButton)
    Button countryButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_landing_page);

        final Product product = (Product) getIntent().getExtras().getSerializable("product");

        Log.d(CommonConstants.APPLICATION_LOG_TAG, product.englishName);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, product.id + "");

        ButterKnife.inject(this);

        setToolbar();
        setDrawerLayout();
        setNavigationView();
        setCartItemsCount();
        setNavigationViewRecyclerView();
        setupNavigationView();
        setCountriesButton();

        countryButton.setVisibility(View.GONE);

        setSupportActionBar(mToolbar);

        if (Language.getCurrentLang(this).equals("ar")) {
            titleTextView.setText(product.arabicName);
        } else {
            titleTextView.setText(product.englishName);
        }

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        searchEditText.setVisibility(View.INVISIBLE);
        searchIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setVisibility(View.VISIBLE);
                titleTextView.setVisibility(View.GONE);
                searchEditText.requestFocus();
                searchEditText.setThreshold(2);
                searchEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        ProductsController productsController = new ProductsController(ProductLandingPageActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", ProductsController.GET_AUTO_COMPLETE_DATABASE);
                        bundle1.putString("keyword", s.toString());
                        productsController.request(ProductsController.GET_AUTO_COMPLETE_DATABASE, bundle1);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            Intent intent = new Intent(ProductLandingPageActivity.this, SearchResultsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("keyword", searchEditText.getText().toString());
                            intent.putExtras(bundle);
                            startActivity(intent);
                            hideKeyboard(searchEditText);
                            return true;
                        }
                        return false;
                    }
                });
            }
        });

        relatedProductsContainer.setVisibility(View.GONE);

        cartIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Singleton.getInstance().cart.size() > 0) {
                    Intent intent = new Intent(ProductLandingPageActivity.this, ResetActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(ProductLandingPageActivity.this, getResources().getString(R.string.cartEmpty), Toast.LENGTH_LONG).show();
                }
            }
        });

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/arcena.ttf");

        productNameTextView.setTypeface(typeface);
        productPriceTextView.setTypeface(typeface);
        descriptionTextView.setTypeface(typeface);
        productDescriptionTextView.setTypeface(typeface);
        availabilityTextView.setTypeface(typeface);
        productAvailabilityTextView.setTypeface(typeface);
        relatedProductsTextView.setTypeface(typeface);

        Picasso.with(this).load(product.imagePath).into(productImageView);
        productImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductLandingPageActivity.this, ProductImageActivity.class);
                Bundle bundle = new Bundle();

//                Bitmap bmp = ((BitmapDrawable) productImageView.getDrawable()).getBitmap();
//                int size = bmp.getRowBytes() * bmp.getHeight();
//                ByteBuffer b = ByteBuffer.allocate(size);
//
//                bmp.copyPixelsToBuffer(b);
//
//                byte[] bytes = new byte[size];
//
//                try {
//                    b.get(bytes, 0, bytes.length);
//                } catch (BufferUnderflowException e) {
//                    // always happens
//                    e.printStackTrace();
//                }
//
//                Log.d(CommonConstants.APPLICATION_LOG_TAG, "bytes length: " + bytes.length);
//                bundle.putByteArray("image", bytes);
                bundle.putString("imagePath", product.imagePath);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        if (Language.getCurrentLang(this).equals("en")) {
            productNameTextView.setText("Name : " + product.englishName);
            productPriceTextView.setText("Price : " + product.price + " " + Singleton.getInstance().country.currency);
            productDescriptionTextView.setText(product.englishDescription);
            if (product.isExist) {
                productAvailabilityTextView.setText("Available");
            } else {
                productAvailabilityTextView.setText("Not available");
            }
        } else {
            productNameTextView.setText("الاسم : " + product.arabicName);
            productPriceTextView.setText("السعر : " + product.price + " " + Singleton.getInstance().country.currency);
            productDescriptionTextView.setText(product.arabicDescription);
            if (product.isExist) {
                productAvailabilityTextView.setText("متاح");
            } else {
                productAvailabilityTextView.setText("غير متاح");
            }
        }

        shareImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = "", extraData = "";

                if (Singleton.getInstance().currentLanguage.equals("en")) {
                    title = "Share to";
                    extraData = product.englishName + " \n " + product.url;
                } else {
                    title = "انشر الي";
                    extraData = product.arabicName + " \n " + product.url;
                }

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, extraData);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, title));
            }
        });

        cartImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isExist = false;

                for (Product product1 : Singleton.getInstance().cart) {
                    if (product1.id == product.id) {
                        isExist = true;
                    }
                }

                if (isExist) {
                    Toast.makeText(ProductLandingPageActivity.this, getResources().getString(R.string.addCartError), Toast.LENGTH_LONG).show();
                } else {
                    Singleton.getInstance().cart.add(product);
                    updateCartItemsCount();
                }
            }
        });

        wishListImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WishList wishList = new WishList(product);

                if (Singleton.getInstance().currentUser == null) {
                    Toast.makeText(ProductLandingPageActivity.this, getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                } else {
                    WishListController wishListController = new WishListController(ProductLandingPageActivity.this);
                    Bundle bundle = new Bundle();
                    bundle.putInt("action", WishListController.ADD_WISH_LIST_DATABASE_ACTION);
                    bundle.putSerializable("wishList", wishList);
                    wishListController.request(WishListController.ADD_WISH_LIST_DATABASE_ACTION, bundle);

                    if (NetworkConnection.networkConnectivity(ProductLandingPageActivity.this)) {
                        WishListController wishListController1 = new WishListController(ProductLandingPageActivity.this);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt("action", WishListController.ADD_WISH_LIST_TO_SERVER_ACTION);
                        bundle1.putSerializable("wishList", wishList);
                        wishListController1.request(WishListController.ADD_WISH_LIST_TO_SERVER_ACTION, bundle1);
                    } else {

                        LogData logData = new LogData(wishList, CommonConstants.ADD_OPERATION, CommonConstants.WISH_LIST_TYPE);
                        ExtrasController extrasController = new ExtrasController(ProductLandingPageActivity.this);
                        Bundle logDataBundle = new Bundle();
                        logDataBundle.putSerializable("logData", logData);
                        logDataBundle.putInt("action", ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION);
                        extrasController.request(ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION, logDataBundle);
                    }
                }
            }
        });

        favouriteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Favourite favourite = new Favourite(product);

                if (Singleton.getInstance().currentUser == null) {
                    Toast.makeText(ProductLandingPageActivity.this, getResources().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
                } else {

                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "favourite product id: " + favourite.getProductId());
                    FavouritesController favouritesController = new FavouritesController(ProductLandingPageActivity.this);
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("action", FavouritesController.ADD_FAVOURITE_DATABASE_ACTION);
                    bundle1.putSerializable("favourite", favourite);
                    favouritesController.request(FavouritesController.ADD_FAVOURITE_DATABASE_ACTION, bundle1);

                    // add to online
                    if (NetworkConnection.networkConnectivity(ProductLandingPageActivity.this)) {
                        FavouritesController favouritesController1 = new FavouritesController(ProductLandingPageActivity.this);
                        Bundle bundle2 = new Bundle();
                        bundle2.putInt("action", FavouritesController.ADD_FAVOURITE_TO_SERVER_ACTION);
                        bundle2.putSerializable("favourite", favourite);
                        favouritesController1.request(FavouritesController.ADD_FAVOURITE_TO_SERVER_ACTION, bundle2);
                    } else {

                        LogData logData = new LogData(favourite, CommonConstants.ADD_OPERATION, CommonConstants.FAVOURITE_TYPE);
                        ExtrasController extrasController = new ExtrasController(ProductLandingPageActivity.this);
                        Bundle logDataBundle = new Bundle();
                        logDataBundle.putSerializable("logData", logData);
                        logDataBundle.putInt("action", ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION);
                        extrasController.request(ExtrasController.ADD_LOG_DATA_TO_DATABASE_ACTION, logDataBundle);
                    }
                }
            }
        });

        ProductsController productsController = new ProductsController(ProductLandingPageActivity.this);
        Bundle bundle1 = new Bundle();
        bundle1.putInt("id", product.id);
        bundle1.putInt("countryId", Singleton.getInstance().country.id);
        bundle1.putInt("action", ProductsController.SIMILAR_PRODUCTS_ACTION);
        productsController.request(ProductsController.SIMILAR_PRODUCTS_ACTION, bundle1);
    }

    private void setDrawerLayout() {
        mDrawer = this.drawerLayout;
    }

    private void setNavigationView() {
        navView = this.navigationView;
    }

    private void setToolbar() {
        mToolbar = toolbar;
    }

    private void setCartItemsCount() {
        mCartItemsCount = cartItemsCount;
    }

    private void setNavigationViewRecyclerView() {
        mNavigationViewRecyclerView = navigationViewRecyclerView;
    }

    private void setCountriesButton() {
        mCountriesButton = countryButton;
    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {

        switch (action) {

            case ProductsController.SIMILAR_PRODUCTS_ACTION:
                ArrayList<Product> products = (ArrayList<Product>) result;

                if (products.size() > 0) {
                    relatedProductsContainer.setVisibility(View.VISIBLE);
                    BestSellersRecyclerViewAdapter bestSellersRecyclerViewAdapter = new BestSellersRecyclerViewAdapter(this, products);
                    relatedProductsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                    relatedProductsRecyclerView.setAdapter(bestSellersRecyclerViewAdapter);
                    relatedProductsRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                        @Override
                        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                            int action = e.getAction();
                            switch (action) {
                                case MotionEvent.ACTION_MOVE:
                                    rv.getParent().requestDisallowInterceptTouchEvent(true);
                                    break;
                            }
                            return false;
                        }

                        @Override
                        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                        }

                        @Override
                        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                        }
                    });

                } else {
                    relatedProductsContainer.setVisibility(View.GONE);
                }
                break;

            case ProductsController.GET_AUTO_COMPLETE_DATABASE:
                try {
                    ArrayAdapter adapter;
                    ArrayList<AutoComplete> autoCompleteCaches = (ArrayList) result;
                    ArrayList<String> autoCompleteNames = new ArrayList<>();

                    for (AutoComplete autoComplete : autoCompleteCaches) {
                        autoCompleteNames.add(autoComplete.getName());
                    }
                    adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, autoCompleteNames);
                    searchEditText.setThreshold(2);
                    searchEditText.setAdapter(adapter);
                } catch (Exception e) {

                }
                break;


            default:
                break;
        }
        Sensor.unlockSensor(this);
    }
}
