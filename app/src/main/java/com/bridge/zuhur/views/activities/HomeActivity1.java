package com.bridge.zuhur.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.AuthenticationController;
import com.bridge.zuhur.controllers.CategoriesController;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.FavouritesController;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.controllers.WishListController;
import com.bridge.zuhur.database.Favourite;
import com.bridge.zuhur.database.WishList;
import com.bridge.zuhur.entities.Category;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.entities.GiftCard;
import com.bridge.zuhur.entities.Order;
import com.bridge.zuhur.entities.PackagingWay;
import com.bridge.zuhur.entities.PaymentMethod;
import com.bridge.zuhur.entities.Place;
import com.bridge.zuhur.entities.Product;
import com.bridge.zuhur.entities.Promotion;
import com.bridge.zuhur.entities.Remainder;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.entities.User;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.utils.Sensor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class HomeActivity1 extends AbstractActivity {

    @InjectView(R.id.button)
    Button button;

    @InjectView(R.id.button1)
    Button button1;

    @InjectView(R.id.button2)
    Button button2;

    @InjectView(R.id.button3)
    Button button3;

    @InjectView(R.id.button4)
    Button button4;

    @InjectView(R.id.button5)
    Button button5;

    @InjectView(R.id.button6)
    Button button6;

    @InjectView(R.id.button7)
    Button button7;

    @InjectView(R.id.button8)
    Button button8;

    @InjectView(R.id.button9)
    Button button9;

    @InjectView(R.id.button10)
    Button button10;

    @InjectView(R.id.button11)
    Button button11;

    @InjectView(R.id.button12)
    Button button12;

    @InjectView(R.id.button13)
    Button button13;

    @InjectView(R.id.button14)
    Button button14;

    @InjectView(R.id.button15)
    Button button15;

    @InjectView(R.id.button16)
    Button button16;

    @InjectView(R.id.button17)
    Button button17;

    @InjectView(R.id.button18)
    Button button18;

    @InjectView(R.id.button19)
    Button button19;

    @InjectView(R.id.button20)
    Button button20;

    @InjectView(R.id.button21)
    Button button21;

    @InjectView(R.id.button22)
    Button button22;

    @InjectView(R.id.button23)
    Button button23;

    @InjectView(R.id.button24)
    Button button24;

    @InjectView(R.id.button25)
    Button button25;

    @InjectView(R.id.button26)
    Button button26;

    @InjectView(R.id.button27)
    Button button27;

    @InjectView(R.id.button28)
    Button button28;

    @InjectView(R.id.button29)
    Button button29;

    @InjectView(R.id.button30)
    Button button30;

    @InjectView(R.id.button31)
    Button button31;

    @InjectView(R.id.button32)
    Button button32;

    @InjectView(R.id.button33)
    Button button33;

    @InjectView(R.id.button34)
    Button button34;

    @InjectView(R.id.button35)
    Button button35;

    @InjectView(R.id.button36)
    Button button36;

    @InjectView(R.id.button37)
    Button button37;

    @InjectView(R.id.button38)
    Button button38;

    @InjectView(R.id.button39)
    Button button39;

    @InjectView(R.id.button40)
    Button button40;

    @InjectView(R.id.button41)
    Button button41;

    @InjectView(R.id.button42)
    Button button42;

    @InjectView(R.id.button43)
    Button button43;

    @InjectView(R.id.button44)
    Button button44;

    @InjectView(R.id.button45)
    Button button45;

    @InjectView(R.id.button46)
    Button button46;

    @InjectView(R.id.button47)
    Button button47;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home1);
        ButterKnife.inject(this);

        CategoriesController controller = new CategoriesController(this);
        final Bundle bundle = new Bundle();
        bundle.putInt("action", CategoriesController.GET_CATEGORIES_ACTION);
        controller.request(CategoriesController.GET_CATEGORIES_ACTION, bundle);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity1.this, CategoryProductsActivity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("categoryId", 1);
                intent.putExtras(bundle1);
                startActivity(intent);
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity1.this, AllProductsActivity.class);
                startActivity(intent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthenticationController authenticationController = new AuthenticationController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                JSONObject mainJsonObject = new JSONObject();
                JSONArray dataJsonArray = new JSONArray();
                try {
                    JSONObject userJsonObject = new JSONObject();
                    userJsonObject.put("name", "Hesham");
                    userJsonObject.put("username", "hesham");
                    userJsonObject.put("password", "hesham");
                    userJsonObject.put("email", "h.khaled2@bridgetomarket.com");
                    userJsonObject.put("address", "Ismailia,Egypt");
//                    userJsonObject.put("code", "akijhsgdjk");

                    JSONArray phonesJsonArray = new JSONArray();
                    JSONObject phone1JsonObject = new JSONObject(), phone2JsonObject = new JSONObject();
                    phone1JsonObject.put("phone", "01119993362");
                    phone1JsonObject.put("is_primary", "1");

                    phone2JsonObject.put("phone", "01119993363");
                    phone2JsonObject.put("is_primary", "0");

                    phonesJsonArray.put(phone1JsonObject);
                    phonesJsonArray.put(phone2JsonObject);

                    userJsonObject.put("phones", phonesJsonArray);

                    dataJsonArray.put(userJsonObject);

                    mainJsonObject.put("data", dataJsonArray);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                User user = new User();
                user.userDataAsJson = mainJsonObject.toString();
                Log.d(CommonConstants.APPLICATION_LOG_TAG, mainJsonObject.toString());
                bundle1.putInt("action", AuthenticationController.SIGN_UP_ACTION);
                bundle1.putSerializable("user", user);
                authenticationController.request(AuthenticationController.SIGN_UP_ACTION, bundle1);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthenticationController authenticationController = new AuthenticationController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                JSONObject mainJsonObject = new JSONObject(), userDataJsonObject = new JSONObject();
                JSONArray dataJsonArray = new JSONArray();
                try {
                    userDataJsonObject.put("username", "hesham");
                    userDataJsonObject.put("password", "hesham");
                    dataJsonArray.put(userDataJsonObject);
                    mainJsonObject.put("data", dataJsonArray);
                    User user = new User();
                    user.userDataAsJson = mainJsonObject.toString();
                    bundle1.putInt("action", AuthenticationController.SIGN_IN_ACTION);
                    bundle1.putSerializable("user", user);
                    authenticationController.request(AuthenticationController.SIGN_IN_ACTION, bundle1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthenticationController authenticationController = new AuthenticationController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", AuthenticationController.FORGET_MY_PASSWORD_ACTION);
                bundle1.putString("email", "heshamkhaled91@yahoo.com");
                authenticationController.request(AuthenticationController.FORGET_MY_PASSWORD_ACTION, bundle1);
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthenticationController authenticationController = new AuthenticationController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                JSONObject mainJsonObject = new JSONObject();
                JSONArray dataJsonArray = new JSONArray();
                try {
                    JSONObject userJsonObject = new JSONObject();
                    userJsonObject.put("name", "Hesham");
                    userJsonObject.put("username", "hesham");
                    userJsonObject.put("password", "123456");
                    userJsonObject.put("email", "heshamkhaled91@yahoo.com");
                    userJsonObject.put("address", "Ismailia, Egypt");

                    JSONArray phonesJsonArray = new JSONArray();
                    JSONObject phone1JsonObject = new JSONObject(), phone2JsonObject = new JSONObject();
                    phone1JsonObject.put("phone", "01119993362");
                    phone1JsonObject.put("is_primary", "1");

                    phone2JsonObject.put("phone", "01119993363");
                    phone2JsonObject.put("is_primary", "0");

                    phonesJsonArray.put(phone1JsonObject);
                    phonesJsonArray.put(phone2JsonObject);

                    userJsonObject.put("phones", phonesJsonArray);

                    dataJsonArray.put(userJsonObject);

                    mainJsonObject.put("data", dataJsonArray);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                User user = new User();
                user.userDataAsJson = mainJsonObject.toString();
                bundle1.putInt("action", AuthenticationController.UPDATE_PROFILE_ACTION);
                bundle1.putSerializable("user", user);
                authenticationController.request(AuthenticationController.UPDATE_PROFILE_ACTION, bundle1);
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.BEST_SELLER_ACTION);
                bundle1.putInt("countryId", 1);
                extrasController.request(ExtrasController.BEST_SELLER_ACTION, bundle1);
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.NEW_ARRIVALS_ACTION);
                bundle1.putInt("countryId", 1);
                extrasController.request(ExtrasController.NEW_ARRIVALS_ACTION, bundle1);
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.OUR_EXPERIENCE_ACTION);
                extrasController.request(ExtrasController.OUR_EXPERIENCE_ACTION, bundle1);
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.OUR_EXPERIENCE_PRODUCTS_ACTION);
                bundle1.putInt("id", 1);
                extrasController.request(ExtrasController.OUR_EXPERIENCE_PRODUCTS_ACTION, bundle1);
            }
        });

        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("countryId", 1);
                bundle1.putInt("action", ExtrasController.OFFERS_ACTION);
                extrasController.request(ExtrasController.OFFERS_ACTION, bundle1);
            }
        });

        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("countryId", 1);
                bundle1.putInt("action", ExtrasController.RECENT_VIEWED_ACTION);
                extrasController.request(ExtrasController.RECENT_VIEWED_ACTION, bundle1);
            }
        });

        button12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                Product product = new Product();
                product.id = 3;
                product.arabicName = "Recent 1 product";
                product.categoryId = 1;
                bundle1.putInt("action", ExtrasController.ADD_RECENT_VIEWED_ACTION);
                bundle1.putInt("countryId", 1);
                bundle1.putSerializable("product", product);
                extrasController.request(ExtrasController.ADD_RECENT_VIEWED_ACTION, bundle1);
            }
        });

        button13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Language.changeLanguage(HomeActivity1.this);
            }
        });

        button14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        button15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductsController productsController = new ProductsController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ProductsController.SEARCH_BY_NAME_ACTION);
                bundle1.putString("sort", CommonConstants.SORT_PRICE_LOW_TO_HIGH);
                bundle1.putString("keyWord", "red");
                bundle1.putInt("countryId", 1);
                productsController.request(ProductsController.SEARCH_BY_NAME_ACTION, bundle1);
            }
        });

        button16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = new Product();
                product.id = 2;
                product.categoryId = 1;
                Favourite favourite = new Favourite(product);

                FavouritesController favouritesController = new FavouritesController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", FavouritesController.ADD_FAVOURITE_TO_SERVER_ACTION);
                bundle1.putSerializable("favourite", favourite);
                favouritesController.request(FavouritesController.ADD_FAVOURITE_TO_SERVER_ACTION, bundle1);
            }
        });

        button17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavouritesController favouritesController = new FavouritesController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", FavouritesController.GET_ALL_FAVOURITES_SERVER_ACTION);
                favouritesController.request(FavouritesController.GET_ALL_FAVOURITES_SERVER_ACTION, bundle1);
            }
        });

        button18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = new Product();
                product.id = 2;
                product.categoryId = 1;
                WishList wishList = new WishList(product);

                WishListController wishListController = new WishListController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", WishListController.ADD_WISH_LIST_TO_SERVER_ACTION);
                bundle1.putSerializable("wishList", wishList);
                wishListController.request(WishListController.ADD_WISH_LIST_TO_SERVER_ACTION, bundle1);
            }
        });

        button19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WishListController favouritesController = new WishListController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", WishListController.GET_WISH_LIST_FROM_SERVER_ACTION);
                favouritesController.request(WishListController.GET_WISH_LIST_FROM_SERVER_ACTION, bundle1);
            }
        });

        button20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FavouritesController favouritesController = new FavouritesController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", FavouritesController.DELETE_FAVOURITE_SERVER_ACTION);
                bundle1.putSerializable("productId", 2);
                favouritesController.request(FavouritesController.DELETE_FAVOURITE_SERVER_ACTION, bundle1);
            }
        });

        button21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WishListController wishListController = new WishListController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", WishListController.DELETE_WISH_LIST_FROM_SERVER_ACTION);
                bundle1.putSerializable("productId", 2);
                wishListController.request(WishListController.DELETE_WISH_LIST_FROM_SERVER_ACTION, bundle1);
            }
        });

        button22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.EXPERIENCE_EVENTS_ACTION);
                extrasController.request(ExtrasController.EXPERIENCE_EVENTS_ACTION, bundle1);
            }
        });

        button23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthenticationController authenticationController = new AuthenticationController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", AuthenticationController.UPDATE_PASSWORD_ACTION);
                bundle1.putString("password", "123456");
                bundle1.putString("newPassword", "hesham");
                authenticationController.request(AuthenticationController.UPDATE_PASSWORD_ACTION, bundle1);
            }
        });

        button24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.PAYMENT_METHODS_ACTION);
                extrasController.request(ExtrasController.PAYMENT_METHODS_ACTION, bundle1);
            }
        });

        button25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PackagingWay packagingWay = new PackagingWay();
                packagingWay.id = 1;
                packagingWay.price = 0.0;

                GiftCard giftCard = new GiftCard();
                giftCard.id = 1;
                giftCard.price = 0.0;
                giftCard.content = "happy birthday";

                Promotion promotion = new Promotion();
                promotion.promotionCode = "mother_day";


                Product product2 = new Product(), product1 = new Product();
                product1.id = 1;
                product1.price = 66.50;
                product1.packagingWay = packagingWay;
                product1.giftCard = giftCard;

                product2.id = 2;
                product2.price = 10.00;
                product2.packagingWay = packagingWay;
                product2.giftCard = giftCard;

                Place billingPlace = new Place(), deliverPlace = new Place();
                billingPlace.address = "Ismailia, Egypt";
                billingPlace.latitude = "34.27648327462786";
                billingPlace.longitude = "30.187638177236";
                deliverPlace.address = "Ismailia, Egypt";
                deliverPlace.latitude = "34.27648327462786";
                deliverPlace.longitude = "30.187638177236";

                PaymentMethod paymentMethod = new PaymentMethod();
                paymentMethod.id = 1;

                Order order = new Order();
                order.products.add(product1);
                order.products.add(product2);
                order.billingPlace = billingPlace;
                order.deliverPlace = deliverPlace;
                order.creationDate = "2016-04-13";
                order.isCompleted = false;
                order.isConfirmed = false;
                order.shippingDate = "2016-04-15";
                order.originalPrice = product1.price + product2.price;
                for (Product product : order.products) {
                    if (product.packagingWay != null) {
                        order.originalPrice = order.originalPrice + product.packagingWay.price;
                    }

                    if (product.giftCard != null) {
                        order.originalPrice = order.originalPrice + product.giftCard.price;
                    }
                }
                order.mainPaymentMethod = paymentMethod;
                order.promotion = promotion;

                OrderController orderController = new OrderController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", OrderController.ADD_ORDER_TO_SERVER_ACTION);
                bundle1.putSerializable("order", order);
                bundle1.putString("code", order.promotion.promotionCode);
                orderController.request(OrderController.ADD_ORDER_TO_SERVER_ACTION, bundle1);

                /*
                {"data":[{"bill_address":"Ismailia, Egypt",
                "payment_method_id":1,"deliver_address":"Ismailia, Egypt",
                "bill_address_Y":"30.187638177236","deliver_address_Y":"30.187638177236",
                "user_id":8,"bill_address_X":"34.27648327462786",
                "products":[{"content":"happy birthday","product_id":1,"card_id":1,"packaging_id":1},
                {"content":"happy birthday","product_id":2,"card_id":1,"packaging_id":1}],
                "shipping_date":"2016-04-15","deliver_address_X":"34.27648327462786","total_price":"50.0","promotion_codes":"mother_day"}]}
                 */
            }
        });

        button26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderController orderController = new OrderController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", OrderController.GET_ORDERS_FROM_SERVER_ACTION);
                orderController.request(OrderController.GET_ORDERS_FROM_SERVER_ACTION, bundle1);
            }
        });

        button27.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Remainder remainder = new Remainder();
                remainder.orderId = 1;
                remainder.remainderDate = "2016-04-20";
                remainder.details = "Remainder for test";

                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.ADD_REMAINDERS_ACTION);
                bundle1.putSerializable("remainder", remainder);
                extrasController.request(ExtrasController.ADD_REMAINDERS_ACTION, bundle1);
            }
        });

        button28.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.REMAINDERS_ACTION);
                extrasController.request(ExtrasController.REMAINDERS_ACTION, bundle1);
            }
        });

        button29.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Remainder remainder = new Remainder();
                remainder.orderId = 1;
                remainder.remainderDate = "2016-04-20";
                remainder.details = "Remainder for test";
                remainder.id = 2;

                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.EDIT_REMAINDERS_ACTION);
                bundle1.putSerializable("remainder", remainder);
                extrasController.request(ExtrasController.EDIT_REMAINDERS_ACTION, bundle1);
            }
        });

        button30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.DELETE_REMAINDERS_ACTION);
                bundle1.putInt("id", 1);// remainder id
                extrasController.request(ExtrasController.DELETE_REMAINDERS_ACTION, bundle1);
            }
        });

        button31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.EVENT_DETAILS_ACTION);
                bundle1.putInt("id", 1);// event id
                extrasController.request(ExtrasController.EVENT_DETAILS_ACTION, bundle1);
            }
        });

        button32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.OFFER_DETAILS_ACTION);
                bundle1.putInt("id", 1);// offer id
                extrasController.request(ExtrasController.OFFER_DETAILS_ACTION, bundle1);
            }
        });

        button33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.MAKE_INTERNATIONAL_SHIPPING_ACTION);
                bundle1.putInt("id", 1);// order id
                bundle1.putString("country", "Egypt");
                extrasController.request(ExtrasController.MAKE_INTERNATIONAL_SHIPPING_ACTION, bundle1);
            }
        });

        button34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductsController productsController = new ProductsController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ProductsController.RATE_PRODUCT_ACTION);
                bundle1.putInt("id", 1);// product id
                bundle1.putDouble("rate", 4.2);
                productsController.request(ProductsController.RATE_PRODUCT_ACTION, bundle1);
            }
        });

        button35.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.SEND_EMAIL_ACTION);
                extrasController.request(ExtrasController.SEND_EMAIL_ACTION, bundle1);
            }
        });

        button36.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.MAKE_CALL_ACTION);
                extrasController.request(ExtrasController.MAKE_CALL_ACTION, bundle1);
            }
        });

        button37.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.OPEN_URL_ACTION);
                extrasController.request(ExtrasController.OPEN_URL_ACTION, bundle1);
            }
        });

        button38.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderController orderController = new OrderController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", OrderController.TRACK_ORDER__ACTION);
                bundle1.putInt("id", 1); // order id
                orderController.request(OrderController.TRACK_ORDER__ACTION, bundle1);
            }
        });

        button39.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity1.this, MainActivity.class);
                startActivity(intent);
            }
        });

        button40.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.EVENTS_ACTION);
                extrasController.request(ExtrasController.EVENTS_ACTION, bundle1);
            }
        });

        button41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductsController productsController = new ProductsController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("id", 1);
                bundle1.putInt("countryId", 1);
                bundle1.putInt("action", ProductsController.RELATED_PRODUCTS_ACTION);
                productsController.request(ProductsController.RELATED_PRODUCTS_ACTION, bundle1);
            }
        });

        button42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderController extrasController = new OrderController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", OrderController.GIFT_CARDS_ACTION);
                extrasController.request(OrderController.GIFT_CARDS_ACTION, bundle1);
            }
        });

        button43.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderController extrasController = new OrderController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", OrderController.PACKAGING_WAYS_ACTION);
                extrasController.request(OrderController.PACKAGING_WAYS_ACTION, bundle1);
            }
        });

        button44.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.TAGS_ACTION);
                extrasController.request(ExtrasController.TAGS_ACTION, bundle1);
            }
        });

        button45.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.GET_TAGS_FROM_DATABASE_ACTION);
                bundle1.putString("query", "te");
                extrasController.request(ExtrasController.GET_TAGS_FROM_DATABASE_ACTION, bundle1);
            }
        });

        button46.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderController orderController = new OrderController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putString("code", "mother_day");
                bundle1.putInt("action", OrderController.PROMOTION_CODE_VALIDATION_ACTION);
                orderController.request(OrderController.PROMOTION_CODE_VALIDATION_ACTION, bundle1);
            }
        });

        button47.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExtrasController extrasController = new ExtrasController(HomeActivity1.this);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("action", ExtrasController.COUNTRIES_ACTION);
                extrasController.request(ExtrasController.COUNTRIES_ACTION, bundle1);
            }
        });

    }

    private void logout() {

        Singleton.getInstance().currentUser = null;
        Singleton.getInstance().token = null;
        SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("isLogged", false);
        editor.remove("name");
        editor.remove("address");
        editor.remove("username");
        editor.remove("email");
        editor.remove("id");
        int phoneNumber = settings.getInt("phoneNumber", 0);
        for (int i = 0; i < phoneNumber; i++) {
            editor.remove("number" + i);
            editor.remove("number" + i + "isPrimary");
        }
        editor.remove("phoneNumber");
        editor.apply();

    }

    @Override
    public void onDataReceived(ArrayList<? extends Entity> result, int action) {
        switch (action) {
            case CategoriesController.GET_CATEGORIES_ACTION:
                ArrayList<Category> categories = (ArrayList<Category>) result;
                for (Category category : categories) {
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, category.arabicName);
                }
                break;

            default:
                break;
        }
        Sensor.unlockSensor(this);
    }
}
