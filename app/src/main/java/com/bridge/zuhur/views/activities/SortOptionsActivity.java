package com.bridge.zuhur.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bridge.zuhur.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SortOptionsActivity extends Activity {


    @InjectView(R.id.sortTypesRadioGroup)
    RadioGroup sortTypesRadioGroup;

    @InjectView(R.id.confirmSortChooseButton)
    Button confirmSortChooseButton;

    int selectedType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort_options);

        ButterKnife.inject(this);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);


        getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        sortTypesRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.sort1RadioButton: //high to low
                        selectedType = 1;
                        break;

                    case R.id.sort2RadioButton: //low to high
                        selectedType = 2;
                        break;

                    case R.id.sort3RadioButton: //best seller
                        selectedType = 3;
                        break;

                    case R.id.sort4RadioButton: // new arrival
                        selectedType = 4;
                        break;

                    case R.id.sort5RadioButton: //most viewed
                        selectedType = 5;
                        break;

                    default:
                        break;
                }
            }
        });

        confirmSortChooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedType != 0) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putInt("type", selectedType);
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(SortOptionsActivity.this, getResources().getString(R.string.sortSelectError), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
