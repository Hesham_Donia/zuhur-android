package com.bridge.zuhur.views.activities;

import android.app.Activity;
import android.os.Bundle;

import com.bridge.zuhur.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class ProductImageActivity extends Activity {

    @InjectView(R.id.productImageView)
    PhotoView productImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_image);

        ButterKnife.inject(this);

        String imagePath = getIntent().getExtras().getString("imagePath");
        final PhotoViewAttacher attacher = new PhotoViewAttacher(productImageView);
        Picasso.with(this).load(imagePath).into(productImageView, new Callback() {
            @Override
            public void onSuccess() {
                attacher.update();
            }

            @Override
            public void onError() {

            }
        });
    }

}
