package com.bridge.zuhur.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bridge.zuhur.R;
import com.bridge.zuhur.entities.Category;
import com.bridge.zuhur.entities.Singleton;
import com.bridge.zuhur.views.activities.CategoryProductsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bridgetomarket on 5/27/16.
 */
public class CategoriesRecyclerViewAdapter extends RecyclerView.Adapter<CategoriesRecyclerViewAdapter.ViewHolder> {


    ViewHolder viewHolder;
    Typeface typeface;
    private Context mContext;
    private ArrayList<Category> mCategories, mLeftCategories = new ArrayList<>(), mRightCategories = new ArrayList<>();
    private LayoutInflater mInflater = null;

    public CategoriesRecyclerViewAdapter(Context context, ArrayList<Category> categories) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mCategories = categories;
        typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/arcena.ttf");
        for (int i = 0; i < mCategories.size(); i++) {
            if (i % 2 == 0) {
                mLeftCategories.add(mCategories.get(i));
            } else {
                mRightCategories.add(mCategories.get(i));
            }
        }
    }

    @Override
    public CategoriesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.categories_item, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(CategoriesRecyclerViewAdapter.ViewHolder holder, final int position) {
        ViewHolder ordinaryHolder = holder;
        ordinaryHolder.emptyViewHolder();

        if (mRightCategories.size() != 0 && position < mRightCategories.size()) {
            Picasso.with(mContext).load(mRightCategories.get(position).imagePath).into(ordinaryHolder.rightCategoryImageView);
            if (Singleton.getInstance().currentLanguage.equals("en")) {
                ordinaryHolder.rightCategoryTextView.setText(mRightCategories.get(position).englishName);
            } else {
                ordinaryHolder.rightCategoryTextView.setText(mRightCategories.get(position).arabicName);
            }

            ordinaryHolder.rightCategoryTextView.setTypeface(typeface);

            ordinaryHolder.rightCategoryCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, CategoryProductsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("categoryId", mRightCategories.get(position).id);
                    bundle.putString("catNameAr", mRightCategories.get(position).arabicName);
                    bundle.putString("catNameEn", mRightCategories.get(position).englishName);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        } else {
            ordinaryHolder.rightCategoryCardView.setVisibility(View.GONE);
        }

        if (mLeftCategories.size() != 0 && position < mLeftCategories.size()) {
            Picasso.with(mContext).load(mLeftCategories.get(position).imagePath).into(ordinaryHolder.leftCategoryImageView);
            if (Singleton.getInstance().currentLanguage.equals("en")) {
                ordinaryHolder.leftCategoryTextView.setText(mLeftCategories.get(position).englishName);
            } else {
                ordinaryHolder.leftCategoryTextView.setText(mLeftCategories.get(position).arabicName);
            }

            ordinaryHolder.leftCategoryTextView.setTypeface(typeface);

            ordinaryHolder.leftCategoryCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, CategoryProductsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("categoryId", mLeftCategories.get(position).id);
                    bundle.putString("catNameAr", mLeftCategories.get(position).arabicName);
                    bundle.putString("catNameEn", mLeftCategories.get(position).englishName);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        } else {
            ordinaryHolder.leftCategoryCardView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.leftCategoryCardView)
        CardView leftCategoryCardView;

        @InjectView(R.id.leftCategoryImageView)
        ImageView leftCategoryImageView;

        @InjectView(R.id.leftCategoryTextView)
        TextView leftCategoryTextView;

        @InjectView(R.id.rightCategoryCardView)
        CardView rightCategoryCardView;

        @InjectView(R.id.rightCategoryImageView)
        ImageView rightCategoryImageView;

        @InjectView(R.id.rightCategoryTextView)
        TextView rightCategoryTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);

        }

        public void emptyViewHolder() {
            leftCategoryCardView.setVisibility(View.VISIBLE);
            rightCategoryCardView.setVisibility(View.VISIBLE);
            leftCategoryImageView.setImageBitmap(null);
            rightCategoryImageView.setImageBitmap(null);
            leftCategoryTextView.setText("");
            rightCategoryTextView.setText("");
        }
    }
}
