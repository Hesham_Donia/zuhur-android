package com.bridge.zuhur.entities;

import java.util.ArrayList;

/**
 * Created by hesham on 06/04/16.
 */
public class User extends Entity {

    public int id = 0;
    public String name = null;
    public String username = null;
    public String email = null;
    public String address = null;
    public ArrayList<Phone> phones = new ArrayList<>();
    public String userDataAsJson = null;
    public int points = 0;
    public String code = null;
    public int level = 0;
}
