package com.bridge.zuhur.entities;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public class Category extends Entity {

    public int id = 0;
    public String arabicName = null;
    public String englishName = null;
    public String imagePath = null;
    public ArrayList<Tag> tags = null;

}
