package com.bridge.zuhur.entities;

/**
 * Created by bridgetomarket on 5/8/16.
 */
public class GiftCard extends Entity {


    public int id = 0;
    public String nameAr = null;
    public String nameEn = null;
    public String detailsAr = null;
    public String detailsEn = null;
    public String imagePath = null;
    public String code = null;
    public double price = 0.0;
    public String content = null;
    public int pointsPrice = 0;

}
