package com.bridge.zuhur.entities;

/**
 * Created by hesham on 14/04/16.
 */
public class Event extends Entity {

    public int id = 0;
    public String arabicName = null;
    public String englishName = null;
    public String arabicDetails = null;
    public String englishDetails = null;
    public String imagePath = null;

}
