package com.bridge.zuhur.entities;

/**
 * Created by hesham on 06/04/16.
 */
public class Phone extends Entity {

    public String number = null;
    public boolean isPrimary;

    public Phone(String number,boolean isPrimary){
        this.isPrimary = isPrimary;
        this.number = number;
    }

    public Phone(){

    }

}
