package com.bridge.zuhur.entities;

/**
 * Created by hesham on 04/04/16.
 */
public class Product extends Entity {
    public int categoryId = 0;
    public int id = 0;
    public String arabicName = null;
    public String englishName = null;
    public String arabicDescription = null;
    public String englishDescription = null;
    public String url = null;
    public String imagePath = null;
    public double price = 0.0;
    public double rate = 0.0;
    public boolean isExist;
    public boolean isNewArrival;
    public String creationDate;
    public int sellCounter = 0;
    public int seenCounter = 0;
    public int peopleRatedCounter = 0;
    public PackagingWay packagingWay = null;
    public GiftCard giftCard = null;
    public int pointsPrice = 0;
    public int quantity = 1;
}
