package com.bridge.zuhur.entities;

import com.bridge.zuhur.database.LogData;
import com.bridge.zuhur.interfaces.ObserverListener;
import com.bridge.zuhur.views.activities.AbstractActivity;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by heshamkhaled on 2/7/16.
 */
public class Singleton {

    public static Stack<AbstractActivity> activityStack = new Stack<>();
    public static AbstractActivity currentActivity;
    private static Singleton singleton = null;
    public User currentUser;
    public String token = null;
    public Country country = null;
    public ArrayList<Country> countries = new ArrayList<>();
    public String currentLanguage = "";
    public ArrayList<ObserverListener> observers = new ArrayList<>();
    public ArrayList<Product> cart = new ArrayList<>();
    public ArrayList<LogData> logDataArrayList = new ArrayList<>();
    public ArrayList<Color> colors = new ArrayList<>();
    public ArrayList<ExperienceEvent> experienceEvents = new ArrayList<>();

    public static Singleton getInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        }

        return singleton;
    }

}
