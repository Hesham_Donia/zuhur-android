package com.bridge.zuhur.entities;

/**
 * Created by hesham on 17/04/16.
 */
public class NotificationEntity extends Entity {

    public int type = 0;
    public String arabicMessage = null;
    public String englishMessage = null;
    public String imagePath = null;
    public Entity entity = null;
}
