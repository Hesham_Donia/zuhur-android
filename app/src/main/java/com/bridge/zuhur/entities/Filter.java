package com.bridge.zuhur.entities;

/**
 * Created by hesham on 04/04/16.
 */
public class Filter extends Entity {
    public String searchQuery = null;
    public long limit = 10;
    public String sortType = null;
    public String username = null;
    public String password = null;
    public String newPassword = null;
    public int id = 0;
    public String productsIds = null;
    public int priceFrom = 0;
    public int priceTo = 0;
    public double rate = 0.0;
    public int productId = 0;
    public int categoryId = 0;
    public int countryId = 0;
    public String tags = null;
    public int remainderId = 0;
    public String orderId = null;
    public String countryName = null;
    public String email = null;
    public String promotionCode = null;
    public double totalPrice = 0.0;
}
