package com.bridge.zuhur.entities;

import java.util.ArrayList;

/**
 * Created by hesham on 09/04/16.
 */
public class ExperienceEvent extends Entity {

    public int id = 0;
    public String arabicName = null;
    public String englishName = null;
    public ArrayList<Product> products = new ArrayList<>();

}
