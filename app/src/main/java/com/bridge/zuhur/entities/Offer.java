package com.bridge.zuhur.entities;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by hesham on 04/04/16.
 */
public class Offer extends Entity {
    public int id = 0;
    public ArrayList<Product> products = new ArrayList<>();
    public String arabicDetails = null;
    public String englishDetails = null;
    public double totalPrice = 0.0;
    public double originalPrice = 0.0;
    public String imagePath = null;
    public String creationDate = null;
    public String endDate = null;
    public boolean isCompleted;
}
