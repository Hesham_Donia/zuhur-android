package com.bridge.zuhur.entities;

import com.stripe.android.model.Token;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public class Order extends Entity {

    public int id = 0;
    public ArrayList<Product> products = new ArrayList<>();
    public double finalPrice = 0.0;
    public double originalPrice = 0.0;
    public int pointsPrice = 0;
    public String creationDate;
    public boolean isCompleted;
    public OrderStatus orderStatus = null;
    public boolean isConfirmed;
    public Place billingPlace = null;
    public Place deliverPlace = null;
    public String shippingDate;
    public PaymentMethod mainPaymentMethod = null;
    public PaymentMethod subPaymentMethod = null;
    public Promotion promotion = null;
    public int countryId = 0;
    public Token token = null;
}
