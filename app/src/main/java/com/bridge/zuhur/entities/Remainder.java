package com.bridge.zuhur.entities;

/**
 * Created by hesham on 13/04/16.
 */
public class Remainder extends Entity {

    public int id = 0;
    public int orderId = 0;
    public String remainderDate = null;
    public String details = null;
    public boolean isCompleted;
    public boolean isUpdate;

}
