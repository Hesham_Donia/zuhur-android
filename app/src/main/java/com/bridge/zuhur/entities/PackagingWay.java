package com.bridge.zuhur.entities;

/**
 * Created by bridgetomarket on 5/9/16.
 */
public class PackagingWay extends Entity {

    public int id = 0;
    public String nameAr = null;
    public String nameEn = null;
    public String detailsAr = null;
    public String detailsEn = null;
    public String imagePath = null;
    public String code = null;
    public Double price = 0.0;
    public int pointsPrice = 0;

}
