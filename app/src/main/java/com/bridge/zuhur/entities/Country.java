package com.bridge.zuhur.entities;

/**
 * Created by hesham on 04/04/16.
 */
public class Country extends Entity {

    public int id = 0;
    public String arabicName = null;
    public String englishName = null;
    public String imagePath = null;
    public String currency = null;
    public double dollarValue = 0.0;

}
