package com.bridge.zuhur.entities;

/**
 * Created by bridgetomarket on 5/30/16.
 */
public class NavigationViewItem extends Entity {

    public int icon = 0;
    public String name = null;

    public NavigationViewItem(int icon, String name) {
        this.name = name;
        this.icon = icon;
    }
}
