package com.bridge.zuhur;

import android.app.Application;

import com.bridge.zuhur.dagger.AddRecentViewedModule;
import com.bridge.zuhur.dagger.AllProductsModule;
import com.bridge.zuhur.dagger.ApplicationModule;
import com.bridge.zuhur.dagger.AutoCompleteDatabaseModule;
import com.bridge.zuhur.dagger.AutoCompleteModule;
import com.bridge.zuhur.dagger.BestSellerModule;
import com.bridge.zuhur.dagger.CategoriesModule;
import com.bridge.zuhur.dagger.CategoryProductsModule;
import com.bridge.zuhur.dagger.ColorsModule;
import com.bridge.zuhur.dagger.CountriesModule;
import com.bridge.zuhur.dagger.EventsModule;
import com.bridge.zuhur.dagger.ExperienceEventsModule;
import com.bridge.zuhur.dagger.FavouritesDatabaseModule;
import com.bridge.zuhur.dagger.FavouritesModule;
import com.bridge.zuhur.dagger.ForgetMyPasswordModule;
import com.bridge.zuhur.dagger.GiftCardsModule;
import com.bridge.zuhur.dagger.InternationalShippingRequestModule;
import com.bridge.zuhur.dagger.LogDataModule;
import com.bridge.zuhur.dagger.NewArrivalsModule;
import com.bridge.zuhur.dagger.NotificationsModule;
import com.bridge.zuhur.dagger.OffersModule;
import com.bridge.zuhur.dagger.OrderStripeModule;
import com.bridge.zuhur.dagger.OrdersDatabaseModule;
import com.bridge.zuhur.dagger.OrdersModule;
import com.bridge.zuhur.dagger.OurExperienceModule;
import com.bridge.zuhur.dagger.OurExperienceProductsModule;
import com.bridge.zuhur.dagger.PackagingWayModule;
import com.bridge.zuhur.dagger.PaymentMethodModule;
import com.bridge.zuhur.dagger.ProductDetailsModule;
import com.bridge.zuhur.dagger.PromotionValidationModule;
import com.bridge.zuhur.dagger.RecentViewedModule;
import com.bridge.zuhur.dagger.RelatedProductsModule;
import com.bridge.zuhur.dagger.RemaindersDatabaseModule;
import com.bridge.zuhur.dagger.RemaindersModule;
import com.bridge.zuhur.dagger.SearchByNameModule;
import com.bridge.zuhur.dagger.SignInFacebookModule;
import com.bridge.zuhur.dagger.SignInModule;
import com.bridge.zuhur.dagger.SignUpFacebookModule;
import com.bridge.zuhur.dagger.SignUpModule;
import com.bridge.zuhur.dagger.TagsDatabaseModule;
import com.bridge.zuhur.dagger.TagsModule;
import com.bridge.zuhur.dagger.UpdatePasswordModule;
import com.bridge.zuhur.dagger.UpdateProfileModule;
import com.bridge.zuhur.dagger.UserPointsModule;
import com.bridge.zuhur.dagger.WishListDatabaseModule;
import com.bridge.zuhur.dagger.WishListModule;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;

/**
 * Created by hesham on 05/04/16.
 */
public class ZuhurApplication extends Application {
    public static ObjectGraph objectGraph;

    public void onCreate() {
        super.onCreate();
        Object[] modules = getModules().toArray();
        objectGraph = ObjectGraph.create(modules);
        objectGraph.inject(this);
    }

    protected List<Object> getModules() {
        ApplicationModule applicationModule = new ApplicationModule(this);
        return Arrays.<Object>asList(
                applicationModule,
                new CategoriesModule(),
                new CategoryProductsModule(),
                new AllProductsModule(),
                new ProductDetailsModule(),
                new SignUpModule(),
                new SignInModule(),
                new ForgetMyPasswordModule(),
                new UpdateProfileModule(),
                new BestSellerModule(),
                new NewArrivalsModule(),
                new OurExperienceModule(),
                new OurExperienceProductsModule(),
                new OffersModule(),
                new RecentViewedModule(),
                new AddRecentViewedModule(),
                new FavouritesModule(),
                new FavouritesDatabaseModule(),
                new WishListModule(),
                new WishListDatabaseModule(),
                new SearchByNameModule(),
                new ExperienceEventsModule(),
                new UpdatePasswordModule(),
                new PaymentMethodModule(),
                new OrdersModule(),
                new OrdersDatabaseModule(),
                new RemaindersModule(),
                new EventsModule(),
                new NotificationsModule(),
                new InternationalShippingRequestModule(),
                new RelatedProductsModule(),
                new GiftCardsModule(),
                new PackagingWayModule(),
                new TagsModule(),
                new TagsDatabaseModule(),
                new PromotionValidationModule(),
                new CountriesModule(),
                new RemaindersDatabaseModule(),
                new AutoCompleteModule(),
                new AutoCompleteDatabaseModule(),
                new LogDataModule(),
                new SignUpFacebookModule(),
                new SignInFacebookModule(),
                new ColorsModule(),
                new UserPointsModule(),
                new OrderStripeModule()
        );
    }
}
