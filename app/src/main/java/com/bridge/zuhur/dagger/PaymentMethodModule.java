package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.PaymentMethodMapper;
import com.bridge.zuhur.models.PaymentMethodModel;
import com.bridge.zuhur.services.PaymentMethodService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 12/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {PaymentMethodService.class, PaymentMethodModel.class,
        PaymentMethodMapper.class}
)

public class PaymentMethodModule {

    @Provides
    PaymentMethodService providePaymentMethodService() {
        return new PaymentMethodService(ZuhurApplication.objectGraph.get(PaymentMethodModel.class));
    }

    @Provides
    PaymentMethodModel providePaymentMethodModel() {
        return new PaymentMethodModel(ZuhurApplication.objectGraph.get(PaymentMethodMapper.class));
    }

    @Provides
    PaymentMethodMapper providePaymentMethodMapper(@ForApplication Context context) {
        return new PaymentMethodMapper(context);
    }
    
}
