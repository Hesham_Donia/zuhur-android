package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.UpdateProfileMapper;
import com.bridge.zuhur.models.UpdateProfileModel;
import com.bridge.zuhur.services.UpdateProfileService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 07/04/16.
 */


@Module(
        library = true
        , complete = false, injects = {UpdateProfileService.class, UpdateProfileModel.class, UpdateProfileMapper.class}
)

public class UpdateProfileModule {

    @Provides
    UpdateProfileService provideUpdateProfileService() {
        return new UpdateProfileService(ZuhurApplication.objectGraph.get(UpdateProfileModel.class));
    }

    @Provides
    UpdateProfileModel provideUpdateProfileModel() {
        return new UpdateProfileModel(ZuhurApplication.objectGraph.get(UpdateProfileMapper.class));
    }

    @Provides
    UpdateProfileMapper provideUpdateProfileMapper(@ForApplication Context context) {
        return new UpdateProfileMapper(context);
    }
}
