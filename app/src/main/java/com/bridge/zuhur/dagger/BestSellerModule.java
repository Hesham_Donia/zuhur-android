package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.BestSellerMapper;
import com.bridge.zuhur.models.BestSellerModel;
import com.bridge.zuhur.services.BestSellerService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 07/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {BestSellerService.class, BestSellerModel.class,
        BestSellerMapper.class}

)
public class BestSellerModule {

    @Provides
    BestSellerService provideBestSellerService() {
        return new BestSellerService(ZuhurApplication.objectGraph.get(BestSellerModel.class));
    }

    @Provides
    BestSellerModel provideBestSellerModel() {
        return new BestSellerModel(ZuhurApplication.objectGraph.get(BestSellerMapper.class));
    }

    @Provides
    BestSellerMapper provideBestSellerMapper(@ForApplication Context context) {
        return new BestSellerMapper(context);
    }
}
