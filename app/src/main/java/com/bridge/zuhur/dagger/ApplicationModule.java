package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.controllers.AuthenticationController;
import com.bridge.zuhur.controllers.CategoriesController;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.FavouritesController;
import com.bridge.zuhur.controllers.NotificationsController;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.controllers.WishListController;

import javax.inject.Singleton;

import dagger.Module;
import dagger.ObjectGraph;
import dagger.Provides;

/**
 * Created by hesham on 05/04/16.
 */

@Module(
        injects = {ZuhurApplication.class, CategoriesController.class, ProductsController.class,
                ExtrasController.class, FavouritesController.class, WishListController.class,
                AuthenticationController.class, OrderController.class, NotificationsController.class},

        includes = {CategoriesModule.class, CategoryProductsModule.class, AllProductsModule.class,
                ProductDetailsModule.class, BestSellerModule.class, NewArrivalsModule.class,
                OurExperienceModule.class, OurExperienceProductsModule.class, OffersModule.class,
                RecentViewedModule.class, FavouritesModule.class, FavouritesDatabaseModule.class,
                WishListModule.class, SearchByNameModule.class, WishListDatabaseModule.class,
                ExperienceEventsModule.class, UpdatePasswordModule.class, PaymentMethodModule.class,
                OrdersModule.class, OrdersDatabaseModule.class, RemaindersModule.class, EventsModule.class,
                NotificationsModule.class, InternationalShippingRequestModule.class, RelatedProductsModule.class,
                GiftCardsModule.class, PackagingWayModule.class, TagsModule.class, TagsDatabaseModule.class,
                PromotionValidationModule.class, CountriesModule.class, RemaindersDatabaseModule.class
                , AutoCompleteModule.class, AutoCompleteDatabaseModule.class, LogDataModule.class,
                SignUpFacebookModule.class, SignInFacebookModule.class, ColorsModule.class, UserPointsModule.class
                , OrderStripeModule.class}
)

public class ApplicationModule {
    ZuhurApplication mApplication;

    public ApplicationModule(ZuhurApplication app) {
        mApplication = app;
    }

    @Provides
    @Singleton
    @ForApplication
    public Context provideApplicationContext() {
        return mApplication.getApplicationContext();
    }

    public ObjectGraph getObjectGraph() {
        return ZuhurApplication.objectGraph;
    }
}
