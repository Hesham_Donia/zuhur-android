package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.WishListMapper;
import com.bridge.zuhur.models.WishListModel;
import com.bridge.zuhur.services.WishListService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 10/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {WishListService.class, WishListModel.class,
        WishListMapper.class}

)

public class WishListModule {

    @Provides
    WishListService provideWishListService() {
        return new WishListService(ZuhurApplication.objectGraph.get(WishListModel.class));
    }

    @Provides
    WishListModel provideWishListModel() {
        return new WishListModel(ZuhurApplication.objectGraph.get(WishListMapper.class));
    }

    @Provides
    WishListMapper provideWishListMapper(@ForApplication Context context) {
        return new WishListMapper(context);
    }
}
