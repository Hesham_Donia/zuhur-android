package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.database_mappers.WishListDatabaseMapper;
import com.bridge.zuhur.models.WishListDatabaseModel;
import com.bridge.zuhur.services.WishListDatabaseService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 11/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {WishListDatabaseService.class, WishListDatabaseModel.class,
        WishListDatabaseMapper.class}

)

public class WishListDatabaseModule {

    @Provides
    WishListDatabaseService provideWishListDatabaseService() {
        return new WishListDatabaseService(ZuhurApplication.objectGraph.get(WishListDatabaseModel.class));
    }

    @Provides
    WishListDatabaseModel provideWishListDatabaseModel() {
        return new WishListDatabaseModel(ZuhurApplication.objectGraph.get(WishListDatabaseMapper.class));
    }

    @Provides
    WishListDatabaseMapper provideWishListDatabaseMapper(@ForApplication Context context) {
        return new WishListDatabaseMapper(context);
    }
    
}
