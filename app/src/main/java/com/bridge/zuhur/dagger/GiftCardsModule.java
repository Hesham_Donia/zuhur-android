package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.GiftCardsMapper;
import com.bridge.zuhur.models.GiftCardsModel;
import com.bridge.zuhur.services.GiftCardsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 5/8/16.
 */

@Module(
        library = true
        , complete = false, injects = {GiftCardsService.class, GiftCardsModel.class,
        GiftCardsMapper.class}

)

public class GiftCardsModule {

    @Provides
    GiftCardsService provideGiftCardsService() {
        return new GiftCardsService(ZuhurApplication.objectGraph.get(GiftCardsModel.class));
    }

    @Provides
    GiftCardsModel provideGiftCardsModel() {
        return new GiftCardsModel(ZuhurApplication.objectGraph.get(GiftCardsMapper.class));
    }

    @Provides
    GiftCardsMapper provideGiftCardsMapper(@ForApplication Context context) {
        return new GiftCardsMapper(context);
    }

}
