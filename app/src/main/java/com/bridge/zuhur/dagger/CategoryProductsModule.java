package com.bridge.zuhur.dagger;

/**
 * Created by hesham on 06/04/16.
 */

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.CategoryProductsMapper;
import com.bridge.zuhur.models.CategoryProductsModel;
import com.bridge.zuhur.services.CategoryProductsService;

import dagger.Module;
import dagger.Provides;

@Module(
        library = true
        , complete = false, injects = {CategoryProductsService.class, CategoryProductsModel.class,
        CategoryProductsMapper.class}
)


public class CategoryProductsModule {

    @Provides
    CategoryProductsService provideCategoryProductsService() {
        return new CategoryProductsService(ZuhurApplication.objectGraph.get(CategoryProductsModel.class));
    }

    @Provides
    CategoryProductsModel provideCategoryProductsModel() {
        return new CategoryProductsModel(ZuhurApplication.objectGraph.get(CategoryProductsMapper.class));
    }

    @Provides
    CategoryProductsMapper provideCategoriesMapper(@ForApplication Context context) {
        return new CategoryProductsMapper(context);
    }


}
