package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.SignInMapper;
import com.bridge.zuhur.models.SignInModel;
import com.bridge.zuhur.services.SignInService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 07/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {SignInService.class, SignInModel.class, SignInMapper.class}
)
public class SignInModule {

    @Provides
    SignInService provideSignInService() {
        return new SignInService(ZuhurApplication.objectGraph.get(SignInModel.class));
    }

    @Provides
    SignInModel provideSignInModel() {
        return new SignInModel(ZuhurApplication.objectGraph.get(SignInMapper.class));
    }

    @Provides
    SignInMapper provideSignInMapper(@ForApplication Context context) {
        return new SignInMapper(context);
    }
}
