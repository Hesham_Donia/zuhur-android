package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.ColorsMapper;
import com.bridge.zuhur.models.ColorsModel;
import com.bridge.zuhur.services.ColorsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 6/19/16.
 */

@Module(
        library = true
        , complete = false, injects = {ColorsService.class, ColorsModel.class,
        ColorsMapper.class}
)

public class ColorsModule {

    @Provides
    ColorsService provideColorsService() {
        return new ColorsService(ZuhurApplication.objectGraph.get(ColorsModel.class));
    }

    @Provides
    ColorsModel provideColorsModel() {
        return new ColorsModel(ZuhurApplication.objectGraph.get(ColorsMapper.class));
    }

    @Provides
    ColorsMapper provideColorsMapper(@ForApplication Context context) {
        return new ColorsMapper(context);
    }

}
