package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.database_mappers.RemaindersDatabaseMapper;
import com.bridge.zuhur.models.RemaindersDatabaseModel;
import com.bridge.zuhur.services.RemaindersDatabaseService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 6/9/16.
 */

@Module(
        library = true
        , complete = false, injects = {RemaindersDatabaseService.class, RemaindersDatabaseModel.class,
        RemaindersDatabaseMapper.class}

)

public class RemaindersDatabaseModule {

    @Provides
    RemaindersDatabaseService provideRemaindersDatabaseService() {
        return new RemaindersDatabaseService(ZuhurApplication.objectGraph.get(RemaindersDatabaseModel.class));
    }

    @Provides
    RemaindersDatabaseModel provideRemaindersDatabaseModel() {
        return new RemaindersDatabaseModel(ZuhurApplication.objectGraph.get(RemaindersDatabaseMapper.class));
    }

    @Provides
    RemaindersDatabaseMapper provideRemaindersDatabaseMapper(@ForApplication Context context) {
        return new RemaindersDatabaseMapper(context);
    }
}
