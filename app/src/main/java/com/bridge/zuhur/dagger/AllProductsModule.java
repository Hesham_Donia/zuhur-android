package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.AllProductsMapper;
import com.bridge.zuhur.models.AllProductsModel;
import com.bridge.zuhur.services.AllProductsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 06/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {AllProductsService.class, AllProductsModel.class,
        AllProductsMapper.class}

)
public class AllProductsModule {

    @Provides
    AllProductsService provideAllProductsService() {
        return new AllProductsService(ZuhurApplication.objectGraph.get(AllProductsModel.class));
    }

    @Provides
    AllProductsModel provideAllProductsModel() {
        return new AllProductsModel(ZuhurApplication.objectGraph.get(AllProductsMapper.class));
    }

    @Provides
    AllProductsMapper provideAllProductsMapper(@ForApplication Context context) {
        return new AllProductsMapper(context);
    }
}
