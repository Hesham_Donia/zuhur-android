package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.database_mappers.AutoCompleteDatabaseMapper;
import com.bridge.zuhur.models.AutoCompleteDatabaseModel;
import com.bridge.zuhur.services.AutoCompleteDatabaseService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 6/12/16.
 */
@Module(
        library = true
        , complete = false, injects = {AutoCompleteDatabaseService.class, AutoCompleteDatabaseModel.class,
        AutoCompleteDatabaseMapper.class}

)
public class AutoCompleteDatabaseModule {

    @Provides
    AutoCompleteDatabaseService provideAutoCompleteDatabaseService() {
        return new AutoCompleteDatabaseService(ZuhurApplication.objectGraph.get(AutoCompleteDatabaseModel.class));
    }

    @Provides
    AutoCompleteDatabaseModel provideAutoCompleteDatabaseModel() {
        return new AutoCompleteDatabaseModel(ZuhurApplication.objectGraph.get(AutoCompleteDatabaseMapper.class));
    }

    @Provides
    AutoCompleteDatabaseMapper provideAutoCompleteDatabaseMapper(@ForApplication Context context) {
        return new AutoCompleteDatabaseMapper(context);
    }
}
