package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.RemaindersMapper;
import com.bridge.zuhur.models.RemaindersModel;
import com.bridge.zuhur.services.RemaindersService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 13/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {RemaindersService.class, RemaindersModel.class,
        RemaindersMapper.class}

)

public class RemaindersModule {

    @Provides
    RemaindersService provideRemaindersService() {
        return new RemaindersService(ZuhurApplication.objectGraph.get(RemaindersModel.class));
    }

    @Provides
    RemaindersModel provideRemaindersModel() {
        return new RemaindersModel(ZuhurApplication.objectGraph.get(RemaindersMapper.class));
    }

    @Provides
    RemaindersMapper provideRemaindersMapper(@ForApplication Context context) {
        return new RemaindersMapper(context);
    }

}
