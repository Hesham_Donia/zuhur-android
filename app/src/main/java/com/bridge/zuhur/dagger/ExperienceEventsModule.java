package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.ExperienceEventsMapper;
import com.bridge.zuhur.models.ExperienceEventsModel;
import com.bridge.zuhur.services.ExperienceEventsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 11/04/16.
 */


@Module(
        library = true
        , complete = false, injects = {ExperienceEventsService.class, ExperienceEventsModel.class,
        ExperienceEventsMapper.class}

)

public class ExperienceEventsModule {

    @Provides
    ExperienceEventsService provideCategoiesService() {
        return new ExperienceEventsService(ZuhurApplication.objectGraph.get(ExperienceEventsModel.class));
    }

    @Provides
    ExperienceEventsModel provideExperienceEventsModel() {
        return new ExperienceEventsModel(ZuhurApplication.objectGraph.get(ExperienceEventsMapper.class));
    }

    @Provides
    ExperienceEventsMapper provideExperienceEventsMapper(@ForApplication Context context) {
        return new ExperienceEventsMapper(context);
    }
    
}
