package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.SearchByNameMapper;
import com.bridge.zuhur.models.SearchByNameModel;
import com.bridge.zuhur.services.SearchByNameService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 11/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {SearchByNameService.class, SearchByNameModel.class,
                                       SearchByNameMapper.class}
)

public class SearchByNameModule {

    @Provides
    SearchByNameService provideSearchByNameService() {
        return new SearchByNameService(ZuhurApplication.objectGraph.get(SearchByNameModel.class));
    }

    @Provides
    SearchByNameModel provideSearchByNameModel() {
        return new SearchByNameModel(ZuhurApplication.objectGraph.get(SearchByNameMapper.class));
    }

    @Provides
    SearchByNameMapper provideSearchByNameMapper(@ForApplication Context context) {
        return new SearchByNameMapper(context);
    }
    
}
