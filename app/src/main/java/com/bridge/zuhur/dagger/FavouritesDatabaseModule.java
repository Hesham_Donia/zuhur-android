package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.database_mappers.FavouritesDatabaseMapper;
import com.bridge.zuhur.models.FavouritesDatabaseModel;
import com.bridge.zuhur.services.FavouritesDatabaseService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 10/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {FavouritesDatabaseService.class, FavouritesDatabaseModel.class,
        FavouritesDatabaseMapper.class}

)

public class FavouritesDatabaseModule {

    @Provides
    FavouritesDatabaseService provideFavouritesDatabaseService() {
        return new FavouritesDatabaseService(ZuhurApplication.objectGraph.get(FavouritesDatabaseModel.class));
    }

    @Provides
    FavouritesDatabaseModel provideFavouritesDatabaseModel() {
        return new FavouritesDatabaseModel(ZuhurApplication.objectGraph.get(FavouritesDatabaseMapper.class));
    }

    @Provides
    FavouritesDatabaseMapper provideFavouritesDatabaseMapper(@ForApplication Context context) {
        return new FavouritesDatabaseMapper(context);
    }
}
