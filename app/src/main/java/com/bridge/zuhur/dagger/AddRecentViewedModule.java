package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.AddRecentViewedMapper;
import com.bridge.zuhur.models.AddRecentViewedModel;
import com.bridge.zuhur.services.AddRecentViewedService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 11/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {AddRecentViewedService.class, AddRecentViewedModel.class,
        AddRecentViewedMapper.class}
)
public class AddRecentViewedModule {

    @Provides
    AddRecentViewedService provideAddRecentViewedService() {
        return new AddRecentViewedService(ZuhurApplication.objectGraph.get(AddRecentViewedModel.class));
    }

    @Provides
    AddRecentViewedModel provideAddRecentViewedModel() {
        return new AddRecentViewedModel(ZuhurApplication.objectGraph.get(AddRecentViewedMapper.class));
    }

    @Provides
    AddRecentViewedMapper provideAddRecentViewedMapper(@ForApplication Context context) {
        return new AddRecentViewedMapper(context);
    }
}
