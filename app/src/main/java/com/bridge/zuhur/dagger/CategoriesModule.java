package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.CategoriesMapper;
import com.bridge.zuhur.models.CategoriesModel;
import com.bridge.zuhur.services.CategoriesService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 05/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {CategoriesService.class, CategoriesModel.class,
        CategoriesMapper.class}

)
public class CategoriesModule {
    @Provides
    CategoriesService provideCategoiesService() {
        return new CategoriesService(ZuhurApplication.objectGraph.get(CategoriesModel.class));
    }

    @Provides
    CategoriesModel provideCategoriesModel() {
        return new CategoriesModel(ZuhurApplication.objectGraph.get(CategoriesMapper.class));
    }

    @Provides
    CategoriesMapper provideCategoriesMapper(@ForApplication Context context) {
        return new CategoriesMapper(context);
    }

}
