package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.RecentViewedMapper;
import com.bridge.zuhur.models.RecentViewedModel;
import com.bridge.zuhur.services.RecentViewedService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 07/04/16.
 */
@Module(
        library = true
        , complete = false, injects = {RecentViewedService.class, RecentViewedModel.class,
        RecentViewedMapper.class}
)
public class RecentViewedModule {

    @Provides
    RecentViewedService provideRecentViewedService() {
        return new RecentViewedService(ZuhurApplication.objectGraph.get(RecentViewedModel.class));
    }

    @Provides
    RecentViewedModel provideRecentViewedModel() {
        return new RecentViewedModel(ZuhurApplication.objectGraph.get(RecentViewedMapper.class));
    }

    @Provides
    RecentViewedMapper provideRecentViewedMapper(@ForApplication Context context) {
        return new RecentViewedMapper(context);
    }
}
