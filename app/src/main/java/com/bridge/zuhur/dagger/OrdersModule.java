package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.OrdersMapper;
import com.bridge.zuhur.models.OrdersModel;
import com.bridge.zuhur.services.OrdersService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 12/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {OrdersService.class, OrdersModel.class,
        OrdersMapper.class}
)

public class OrdersModule {

    @Provides
    OrdersService provideOrdersService() {
        return new OrdersService(ZuhurApplication.objectGraph.get(OrdersModel.class));
    }

    @Provides
    OrdersModel provideOrdersModel() {
        return new OrdersModel(ZuhurApplication.objectGraph.get(OrdersMapper.class));
    }

    @Provides
    OrdersMapper provideOrdersMapper(@ForApplication Context context) {
        return new OrdersMapper(context);
    }
    
}
