package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.CountriesMapper;
import com.bridge.zuhur.models.CountriesModel;
import com.bridge.zuhur.services.CountriesService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 5/12/16.
 */

@Module(
        library = true
        , complete = false, injects = {CountriesService.class, CountriesModel.class,
        CountriesMapper.class}
)

public class CountriesModule {

    @Provides
    CountriesService provideCountriesService() {
        return new CountriesService(ZuhurApplication.objectGraph.get(CountriesModel.class));
    }

    @Provides
    CountriesModel provideCountriesModel() {
        return new CountriesModel(ZuhurApplication.objectGraph.get(CountriesMapper.class));
    }

    @Provides
    CountriesMapper provideCountriesMapper(@ForApplication Context context) {
        return new CountriesMapper(context);
    }

}
