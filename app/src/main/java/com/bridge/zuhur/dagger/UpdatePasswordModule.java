package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.UpdatePasswordMapper;
import com.bridge.zuhur.models.UpdatePasswordModel;
import com.bridge.zuhur.services.UpdatePasswordService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 12/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {UpdatePasswordService.class, UpdatePasswordModel.class, UpdatePasswordMapper.class}
)

public class UpdatePasswordModule {

    @Provides
    UpdatePasswordService provideUpdatePasswordService() {
        return new UpdatePasswordService(ZuhurApplication.objectGraph.get(UpdatePasswordModel.class));
    }

    @Provides
    UpdatePasswordModel provideUpdatePasswordModel() {
        return new UpdatePasswordModel(ZuhurApplication.objectGraph.get(UpdatePasswordMapper.class));
    }

    @Provides
    UpdatePasswordMapper provideUpdatePasswordMapper(@ForApplication Context context) {
        return new UpdatePasswordMapper(context);
    }
    
}
