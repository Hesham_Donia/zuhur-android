package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.SignInFacebookMapper;
import com.bridge.zuhur.models.SignInFacebookModel;
import com.bridge.zuhur.services.SignInFacebookService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 6/15/16.
 */

@Module(
        library = true
        , complete = false, injects = {SignInFacebookService.class, SignInFacebookModel.class, SignInFacebookMapper.class}
)

public class SignInFacebookModule {
    @Provides
    SignInFacebookService provideSignInFacebookService() {
        return new SignInFacebookService(ZuhurApplication.objectGraph.get(SignInFacebookModel.class));
    }

    @Provides
    SignInFacebookModel provideSignInFacebookModel() {
        return new SignInFacebookModel(ZuhurApplication.objectGraph.get(SignInFacebookMapper.class));
    }

    @Provides
    SignInFacebookMapper provideSignInFacebookMapper(@ForApplication Context context) {
        return new SignInFacebookMapper(context);
    }
}
