package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.FavouritesMapper;
import com.bridge.zuhur.models.FavouritesModel;
import com.bridge.zuhur.services.FavouritesService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 10/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {FavouritesService.class, FavouritesModel.class,
        FavouritesMapper.class}

)

public class FavouritesModule {

    @Provides
    FavouritesService provideFavouritesService() {
        return new FavouritesService(ZuhurApplication.objectGraph.get(FavouritesModel.class));
    }

    @Provides
    FavouritesModel provideFavouritesModel() {
        return new FavouritesModel(ZuhurApplication.objectGraph.get(FavouritesMapper.class));
    }

    @Provides
    FavouritesMapper provideFavouritesMapper(@ForApplication Context context) {
        return new FavouritesMapper(context);
    }
}
