package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.NotificationsMapper;
import com.bridge.zuhur.models.NotificationsModel;
import com.bridge.zuhur.services.NotificationsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 17/04/16.
 */
@Module(
        library = true
        , complete = false, injects = {NotificationsService.class, NotificationsModel.class,
        NotificationsMapper.class}
)

public class NotificationsModule {

    @Provides
    NotificationsService provideNotificationsService() {
        return new NotificationsService(ZuhurApplication.objectGraph.get(NotificationsModel.class));
    }

    @Provides
    NotificationsModel provideNotificationsModel() {
        return new NotificationsModel(ZuhurApplication.objectGraph.get(NotificationsMapper.class));
    }

    @Provides
    NotificationsMapper provideNotificationsMapper(@ForApplication Context context) {
        return new NotificationsMapper(context);
    }
}
