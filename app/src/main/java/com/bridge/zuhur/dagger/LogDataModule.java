package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.database_mappers.LogDataDatabaseMapper;
import com.bridge.zuhur.models.LogDataDatabaseModel;
import com.bridge.zuhur.services.LogDataDatabaseService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by heshamkhaled on 6/13/16.
 */

@Module(
        library = true
        , complete = false, injects = {LogDataDatabaseService.class, LogDataDatabaseModel.class,
        LogDataDatabaseMapper.class}

)

public class LogDataModule {

    @Provides
    LogDataDatabaseService provideLogDataDatabaseService() {
        return new LogDataDatabaseService(ZuhurApplication.objectGraph.get(LogDataDatabaseModel.class));
    }

    @Provides
    LogDataDatabaseModel provideLogDataDatabaseModel() {
        return new LogDataDatabaseModel(ZuhurApplication.objectGraph.get(LogDataDatabaseMapper.class));
    }

    @Provides
    LogDataDatabaseMapper provideLogDataDatabaseMapper(@ForApplication Context context) {
        return new LogDataDatabaseMapper(context);
    }

}
