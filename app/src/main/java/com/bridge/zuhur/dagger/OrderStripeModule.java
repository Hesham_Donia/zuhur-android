package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.OrderStripeMapper;
import com.bridge.zuhur.models.OrderStripeModel;
import com.bridge.zuhur.services.OrderStripeService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 6/21/16.
 */

@Module(
        library = true
        , complete = false, injects = {OrderStripeService.class, OrderStripeModel.class,
        OrderStripeMapper.class}
)

public class OrderStripeModule {
    @Provides
    OrderStripeService provideOrderStripeService() {
        return new OrderStripeService(ZuhurApplication.objectGraph.get(OrderStripeModel.class));
    }

    @Provides
    OrderStripeModel provideOrderStripeModel() {
        return new OrderStripeModel(ZuhurApplication.objectGraph.get(OrderStripeMapper.class));
    }

    @Provides
    OrderStripeMapper provideOrderStripeMapper(@ForApplication Context context) {
        return new OrderStripeMapper(context);
    }
}
