package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.ForgetMyPasswordMapper;
import com.bridge.zuhur.models.ForgetMyPasswordModel;
import com.bridge.zuhur.services.ForgetMyPasswordService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 07/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {ForgetMyPasswordService.class, ForgetMyPasswordModel.class, ForgetMyPasswordMapper.class}
)

public class ForgetMyPasswordModule {

    @Provides
    ForgetMyPasswordService provideForgetMyPasswordService() {
        return new ForgetMyPasswordService(ZuhurApplication.objectGraph.get(ForgetMyPasswordModel.class));
    }

    @Provides
    ForgetMyPasswordModel provideForgetMyPasswordModel() {
        return new ForgetMyPasswordModel(ZuhurApplication.objectGraph.get(ForgetMyPasswordMapper.class));
    }

    @Provides
    ForgetMyPasswordMapper provideForgetMyPasswordMapper(@ForApplication Context context) {
        return new ForgetMyPasswordMapper(context);
    }
}
