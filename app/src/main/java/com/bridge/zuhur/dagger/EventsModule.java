package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.EventsMapper;
import com.bridge.zuhur.models.EventsModel;
import com.bridge.zuhur.services.EventsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 14/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {EventsService.class, EventsModel.class,
        EventsMapper.class}
)

public class EventsModule {

    @Provides
    EventsService provideEventsService() {
        return new EventsService(ZuhurApplication.objectGraph.get(EventsModel.class));
    }

    @Provides
    EventsModel provideEventsModel() {
        return new EventsModel(ZuhurApplication.objectGraph.get(EventsMapper.class));
    }

    @Provides
    EventsMapper provideEventsMapper(@ForApplication Context context) {
        return new EventsMapper(context);
    }
}
