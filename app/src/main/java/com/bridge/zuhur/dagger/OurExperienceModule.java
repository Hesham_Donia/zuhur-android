package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.OurExperienceMapper;
import com.bridge.zuhur.models.OurExperienceModel;
import com.bridge.zuhur.services.OurExperienceService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 07/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {OurExperienceService.class, OurExperienceModel.class,
        OurExperienceMapper.class}
)

public class OurExperienceModule {

    @Provides
    OurExperienceService provideOurExperienceService() {
        return new OurExperienceService(ZuhurApplication.objectGraph.get(OurExperienceModel.class));
    }

    @Provides
    OurExperienceModel provideOurExperienceModel() {
        return new OurExperienceModel(ZuhurApplication.objectGraph.get(OurExperienceMapper.class));
    }

    @Provides
    OurExperienceMapper provideOurExperienceMapper(@ForApplication Context context) {
        return new OurExperienceMapper(context);
    }
    
}
