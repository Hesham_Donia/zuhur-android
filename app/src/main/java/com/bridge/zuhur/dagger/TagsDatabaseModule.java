package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.database_mappers.TagsDatabaseMapper;
import com.bridge.zuhur.models.TagsDatabaseModel;
import com.bridge.zuhur.services.TagsDatabaseService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 5/9/16.
 */

@Module(
        library = true
        , complete = false, injects = {TagsDatabaseService.class, TagsDatabaseModel.class,
        TagsDatabaseMapper.class}

)

public class TagsDatabaseModule {

    @Provides
    TagsDatabaseService provideTagsDatabaseService() {
        return new TagsDatabaseService(ZuhurApplication.objectGraph.get(TagsDatabaseModel.class));
    }

    @Provides
    TagsDatabaseModel provideTagsDatabaseModel() {
        return new TagsDatabaseModel(ZuhurApplication.objectGraph.get(TagsDatabaseMapper.class));
    }

    @Provides
    TagsDatabaseMapper provideTagsDatabaseMapper(@ForApplication Context context) {
        return new TagsDatabaseMapper(context);
    }
}
