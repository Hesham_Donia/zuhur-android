package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.OffersMapper;
import com.bridge.zuhur.models.OffersModel;
import com.bridge.zuhur.services.OffersService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 07/04/16.
 */
@Module(
        library = true
        , complete = false, injects = {OffersService.class, OffersModel.class,
        OffersMapper.class}
)
public class OffersModule {

        @Provides
        OffersService provideOffersService() {
            return new OffersService(ZuhurApplication.objectGraph.get(OffersModel.class));
        }

        @Provides
        OffersModel provideOffersModel() {
            return new OffersModel(ZuhurApplication.objectGraph.get(OffersMapper.class));
        }

        @Provides
        OffersMapper provideOffersMapper(@ForApplication Context context) {
            return new OffersMapper(context);
        }
}
