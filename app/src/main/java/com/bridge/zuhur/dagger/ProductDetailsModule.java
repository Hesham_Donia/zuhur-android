package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.ProductDetailsMapper;
import com.bridge.zuhur.models.ProductDetailsModel;
import com.bridge.zuhur.services.ProductDetailsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 06/04/16.
 */


@Module(
        library = true
        , complete = false, injects = {ProductDetailsService.class, ProductDetailsModel.class,
        ProductDetailsMapper.class}

)

public class ProductDetailsModule {

    @Provides
    ProductDetailsService provideProductDetailsService() {
        return new ProductDetailsService(ZuhurApplication.objectGraph.get(ProductDetailsModel.class));
    }

    @Provides
    ProductDetailsModel provideProductDetailsModel() {
        return new ProductDetailsModel(ZuhurApplication.objectGraph.get(ProductDetailsMapper.class));
    }

    @Provides
    ProductDetailsMapper provideProductDetailsMapper(@ForApplication Context context) {
        return new ProductDetailsMapper(context);
    }
}
