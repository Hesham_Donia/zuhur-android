package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.PackagingWayMapper;
import com.bridge.zuhur.models.PackagingWayModel;
import com.bridge.zuhur.services.PackagingWayService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 5/9/16.
 */

@Module(
        library = true
        , complete = false, injects = {PackagingWayService.class, PackagingWayModel.class,
        PackagingWayMapper.class}
)

public class PackagingWayModule {

    @Provides
    PackagingWayService providePackagingWayService() {
        return new PackagingWayService(ZuhurApplication.objectGraph.get(PackagingWayModel.class));
    }

    @Provides
    PackagingWayModel providePackagingWayModel() {
        return new PackagingWayModel(ZuhurApplication.objectGraph.get(PackagingWayMapper.class));
    }

    @Provides
    PackagingWayMapper providePackagingWayMapper(@ForApplication Context context) {
        return new PackagingWayMapper(context);
    }

}
