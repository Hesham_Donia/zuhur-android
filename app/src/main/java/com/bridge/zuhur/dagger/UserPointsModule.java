package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.UserPointsMapper;
import com.bridge.zuhur.models.UserPointsModel;
import com.bridge.zuhur.services.UserPointsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 6/21/16.
 */

@Module(
        library = true
        , complete = false, injects = {UserPointsService.class, UserPointsModel.class,
        UserPointsMapper.class}

)

public class UserPointsModule {
    @Provides
    UserPointsService provideUserPointsService() {
        return new UserPointsService(ZuhurApplication.objectGraph.get(UserPointsModel.class));
    }

    @Provides
    UserPointsModel provideUserPointsModel() {
        return new UserPointsModel(ZuhurApplication.objectGraph.get(UserPointsMapper.class));
    }

    @Provides
    UserPointsMapper provideUserPointsMapper(@ForApplication Context context) {
        return new UserPointsMapper(context);
    }
}
