package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.database_mappers.OrdersDatabaseMapper;
import com.bridge.zuhur.models.OrdersDatabaseModel;
import com.bridge.zuhur.services.OrdersDatabaseService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 13/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {OrdersDatabaseService.class, OrdersDatabaseModel.class,
        OrdersDatabaseMapper.class}

)

public class OrdersDatabaseModule {

    @Provides
    OrdersDatabaseService provideOrdersDatabaseService() {
        return new OrdersDatabaseService(ZuhurApplication.objectGraph.get(OrdersDatabaseModel.class));
    }

    @Provides
    OrdersDatabaseModel provideOrdersDatabaseModel() {
        return new OrdersDatabaseModel(ZuhurApplication.objectGraph.get(OrdersDatabaseMapper.class));
    }

    @Provides
    OrdersDatabaseMapper provideOrdersDatabaseMapper(@ForApplication Context context) {
        return new OrdersDatabaseMapper(context);
    }
}
