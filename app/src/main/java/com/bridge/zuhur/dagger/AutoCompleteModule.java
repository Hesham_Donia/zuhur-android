package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.AutoCompleteMapper;
import com.bridge.zuhur.models.AutoCompleteModel;
import com.bridge.zuhur.services.AutoCompleteService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 6/12/16.
 */


@Module(
        library = true
        , complete = false, injects = {AutoCompleteService.class, AutoCompleteModel.class,
        AutoCompleteMapper.class}

)

public class AutoCompleteModule {

    @Provides
    AutoCompleteService provideAutoCompleteService() {
        return new AutoCompleteService(ZuhurApplication.objectGraph.get(AutoCompleteModel.class));
    }

    @Provides
    AutoCompleteModel provideAutoCompleteModel() {
        return new AutoCompleteModel(ZuhurApplication.objectGraph.get(AutoCompleteMapper.class));
    }

    @Provides
    AutoCompleteMapper provideAutoCompleteMapper(@ForApplication Context context) {
        return new AutoCompleteMapper(context);
    }
}
