package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.PromotionValidationMapper;
import com.bridge.zuhur.models.PromotionValidationModel;
import com.bridge.zuhur.services.PromotionValidationService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 5/9/16.
 */

@Module(
        library = true
        , complete = false, injects = {PromotionValidationService.class, PromotionValidationModel.class,
        PromotionValidationMapper.class}

)

public class PromotionValidationModule {

    @Provides
    PromotionValidationService providePromotionValidationService() {
        return new PromotionValidationService(ZuhurApplication.objectGraph.get(PromotionValidationModel.class));
    }

    @Provides
    PromotionValidationModel providePromotionValidationModel() {
        return new PromotionValidationModel(ZuhurApplication.objectGraph.get(PromotionValidationMapper.class));
    }

    @Provides
    PromotionValidationMapper providePromotionValidationMapper(@ForApplication Context context) {
        return new PromotionValidationMapper(context);
    }
}
