package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.TagsMapper;
import com.bridge.zuhur.models.TagsModel;
import com.bridge.zuhur.services.TagsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 5/9/16.
 */

@Module(
        library = true
        , complete = false, injects = {TagsService.class, TagsModel.class,
        TagsMapper.class}

)

public class TagsModule {

    @Provides
    TagsService provideTagsService() {
        return new TagsService(ZuhurApplication.objectGraph.get(TagsModel.class));
    }

    @Provides
    TagsModel provideTagsModel() {
        return new TagsModel(ZuhurApplication.objectGraph.get(TagsMapper.class));
    }

    @Provides
    TagsMapper provideTagsMapper(@ForApplication Context context) {
        return new TagsMapper(context);
    }

}
