package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.SignUpFacebookMapper;
import com.bridge.zuhur.models.SignUpFacebookModel;
import com.bridge.zuhur.services.SignUpFacebookService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 6/15/16.
 */

@Module(
        library = true
        , complete = false, injects = {SignUpFacebookService.class, SignUpFacebookModel.class, SignUpFacebookMapper.class}
)

public class SignUpFacebookModule {

    @Provides
    SignUpFacebookService provideSignUpFacebookService() {
        return new SignUpFacebookService(ZuhurApplication.objectGraph.get(SignUpFacebookModel.class));
    }

    @Provides
    SignUpFacebookModel provideSignUpFacebookModel() {
        return new SignUpFacebookModel(ZuhurApplication.objectGraph.get(SignUpFacebookMapper.class));
    }

    @Provides
    SignUpFacebookMapper provideSignUpFacebookMapper(@ForApplication Context context) {
        return new SignUpFacebookMapper(context);
    }
}
