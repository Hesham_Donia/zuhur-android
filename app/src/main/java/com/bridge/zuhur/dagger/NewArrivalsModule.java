package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.NewArrivalsMapper;
import com.bridge.zuhur.models.NewArrivalsModel;
import com.bridge.zuhur.services.NewArrivalsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 07/04/16.
 */
@Module(
        library = true
        , complete = false, injects = {NewArrivalsService.class, NewArrivalsModel.class,
        NewArrivalsMapper.class}

)

public class NewArrivalsModule {
    @Provides
    NewArrivalsService provideNewArrivalsService() {
        return new NewArrivalsService(ZuhurApplication.objectGraph.get(NewArrivalsModel.class));
    }

    @Provides
    NewArrivalsModel provideNewArrivalsModel() {
        return new NewArrivalsModel(ZuhurApplication.objectGraph.get(NewArrivalsMapper.class));
    }

    @Provides
    NewArrivalsMapper provideNewArrivalsMapper(@ForApplication Context context) {
        return new NewArrivalsMapper(context);
    }
}
