package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.RelatedProductsMapper;
import com.bridge.zuhur.models.RelatedProductsModel;
import com.bridge.zuhur.services.RelatedProductsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 5/8/16.
 */
@Module(
        library = true
        , complete = false, injects = {RelatedProductsService.class, RelatedProductsModel.class,
        RelatedProductsMapper.class}

)

public class RelatedProductsModule {

    @Provides
    RelatedProductsService provideRelatedProductsService() {
        return new RelatedProductsService(ZuhurApplication.objectGraph.get(RelatedProductsModel.class));
    }

    @Provides
    RelatedProductsModel provideRelatedProductsModel() {
        return new RelatedProductsModel(ZuhurApplication.objectGraph.get(RelatedProductsMapper.class));
    }

    @Provides
    RelatedProductsMapper provideRelatedProductsMapper(@ForApplication Context context) {
        return new RelatedProductsMapper(context);
    }

}
