package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.SignUpMapper;
import com.bridge.zuhur.models.SignUpModel;
import com.bridge.zuhur.services.SignUpService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 06/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {SignUpService.class, SignUpModel.class, SignUpMapper.class}
)

public class SignUpModule {

    @Provides
    SignUpService provideSignUpService() {
        return new SignUpService(ZuhurApplication.objectGraph.get(SignUpModel.class));
    }

    @Provides
    SignUpModel provideSignUpModel() {
        return new SignUpModel(ZuhurApplication.objectGraph.get(SignUpMapper.class));
    }

    @Provides
    SignUpMapper provideSignUpMapper(@ForApplication Context context) {
        return new SignUpMapper(context);
    }
}
