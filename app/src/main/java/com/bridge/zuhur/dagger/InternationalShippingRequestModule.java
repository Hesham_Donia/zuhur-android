package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.InternationalShippingRequestMapper;
import com.bridge.zuhur.models.InternationalShippingRequestModel;
import com.bridge.zuhur.services.InternationalShippingRequestService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 17/04/16.
 */

@Module(
        library = true
        , complete = false, injects = {InternationalShippingRequestService.class, InternationalShippingRequestModel.class,
        InternationalShippingRequestMapper.class}

)

public class InternationalShippingRequestModule {

    @Provides
    InternationalShippingRequestService provideInternationalShippingRequestService() {
        return new InternationalShippingRequestService(ZuhurApplication.objectGraph.get(InternationalShippingRequestModel.class));
    }

    @Provides
    InternationalShippingRequestModel provideInternationalShippingRequestModel() {
        return new InternationalShippingRequestModel(ZuhurApplication.objectGraph.get(InternationalShippingRequestMapper.class));
    }

    @Provides
    InternationalShippingRequestMapper provideInternationalShippingRequestMapper(@ForApplication Context context) {
        return new InternationalShippingRequestMapper(context);
    }
    
}
