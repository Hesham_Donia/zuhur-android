package com.bridge.zuhur.dagger;

import android.content.Context;

import com.bridge.zuhur.ZuhurApplication;
import com.bridge.zuhur.mappers.web_service_mappers.OurExperienceProductsMapper;
import com.bridge.zuhur.models.OurExperienceProductsModel;
import com.bridge.zuhur.services.OurExperienceProductsService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hesham on 07/04/16.
 */
@Module(
        library = true
        , complete = false, injects = {OurExperienceProductsService.class, OurExperienceProductsModel.class,
        OurExperienceProductsMapper.class}
)
public class OurExperienceProductsModule {

    @Provides
    OurExperienceProductsService provideOurExperienceProductsService() {
        return new OurExperienceProductsService(ZuhurApplication.objectGraph.get(OurExperienceProductsModel.class));
    }

    @Provides
    OurExperienceProductsModel provideOurExperienceProductsModel() {
        return new OurExperienceProductsModel(ZuhurApplication.objectGraph.get(OurExperienceProductsMapper.class));
    }

    @Provides
    OurExperienceProductsMapper provideOurExperienceProductsMapper(@ForApplication Context context) {
        return new OurExperienceProductsMapper(context);
    }
}
