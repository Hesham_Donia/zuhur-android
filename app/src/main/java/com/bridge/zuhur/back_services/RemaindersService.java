package com.bridge.zuhur.back_services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.database.Remainder;
import com.bridge.zuhur.utils.CommonConstants;
import com.bridge.zuhur.utils.Language;
import com.bridge.zuhur.views.activities.HomeActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class RemaindersService extends Service {


    static Context context;
    private static Timer timer = new Timer();
    private final Handler notificationsHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            ExtrasController extrasController = new ExtrasController(RemaindersService.this);
            Bundle bundle = new Bundle();
            bundle.putInt("fromService", 1);
            bundle.putInt("action", ExtrasController.GET_ALL_REMAINDERS_DATABASE_ACTION);
            extrasController.request(ExtrasController.GET_ALL_REMAINDERS_DATABASE_ACTION, bundle);
        }
    };

    public RemaindersService() {

    }

    public static void showRemainders(ArrayList<Remainder> remainders) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);

        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String title, message;

        String currentDate = "" + year + "-" + (month + 1) + "-" + day;
        for (Remainder remainder : remainders) {
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "currentDate: " + currentDate);
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "remainderDate: " + remainder.getRemainder().remainderDate);
            if (remainder.getRemainder().remainderDate.equals(currentDate)) {
                if (Language.getCurrentLang(context).equals("en")) {
                    message = remainder.getRemainder().details;
                    title = remainder.getRemainder().remainderDate;
                } else {
                    message = remainder.getRemainder().details;
                    title = remainder.getRemainder().remainderDate;
                }
                Intent intent = new Intent(context, HomeActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, 0);
                createNotification(title, message, pendingIntent);
            }
        }
    }

    private static void createNotification(String title, String message, PendingIntent pendingIntent) {
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            Notification mNotification = new Notification.Builder(context)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setSound(soundUri)
                    .addAction(R.drawable.messi, "View", pendingIntent)
                    .build();
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(0, mNotification);
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        startService();
    }

    private void startService() {
        timer.scheduleAtFixedRate(new mainTask(), 0, 1000 * 60 * 60 * 24);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(this, RemaindersService.class);
        startService(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class mainTask extends TimerTask {
        public void run() {
            notificationsHandler.sendEmptyMessage(0);
        }
    }
}
