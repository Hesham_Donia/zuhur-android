package com.bridge.zuhur.back_services;


import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.bridge.zuhur.controllers.NotificationsController;
import com.bridge.zuhur.entities.Singleton;

import java.util.Timer;
import java.util.TimerTask;

public class NotificationsService extends Service {

    public static boolean isFromMyApp = false;
    private static Timer timer = new Timer();
    private final Handler notificationsHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (Singleton.getInstance().country != null) {
                NotificationsController notificationsController = new NotificationsController(NotificationsService.this);
                Bundle bundle = new Bundle();
                bundle.putInt("action", NotificationsController.NOTIFICATIONS_ACTION);
                notificationsController.request(NotificationsController.NOTIFICATIONS_ACTION, bundle);
            }
        }
    };

    public NotificationsService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startService();
    }

    private void startService() {
        timer.scheduleAtFixedRate(new mainTask(), 0, 1000 * 30);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!isFromMyApp) {
            Intent intent = new Intent(this, NotificationsService.class);
            startService(intent);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class mainTask extends TimerTask {
        public void run() {
            notificationsHandler.sendEmptyMessage(0);
        }
    }
}
