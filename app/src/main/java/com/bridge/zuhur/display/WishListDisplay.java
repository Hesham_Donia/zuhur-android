package com.bridge.zuhur.display;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.WishListController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.views.activities.WishListActivity;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 6/1/16.
 */
public class WishListDisplay extends AbstractDisplay {
    @Override
    public void display(int action, ArrayList<? extends Entity> result, Context context, Bundle request) {
        switch (action) {
            case WishListController.GET_ALL_WISH_LIST_DATABASE_ACTION:
                if (context instanceof WishListActivity) {
                    if (result.size() == 0) {
                        Toast.makeText(context, context.getResources().getString(R.string.noWishError), Toast.LENGTH_LONG).show();
                    }
                    ((WishListActivity) context).onDataReceived(result, action);
                }
                break;

            default:
                break;
        }
    }
}
