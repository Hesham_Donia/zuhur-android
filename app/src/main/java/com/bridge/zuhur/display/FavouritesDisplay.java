package com.bridge.zuhur.display;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.FavouritesController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.views.activities.FavouritesActivity;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/31/16.
 */
public class FavouritesDisplay extends AbstractDisplay {
    @Override
    public void display(int action, ArrayList<? extends Entity> result, Context context, Bundle request) {
        switch (action) {

            case FavouritesController.GET_ALL_FAVOURITES_DATABASE_ACTION:
                if (result.size() == 0) {
                    Toast.makeText(context, context.getResources().getString(R.string.noFavsError), Toast.LENGTH_LONG).show();
                }
                ((FavouritesActivity) context).onDataReceived(result, action);
                break;

            default:
                break;
        }
    }
}
