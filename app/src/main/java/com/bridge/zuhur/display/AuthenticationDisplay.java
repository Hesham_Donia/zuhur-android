package com.bridge.zuhur.display;

import android.content.Context;
import android.os.Bundle;

import com.bridge.zuhur.controllers.AuthenticationController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.views.activities.ForgetMyPasswordActivity;
import com.bridge.zuhur.views.activities.LoginActivity;
import com.bridge.zuhur.views.activities.SignUpActivity;
import com.bridge.zuhur.views.activities.UpdatePasswordActivity;
import com.bridge.zuhur.views.activities.UpdateProfileActivity;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/30/16.
 */
public class AuthenticationDisplay extends AbstractDisplay {
    @Override
    public void display(int action, ArrayList<? extends Entity> result, Context context, Bundle request) {
        switch (action) {

            case AuthenticationController.SIGN_IN_ACTION:
                ((LoginActivity) context).onDataReceived(result, action);
                break;

            case AuthenticationController.SIGN_IN_FACEBOOK_ACTION:
                ((LoginActivity) context).onDataReceived(result, action);
                break;

            case AuthenticationController.SIGN_UP_ACTION:
                ((SignUpActivity) context).onDataReceived(result, action);
                break;

            case AuthenticationController.FORGET_MY_PASSWORD_ACTION:
                ((ForgetMyPasswordActivity) context).onDataReceived(result, action);
                break;

            case AuthenticationController.UPDATE_PROFILE_ACTION:
                ((UpdateProfileActivity) context).onDataReceived(result, action);
                break;

            case AuthenticationController.UPDATE_PASSWORD_ACTION:
                ((UpdatePasswordActivity) context).onDataReceived(result, action);
                break;

            default:
                break;
        }
    }
}
