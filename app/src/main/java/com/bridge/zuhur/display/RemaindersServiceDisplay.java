package com.bridge.zuhur.display;

import android.content.Context;
import android.os.Bundle;

import com.bridge.zuhur.back_services.RemaindersService;
import com.bridge.zuhur.database.Remainder;
import com.bridge.zuhur.entities.Entity;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 6/14/16.
 */
public class RemaindersServiceDisplay extends AbstractDisplay {
    @Override
    public void display(int action, ArrayList<? extends Entity> result, Context context, Bundle request) {
        RemaindersService.showRemainders((ArrayList<Remainder>) result);
    }
}
