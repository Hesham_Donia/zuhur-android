package com.bridge.zuhur.display;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.controllers.OrderController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.views.activities.OrderConfirmationActivity;
import com.bridge.zuhur.views.activities.OrdersHistoryActivity;
import com.bridge.zuhur.views.activities.ResetActivity;
import com.bridge.zuhur.views.activities.UnCompletedOrdersActivity;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/29/16.
 */
public class OrderDisplay extends AbstractDisplay {
    @Override
    public void display(int action, ArrayList<? extends Entity> result, Context context, Bundle request) {
        switch (action) {
            case OrderController.PROMOTION_CODE_VALIDATION_ACTION:
                ((OrderConfirmationActivity) context).onDataReceived(result, action);
                break;

            case ExtrasController.PAYMENT_METHODS_ACTION:
                ((OrderConfirmationActivity) context).onDataReceived(result, action);
                break;

            case OrderController.GET_ALL_ORDER_DATABASE_ACTION:
                if (result.size() == 0) {
                    Toast.makeText(context, context.getResources().getString(R.string.noOrdersError), Toast.LENGTH_LONG).show();
                }
                ((OrdersHistoryActivity) context).onDataReceived(result, action);
                break;

            case OrderController.GET_UN_COMPLETED_ORDERS_ACTION:
                ((UnCompletedOrdersActivity) context).onDataReceived(result, action);
                break;

            case OrderController.PACKAGING_WAYS_ACTION:
                ((ResetActivity) context).onDataReceived(result, action, request);
                break;

            case OrderController.GIFT_CARDS_ACTION:
                ((ResetActivity) context).onDataReceived(result, action, request);
                break;

            case OrderController.TRACK_ORDER__ACTION:
                ((UnCompletedOrdersActivity) context).onDataReceived(result, action);
                break;

            default:
                break;
        }
    }
}
