package com.bridge.zuhur.display;

import android.content.Context;
import android.os.Bundle;

import com.bridge.zuhur.controllers.ExtrasController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.views.activities.EventsActivity;
import com.bridge.zuhur.views.activities.FilterOptionsActivity;
import com.bridge.zuhur.views.activities.HomeActivity;
import com.bridge.zuhur.views.activities.NewArrivalsActivity;
import com.bridge.zuhur.views.activities.RemaindersActivity;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 5/27/16.
 */
public class ExtrasDisplay extends AbstractDisplay {
    @Override
    public void display(int action, ArrayList<? extends Entity> result, Context context, Bundle request) {
        switch (action) {
            case ExtrasController.COUNTRIES_ACTION:
                ((HomeActivity) context).onDataReceived(result, action);
                break;

            case ExtrasController.OFFERS_ACTION:
                ((HomeActivity) context).onDataReceived(result, action);
                break;

            case ExtrasController.BEST_SELLER_ACTION:
                ((HomeActivity) context).onDataReceived(result, action);
                break;

            case ExtrasController.GET_TAGS_FROM_DATABASE_ACTION:
                ((FilterOptionsActivity) context).onDataReceived(result, action);
                break;

            case ExtrasController.NEW_ARRIVALS_ACTION:
                ((NewArrivalsActivity) context).onDataReceived(result, action);
                break;

            case ExtrasController.EVENTS_ACTION:
                ((EventsActivity) context).onDataReceived(result, action);
                break;

            case ExtrasController.GET_ALL_REMAINDERS_DATABASE_ACTION:
                ((RemaindersActivity) context).onDataReceived(result, action);
                break;

            case ExtrasController.GET_ALL_TAGS_FROM_DATABASE:
                ((FilterOptionsActivity) context).onDataReceived(result, action);
                break;


            default:
                break;
        }
    }
}
