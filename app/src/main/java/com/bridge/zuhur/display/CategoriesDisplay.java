package com.bridge.zuhur.display;

import android.content.Context;
import android.os.Bundle;

import com.bridge.zuhur.controllers.CategoriesController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.views.activities.HomeActivity;

import java.util.ArrayList;

/**
 * Created by hesham on 05/04/16.
 */
public class CategoriesDisplay extends AbstractDisplay {

    @Override
    public void display(int action, ArrayList<? extends Entity> result, Context context, Bundle request) {

        switch (action) {
            case CategoriesController.GET_CATEGORIES_ACTION:
                ((HomeActivity) context).onDataReceived(result, action);
                break;

            default:
                break;
        }


    }
}
