package com.bridge.zuhur.display;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.bridge.zuhur.R;
import com.bridge.zuhur.controllers.ProductsController;
import com.bridge.zuhur.entities.Entity;
import com.bridge.zuhur.views.activities.AddRemainderActivity;
import com.bridge.zuhur.views.activities.AllProductsActivity;
import com.bridge.zuhur.views.activities.CategoryProductsActivity;
import com.bridge.zuhur.views.activities.ContactUsActivity;
import com.bridge.zuhur.views.activities.EditRemainderActivity;
import com.bridge.zuhur.views.activities.EventsActivity;
import com.bridge.zuhur.views.activities.FavouritesActivity;
import com.bridge.zuhur.views.activities.HomeActivity;
import com.bridge.zuhur.views.activities.MoreActivity;
import com.bridge.zuhur.views.activities.NewArrivalsActivity;
import com.bridge.zuhur.views.activities.OfferDetailsActivity;
import com.bridge.zuhur.views.activities.OrderConfirmationActivity;
import com.bridge.zuhur.views.activities.OrderDetailsActivity;
import com.bridge.zuhur.views.activities.OrdersHistoryActivity;
import com.bridge.zuhur.views.activities.ProductLandingPageActivity;
import com.bridge.zuhur.views.activities.RemaindersActivity;
import com.bridge.zuhur.views.activities.ResetActivity;
import com.bridge.zuhur.views.activities.SearchResultsActivity;
import com.bridge.zuhur.views.activities.WishListActivity;

import java.util.ArrayList;

/**
 * Created by hesham on 06/04/16.
 */
public class ProductsDisplay extends AbstractDisplay {


    @Override
    public void display(int action, ArrayList<? extends Entity> result, Context context, Bundle request) {
        switch (action) {
            case ProductsController.GET_CATEGORY_PRODUCTS_ACTION:
                if (result.size() == 0) {
                    Toast.makeText(context, context.getResources().getString(R.string.noProductsError), Toast.LENGTH_SHORT).show();
                }
                ((CategoryProductsActivity) context).onDataReceived(result, action);
                break;

            case ProductsController.GET_ALL_PRODUCTS_ACTION:
                if (result.size() == 0) {
                    Toast.makeText(context, context.getResources().getString(R.string.noProductsError), Toast.LENGTH_SHORT).show();
                }
                ((AllProductsActivity) context).onDataReceived(result, action);
                break;

            case ProductsController.GET_PRODUCT_ACTION:
                ((ProductLandingPageActivity) context).onDataReceived(result, action);
                break;

            case ProductsController.RELATED_PRODUCTS_ACTION:
                ((ResetActivity) context).onDataReceived(result, action);
                break;

            case ProductsController.SIMILAR_PRODUCTS_ACTION:
                ((ProductLandingPageActivity) context).onDataReceived(result, action);
                break;

            case ProductsController.GET_AUTO_COMPLETE_DATABASE:
                if (context instanceof AddRemainderActivity) {
                    ((AddRemainderActivity) context).onDataReceived(result, action);
                } else if (context instanceof AllProductsActivity) {
                    ((AllProductsActivity) context).onDataReceived(result, action);
                } else if (context instanceof CategoryProductsActivity) {
                    ((CategoryProductsActivity) context).onDataReceived(result, action);
                } else if (context instanceof ContactUsActivity) {
                    ((ContactUsActivity) context).onDataReceived(result, action);
                } else if (context instanceof EditRemainderActivity) {
                    ((EditRemainderActivity) context).onDataReceived(result, action);
                } else if (context instanceof EventsActivity) {
                    ((EventsActivity) context).onDataReceived(result, action);
                } else if (context instanceof FavouritesActivity) {
                    ((FavouritesActivity) context).onDataReceived(result, action);
                } else if (context instanceof HomeActivity) {
                    ((HomeActivity) context).onDataReceived(result, action);
                } else if (context instanceof MoreActivity) {
                    ((MoreActivity) context).onDataReceived(result, action);
                } else if (context instanceof NewArrivalsActivity) {
                    ((NewArrivalsActivity) context).onDataReceived(result, action);
                } else if (context instanceof OfferDetailsActivity) {
                    ((OfferDetailsActivity) context).onDataReceived(result, action);
                } else if (context instanceof OrderConfirmationActivity) {
                    ((OrderConfirmationActivity) context).onDataReceived(result, action);
                } else if (context instanceof OrderDetailsActivity) {
                    ((OrderDetailsActivity) context).onDataReceived(result, action);
                } else if (context instanceof OrdersHistoryActivity) {
                    ((OrdersHistoryActivity) context).onDataReceived(result, action);
                } else if (context instanceof ProductLandingPageActivity) {
                    ((ProductLandingPageActivity) context).onDataReceived(result, action);
                } else if (context instanceof RemaindersActivity) {
                    ((RemaindersActivity) context).onDataReceived(result, action);
                } else if (context instanceof ResetActivity) {
                    ((ResetActivity) context).onDataReceived(result, action);
                } else if (context instanceof WishListActivity) {
                    ((WishListActivity) context).onDataReceived(result, action);
                }

                break;

            case ProductsController.SEARCH_BY_NAME_ACTION:
                ((SearchResultsActivity) context).onDataReceived(result, action);
                break;

            default:
                break;
        }
    }
}
