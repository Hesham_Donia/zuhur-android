package com.bridge.zuhur.display;

import android.content.Context;
import android.os.Bundle;

import com.bridge.zuhur.entities.Entity;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
abstract public class AbstractDisplay {

    public abstract void display(int action, ArrayList<? extends Entity> result, Context context, Bundle request);

}
